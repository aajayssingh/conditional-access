import os, sys, re, os.path, time
import platform
import subprocess
import signal

corenum = 8
finalpath = "../results/ascend/"
jobs = {}

benchmarks = ["fft", "radix", "lu_contiguous", "lu_non_contiguous", "cholesky", "barnes", "fmm", "ocean_contiguous", "ocean_non_contiguous", "water-nsquared", "water-spatial", "raytrace", "volrend"]
#benchmarks = ["fft", "radix", "lu_contiguous", "lu_non_contiguous", "cholesky", "barnes", "fmm", "ocean_contiguous"] 
#benchmarks = ["ocean_non_contiguous", "water-nsquared", "water-spatial", "raytrace", "volrend"]
apps = ["hello_world"]
#benchmarks = ['fft', "lu_contiguous", "volrend"]
enable_prefetch_cmd = '--prefetcher/dram_prefetch_enable=true'
disable_prefetch_cmd = '--prefetcher/dram_prefetch_enable=false'
def get_command() :
	command = {
		'fft' 				: '-p%d -m20' % corenum,
		'radix'				: '-p%d' % corenum,
		'lu_contiguous'		: '-p%d -n1024' % corenum,
		'lu_non_contiguous'	: '-p%d -n1024' % corenum,
		'cholesky'			: '-p%d ./tests/benchmarks/cholesky/inputs/tk15.O' % corenum,
		'barnes'			: '< ./tests/benchmarks/barnes/input.p%d' % corenum,
		'fmm'				: '< ./tests/benchmarks/fmm/inputs/input.16384.p%d' %corenum,
		'ocean_contiguous'	: '-p%d' % corenum,
		'ocean_non_contiguous'	: '-p%d' % corenum,
		'water-nsquared'	: '< ./tests/benchmarks/water-nsquared/input.p%d' % corenum,
		'water-spatial'		: '< ./tests/benchmarks/water-spatial/input.c%d' % corenum,
		'raytrace'			: '-p%d -m64 ./tests/benchmarks/raytrace/inputs/car.env' % corenum,
		'volrend'			: '%d ./tests/benchmarks/volrend/inputs/head' % corenum
	}
	return command

def replace(filename, pattern, replacement):
	f = open(filename)
	s = f.read()
	f.close()
	s = re.sub(pattern,replacement,s)
	f = open(filename,'w')
	f.write(s)
	f.close()

def insert_job(oram_cfg, app, app_cfg = ''):
	if app_cfg == '' and app in benchmarks:
		app_cfg = command[app]
	if not oram_cfg in jobs.keys():
		jobs[oram_cfg] = {}
	jobs[oram_cfg][app] = app_cfg

def wait_job(graphite_procs):
    while True:
        ret = graphite_procs.poll()
        if ret != None: return ret
        time.sleep(5)

###########################################
## RUN JOBS
###########################################
prefetch = False
command = get_command()
for bm in benchmarks:
	if 'draco2' == platform.node() : 
#		corenum = 2	bench1	
		insert_job('orambase', bm)
	if 'draco3' == platform.node() : 
#		corenum = 8 bench1
		insert_job('orambase_intvl', bm)
	if 'draco4' == platform.node() : 
#		corenum = 1 bench2
		insert_job('stat2_intvl', bm)
	if 'draco5' == platform.node() : 
#		corenum = 2 bench2
		insert_job('dyn2_aa_intvl', bm)
	if 'draco6' == platform.node() : 
		# cnum=1
		insert_job('dram', bm)
	if 'draco7' == platform.node() : 
		# cnum=2
		insert_job('dram', bm)
	if 'draco8' == platform.node() : 
		# cnum=4
		insert_job('dram', bm)
	if 'draco9' == platform.node() : 
		# cnum=8
		insert_job('dram', bm)
	if 'draco10' == platform.node() : 
		# cnum=4, prefetch
		insert_job('dram', bm)
		prefetch = True


"""
	if 'draco3' in platform.node() : 
		insert_job('stat2', bm)
	if 'draco4' in platform.node() : 
		insert_job('orambase', bm)
	if 'draco5' in platform.node() :
		insert_job('dyn2_sn', bm)
	if 'draco6' in platform.node() :
		insert_job('dyn2_a0.5a1', bm)
	if 'draco7' in platform.node() :
		insert_job('dyn2_a2a1', bm)
	if 'draco8' in platform.node() :
		insert_job('dyn2_a4a1', bm)
	if 'draco9' in platform.node() :
		insert_job('dyn2_a1a0.5', bm)
	if 'draco10' == platform.node() :
		insert_job('dyn2_a1a2', bm)
	if 'draco11' == platform.node() :
		insert_job('dyn2_a1a4', bm)
	if 'draco12' == platform.node() :
		insert_job('dram', bm)
		prefetch = True
	if 'draco13' == platform.node() :
		insert_job('orambase', bm)
		prefetch = True
	if 'draco14' in platform.node() :
		insert_job('dyn2_an', bm)
	if 'draco15' in platform.node() :
		insert_job('dyn4_aa', bm)
	if 'draco16' in platform.node() :
		insert_job('dyn8_aa', bm)
	if 'draco18' in platform.node() :
		insert_job('stat4', bm)
	if 'draco19' in platform.node() :
		insert_job('stat8', bm)
"""

for (oram_cfg, v) in jobs.iteritems():
	for app in jobs[oram_cfg].keys():

		# make sure the correct libcarbon_sim is used!
		os.system("cp common/Makefile.common.std common/Makefile.common")
		replace("common/Makefile.common", r"lcarbon_sim",\
			"lcarbon_sim_c4_" + oram_cfg)
		replace("common/Makefile.common", r"ROOT\)/lib", "ROOT)/lib/carbon_sim/")

		outdir = app+'_'+oram_cfg+'_c'+str(corenum)
		if prefetch:
			outdir += '_prefetch'
		os.system('mkdir -p ./results/' + outdir)
		if (os.path.exists("../results/ascend/" + outdir + '.out')):
			continue
		
		# make the application without executing
		os.system("cp tests/Makefile.tests.std tests/Makefile.tests")
		replace("./tests/Makefile.tests", r'.*\(RUN\).*', '')
		if app in benchmarks:
			os.system("make -j " + app + '_bench_test')
		else:
			os.system("make -j " + app + '_app_test')

		
		# execute the application
		execmd = '/afs/csail/group/carbon/tools/pin/pin-2.13-61206-gcc.4.4.7-linux/intel64/bin/pinbin -tool_exit_timeout 1 -mt'
		execmd += ' -t ./lib/pins/pin_sim_' + oram_cfg
		execmd += ' -c carbon_sim.cfg --general/total_cores=' + str(corenum)
		execmd += ' --general/output_dir=./results/' + outdir
		if prefetch:
			execmd += ' --prefetcher/dram_prefetch_enable=true'
		if app in benchmarks:
			execmd += ' -- ./tests/benchmarks/' + app + '/' + app + ' '
		else: 
			execmd += ' -- ./tests/apps/' + app + '/' + app + ' '
		execmd += jobs[oram_cfg][app] #benchmarks[app]
		print execmd
		proc = subprocess.Popen(execmd, shell=True, preexec_fn=os.setsid, env=os.environ)
		try:
			wait_job(proc)
		except KeyboardInterrupt:
			msg = 'Keyboard interrupt. Killing simulation'
			print "%s" % (msg)
			os.killpg(proc.pid, signal.SIGKILL)
			sys.exit( signal.SIGINT )

		os.system("cp results/"+outdir+'/sim.out ../results/ascend/' + outdir + '.out')
