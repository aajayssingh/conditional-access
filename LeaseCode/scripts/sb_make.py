import os, sys, re, os.path
import platform, time

oram_cfg_std = "common/tile/memory_subsystem/Parameters_ORAM.h.std"
oram_cfg = "common/tile/memory_subsystem/Parameters_ORAM.h"

finalpath = "../results/ascend/"
jobs = {}

def replace(filename, pattern, replacement):
	f = open(filename)
	s = f.read()
	f.close()
	s = re.sub(pattern,replacement,s)
	f = open(filename,'w')
	f.write(s)
	f.close()

def insert_job(memtype, intvl, dynsb, maxsbsize, sbmerge, sbbreak, jobname='', mcoeff=1, bcoeff=1):
	if jobname == '':
		jobname = memtype + '_'
		jobname += 'core' + str(cnum) + '_'
		jobname += 'int_' if intvl else 'noint_'
		jobname += 'dyn' if dynsb else 'stat'
		jobname += str(maxsbsize)
		jobname += '_' + sbmerge[0].lower() + sbbreak[0].lower()

	jobs[jobname] = {
		'USE_ORAM' 		: 'true' if memtype == 'oram' else 'false',
		'USE_ORAM_INTERVAL' : 'true' if intvl else 'false',
		'DYNAMIC_SB'	: 'true' if dynsb else 'false',
		'MAX_SB_SIZE'	: maxsbsize,
		'MERGE_SCHEME'	: sbmerge,
		'BREAK_SCHEME'	: sbbreak,
		'MERGE_COEFF'	: mcoeff,
		'BREAK_COEFF'	: bcoeff
	}

#def insert_job(memtype, intvl, dynsb, maxsbsize, sbmerge, sbbreak):
insert_job('dram', False, False, 1, 'NOOP', 'NOOP', 'c4_dram')
insert_job('oram', True, False, 1, 'NOOP', 'NOOP', 'c4_orambase_intvl')
insert_job('oram', True, False, 2, 'NOOP', 'NOOP', 'c4_stat2_intvl')
insert_job('oram', True, True, 2, 'ADAPT', 'ADAPT', 'c4_dyn2_aa_intvl')

os.system('make clean')
os.system('cp common/Makefile.common.std common/Makefile.common')
for (jobname, v) in jobs.iteritems():
	os.system("cp "+ oram_cfg_std +' ' + oram_cfg)
	print jobname
	for (var, val) in v.iteritems():
		pattern = r"\#define\s*" + re.escape(var) + r'\s+.*'
		replacement = "#define " + var + ' ' + str(val)
		replace(oram_cfg, pattern, replacement)
		print pattern
	os.system('make -j16')
	os.system('make -j16')
	libname = 'libcarbon_sim_' + jobname + '.a'
	os.system('cp lib/libcarbon_sim.a lib/carbon_sim/' + libname)
	os.system('cp lib/pin_sim.so lib/pins/pin_sim_' + jobname[3:] + '.so')
