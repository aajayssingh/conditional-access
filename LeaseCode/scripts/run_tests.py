#!/usr/bin/env python

import sys
import os
import random

sys.path.append("./scripts/")
sys.path.append("./scripts/scheduler")
sys.path.append("./scripts/job")

from simulate import *
from benchmark_config import *
from utils import *
from sim_job import SimJob
from oram_config import *

# scheduler: Use 'condor' for the condor scheduling system or 'basic' for using Graphite's scheduling system
scheduler = "basic"

results_dir = "./results/1_line_lease/"
# config_filename: Config file to use for the simulation
config_filename = "carbon_sim.cfg"

# machines: List of machines to run the simulation on
#   Is only used for the 'basic' scheduling system
#   Warning: DO NOT use 'localhost' or '127.0.0.1', use the machine name

machines = []
#for i in range(10, 14):
#   machines.append("cag%d" % i)
#machines.append("cag1")
#machines.append("cag2")
#machines.append("cag3")
#machines.append("cag4")
#machines.append("cag5")
#machines.append("cag6")
#machines.append("cag7")
#machines.append("cag8")

machines.append("127.0.0.1")

# Generate jobs
jobs = []

"""
benchmarks = ["counters"]
configs = ['lease', 'nolease']
#configs = ['nolease']
threads_list = [2, 4, 8, 16, 32, 64]
#threads_list = [16]
lease_times_list = [100000]
#jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=2, lease_count=1, max_duration=100, res_dir_suffix="_pthread_1lock")
#jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=4, lease_count=1, max_duration=100, res_dir_suffix="_CAS_2lock")
#configs = ['lease']
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=5, lease_count=2, max_duration=100, res_dir_suffix="_CAS_2lock_simple_leases")
"""


benchmarks = ["counters"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [10000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=0, lease_count=1, max_duration=100, res_dir_suffix="_lockfree")

"""
benchmarks = ["stack"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [10000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=0, lease_count=1, max_duration=20, res_dir_suffix="_perc")
"""


"""
benchmarks = ["queue"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [1000000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=0, lease_count=1, max_duration=100)
configs = ['lease']
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", op_type=0, lease_count=2, max_duration=100)


benchmarks = ["multiqueue"]
configs = ['lease', 'nolease']
#configs = ['lease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=2, max_duration=100, res_dir_suffix="_8lock")
"""

"""
benchmarks = ["bst_aravind"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=20, res_dir_suffix="_perc")
"""

"""
benchmarks = ["htjava"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=20, res_dir_suffix="_perc")
"""

"""
benchmarks = ["lbpq_alistarh_pugh"]
configs = ['nolease']
#configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
#threads_list = [4]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=100, res_dir_suffix="_spray")
"""

"""
benchmarks = ["lb_counter"]
#configs = ['lease', 'nolease']
configs = ['nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=100, res_dir_suffix="_TAS_1")
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=100, res_dir_suffix="_TAS_2")
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, max_duration=100, res_dir_suffix="_TAS_3")
"""


"""
benchmarks = ["simstack"]
configs = ['nolease']
#threads_list = [2, 4, 8, 16, 32, 64]
threads_list = [2]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", )
"""

"""
benchmarks = ["raytrace"]
configs = ['lease', 'nolease']
threads_list = [4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, res_dir_suffix="Try_Lock")
"""

"""
benchmarks = ["pagerank"]
configs = ['lease', 'nolease']
threads_list = [2, 4, 8, 16, 32, 64]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1 )
"""

"""
benchmarks = ["skip_mcas"]
configs = ['lease', 'nolease']
#threads_list = [2, 4, 8, 16, 32, 64]
threads_list = [16]
lease_times_list = [100000]
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/ppopp_main_exp", lease_count=1, res_dir_suffix="_only_updates" )
"""

"""
# Real Machine Testing
benchmarks = ["counters"]
configs = ['nolease']
threads_list = [2, 4, 8, 12, 16, 20]
lease_times_list = []
jobs += main_exp(benchmarks, configs, threads_list, lease_times_list, results_dir="./results/5_inc_real/", mode="")
"""



"""
# Main experiments
#configs = ['dram']
#configs = ['oram']
configs = ['stat2']
#configs = ['dyn2']
#configs = ['dyn4']
#configs = ['oram','stat2', 'dyn2', 'dram']
#benchlist = list(splash2_list + spec_list + dbms_list)
#benchlist = list(splash2_list)
#benchlist = list(spec_list)
#benchlist.remove('libquantum')
#benchlist = list(dbms_list)
#benchlist = ["sjeng"]
benchlist = ["gobmk","hmmer","sjeng","omnetpp",]
#benchlist = ["sjeng",]
jobs += main_exp(benchlist, configs)


# INTERVAL 
configs = ['oram', 'stat2', 'dyn2']
#configs = ['oram', 'stat2']
#benchlist = list(splash2_list)
#benchlist = list(spec_list)
#benchlist.remove('libquantum')
benchlist = ["gobmk","hmmer","sjeng","omnetpp",]
#jobs += main_exp(benchlist, configs, 128, results_dir="./results/ascend_new/mainexp-interval-stash400", interval=True, oram_stash_size=475, app_flags="")
jobs += main_exp(benchlist, configs, 128, results_dir="./results/ascend_new/mainexp-interval-stash100", interval=True, oram_stash_size=175, app_flags="")
"""


"""
# Sweep ORAM Z size
configs = ['oram', 'stat2', 'dyn2']
benchmarks = ["fft", "ocean_contiguous", "ocean_non_contiguous", "volrend"]
jobs += main_exp(['volrend'], ['oram'], 128, results_dir="./results/ascend_new/mainexp-z4/", oram_z=4)
jobs += main_exp(['volrend', 'ocean_non_contiguous'], ['stat2'], 128, results_dir="./results/ascend_new/mainexp-z4/", oram_z=4)
jobs += main_exp(benchmarks, ['dyn2'], 128, results_dir="./results/ascend_new/mainexp-z4/", oram_z=4)
"""

"""
# Sweep cache line size
#configs = ['oram', 'stat2', 'dyn2']
configs = ['dram']
jobs += main_exp(['ocean_contiguous', 'volrend'], configs, 64)
jobs += main_exp(['ocean_contiguous', 'volrend'], configs, 256)
#jobs += main_exp(['mcf', 'ocean_contiguous', 'omnetpp'], configs, 64)
#jobs += main_exp(['mcf', 'ocean_contiguous', 'omnetpp'], configs, 256)
"""

"""
# Sweep the merge and break coeff
#benchmarks = ["fft", "ocean_contiguous", "ocean_non_contiguous", "volrend"]
#benchmarks = ["ocean_contiguous", "ocean_non_contiguous"]
#jobs += main_exp(benchmarks, ['dyn2'], results_dir="./results/ascend_new/m2b2", merge_coeff=2, break_coeff=2)
#jobs += main_exp(benchmarks, ['dyn2'], results_dir="./results/ascend_new/m4b4", merge_coeff=4, break_coeff=4)
jobs += main_exp(['ocean_non_contiguous'], ['dyn2'], results_dir="./results/ascend_new/m4b1", merge_coeff=4, break_coeff=1)
#jobs += main_exp(benchmarks, ['dyn2'], results_dir="./results/ascend_new/m8b8", merge_coeff=8, break_coeff=8)
#jobs += main_exp(['volrend'], ['dyn2'], results_dir="./results/ascend_new/m8b8", merge_coeff=8, break_coeff=8)
#jobs += main_exp(benchmarks, ['dyn2'], results_dir="./results/ascend_new/m4b1", merge_coeff=4, break_coeff=1)
"""

"""
# Sweep the DRAM bandwidth
benchmarks = ["ocean_contiguous", "volrend"]
#configs = ['oram', 'stat2', 'dyn2']
configs = ['dram']
jobs += main_exp(benchmarks, configs, dram_bandwidth=4, results_dir="./results/ascend_new/dram_band4")
jobs += main_exp(benchmarks, configs, dram_bandwidth=8, results_dir="./results/ascend_new/dram_band8")
"""


"""
# Sweep the ORAM stash size
#benchmarks = ["ocean_contiguous", "volrend"]
benchmarks = ["volrend"]
jobs += main_exp(benchmarks, ["dyn2"], oram_stash_size=125, results_dir="./results/ascend_new/stash50/")
#jobs += main_exp(benchmarks, ["oram", "stat2", "dyn2"], oram_stash_size=125, results_dir="./results/ascend_new/stash50/")
#jobs += main_exp(benchmarks, ["oram", "stat2", "dyn2"], oram_stash_size=275, results_dir="./results/ascend_new/stash200/")
#jobs += main_exp(benchmarks, ["oram", "stat2", "dyn2"], oram_stash_size=475, results_dir="./results/ascend_new/stash400/")
"""


"""
# sweep the last level cache size
benchmarks = ["fft", "ocean_contiguous", "ocean_non_contiguous", "omnetpp"]
jobs += main_exp(benchmarks, ["oram", "stat2", "dyn2", "dram"], llc=256, results_dir="./results/ascend/llc256/")
jobs += main_exp(benchmarks, ["oram", "stat2", "dyn2", "dram"], llc=1024, results_dir="./results/ascend/llc1024/")

jobs += main_exp(["fft"], ["oram", "stat2", "dyn2", "dram"], app_flags="-p4 -m16 -l128", results_dir="./results/ascend/fft_m16/")
jobs += main_exp(["fft"], ["oram", "stat2", "dyn2", "dram"], app_flags="-p4 -m24 -l128", results_dir="./results/ascend/fft_m24/")

jobs += main_exp(["lu_contiguous"], ["oram", "stat2", "dyn2", "dram"], app_flags="-p4 -n512", results_dir="./results/ascend/luc_n512/")
jobs += main_exp(["lu_contiguous"], ["oram", "stat2", "dyn2", "dram"], app_flags="-p4 -n2048", results_dir="./results/ascend/luc_n2048/")
"""
"""jobs += main_exp(["mcf"], ["oram", "stat2", "dyn2"], oram_stash_size=475, results_dir="./results/ascend/stash400/")
jobs += main_exp(["perlbench"], ["oram"], oram_stash_size=475, results_dir="./results/ascend/stash400/")
jobs += main_exp(splash2_list + spec_list, ["oram", "stat2", "dyn2"], oram_stash_size=275, results_dir="./results/ascend/stash200/")
"""
#jobs += main_exp(['fmm'], ["dyn2"], oram_stash_size=275, results_dir="./results/ascend/stash200/")
"""
jobs += main_exp(['fft'], ["oram", "stat2", "dyn2"], oram_stash_size=125, results_dir="./results/ascend/stash50/")
jobs += main_exp(['fft'], ["oram", "stat2", "dyn2"], oram_stash_size=275, results_dir="./results/ascend/stash200/")
jobs += main_exp(['fft'], ["oram", "stat2", "dyn2"], oram_stash_size=475, results_dir="./results/ascend/stash400/")
configs = ['oram', 'stat2', 'dyn2']
"""

"""
# synthetic benchmark
# super block size sweep study
configs = ['oram', 'stat2', 'dyn2']
for sbsize in [2, 4, 8]:
   jobs += synthetic_benchmark(configs, locality=1, sbsize=sbsize, results_dir = "./results/ascend/synth-sbsize/")


#
# synthetic benchmark
# locality sweep study
for locality in [0, 0.2, 0.4, 0.6, 0.8, 1]:
   jobs += synthetic_benchmark(configs, locality=locality)
#jobs += synthetic_benchmark(['dyn2'], locality=1)
#jobs += synthetic_benchmark(['stat2'], locality=0.6)
#
# synthetic benchmark
# phase change behavior
configs = ['dyn2']
jobs += synthetic_benchmark(configs, phase_change=True, merge_scheme="static", break_scheme="noop", results_dir = "./results/ascend/synth-phase/", name="sm_nb")
jobs += synthetic_benchmark(configs, phase_change=True, merge_scheme="adapt", break_scheme="noop", results_dir = "./results/ascend/synth-phase/", name="am_nb")
jobs += synthetic_benchmark(configs, phase_change=True, merge_scheme="adapt", break_scheme="adapt", results_dir = "./results/ascend/synth-phase/", name="am_ab")
jobs += synthetic_benchmark(['stat2'], phase_change=True, results_dir = "./results/ascend/synth-phase/", name="static")
jobs += synthetic_benchmark(['oram'], phase_change=True, results_dir = "./results/ascend/synth-phase/", name="oram")
"""


# Go!
simulate(scheduler, jobs, machines, results_dir, config_filename)
