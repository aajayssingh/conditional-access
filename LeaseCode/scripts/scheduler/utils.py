#!/usr/bin/env python

import os
import sys

from benchmark_config import *

def getCommand(benchmark):
   if benchmark in splash2_list:
      return "make %s_bench_test" % (benchmark)
   elif benchmark in parsec_list:
      return "make %s_parsec" % (benchmark)
   elif benchmark in spec_list:
      return "make %s_spec_test" % (benchmark)
   elif benchmark in dbms_list:
      return "make %s_app_test" % (benchmark)
   elif benchmark in lease_list:
      return "make %s_bench_test" % (benchmark)
   elif benchmark in ascylib_list:
      return "make %s_ASCYLIB_test" % (benchmark)
   elif benchmark in sim_univ_list:
      return "make %s_SIM_UNIV_test" % (benchmark)
   elif benchmark in lockfree_list:
      return "make %s_LOCKFREE_LIB_test" % (benchmark)
   elif benchmark in crono_list:
      return "make %s_CRONO_test" % (benchmark)
   else:
      print "Benchmark: %s not found in the defined lists" % (benchmark)
      sys.exit(-1)

def getAppFlags(benchmark):
   app_flags = None
   if benchmark in app_flags_table:
      app_flags = app_flags_table[benchmark]
   else:
      print "app_flags does not exist"
      sys.exit(-1)
   print benchmark, "   ", app_flags
   return app_flags

def compileBenchmarks(benchmark_list):
   # Compile all benchmarks first
   for benchmark in benchmark_list:
      if (benchmark in parsec_list) and (not os.path.exists("tests/parsec/parsec-3.0")):
         print "[regress] Creating PARSEC applications directory."
         os.system("make setup_parsec")
      os.system("%s BUILD_MODE=build" % getCommand(benchmark))
