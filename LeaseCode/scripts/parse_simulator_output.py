#!/usr/bin/env python

import re
import sys
import numpy
from numpy import *
from optparse import OptionParser

global graphiteResults

def rowSearch(heading, key, numColumns, reportError=True):
   global graphiteResults
   key += "(.*)"
   
   headingFound = False
   
   for line in graphiteResults:
      if headingFound:
         matchKey = re.search(key, line)
         if matchKey:
            counts = line.split('|')
            eventCounts = counts[1:numColumns+1]
            for i in range(0, numColumns):
               if (len(eventCounts[i].split()) == 0):
                  eventCounts[i] = "0.0"
               #elif (eventCounts[i].split()[0] == "nan"):
               #   eventCounts[i] = "0.0"
            #print eventCounts
            #print "%d" % size(eventCounts)
            return array(map(lambda x: float(x), eventCounts))
      else:
         if (re.search(heading, line)):
            headingFound = True

   if (reportError == True):
      print "\n\n****  ERROR  ***** Could not Find [%s,%s]\n\n" % (heading, key)
      sys.exit(-1)
   else: # reportError == False
      return array([0.0] * numColumns)

parser = OptionParser()
parser.add_option("--results-dir", dest="results_dir", help="Graphite Results Directory")
parser.add_option("--num-cores", dest="num_cores", type="int", help="Number of Target Cores")
parser.add_option("--num-threads", dest="num_threads", type="int", help="Number of Application Threads")
(options,args) = parser.parse_args()

print "python ./tools/parse_simulator_output.py --results-dir %s --num-cores %i --num-threads %i" % \
      (options.results_dir, options.num_cores, options.num_threads)

# Read Results Files
try:
   graphiteResults = open("%s/sim.out" % (options.results_dir), 'r').readlines()
except IOError:
   print "*ERROR* File: %s/sim.out not present" % (options.results_dir)
   sys.exit(-1)

numCores = options.num_cores
numThreads = options.num_threads

# Total Instructions
totalInstructions = sum(rowSearch("Core Summary", "Total Instructions", numThreads))

# Completion Time
completionTime = numpy.average(rowSearch("Core Summary", "Completion Time \(in nanoseconds\)", numThreads))
totalCompletionTime = sum(rowSearch("Core Summary", "Completion Time \(in nanoseconds\)", numThreads))
memoryStallTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Memory", numThreads))
computeTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Execution Unit", numThreads))
synchronizationTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Synchronization", numThreads))
netRecvTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Network Recv", numThreads))

# Network Traffic
networkFlitsSent = sum(rowSearch("Network \(Memory\)", "Total Flits Sent", numCores))
networkFlitsReceived = sum(rowSearch("Network \(Memory\)", "Total Flits Received", numCores))

# Memory Access Latency
totalInstructionMemoryAccessesRow = rowSearch("Shared Memory Model Summary", "Total Instruction Memory Accesses", numThreads);
instructionMemoryAccessLatencyRow = rowSearch("Shared Memory Model Summary", "Average Instruction Memory Access Latency \(in nanoseconds\)", numThreads)
averageInstructionMemoryAccessLatency = sum(instructionMemoryAccessLatencyRow * totalInstructionMemoryAccessesRow) / sum(totalInstructionMemoryAccessesRow)
totalInstructionMemoryAccessLatency = sum(instructionMemoryAccessLatencyRow * totalInstructionMemoryAccessesRow)
totalInstructionBufferHits = sum(rowSearch("Shared Memory Model Summary", "Instruction Buffer Hits", numThreads))

totalDataMemoryAccessesRow = rowSearch("Shared Memory Model Summary", "Total Data Memory Accesses", numThreads);
dataMemoryAccessLatencyRow = rowSearch("Shared Memory Model Summary", "Average Data Memory Access Latency \(in nanoseconds\)", numThreads)
averageDataMemoryAccessLatency = sum(dataMemoryAccessLatencyRow * totalDataMemoryAccessesRow) / sum(totalDataMemoryAccessesRow)
totalDataMemoryAccessLatency = sum(dataMemoryAccessLatencyRow * totalDataMemoryAccessesRow)

l1IAccesses = sum(rowSearch("Cache L1-I","Cache Accesses", numThreads))
l1DAccesses = sum(rowSearch("Cache L1-D","Cache Accesses", numThreads))
l2Accesses = sum(rowSearch("Cache L2","Cache Accesses", numCores))
l1IMisses = sum(rowSearch("Cache L1-I","Cache Misses", numThreads))
l1DMisses = sum(rowSearch("Cache L1-D","Cache Misses", numThreads))
l2Misses = sum(rowSearch("Cache L2","Cache Misses", numCores))

# Cache miss types

#coldMisses = sum(rowSearch("Miss Type Counters","Cold Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Cold Misses \(Remote\)", numThreads))

#capacityMisses = sum(rowSearch("Miss Type Counters","Capacity Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Capacity Misses \(Remote\)", numThreads))

#upgradeMisses = sum(rowSearch("Miss Type Counters","Upgrade Misses \(Local\)", numThreads)) + \
#                sum(rowSearch("Miss Type Counters","Upgrade Misses \(Remote\)", numThreads))

#sharingMisses = sum(rowSearch("Miss Type Counters","Sharing Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Sharing Misses \(Remote\)", numThreads))

#wordMisses = sum(rowSearch("Miss Type Counters","Word Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Word Misses \(Remote\)", numThreads))


# Replica/home read/write counters
#replica_read_accesses = sum(rowSearch("L2 Cache Cntlr","Replica Read Accesses", numCores))
#replica_read_misses = sum(rowSearch("L2 Cache Cntlr","Replica Read Misses", numCores))
#replica_write_accesses = sum(rowSearch("L2 Cache Cntlr","Replica Write Accesses", numCores))
#replica_write_misses = sum(rowSearch("L2 Cache Cntlr","Replica Write Misses", numCores))

#home_read_accesses = sum(rowSearch("L2 Cache Cntlr","Home Read Accesses", numCores))
#home_read_misses = sum(rowSearch("L2 Cache Cntlr","Home Read Misses", numCores))
#home_write_accesses = sum(rowSearch("L2 Cache Cntlr","Home Write Accesses", numCores))
#home_write_misses = sum(rowSearch("L2 Cache Cntlr","Home Write Misses", numCores))

# Total DRAM accesses
#dramAccesses = sum(rowSearch("ORam Performance Model Summary", "Total ORam Accesses", numCores))

# Cache Tag/Data array reads/writes
l1I_TagReads = sum(rowSearch("Cache L1-I","Tag Array Reads", numCores))
l1I_TagWrites = sum(rowSearch("Cache L1-I","Tag Array Writes", numCores))
#l1I_DataReads_Word = sum(rowSearch("Cache L1-I","Data Array Reads \(Word\)", numCores))
#l1I_DataWrites_Word = sum(rowSearch("Cache L1-I","Data Array Writes \(Word\)", numCores)) 
#l1I_DataReads_CacheLine = sum(rowSearch("Cache L1-I","Data Array Reads \(Cache-Line\)", numCores))
#l1I_DataWrites_CacheLine = sum(rowSearch("Cache L1-I","Data Array Writes \(Cache-Line\)", numCores))

l1D_TagReads = sum(rowSearch("Cache L1-D","Tag Array Reads", numCores))
l1D_TagWrites = sum(rowSearch("Cache L1-D","Tag Array Writes", numCores))
#l1D_DataReads_Word = sum(rowSearch("Cache L1-D","Data Array Reads \(Word\)", numCores))
#l1D_DataWrites_Word = sum(rowSearch("Cache L1-D","Data Array Writes \(Word\)", numCores)) 
#l1D_DataReads_CacheLine = sum(rowSearch("Cache L1-D","Data Array Reads \(Cache-Line\)", numCores))
#l1D_DataWrites_CacheLine = sum(rowSearch("Cache L1-D","Data Array Writes \(Cache-Line\)", numCores))

l2_TagReads = sum(rowSearch("Cache L2","Tag Array Reads", numCores))
l2_TagWrites = sum(rowSearch("Cache L2","Tag Array Writes", numCores))
#l2_DataReads_Word = sum(rowSearch("Cache L2","Data Array Reads \(Word\)", numCores))
#l2_DataWrites_Word = sum(rowSearch("Cache L2","Data Array Writes \(Word\)", numCores)) 
#l2_DataReads_CacheLine = sum(rowSearch("Cache L2","Data Array Reads \(Cache-Line\)", numCores))
#l2_DataWrites_CacheLine = sum(rowSearch("Cache L2","Data Array Writes \(Cache-Line\)", numCores))

# Network access counters
#routerBufferWrites = sum(rowSearch("Network \(Memory\)","Buffer Writes", numCores))
#routerBufferReads = sum(rowSearch("Network \(Memory\)","Buffer Reads", numCores))
#routerCrossbarTraversals = sum(rowSearch("Network \(Memory\)","Crossbar\[1\] Traversals", numCores)) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[2\] Traversals", numCores)) * 2) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[3\] Traversals", numCores)) * 3) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[4\] Traversals", numCores)) * 4) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[5\] Traversals", numCores)) * 5)
#routerSwitchAllocatorRequests = sum(rowSearch("Network \(Memory\)","Switch Allocator Requests", numCores))
#linkTraversals = sum(rowSearch("Network \(Memory\)","Link Traversals", numCores))

# Instruction Memory Access Latency Breakdown
#total_IMAT_L1Cache = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache", numCores))
#total_IMAT_L1Cache_L2Replica = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Replica", numCores))
#total_IMAT_L2Replica_Waiting = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica Waiting", numCores))
#total_IMAT_L2Replica_Sharers = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica And Sharers", numCores, reportError=False))
#total_IMAT_L2Replica_L2Home = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica And L2 Home", numCores))
#total_IMAT_L1Cache_L2Home = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Home", numCores))
#total_IMAT_L2Home_Waiting = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home Waiting", numCores))
#total_IMAT_L2Home_Sharers = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home And Sharers", numCores))
#total_IMAT_L2Home_DRAM = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home And DRAM", numCores))

# Data Memory Access Latency Breakdown
#total_DMAT_L1Cache = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache", numCores))
#total_DMAT_L1Cache_L2Replica = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Replica", numCores))
#total_DMAT_L2Replica_Waiting = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica Waiting", numCores))
#total_DMAT_L2Replica_Sharers = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica And Sharers", numCores, reportError=False))
#total_DMAT_L2Replica_L2Home = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica And L2 Home", numCores))
#total_DMAT_L1Cache_L2Home = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Home", numCores))
#total_DMAT_L2Home_Waiting = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home Waiting", numCores))
#total_DMAT_L2Home_Sharers = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home And Sharers", numCores))
#total_DMAT_L2Home_DRAM = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home And DRAM", numCores))

#total_page_migration_stall_time = sum(rowSearch("Page Migration Stall Time \(in nanoseconds\)","Total", numCores))


# Write event counters to a file
stats_file = open("%s/stats.out" % (options.results_dir), 'w')
stats_file.write("Total-Instructions = %f\n" % (totalInstructions))
stats_file.write("Completion-Time = %f\n" % (completionTime))

stats_file.write("Network-Flits-Sent = %f\n" % (networkFlitsSent))
stats_file.write("Network-Flits-Received = %f\n" % (networkFlitsReceived))

stats_file.write("Average-Instruction-Memory-Access-Latency = %f\n" % (averageInstructionMemoryAccessLatency))
stats_file.write("Average-Data-Memory-Access-Latency = %f\n" % (averageDataMemoryAccessLatency))
stats_file.write("Total-Instruction-Memory-Access-Latency = %f\n" % (totalInstructionMemoryAccessLatency))
stats_file.write("Total-Data-Memory-Access-Latency = %f\n" % (totalDataMemoryAccessLatency))
stats_file.write("Instruction-Fetch-Buffer-Hits = %f\n" % (totalInstructionBufferHits))

stats_file.write("Private-Cache-Hierarchy-Miss-Rate = %f\n" % ((l1IMisses + l1DMisses) / (l1IAccesses + l1DAccesses)))

stats_file.write("L1I-Cache-Miss-Rate = %f\n" % (l1IMisses / l1IAccesses))
stats_file.write("L1D-Cache-Miss-Rate = %f\n" % (l1DMisses / l1DAccesses))
stats_file.write("L2-Cache-Miss-Rate = %f\n" % (l2Misses / l2Accesses))
stats_file.write("L1I-Cache-Accesses = %f\n" % (l1IAccesses))
stats_file.write("L1D-Cache-Accesses = %f\n" % (l1DAccesses))
stats_file.write("L2-Cache-Accesses = %f\n" % (l2Accesses))
stats_file.write("L1I-Cache-Misses = %f\n" % (l1IMisses))
stats_file.write("L1D-Cache-Misses = %f\n" % (l1DMisses))
stats_file.write("L2-Cache-Misses = %f\n" % (l2Misses))
stats_file.write("L1I-MPKI = %f\n" % (l1IMisses * 1000 /  totalInstructions))

#stats_file.write("Replica-Read-Accesses = %f\n" % (replica_read_accesses))
#stats_file.write("Replica-Read-Misses = %f\n" % (replica_read_misses))
#stats_file.write("Replica-Write-Accesses = %f\n" % (replica_write_accesses))
#stats_file.write("Replica-Write-Misses = %f\n" % (replica_write_misses))
#stats_file.write("Home-Read-Accesses = %f\n" % (home_read_accesses))
#stats_file.write("Home-Read-Misses = %f\n" % (home_read_misses))
#stats_file.write("Home-Write-Accesses = %f\n" % (home_write_accesses))
#stats_file.write("Home-Write-Misses = %f\n" % (home_write_misses))
#stats_file.write("L2Replica-Hits = %f\n" % (replica_read_accesses + replica_write_accesses - (replica_read_misses + replica_write_misses)))
#stats_file.write("L2Home-Hits = %f\n" % (home_read_accesses + home_write_accesses - (home_read_misses + home_write_misses)))
#stats_file.write("OffChip-Misses = %f\n" % (home_read_misses + home_write_misses))

#stats_file.write("Dram-Accesses = %f\n" % (dramAccesses))

# Cache miss types
#stats_file.write("Cold-Misses = %f\n" % (coldMisses))
#stats_file.write("Capacity-Misses = %f\n" % (capacityMisses))
#stats_file.write("Upgrade-Misses = %f\n" % (upgradeMisses))
#stats_file.write("Sharing-Misses = %f\n" % (sharingMisses))
#stats_file.write("Word-Misses = %f\n" % (wordMisses))

# Core Counters
stats_file.write("Total-Completion-Time = %f\n" % (totalCompletionTime))
stats_file.write("Memory-Stall-Time = %f\n" % (memoryStallTime))
stats_file.write("Compute-Time = %f\n" % (computeTime))
stats_file.write("Synchronization-Stall-Time = %f\n" % (synchronizationTime))
stats_file.write("Network-Recv-Stall-Time = %f\n" % (netRecvTime))

# Cache counters
stats_file.write("L1I-Tag-Reads = %f\n" % (l1I_TagReads))
stats_file.write("L1I-Tag-Writes = %f\n" % (l1I_TagWrites))
#stats_file.write("L1I-Data-Reads-Word = %f\n" % (l1I_DataReads_Word))
#stats_file.write("L1I-Data-Writes-Word = %f\n" % (l1I_DataWrites_Word))
#stats_file.write("L1I-Data-Reads-CacheLine = %f\n" % (l1I_DataReads_CacheLine))
#stats_file.write("L1I-Data-Writes-CacheLine = %f\n" % (l1I_DataWrites_CacheLine))

stats_file.write("L1D-Tag-Reads = %f\n" % (l1D_TagReads))
stats_file.write("L1D-Tag-Writes = %f\n" % (l1D_TagWrites))
#stats_file.write("L1D-Data-Reads-Word = %f\n" % (l1D_DataReads_Word))
#stats_file.write("L1D-Data-Writes-Word = %f\n" % (l1D_DataWrites_Word))
#stats_file.write("L1D-Data-Reads-CacheLine = %f\n" % (l1D_DataReads_CacheLine))
#stats_file.write("L1D-Data-Writes-CacheLine = %f\n" % (l1D_DataWrites_CacheLine))

stats_file.write("L2-Tag-Reads = %f\n" % (l2_TagReads))
stats_file.write("L2-Tag-Writes = %f\n" % (l2_TagWrites))
#stats_file.write("L2-Data-Reads-Word = %f\n" % (l2_DataReads_Word))
#stats_file.write("L2-Data-Writes-Word = %f\n" % (l2_DataWrites_Word))
#stats_file.write("L2-Data-Reads-CacheLine = %f\n" % (l2_DataReads_CacheLine))
#stats_file.write("L2-Data-Writes-CacheLine = %f\n" % (l2_DataWrites_CacheLine))

# Network counters
#stats_file.write("Router-Buffer-Writes = %f\n" % (routerBufferWrites))
#stats_file.write("Router-Buffer-Reads = %f\n" % (routerBufferReads))
#stats_file.write("Router-Crossbar-Traversals = %f\n" % (routerCrossbarTraversals))
#stats_file.write("Router-Switch-Allocator-Requests = %f\n" % (routerSwitchAllocatorRequests))
#stats_file.write("Link-Traversals = %f\n" % (linkTraversals))

# Instruction Memory Access Latency breakdown
#stats_file.write("TIMAL-L1Cache = %f\n" % (total_IMAT_L1Cache))
#stats_file.write("TIMAL-L1Cache-L2Replica = %f\n" % (total_IMAT_L1Cache_L2Replica))
#stats_file.write("TIMAL-L2Replica-Waiting = %f\n" % (total_IMAT_L2Replica_Waiting))
#stats_file.write("TIMAL-L2Replica-Sharers = %f\n" % (total_IMAT_L2Replica_Sharers))
#stats_file.write("TIMAL-L2Replica-L2Home = %f\n" % (total_IMAT_L2Replica_L2Home))
#stats_file.write("TIMAL-L1Cache-L2Home = %f\n" % (total_IMAT_L1Cache_L2Home))
#stats_file.write("TIMAL-L2Home-Waiting = %f\n" % (total_IMAT_L2Home_Waiting))
#stats_file.write("TIMAL-L2Home-Sharers = %f\n" % (total_IMAT_L2Home_Sharers))
#stats_file.write("TIMAL-L2Home-OffChip = %f\n" % (total_IMAT_L2Home_DRAM))

# Data Memory Access Latency breakdown
#stats_file.write("TDMAL-L1Cache = %f\n" % (total_DMAT_L1Cache))
#stats_file.write("TDMAL-L1Cache-L2Replica = %f\n" % (total_DMAT_L1Cache_L2Replica))
#stats_file.write("TDMAL-L2Replica-Waiting = %f\n" % (total_DMAT_L2Replica_Waiting))
#stats_file.write("TDMAL-L2Replica-Sharers = %f\n" % (total_DMAT_L2Replica_Sharers))
#stats_file.write("TDMAL-L2Replica-L2Home = %f\n" % (total_DMAT_L2Replica_L2Home))
#stats_file.write("TDMAL-L1Cache-L2Home = %f\n" % (total_DMAT_L1Cache_L2Home))
#stats_file.write("TDMAL-L2Home-Waiting = %f\n" % (total_DMAT_L2Home_Waiting))
#stats_file.write("TDMAL-L2Home-Sharers = %f\n" % (total_DMAT_L2Home_Sharers))
#stats_file.write("TDMAL-L2Home-OffChip = %f\n" % (total_DMAT_L2Home_DRAM))

# Page Migration
#stats_file.write("Page-Migration-Stall-Time = %f\n" % (total_page_migration_stall_time))

#page_type_list = ["Instruction", "Private-Data", "Shared-Read-Only-Data", "Shared-Read-Write-Data"]
#interval_list = ["1-2", "3-9", "10-"]
#for page_type in page_type_list:
#   aggregate_counters = {}
#   for interval in interval_list:
#      aggregate_counters[interval] = 0
#   for run_length in range(1,1001):
#      num_accesses = sum(rowSearch("Data Placement Summary", "%s--%i" % (page_type, run_length), numCores))
#      if (run_length >= 1 and run_length <= 2):
#         aggregate_counters["1-2"] = aggregate_counters["1-2"] + num_accesses
#      elif (run_length >= 3 and run_length <= 9):
#         aggregate_counters["3-9"] = aggregate_counters["3-9"] + num_accesses
#      else:
#         aggregate_counters["10-"] = aggregate_counters["10-"] + num_accesses
#   for interval in interval_list:
#      stats_file.write("%s--%s = %f\n" % (page_type, interval, aggregate_counters[interval]))

stats_file.close()
