#!/usr/bin/env python

splash2_list = [
      "fft",
      "radix",
      "lu_contiguous",
      "lu_non_contiguous",
      "cholesky",
      "barnes",
      "fmm",
      "ocean_contiguous",
      "ocean_non_contiguous",
      "water-nsquared",
      "water-spatial",
      "raytrace",
      "volrend",
      "radiosity",
      ]

parsec_list = [
      "blackscholes",
      "swaptions",
      "canneal",
      "fluidanimate",
      "streamcluster",
      "facesim",
      "freqmine",
      "dedup",
      "ferret",
      "bodytrack",
      ]

spec_list = [
      "perlbench",
      "bzip2",
      "gcc",
      "mcf",
      "gobmk",
      "hmmer",
      "sjeng",
      "libquantum",
      "h264ref",
      "omnetpp",
      "astar",
      ]

lite_mode_list = [
      "freqmine",
      "dedup",
      "ferret",
      "bodytrack",
      ]

dbms_list = [
      "dbx1000",
      ]

lease_list = [
      "counters",
      "stack",
      "queue",
      "multiqueue",
      ]

ascylib_list = [
      "bst_aravind",
      "htjava",
      "lbpq_alistarh",
      "lbpq_alistarh_pugh",
      "lb_counter" ,
      ]

sim_univ_list = [
      "simstack" ,
      ]

lockfree_list = [
      "skip_mcas" ,
      ]

crono_list = [
      "pagerank" ,
      "dijkstra",
      ]

app_flags_table = {
      # splash
      "radix"                                      : "-p4 -n1048576",
      "fft"                                        : "-p4 -m20",
      "lu_contiguous"                              : "-p4 -n1024",
      "lu_non_contiguous"                          : "-p4 -n1024",
      "cholesky"                                   : "-p4 inputs/tk29.O",
      "barnes"                                     : "< inputs/input.65536.4",
      "fmm"                                        : "< inputs/input.65536.4",
      "ocean_contiguous"                           : "-p4 -n258",
      "ocean_non_contiguous"                       : "-p4 -n258",
      "water-spatial"                              : "< input.4",
      "water-nsquared"                             : "< input.4",
      "raytrace"                                   : "-p4 -m64 inputs/car.env",
      "volrend"                                    : "4 inputs/head",
      "radiosity"                                  : "-p 4 -batch -room",
# parsec
      "blackscholes"                               : "64 in_64K.txt prices.txt",
      "swaptions"                                  : "-ns 64 -sm 40000 -nt 64",
      "fluidanimate"                               : "64 5 in_300K.fluid out.fluid",
      "canneal"                                    : "64 15000 2000 400000.nets 128",
      "streamcluster"                              : "10 20 128 16384 16384 1000 none output.txt 64",
      "freqmine"                                   : "kosarak_990k.dat 790",
      "dedup"                                      : "-c -p -v -t 20 -i media.dat -o output.dat.ddp",
      "ferret"                                     : "corel lsh queries 10 20 15 output.txt",
      "bodytrack"                                  : "sequenceB_4 4 4 4000 5 0 62",
      "facesim"                                    : "-timing -threads 64",
# spec   
      "perlbench"                                  : "diffmail.pl 4 800 10 17 19 300",
      "bzip2"                                      : "chicken.jpg 30", 
      "gcc"                                        : "166.i -o 166.s",
      "mcf"                                        : "inp.in", 
      "gobmk"                                      :  "--quiet --mode gtp < score2.tst",
      "hmmer"                                      : "nph3.hmm swiss41",
      "sjeng"                                      : "ref.txt",
      "libquantum"                                 : "1397 8",
      "h264ref"                                    : " -d foreman_ref_encoder_main.cfg",
      "omnetpp"                                    : "omnetpp.ini",
      "astar"                                      : "rivers.cfg",
# DBMS   
      "dbx1000"                                    : "",
# LEASE   
      "counters"                                   : "",
      "stack"                                      : "",
      "queue"                                      : "",
      "multiqueue"                                 : " -i100 -c8 ",
# ASCYLIB   
      "bst_aravind"                                : " -i1024 -r10000 -u50 -p25 ",
      "htjava"                                     : " -i1024 -r10000 -u50 -p25 ",
      "lbpq_alistarh"                              : " -i4 -r64 -u100 -p100 ",
      "lbpq_alistarh_pugh"                         : " -i20000 -r100000 -u100 -p100 ",
      "lb_counter"                                 : " -l100000 -f2 -k8 ",
# SIM_UNIV
      "simstack"                                   : " 10 ",
# LOCKFREE_LIB
      "skip_mcas"                                  : "",
# CRONO
      "pagerank"                                   : " 0 ",
      "dijkstra"                                   : " 0 ",

      }

name_table = {
      "radix"                                      : "RADIX",
      "fft"                                        : "FFT",
      "lu_contiguous"                              : "LU-C",
      "lu_non_contiguous"                          : "LU-NC",
      "cholesky"                                   : "CHOLESKY",
      "barnes"                                     : "BARNES",
      "fmm"                                        : "FMM",
      "ocean_contiguous"                           : "OCEAN-C",
      "ocean_non_contiguous"                       : "OCEAN-NC",
      "water-spatial"                              : "WATER-SP",
      "water-nsquared"                             : "WATER-NSQ",
      "raytrace"                                   : "RAYTRACE",
      "volrend"                                    : "VOLREND",
      "radiosity"                                  : "RADIOSITY",
      "blackscholes"                               : "BLACKSCH.",
      "swaptions"                                  : "SWAPTIONS",
      "fluidanimate"                               : "FLUIDANIM.",
      "canneal"                                    : "CANNEAL",
      "streamcluster"                              : "STREAMCLUS.",
      "freqmine"                                   : "FREQMINE",
      "dedup"                                      : "DEDUP",
      "ferret"                                     : "FERRET",
      "bodytrack"                                  : "BODYTRACK",
      "facesim"                                    : "FACESIM",
      "dbx1000"                                    : "DBX1000",
      "counters"                                   : "COUNTERS",
      "stack"                                      : "STACK",
      "queue"                                      : "QUEUE",
      }

threads_table = {
      "radix"                                      : 4,
      "fft"                                        : 4,
      "lu_contiguous"                              : 4,
      "lu_non_contiguous"                          : 4,
      "cholesky"                                   : 4,
      "barnes"                                     : 4,
      "fmm"                                        : 4,
      "ocean_contiguous"                           : 4,
      "ocean_non_contiguous"                       : 4,
      "water-spatial"                              : 4,
      "water-nsquared"                             : 4,
      "raytrace"                                   : 4,
      "volrend"                                    : 4,
      "radiosity"                                  : 4,
      "blackscholes"                               : 64,
      "swaptions"                                  : 64,
      "fluidanimate"                               : 64,
      "canneal"                                    : 64,
      "streamcluster"                              : 64,
      "freqmine"                                   : 64,
      "dedup"                                      : 63,
      "ferret"                                     : 63,
      "bodytrack"                                  : 63,
      "facesim"                                    : 64,
      }
