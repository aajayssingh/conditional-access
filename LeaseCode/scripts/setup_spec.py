import os, sys, glob, shutil, re

benchmarks = {
	"400.perlbench",
	"401.bzip2",
	"403.gcc",
	"429.mcf",
	"445.gobmk",
	"456.hmmer",
	"458.sjeng",
	"462.libquantum",
	"464.h264ref",
	"471.omnetpp",
	"473.astar",
	"483.xalancbmk",
}

HOME = os.environ['HOME']
SPEC_HOME = HOME + "/research/spec06/"
GRAPHITE_HOME = HOME + "/research/Graphite-Ascend/"

SRC_DIR = SPEC_HOME + "benchspec/CPU2006/"
DEST_DIR = GRAPHITE_HOME + "tests/spec06/"
BuildDirInSpec = "/run/build_base_amd64-m64-gcc42-nn.0000/"

# get benchmark list
os.chdir(SRC_DIR)
spec_benchs = glob.glob("4*.*")

os.chdir(GRAPHITE_HOME)

App_arguments = {
"perlbench": "$(CURDIR)/diffmail.pl 4 800 10 17 19 300",
"bzip2" : "$(CURDIR)/chicken.jpg 30", 
"gcc" : "$(CURDIR)/166.i -o $(CURDIR)/166.s",
"mcf" : "$(CURDIR)/inp.in", 
"gobmk" :  "--quiet --mode gtp < $(CURDIR)/score2.tst",
"hmmer" : "$(CURDIR)/nph3.hmm $(CURDIR)/swiss41",
"sjeng" : "$(CURDIR)/ref.txt",
"libquantum" : "1397 8",
"h264ref" : " -d $(CURDIR)/foreman_ref_encoder_main.cfg",
"omnetpp" : "$(CURDIR)/omnetpp.ini",
"astar" : "$(CURDIR)/rivers.cfg",
"xalancbmk" : " -v $(CURDIR)/t5.xml $(CURDIR)/xalanc.xsl",

"bwaves" : "",
"gamess" : "< $(CURDIR)/cytosine.2.config",
"milc" : "< $(CURDIR)/su3imp.in",
"zeusmp" : "",
"gromacs" : "-silent -deffnm $(CURDIR)/gromacs -nice 0",
"cactusADM" : "$(CURDIR)/benchADM.par",
"leslie3d" : "$(CURDIR)/leslie3d.in",
"namd" : "--input $(CURDIR)/namd.input --iterations 38 --output namd.out",
"dealII" : "23",
"soplex" : "-s1 -e -m45000 $(CURDIR)/pds-50.mps",
"povray" : "$(CURDIR)/SPEC-benchmark-ref.ini",
"calculix" : "-i  $(CURDIR)/hyperviscoplastic",
"GemsFDTD" : "",
"tonto" : "",
"lbm" : "3000 $(CURDIR)/reference.dat 0 0 $(CURDIR)/100_100_130_ldc.of",
"wrf" : "",
"sphinx3" : "$(CURDIR)/ctlfile . $(CURDIR)/args.an4"
}

def createMakefile(dest_dir, bench_name):
	f = open(dest_dir + 'Makefile', 'w')
	f.write("TARGET = " + bench_name + '\n')
	f.write("APP_FLAGS ?= " + App_arguments[bench_name] + '\n')
	f.write("include ../../Makefile.tests" + '\n')
	f.close()

for bench in benchmarks:
	bench_name = bench[4:]
	dest_dir = DEST_DIR + bench_name + '/'
	if not os.path.exists(dest_dir):
		os.makedirs(dest_dir)	
	if os.path.exists(SRC_DIR +  bench + '/data/ref/input/'):
		os.system("cp -r " + SRC_DIR +  bench + '/data/ref/input/*' + ' ' + dest_dir)
	if os.path.exists(SRC_DIR +  bench + '/data/all/input/'):
		os.system("cp -r " + SRC_DIR +  bench + '/data/all/input/*' + ' ' + dest_dir)			
	
	src_file = SRC_DIR + bench + BuildDirInSpec + bench_name
	if os.path.isfile(src_file):
		print "copying " + bench
		os.system("cp " + src_file + ' ' + dest_dir + bench_name)		
	else :
		print "ERROR %s not found!" % src_file
	
	createMakefile(dest_dir, bench_name)
