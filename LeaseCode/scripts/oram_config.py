#!/usr/bin/env python

import sys
import os
import random

sys.path.append("./tools/")
sys.path.append("./tools/scheduler")
sys.path.append("./tools/job")

from simulate import *
from benchmark_config import *
from utils import *
from sim_job import SimJob

# scheduler: Use 'condor' for the condor scheduling system or 'basic' for using Graphite's scheduling system
scheduler = "basic"

def main_exp(benchmark_list, configs, 
   app_threads_list,
   lease_times_list,
   results_dir = "./results/1_line_lease/",
   mode = "pin",
   op_type = 0,
   max_duration = 200,
   lease_count = 1,
   res_dir_suffix = ""
   ):

   jobs = []
   # config_filename: Config file to use for the simulation
   config_filename = "carbon_sim.cfg"
      
   for benchmark in benchmark_list:
      command = getCommand(benchmark)
      for app_threads in app_threads_list:
         for config in configs: 
            if config == 'lease' :
               for lease_time in lease_times_list:
                  
                  base_sim_flags = ""
                  base_sim_flags += "--caching_protocol/enable_lease=true "
                  app_flags = app_flags_table[benchmark]
                  if benchmark in crono_list :
                     app_flags += " %d " % app_threads   # Threads
                     app_flags += " 1048576 16 "         # Nodes & Degree
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  elif benchmark == 'raytrace' :
                     app_flags = "-p%d " % app_threads  # Threads
                     app_flags += " -m64 /share/hnas_cagshare/skh13002/graphite/LeaseCode/tests/benchmarks/raytrace/inputs/car.env " 
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  elif benchmark == 'skip_mcas' :
                     app_flags += " %d " % app_threads  # Threads
                     app_flags += " 0 12 "  # Nodes & Degree
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  elif benchmark in sim_univ_list :
                     base_sim_flags += "-DN_THREADS=%d " % (app_threads)
                     base_sim_flags += "-DUSE_CPUS=%d " % (app_threads)
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  elif benchmark in ascylib_list :
                     app_flags += "-n%d " % app_threads  # Threads
                     app_flags += "-d%d " % max_duration # Duration
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  elif benchmark == 'multiqueue' :
                     app_flags += "-n%d " % app_threads  # Threads
                     app_flags += "-u%d " % max_duration # Duration
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  else :
                     app_flags += "%d " % app_threads    # Threads
                     app_flags += "%d " % op_type        # Operation Type
                     app_flags += "%d " % lease_time     # Lease Time
                     app_flags += "%d " % max_duration   # MAX_COUNT
                     app_flags += "%d " % lease_count    # Number of leases
                     base_sim_flags += "--general/total_cores=%d " % (app_threads)
                  print benchmark, app_flags

                  sim_flags = base_sim_flags
                  sim_flags += "--general/mode=full " + \
                        "--transport/base_port=%d " % (random.randrange(30000) + 2000)
                  sim_flags += "--general/trigger_models_within_application=true "
                  sim_flags += "--log/enabled_modules='' "
               
                  # Generate sub_dir where results are going to be placed
                  sub_dir = "/%s%s/threads%d/%s/lease_count%d/lease_time%d" % (benchmark, res_dir_suffix, app_threads, config, lease_count, lease_time)
           
                  # Append to jobs list
                  jobs.append(SimJob(command, 1, config_filename, results_dir, sub_dir, sim_flags, app_flags, mode, scheduler))
                  
            elif config == 'nolease' :
               
               base_sim_flags = ""
               base_sim_flags += "--caching_protocol/enable_lease=false "
               
               app_flags = app_flags_table[benchmark]
               if benchmark in crono_list :
                  app_flags += " %d " % app_threads   # Threads
                  app_flags += " 1048576 16 "         # Nodes & Degree
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               elif benchmark == 'raytrace' :
                  app_flags = "-p%d " % app_threads  # Threads
                  app_flags += " -m64 /share/hnas_cagshare/skh13002/graphite/LeaseCode/tests/benchmarks/raytrace/inputs/car.env " 
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               elif benchmark == 'skip_mcas' :
                  app_flags += " %d " % app_threads  # Threads
                  app_flags += " 0 12 "  # Nodes & Degree
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               elif benchmark in sim_univ_list :
                  base_sim_flags += "-DN_THREADS=%d " % (app_threads)
                  base_sim_flags += "-DUSE_CPUS=%d " % (app_threads)
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               elif benchmark in ascylib_list :
                  app_flags += "-n%d " % app_threads  # Threads
                  app_flags += "-d%d " % max_duration # Duration
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               elif benchmark == 'multiqueue' :
                  app_flags += "-n%d " % app_threads  # Threads
                  app_flags += "-u%d " % max_duration # Duration
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               else :
                  app_flags += "%d " % app_threads    # Threads
                  app_flags += "%d " % op_type        # Operation Type
                  app_flags += "%d " % -1             # Dummy Lease time
                  app_flags += "%d " % max_duration   # MAX_COUNT
                  app_flags += "%d " % lease_count    # Number of leases
                  base_sim_flags += "--general/total_cores=%d " % (app_threads)
               print benchmark, app_flags

               sim_flags = base_sim_flags
               sim_flags += "--general/mode=full " + \
                     "--transport/base_port=%d " % (random.randrange(30000) + 2000)
               sim_flags += "--general/trigger_models_within_application=true "
               sim_flags += "--log/enabled_modules='' "
            
               # Generate sub_dir where results are going to be placed
               sub_dir = "/%s%s/threads%d/%s" % (benchmark, res_dir_suffix, app_threads, config)
        
               # Append to jobs list
               jobs.append(SimJob(command, 1, config_filename, results_dir, sub_dir, sim_flags, app_flags, mode, scheduler))
         
   return jobs

def synthetic_benchmark(
   configs,
   results_dir = "./results/1_line_lease/",
   merge_coeff=1,
   break_coeff=1, 
   phase_change = False,
   locality = 1,
   sbsize = 2,
   merge_scheme = 'adapt',
   break_scheme = 'adapt',
   name = ""
):

   jobs = []
   # config_filename: Config file to use for the simulation
   config_filename = "carbon_sim.cfg"
   if name != "" :
      assert(len(configs) == 1)
#for config in ['dram', 'oram', 'stat2', 'dyn2']:
   for config in configs: 
      base_sim_flags = ""
      base_sim_flags += "--l1_icache/T1/cache_line_size=%d " % (cache_line_size) + \
                     "--l1_dcache/T1/cache_line_size=%d " % (cache_line_size) + \
                     "--l2_cache/T1/cache_line_size=%d " % (cache_line_size) + \
                     "--oram/block_size=%d " % (cache_line_size)

      base_sim_flags += "--oram/superblock/dynamic/merge_coeff=%d " % (merge_coeff)
      base_sim_flags += "--oram/superblock/dynamic/break_coeff=%d " % (break_coeff)

      if config == 'dram' :
         base_sim_flags += "--oram/enable=false "
      elif config == 'oram' :
         base_sim_flags += "--oram/enable=true "
      elif config == 'stat2' :
         base_sim_flags += "--oram/enable=true " + \
                 "--oram/superblock/max_sb_size=%d " % (sbsize)
      elif config == 'dyn2' :
         base_sim_flags += "--oram/enable=true " + \
                  "--oram/superblock/max_sb_size=%d " % (sbsize) + \
                  "--oram/superblock/dynamic_superblock=true " + \
				  "--oram/superblock/dynamic/merge_scheme=%s " % (merge_scheme)+ \
				  "--oram/superblock/dynamic/break_scheme=%s " % (break_scheme)

      # Generate command
      command = 'make sb_synth_app_test' 
      # Get APP_FLAGS
      app_flags = ""
      if phase_change :
         app_flags = " -w32 -P -s%d" % (sbsize)
      else :
         app_flags = " -w32 -L%s -s%d" % (locality, sbsize)
      
      sim_flags = base_sim_flags
      sim_flags += "--general/mode=lite " + \
            "--transport/base_port=3000 " #%d " % random.randrange(10000)

      sim_flags += "--general/total_cores=1 " + \
               "--general/trigger_models_within_application=false " + \
               "--general/max_instruction=-1 "
      
      # Generate sub_dir where results are going to be placed
      if name != "":
         sub_dir = "/%s" % (name)
      else :
         sub_dir = "/%s/P%d_L%s_sb%d" % (config, phase_change, locality, sbsize)
  
      # Append to jobs list
      jobs.append(SimJob(command, 1, config_filename, results_dir, sub_dir, sim_flags, app_flags, "pin", scheduler))
   return jobs


#def sweep_stash_size() :                 DONE
#def sweep_dram_bandwidth() :             DONE
#def sweep_cache_line_size() :            DONE
#def sweep_merge_break_coeff() :          DONE
#def sweep_input_size() :                 
#def sweep_core_cnt() :
#def main_exp_with_interval() :
# sweep last level cache size             
