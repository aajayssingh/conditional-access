#!/usr/bin/env python

import re
import sys
import numpy
from optparse import OptionParser

global output_file_contents
global num_cores

def searchKey(key, line):
   global num_cores
   key += "(.*)"
   match_key = re.search(key, line)
   if match_key:
      counts = line.split('|')
      event_counts = counts[1:num_cores+1]
      for i in range(0, num_cores):
         if (len(event_counts[i].split()) == 0):
            event_counts[i] = "0.0"
      return map(lambda x: float(x), event_counts)
   return None

def rowSearch1(heading, key):
   global output_file_contents
   heading_found = False
   for line in output_file_contents:
      if heading_found:
         value = searchKey(key, line)
         if value:
            return value
      else:
         heading_found = (re.search(heading, line) != None)

   print "ERROR: Could not find key [%s,%s]" % (heading, key)
   sys.exit(1)

def rowSearch2(heading, sub_heading, key):
   global output_file_contents
   heading_found = False
   sub_heading_found = False
   for line in output_file_contents:
      if heading_found:
         if sub_heading_found:
            value = searchKey(key, line)
            if value:
               return value
         else:
            sub_heading_found = (re.search(sub_heading, line) != None)
      else:
         heading_found = (re.search(heading, line) != None)

   print "ERROR: Could not find key [%s,%s,%s]" % (heading, sub_heading, key)
   sys.exit(1)

def rowSearch3(heading):
   global output_file_contents
   heading_found = False
   for line in output_file_contents:
      heading_found = (re.search(heading, line) != None)
      if heading_found:
         counts = line.split('=')
         event_counts = counts[1]
         return float(event_counts)
   
   print "ERROR: Could not find key [%s]" % (heading)
   sys.exit(1)

def getTime(key):
   global output_file_contents

   key += "\s+([0-9]+)\s*"
   for line in output_file_contents:
      match_key = re.search(key, line)
      if match_key:
         return float(match_key.group(1))
   print "ERROR: Could not find key [%s]" % (key)
   sys.exit(2)


###########################################################################################################
# UPDATE VARIABLES IN THIS SECTION #
###########################################################################################################

root_results_dir = "/home/t-syhai/graphite/results/ppopp_main_exp/"

benchmarks = [
      #"counters_pthread_1lock" ,
      #"counters_CAS_2lock" ,
      #"counters_lockfree" ,
      #"stack" ,
      #"queue" ,
      #"multiqueue_8lock" ,
      #"lbpq_alistarh_pugh_GL" ,
      #"lbpq_alistarh_pugh_FL" ,
      #"lbpq_alistarh_pugh_GTRL" ,
      #"simstack" ,
      #"lb_counter_MUTEX" ,
      #"lb_counter_CLH" ,
      #"lb_counter_HTICKET" ,
      #"lbpq_alistarh_pugh_spray_GL" ,
      #"lb_counter_CLH2" ,
      #"lbpq_alistarh_pugh_spray" ,
      #"lb_counter_CLH_1" ,
      #"lb_counter_CLH_2" ,
      #"lb_counter_CLH_3" ,
      #"lb_counter_TTAS"  ,
      ]

threads_list = [2, 4, 8, 16, 32, 64]

configs_table = { 
      "counters_pthread_1lock"      :  ['nolease', 'lease'],
      "counters_CAS_2lock"          :  ['nolease', 'lease'],
      "counters_lockfree"           :  ['nolease', 'lease'],
      "stack"                       :  ['nolease', 'lease'],
      "queue"                       :  ['nolease', 'lease'],
      "multiqueue_8lock"            :  ['nolease', 'lease'],
      "lbpq_alistarh_pugh_GL"       :  ['nolease', 'lease'],
      "lbpq_alistarh_pugh_GTRL"     :  ['nolease', 'lease'],
      "lbpq_alistarh_pugh_FL"       :  ['nolease'],
      "simstack"                    :  ['nolease'],
      "lb_counter_MUTEX"            :  ['nolease', 'lease'],
      "lb_counter_TTAS"             :  ['nolease', 'lease'],
      "lb_counter_CLH"              :  ['nolease'],
      "lb_counter_CLH2"             :  ['nolease'],
      "lb_counter_HTICKET"          :  ['nolease'],
      "lbpq_alistarh_pugh_spray_GL" :  ['nolease'],
      "lbpq_alistarh_pugh_spray"    :  ['nolease'],
      "stack_run1"                  :  ['nolease', 'lease'],
      "stack_run2"                  :  ['nolease', 'lease'],
      "stack_run3"                  :  ['nolease', 'lease'],
      "stack_run4"                  :  ['nolease', 'lease'],
      "stack_run5"                  :  ['nolease', 'lease'],
      "stack_run6"                  :  ['nolease', 'lease'],
      "stack_run7"                  :  ['nolease', 'lease'],
      "lb_counter_CLH_1"            :  ['nolease'],
      "lb_counter_CLH_2"            :  ['nolease'],
      "lb_counter_CLH_3"            :  ['nolease'],
      }

lease_counts = { 
      "counters_pthread_1lock"      :  [1],
      "counters_CAS_2lock"          :  [1, 2],
      "counters_lockfree"           :  [1],
      "stack"                       :  [1],
      "queue"                       :  [1, 2],
      "multiqueue_8lock"            :  [2],
      "lbpq_alistarh_pugh_GL"       :  [1],
      "lbpq_alistarh_pugh_GTRL"     :  [1],
      "lb_counter_MUTEX"            :  [1],
      "lb_counter_TTAS"             :  [1],
      "stack_run1"                  :  [1],
      "stack_run2"                  :  [1],
      "stack_run3"                  :  [1],
      "stack_run4"                  :  [1],
      "stack_run5"                  :  [1],
      "stack_run6"                  :  [1],
      "stack_run7"                  :  [1],
      }

lease_times = { 
      "counters_pthread_1lock"      :  [100000],
      "counters_CAS_2lock"          :  [100000],
      "counters_lockfree"           :  [10000],
      "stack"                       :  [10000],
      "queue"                       :  [1000000],
      "multiqueue_8lock"            :  [100000],
      "lbpq_alistarh_pugh_GL"       :  [20000],
      "lbpq_alistarh_pugh_GTRL"     :  [20000, 100000],
      "lb_counter_MUTEX"            :  [100000],
      "lb_counter_TTAS"             :  [100000],
      "stack_run1"                  :  [10000],
      "stack_run2"                  :  [10000],
      "stack_run3"                  :  [10000],
      "stack_run4"                  :  [10000],
      "stack_run5"                  :  [10000],
      "stack_run6"                  :  [10000],
      "stack_run7"                  :  [10000],
      }


###########################################################################################################
# DON'T CHANGE ANYTHING BELOW THIS #
###########################################################################################################

conf_name = {
      "lease"     : "LEASE" ,
      "nolease"   : "NO_LEASE" ,
      }

# Write Aggregate results in files
aggregate_perf = open("aggregate_perf.out", 'w')
aggregate_energy = open("aggregate_energy.out", 'w')
aggregate_cache = open("aggregate_cache.out", 'w')
aggregate_network = open("aggregate_network.out", 'w')

aggregate_perf.write("\t Throughput (Operations/sec) \n\n")
aggregate_energy.write("\t Energy Consumption (NanoJoules/Operation)\n\n")
aggregate_cache.write("\t Cache Misses per Operation\n\n")
aggregate_network.write("\t Network messages exchangd per Operation \n\n")

for benchmark in benchmarks :
   results_path = "./"
   
   # Create Heading in results file
   aggregate_perf.write("\nBenchmark: %s\n" % (benchmark) )
   aggregate_energy.write("\nBenchmark: %s\n" % (benchmark) )
   aggregate_cache.write("\nBenchmark: %s\n" % (benchmark) )
   aggregate_network.write("\nBenchmark: %s\n" % (benchmark) )
   
   aggregate_perf.write("Threads, \t")
   aggregate_energy.write("Threads, \t")
   aggregate_cache.write("Threads, \t")
   aggregate_network.write("Threads, \t")

   configs = configs_table[benchmark]
   for conf in configs :
      if conf == 'lease' :
         lcounts = lease_counts[benchmark]
         ltimes = lease_times[benchmark]
         for lc in lcounts :
            for lt in ltimes :
               aggregate_perf.write("%d_%s_%d, \t" % (lc, conf_name[conf], lt) )
               aggregate_energy.write("%d_%s_%d, \t" % (lc, conf_name[conf], lt) )
               aggregate_cache.write("%d_%s_%d, \t" % (lc, conf_name[conf], lt) )
               aggregate_network.write("%d_%s_%d, \t" % (lc, conf_name[conf], lt) )
      else :
         aggregate_perf.write("%s, \t" % (conf_name[conf]) )
         aggregate_energy.write("%s, \t" % (conf_name[conf]) )
         aggregate_cache.write("%s, \t" % (conf_name[conf]) )
         aggregate_network.write("%s, \t" % (conf_name[conf]) )
   aggregate_perf.write("\n")
   aggregate_energy.write("\n")
   aggregate_cache.write("\n")
   aggregate_network.write("\n")

   # Collect Results
   for threads in threads_list :
      aggregate_perf.write("%d, \t" % (threads) )
      aggregate_energy.write("%d, \t" % (threads) )
      aggregate_cache.write("%d, \t" % (threads) )
      aggregate_network.write("%d, \t" % (threads) )
      for conf in configs :
         if conf == 'lease' :
            lcounts = lease_counts[benchmark]
            ltimes = lease_times[benchmark]
            for lc in lcounts :
               for lt in ltimes :
                  results_path = root_results_dir + "%s/threads%d/%s/lease_count%d/lease_time%d/" % (benchmark, threads, conf, lc, lt)
                  print "Processing directory: %s" % (results_path)
                  # Read Results Files
                  try:
                     output_file_contents = open("%s/sim.out" % (results_path), 'r').readlines()
                  except IOError:
                     print "ERROR: Could not open file (%s/sim.out)" % (results_path)
                     sys.exit(3)
                  
                  # Set number of cores
                  num_cores = threads
                  
                  # Energy - In joules
                  core_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Core", "Total Energy \(in J\)"))
                  cache_hierarchy_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Cache Hierarchy \(L1-I, L1-D, L2\)", "Total Energy \(in J\)"))
                  networks_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Networks \(User, Memory\)", "Total Energy \(in J\)"))
                  target_energy = core_energy + cache_hierarchy_energy + networks_energy
                  
                  # Network 
                  total_messages = sum(rowSearch2("Network Summary", "Network \(Memory\)", "Total Packets Sent"))
                  
                  # Cache Misses
                  total_l1_misses = sum(rowSearch2("Cache Summary", "Cache L1-D", "Cache Misses"))

                  # Read Results Files
                  try:
                     output_file_contents = open("%s/output" % (results_path), 'r').readlines()
                  except IOError:
                     print "ERROR: Could not open file (%s/output)" % (results_path)
                     sys.exit(3)

                  throughput = rowSearch3("Throughput")
                  operations = rowSearch3("Successful Ops")

                  # Write in the file
                  aggregate_perf.write("%f, \t" % (throughput) )
                  aggregate_energy.write("%lf, \t" % ((target_energy*1000000000)/operations) )
                  aggregate_cache.write("%lf, \t" % ((total_l1_misses)/operations) )
                  aggregate_network.write("%lf, \t" % ((total_messages)/operations) )

         else :
            results_path = root_results_dir + "%s/threads%d/%s/" % (benchmark, threads, conf)
            print "Processing directory: %s" % (results_path)
            try:
               output_file_contents = open("%s/sim.out" % (results_path), 'r').readlines()
            except IOError:
               print "ERROR: Could not open file (%s/sim.out)" % (results_path)
               sys.exit(3)

            # Set number of cores
            num_cores = threads
            
            # Energy - In joules
            core_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Core", "Total Energy \(in J\)"))
            cache_hierarchy_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Cache Hierarchy \(L1-I, L1-D, L2\)", "Total Energy \(in J\)"))
            networks_energy = sum(rowSearch2("Tile Energy Monitor Summary", "Networks \(User, Memory\)", "Total Energy \(in J\)"))
            target_energy = core_energy + cache_hierarchy_energy + networks_energy

            # Network 
            total_messages = sum(rowSearch2("Network Summary", "Network \(Memory\)", "Total Packets Sent"))
            
            # Cache Misses
            total_l1_misses = sum(rowSearch2("Cache Summary", "Cache L1-D", "Cache Misses"))
            
            # Read Results Files
            try:
               output_file_contents = open("%s/output" % (results_path), 'r').readlines()
            except IOError:
               print "ERROR: Could not open file (%s/output)" % (results_path)
               sys.exit(3)

            throughput = rowSearch3("Throughput")
            operations = rowSearch3("Successful Ops")
            
            # Write in the file
            aggregate_perf.write("%f, \t" % (throughput) )
            aggregate_energy.write("%lf, \t" % ((target_energy*1000000000)/operations) )
            aggregate_cache.write("%lf, \t" % ((total_l1_misses)/operations) )
            aggregate_network.write("%lf, \t" % ((total_messages)/operations) )

      
      aggregate_perf.write("\n")
      aggregate_energy.write("\n")
      aggregate_cache.write("\n")
      aggregate_network.write("\n")
      

aggregate_perf.close()
aggregate_energy.close()
aggregate_cache.close()
aggregate_network.close()

print "Stats written in files: aggregate_perf.out and aggregate_energy.out"

###########################################################################################################
