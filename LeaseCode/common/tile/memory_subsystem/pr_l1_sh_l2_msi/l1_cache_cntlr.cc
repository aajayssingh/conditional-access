#include "l1_cache_cntlr.h"
#include "memory_manager.h"
#include "config.h"
#include "log.h"

namespace PrL1ShL2MSI
{

void print_set(std::set<IntPtr> dataset)
{
   std::set<IntPtr>::const_iterator it;
   for (it = dataset.begin(); it != dataset.end(); it++)
      printf("%#lx ", *it);
   printf("\n");
}

L1CacheCntlr::L1CacheCntlr(MemoryManager* memory_manager,
                           AddressHomeLookup* L2_cache_home_lookup,
                           UInt32 cache_line_size,
                           UInt32 L1_icache_size,
                           UInt32 L1_icache_associativity,
                           UInt32 L1_icache_num_banks,
                           string L1_icache_replacement_policy,
                           UInt32 L1_icache_data_access_cycles,
                           UInt32 L1_icache_tags_access_cycles,
                           string L1_icache_perf_model_type,
                           bool L1_icache_track_miss_types,
                           UInt32 L1_dcache_size,
                           UInt32 L1_dcache_associativity,
                           UInt32 L1_dcache_num_banks,
                           string L1_dcache_replacement_policy,
                           UInt32 L1_dcache_data_access_cycles,
                           UInt32 L1_dcache_tags_access_cycles,
                           string L1_dcache_perf_model_type,
                           bool L1_dcache_track_miss_types)
   : _memory_manager(memory_manager)
   , _L2_cache_home_lookup(L2_cache_home_lookup)
{
   _L1_icache_replacement_policy_obj = 
      CacheReplacementPolicy::create(L1_icache_replacement_policy, L1_icache_size, L1_icache_associativity, cache_line_size);
   _L1_dcache_replacement_policy_obj = 
      CacheReplacementPolicy::create(L1_dcache_replacement_policy, L1_dcache_size, L1_dcache_associativity, cache_line_size);
   _L1_icache_hash_fn_obj = new CacheHashFn(L1_icache_size, L1_icache_associativity, cache_line_size);
   _L1_dcache_hash_fn_obj = new CacheHashFn(L1_dcache_size, L1_dcache_associativity, cache_line_size);

   _L1_icache = new Cache("L1-I",
         PR_L1_SH_L2_MSI,
         Cache::INSTRUCTION_CACHE,
         L1,
         Cache::UNDEFINED_WRITE_POLICY,
         L1_icache_size,
         L1_icache_associativity, 
         cache_line_size,
         L1_icache_num_banks,
         _L1_icache_replacement_policy_obj,
         _L1_icache_hash_fn_obj,
         L1_icache_data_access_cycles,
         L1_icache_tags_access_cycles,
         L1_icache_perf_model_type,
         L1_icache_track_miss_types);
   _L1_dcache = new Cache("L1-D",
         PR_L1_SH_L2_MSI,
         Cache::DATA_CACHE,
         L1,
         Cache::WRITE_BACK,
         L1_dcache_size,
         L1_dcache_associativity, 
         cache_line_size,
         L1_dcache_num_banks,
         _L1_dcache_replacement_policy_obj,
         _L1_dcache_hash_fn_obj,
         L1_dcache_data_access_cycles,
         L1_dcache_tags_access_cycles,
         L1_dcache_perf_model_type,
         L1_dcache_track_miss_types);

   _last_access_time = Time(0);
   _blocked = false;
}

L1CacheCntlr::~L1CacheCntlr()
{
   delete _L1_icache;
   delete _L1_dcache;
   delete _L1_icache_replacement_policy_obj;
   delete _L1_dcache_replacement_policy_obj;
   delete _L1_icache_hash_fn_obj;
   delete _L1_dcache_hash_fn_obj;
}      

bool
L1CacheCntlr::processMemOpFromCore(MemComponent::Type mem_component,
                                   Core::lock_signal_t lock_signal,
                                   Core::mem_op_t mem_op_type, 
                                   IntPtr ca_address, UInt32 offset,
                                   Byte* data_buf, UInt32 data_length,
                                   bool modeled)
{
   IntPtr address = ca_address;
   Core::mem_op_t curr_mem_op;
   bool is_leased =false;
   bool load_next =false;
   bool is_watched = false;
   //_group_lease.clear();
   
   map<IntPtr, int> temp_addr_map;

   // Check if it is leased
   is_leased = (_to_lease.find(address) != _to_lease.end())? true : false ;
  
   // Check if it is watched through Watchset
   is_watched = (_to_watch.find(address) != _to_watch.end())? true : false ;   
  
#ifdef DEBUG_SIM   
   if(is_watched){
      //printf("%i: processMemOpFromCore: Watched Address %#lx \n", getTileId(), address);
      LOG_ASSERT_ERROR(mem_op_type == Core::READ, "Watchset supports Read-Only data, whereas requested for Writing data. Address(%#llx).", address);
   }
#endif
   // Requesting the first to_be_leased line first to ensure a strict order
   if(is_leased){
      address = _to_lease.begin()->first;
      LOG_PRINT("processMemOpFromCore(): Loading first Leased Line:  address(%#llx)", address);
   }

   do{

      // Check if it is leased
      is_leased = (_to_lease.find(address) != _to_lease.end())? true : false ;

      // Leased Requests should be EXCLUSIVE requests
      curr_mem_op = (is_leased)? Core::WRITE : mem_op_type;

      LOG_PRINT("processMemOpFromCore(), lock_signal(%u), mem_op_type(%u), address(%#llx)",
                lock_signal, curr_mem_op, address);

      bool L1_cache_hit = true;
      UInt32 access_num = 0;

      // Core synchronization delay
      // No delay for First access to Watched lines
      if(!is_watched)
         getShmemPerfModel()->incrCurrTime(getL1Cache(mem_component)->getSynchronizationDelay(CORE));

      while(1)
      {
         access_num ++;

         LOG_ASSERT_ERROR((access_num == 1) || (access_num == 2), "access_num(%u)", access_num);

         // Wake up the sim thread after acquiring the lock
         if (access_num == 2)
         {
            _memory_manager->wakeUpSimThread();
         }

         pair<bool, Cache::MissType> cache_miss_info = operationPermissibleinL1Cache(mem_component, address, curr_mem_op, access_num);
         
         bool cache_hit = !cache_miss_info.first;

         if(_lease_time_map.find(address) != _lease_time_map.end()){
            LOG_ASSERT_ERROR(cache_hit, "Leased line (%#llx) not found in L1 Cache while it held the Lease! ", address);
         }

         if (cache_hit)
         {
            // Increment Shared Mem Perf model current time
            // L1 Cache
            // No delay for First access to Watched lines
            if(!is_watched)
            {
               _memory_manager->incrCurrTime(mem_component, CachePerfModel::ACCESS_DATA_AND_TAGS);

               // Record this access timer
               _last_access_time = getShmemPerfModel()->getCurrTime();
            }

            accessCache(mem_component, mem_op_type, address, offset, data_buf, data_length);
       
            LOG_PRINT("processMemOpFromCore(): Cache Hit, mem_op_type(%u), address(%#llx)", 
                  curr_mem_op, address);

            // Initiate Lease Time if it was a Leased Request
            if(is_leased){
               
               _group_lease[address] = _to_lease.find(address)->second;
              
               temp_addr_map[address] = _to_lease.find(address)->second;

               int lease_time = _to_lease.find(address)->second;
               addToLeaseMap(address, lease_time);
               _to_lease.erase(address);
               
               // Update the Lease Counters
               getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::LEASE);

               // Check for Multiple leases, and load them simulteneously if so.
               if(!_to_lease.empty()){
                  address = _to_lease.begin()->first;
                  load_next = true;
                  LOG_PRINT("processMemOpFromCore(): Loading next Leased Line:  address(%#llx)", address);
                  break;
               }
               else{
                  load_next = false;
                  
                  /*
                  if(load_next){
                     for(map<IntPtri, int>::iterator iter= temp_addr_map.begin(); iter != temp_addr_map.end(); iter++ ){
                     }
                  }
                  */
                  _group_lease.clear();
                  
               }
                  int leased_line_no = 0;
                  IntPtr last_address = 0;

                  for(map<IntPtr, int>::iterator iter= temp_addr_map.begin(); iter != temp_addr_map.end(); iter++ ){
                     leased_line_no++;
                     pair<bool, Cache::MissType> curr_cache_miss_info = operationPermissibleinL1Cache(MemComponent::L1_DCACHE, iter->first, Core::WRITE, 2);
                     bool curr_cache_hit = !curr_cache_miss_info.first;
                     
                     if(!curr_cache_hit){
                        printf("Group Lease Lines: ");
                        for(map<IntPtr, int>::iterator it= temp_addr_map.begin(); it != temp_addr_map.end(); it++ )
                           printf("0x%lx, ", it->first);
                        printf("\n");
                     }
                     LOG_ASSERT_ERROR(curr_cache_hit, "Leased line(%d) of group lease, Address(%#llx) not found in L1 Cache after getting Lease! ", leased_line_no, iter->first);
                     
                     LOG_ASSERT_ERROR( last_address < iter->first, "Lease Ordering violated: LastAddr(%#llx), CurrAddr(%#llx) ", last_address, iter->first);
                     last_address = iter->first;
                     
                     /*
                     if(!curr_cache_hit)
                        load_next = true;
                     */
                  }
            } // End if(_leased)
            else if ( is_watched )
            {
#ifdef DEBUG_SIM   
               LOG_ASSERT_ERROR(_watchset.find(address) == _watchset.end(),"address(%#llx) already in watchset", address);   // This address should not be already watched
#endif
               LOG_PRINT("Adding to watchset: Address(%#llx), Core(%i)", address, getTileId());
//               printf("Adding to watchset: Address(%lu), Core(%i)\n", address, getTileId());
//               fflush(stdout);

               _watchset.insert(address);
               _to_watch.erase(address);

#ifdef DEBUG_SIM   
               LOG_ASSERT_ERROR(_to_watch.find(address) == _to_watch.end(),"address(%#llx) should have been moved to permanent watchset", address);   // This address is now moved from temp to permanent Watchset
#endif
               getL1Cache(MemComponent::L1_DCACHE)->updateWatchsetOpsCounters(Cache::ADD, 1);
            }

            return L1_cache_hit;
         } // if (cache_hit)

         LOG_PRINT("processMemOpFromCore(): Cache Miss, mem_op_type(%u), address(%#llx)", 
               curr_mem_op, address);
      
         LOG_ASSERT_ERROR(access_num == 1, "Core(%i) Should find line(%#llx) in cache on second access",
               getTileId(), address);

         // Expect to find address in the L1-I/L1-D cache if there is an UNLOCK signal
         LOG_ASSERT_ERROR(lock_signal != Core::UNLOCK, "Expected to find address(%#lx) in L1 Cache", address);

         _memory_manager->incrCurrTime(mem_component, CachePerfModel::ACCESS_TAGS);

         // The memory request misses in the L1 cache
         L1_cache_hit = false;

         // Send out a request to the network thread for the cache data
         bool msg_modeled = Config::getSingleton()->isApplicationTile(getTileId());
         ShmemMsg::Type shmem_msg_type = getShmemMsgType(curr_mem_op);
         ShmemMsg shmem_msg(shmem_msg_type, MemComponent::CORE, mem_component,
                            getTileId(), false, address,
                            msg_modeled, is_leased);
         _memory_manager->sendMsg(getTileId(), shmem_msg);
         
         LOG_PRINT("Core(%i) waiting on Semaphore for line(%#llx)", getTileId(), address);
     
         //_blocked = true;
         
         // Wait for the sim thread
         _memory_manager->waitForSimThread();
         //_blocked = false;
//         LOG_PRINT("Core(%i) waiting on Semaphore for line(%#llx) Ends gonna do second access", getTileId(), address);

      }

   } while (load_next);

   LOG_PRINT_ERROR("Should not reach here");
   return false;
}

void
L1CacheCntlr::accessCache(MemComponent::Type mem_component,
      Core::mem_op_t mem_op_type, IntPtr ca_address, UInt32 offset,
      Byte* data_buf, UInt32 data_length)
{
   Cache* L1_cache = getL1Cache(mem_component);
   switch (mem_op_type)
   {
   case Core::READ:
   case Core::READ_EX:
      L1_cache->accessCacheLine(ca_address + offset, Cache::LOAD, data_buf, data_length);
      break;

   case Core::WRITE:
      L1_cache->accessCacheLine(ca_address + offset, Cache::STORE, data_buf, data_length);
      break;

   default:
      LOG_PRINT_ERROR("Unsupported Mem Op Type: %u", mem_op_type);
      break;
   }
}

pair<bool, Cache::MissType>
L1CacheCntlr::operationPermissibleinL1Cache(MemComponent::Type mem_component,
                                            IntPtr address, Core::mem_op_t mem_op_type,
                                            UInt32 access_num)
{
   PrL1CacheLineInfo L1_cache_line_info;
   getCacheLineInfo(mem_component, address, &L1_cache_line_info);
   CacheState::Type cstate = L1_cache_line_info.getCState();
   
   bool cache_hit = false;

//   LOG_PRINT("operationPermissibleinL1Cache: Address(%#llx) CSTATE(%u) ", cstate, address);
   switch (mem_op_type)
   {
   case Core::READ:
      cache_hit = CacheState(cstate).readable();
      break;

   case Core::READ_EX:
   case Core::WRITE:
      cache_hit = CacheState(cstate).writable();
      break;

   default:
      LOG_PRINT_ERROR("Unsupported mem_op_type: %u", mem_op_type);
      break;
   }

   Cache::MissType cache_miss_type = Cache::INVALID_MISS_TYPE;
   if (access_num == 1)
   {
      // Update the Cache Counters
      cache_miss_type = getL1Cache(mem_component)->updateMissCounters(address, mem_op_type, !cache_hit);
   }
#ifdef DEBUG_SIM   
   LOG_PRINT("operationPermissibleinL1Cache returns(%s)  Address(%#llx)", cache_hit ? "true" : "false", address);
#endif
   return make_pair(!cache_hit, cache_miss_type);
}

void
L1CacheCntlr::getCacheLineInfo(MemComponent::Type mem_component, IntPtr address, PrL1CacheLineInfo* L1_cache_line_info)
{
   Cache* L1_cache = getL1Cache(mem_component);
   assert(L1_cache);
   L1_cache->getCacheLineInfo(address, L1_cache_line_info);
//   LOG_PRINT("getCacheLineInfo::  Address(%#llx) CSTATE(%u)", address, L1_cache_line_info->getCState()  ); //crashaj 
}

void
L1CacheCntlr::setCacheLineInfo(MemComponent::Type mem_component, IntPtr address, PrL1CacheLineInfo* L1_cache_line_info)
{
   Cache* L1_cache = getL1Cache(mem_component);
   assert(L1_cache);
   L1_cache->setCacheLineInfo(address, L1_cache_line_info);
//   LOG_PRINT("setCacheLineInfo::end address CSTATE(%u)", address, L1_cache_line_info->getCState()  ); //crashaj 

}

void
L1CacheCntlr::readCacheLine(MemComponent::Type mem_component, IntPtr address, Byte* data_buf)
{
   Cache* L1_cache = getL1Cache(mem_component);
   assert(L1_cache);
   L1_cache->accessCacheLine(address, Cache::LOAD, data_buf, getCacheLineSize());
}

void
L1CacheCntlr::insertCacheLine(MemComponent::Type mem_component, IntPtr address, CacheState::Type cstate, Byte* fill_buf)
{

   IntPtr original_addr = address;

   Cache* L1_cache = getL1Cache(mem_component);
   assert(L1_cache);


   bool eviction;
   IntPtr evicted_address;
   PrL1CacheLineInfo evicted_cache_line_info;
   Byte writeback_buf[getCacheLineSize()];

retry:
   PrL1CacheLineInfo L1_cache_line_info(L1_cache->getTag(address), cstate);
   
   L1_cache->insertCacheLine(address, &L1_cache_line_info, fill_buf,
                             &eviction, &evicted_address, &evicted_cache_line_info, writeback_buf);

   if (eviction)
   {
      assert(evicted_cache_line_info.isValid());
//      LOG_PRINT("evicted address(%#lx) while inserting address(%#lx)", evicted_address, address);

      if(_lease_time_map.find(evicted_address) != _lease_time_map.end() || evicted_address == original_addr ){
         address = evicted_address;
         cstate = evicted_cache_line_info.getCState();
         fill_buf = writeback_buf;
#ifdef DEBUG_SIM   
         LOG_ASSERT_ERROR(evicted_address != original_addr, "aj evicted_address == original_addr to understand when this could happen.");
#endif
         goto retry;
      }
      
      // think this code in original mtag is wrong this will downgrade tagged address
      // when that is inserted in cacheline.
      // expected correct behaviour: if the evicted address is watched one then it should be downgraded.
//      if(_watchset.find(address) != _watchset.end())
//      {
//         // Downgraded
//         _downgraded_watchset.insert(address);
//         LOG_PRINT("EVICTION:: _downgraded_watchset evicted address(%#lx) evicted_address(%#lx) eviction(%i)", address, evicted_address, eviction);
//      }

      //fix for above mentioned problem.
      if(_watchset.find(evicted_address) != _watchset.end())
      {
         // Downgraded
         _downgraded_watchset.insert(evicted_address);
#ifdef DEBUG_SIM   
         LOG_PRINT("EVICTION:: _downgraded_watchset evicted address(%#lx) while inserting address(%#lx)", evicted_address, address);
#endif
      }      
      
#ifdef DEBUG_SIM   
      LOG_ASSERT_ERROR( _lease_time_map.find(evicted_address) == _lease_time_map.end(), "Leased line (%#llx) evicted from L1 Cache while holding the Lease! ", evicted_address);
      LOG_ASSERT_ERROR( _group_lease.find(evicted_address) == _group_lease.end(), "Leased line (%#llx) evicted from L1 Cache while ongoing Group Lease Transaction! ", evicted_address);
#endif

      UInt32 L2_cache_home = getL2CacheHome(evicted_address);
      bool msg_modeled = Config::getSingleton()->isApplicationTile(getTileId());

      CacheState::Type evicted_cstate = evicted_cache_line_info.getCState();
      if (evicted_cstate == CacheState::MODIFIED)
      {
         // Send back the data also
         ShmemMsg send_shmem_msg(ShmemMsg::FLUSH_REP, mem_component, MemComponent::L2_CACHE,
                                 getTileId(), false, evicted_address,
                                 writeback_buf, getCacheLineSize(),
                                 msg_modeled);
         _memory_manager->sendMsg(L2_cache_home, send_shmem_msg);
      }
      else
      {
         LOG_ASSERT_ERROR(evicted_cstate == CacheState::SHARED, "evicted_address(%#lx), evicted_cstate(%u)",
                          evicted_address, evicted_cstate);
         ShmemMsg send_shmem_msg(ShmemMsg::INV_REP, mem_component, MemComponent::L2_CACHE,
                                 getTileId(), false, evicted_address,
                                 msg_modeled);
         _memory_manager->sendMsg(L2_cache_home, send_shmem_msg);
      }
   }
}

void
L1CacheCntlr::invalidateCacheLine(MemComponent::Type mem_component, IntPtr address)
{
//   LOG_PRINT("invalidateCacheLine begin");
#ifdef DEBUG_SIM   
   LOG_ASSERT_ERROR( _lease_time_map.find(address) == _lease_time_map.end(), "Leased line (%#llx) invalidated from L1 Cache while holding the Lease! ", address);
   LOG_ASSERT_ERROR( _group_lease.find(address) == _group_lease.end(), "Leased line (%#llx) invalidated from L1 Cache while ongoing Group Lease Transaction! ", address);
#endif

#ifdef DEBUG_SIM   
   if( _watchset.find(address) != _watchset.end() )
   {
      //commenting assert because of change done in handleL2Msg to skip downgrading if requestor is the tile id.
//      LOG_ASSERT_ERROR( _downgraded_watchset.find(address) != _downgraded_watchset.end(), "Watchset line (%#llx) must have been downgraded at this point! ", address);
      LOG_PRINT("Watchset line (%#llx) must have been downgraded at this point! (%i)", address, _downgraded_watchset.find(address) != _downgraded_watchset.end());

   }
#endif   

   Cache* L1_cache = getL1Cache(mem_component);
   assert(L1_cache);

   // Invalidate cache line
   PrL1CacheLineInfo L1_cache_line_info;
   L1_cache->getCacheLineInfo(address, &L1_cache_line_info);
   L1_cache_line_info.invalidate();
   L1_cache->setCacheLineInfo(address, &L1_cache_line_info);

//   LOG_PRINT("invalidateCacheLine end");
   
}

void
L1CacheCntlr::handleMsgFromCore(ShmemMsg* shmem_msg)
{
   ShmemMsg::Type shmem_msg_type = shmem_msg->getType();
   IntPtr address = shmem_msg->getAddress();

   if(shmem_msg_type != ShmemMsg::RELEASE_REP){
      _outstanding_shmem_msg = *shmem_msg;
      _outstanding_shmem_msg_time = getShmemPerfModel()->getCurrTime();

      // Send msg out to L2 cache
      ShmemMsg send_shmem_msg(shmem_msg->getType(), shmem_msg->getReceiverMemComponent(), MemComponent::L2_CACHE,
                              shmem_msg->getRequester(), false, address,
                              shmem_msg->isModeled(),
                              shmem_msg->isLeased());
      tile_id_t receiver = _L2_cache_home_lookup->getHome(address);
      _memory_manager->sendMsg(receiver, send_shmem_msg);
      
   }
   else{
      // Process the pending requests to this Leased Cacheline
      processL2RequestToLeasedLine(address);
            
      _memory_manager->wakeUpAppThread();
      _memory_manager->waitForAppThread();
   }
}

void
L1CacheCntlr::handleMsgFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
//   LOG_PRINT("handleMsgFromL2Cache");

   ShmemMsg::Type shmem_msg_type = shmem_msg->getType();
   IntPtr address = shmem_msg->getAddress();
   
   // First Handle Requests to Leased Cache Lines
   if (  (shmem_msg_type == ShmemMsg::INV_REQ)     || 
         (shmem_msg_type == ShmemMsg::FLUSH_REQ)   || 
         (shmem_msg_type == ShmemMsg::WB_REQ)
      )
   {
      if(_watchset.find(address) != _watchset.end())
      {
         // Downgraded. AJ: not downgrading for WB_REQ because If this core got WBREQ that means someone wants to just READ. So someone reading is not a conflicting change.
         //as far as validation is concerned. Because, say, T0 reads and then validates. T1 reading in between is not a problem. Writing is. So downgrade only
         // if some wants to WRITE, ie when I get INV REQ or FLUSH req.
         if(shmem_msg_type == ShmemMsg::INV_REQ || shmem_msg_type == ShmemMsg::FLUSH_REQ){ 
            
            // why is the check safe?
            // core 0 and 1 have addr in S state. If 0 does Vread/write it will send INV req to 0 and 1 both.
            // 1 will addr but 0 will not but both will invalidate cacvheline due to MSI. Doesn't that violate semantic of downgradeset
            // which is if addr is invalidated add that to downgrade set.
            // Ans is no. because VREAD/VWRITE at 0 will atomically get addr back in M state. As if cacheline was never invalidated.
            // No but the problem is the content of invalidated cache line might have been useful?? and sincve invalidated I may not realise and when load addr in M state 
            // after cache miss may make 0 work on totally diff content???? I think INV is fine for FLUSH back its problem.
            if (shmem_msg->getRequester() != getTileId())
            {
               _downgraded_watchset.insert(address);
               LOG_PRINT("Cacheline from Watchset downgraded! Address(%#lx) shmem_msg_type(%u)", address, shmem_msg_type);
//               printf("Cacheline from Watchset downgraded! Address(%#lx) shmem_msg_type(%u) sender(%i) Requester(%i)\n", address, shmem_msg_type, sender, shmem_msg->getRequester());

               //               LOG_ASSERT_ERROR(0,"Cache line downgraded in 1 thread case via INV/FLUSH Sender(%i)", sender );
            }
         }
      }

      UInt64 current_time = getShmemPerfModel()->getCurrTime().toNanosec();
      map<IntPtr, UInt64>::iterator iter = _lease_time_map.find(address);

      // Check if this ache line is leased
      if(iter != _lease_time_map.end() ){
         // If Lease is not expired yet
         //if( current_time < iter->second ){
            // Enqueue this request and wait for the lease to expire/released
            //printf("handleMsgFromL2Cache() Core(%i): Enqueuing request from Core(%i) for Address(%lx)\n", 
              //    getTileId(), sender, address); fflush(stdout);
            ShmemReq* shmem_req = new ShmemReq(shmem_msg, getShmemPerfModel()->getCurrTime(), sender);
            _L1_outstanding_req_queue[address] = shmem_req;
            //_outstanding_reqs = true;

            LOG_ASSERT_ERROR(sender >= 0, "Sender(%i)", sender );
            LOG_ASSERT_ERROR(shmem_msg->getRequester() >= 0, "Requester(%i)", shmem_msg->getRequester() );

            /*
            //if(_blocked){
               // Send out a NAK message to the requester via L2 cache
               ShmemMsg send_shmem_msg(ShmemMsg::RELEASE_NAK, MemComponent::L1_DCACHE, MemComponent::L2_CACHE,
                                       shmem_msg->getRequester(), false, address,
                                       false);
               _memory_manager->sendMsg(sender, send_shmem_msg);
            //}
            */
            

            //getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::NAK);
            LOG_PRINT("Request for Leased Cacheline from Tile(%i) queued: Address(%#llx), CurrentTime(%llu), LeasedExpTime(%llu)",
                  sender, address, current_time, iter->second);

            return;
         //}
         /*
         else  // The lease has Expired. Clean it up!
         {
            // Remove from the Lease Map
            _lease_time_map.erase(address);

            LOG_PRINT("Lease Time Expired, Removed from Lease Map: Address(%#llx), CurrentTime(%llu)", 
                  address, getShmemPerfModel()->getCurrTime().toNanosec());
               
            // Update the Lease Counters
            getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::EXPIRED);
         }
         */
      }
   }
   else if (  (shmem_msg_type == ShmemMsg::RELEASE_REQ) )
   {
      map<IntPtr, UInt64>::iterator iter = _lease_time_map.find(address);

      // Check if this cache line is leased
      if(iter != _lease_time_map.end() ){

         //if(_blocked){
            if( getShmemPerfModel()->getCurrTime() > _last_access_time ){
               _last_access_time = getShmemPerfModel()->getCurrTime();
            } 
            else {
               // Make some progress in time
               getShmemPerfModel()->incrCurrTime(Time(1));
               _last_access_time = getShmemPerfModel()->getCurrTime();

               // Update statistics counter to indicate these redundant increments
               getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::NAK);
            }
         //}

         LOG_ASSERT_ERROR(sender >= 0, "Sender(%i)", sender );
         LOG_ASSERT_ERROR(shmem_msg->getRequester() >= 0, "Requester(%i)", shmem_msg->getRequester() );
         // Resend RELEASE_NAK message to the requester of "address" via L2 Cache Home
         ShmemMsg send_shmem_msg(ShmemMsg::RELEASE_NAK, MemComponent::L1_DCACHE, MemComponent::L2_CACHE,
                                 shmem_msg->getRequester(), false, address,
                                 false);
         _memory_manager->sendMsg(sender, send_shmem_msg);
         
         
         LOG_PRINT("Core(%i) Sent ShmemMsg type(RELEASE_NAK) to Core(%i), Address(%#llx)", 
               getTileId(), sender, address);
      }
      else
      {
         LOG_PRINT("ShmemMsg type(RELEASE_REQ) dropped! Core(%i), Address(%#llx)", getTileId(), address);
      }
      return;
   }
   else if (  (shmem_msg_type == ShmemMsg::RELEASE_NAK) )
   {
      //if(_to_lease.find(address) != _to_lease.end()){
         
         // Make some progress in time
         //getShmemPerfModel()->incrCurrTime(Time(2));

         LOG_ASSERT_ERROR(sender >= 0, "Sender(%i)", sender );
         LOG_ASSERT_ERROR(getTileId() >= 0, "Requester(%i)", getTileId() );

         // Resend RELEASE_REQ message to the Owner of "address" via L2 Cache Home
         ShmemMsg send_shmem_msg(ShmemMsg::RELEASE_REQ, MemComponent::L1_DCACHE, MemComponent::L2_CACHE,
                                 getTileId(), false, address,
                                 false);
         _memory_manager->sendMsg(sender, send_shmem_msg);
         LOG_PRINT("Core(%i) Sent ShmemMsg type(RELEASE_REQ) to Core(%i), Address(%#llx)", 
               getTileId(), sender, address);
      /*
      }
      //else
      {
         LOG_PRINT("ShmemMsg type(RELEASE_NAK) dropped! Core(%i), Address(%#llx)", getTileId(), address);
      }
      */
      return;
   }

   // L2 Cache synchronization delay 
   if (sender == getTileId())
      getShmemPerfModel()->incrCurrTime(getL1Cache(shmem_msg->getReceiverMemComponent())->getSynchronizationDelay(L2_CACHE));
   else{
      getShmemPerfModel()->incrCurrTime(getL1Cache(shmem_msg->getReceiverMemComponent())->getSynchronizationDelay(NETWORK_MEMORY));
   }

   switch (shmem_msg_type)
   {
   case ShmemMsg::EX_REP:
      processExRepFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::SH_REP:
      processShRepFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::UPGRADE_REP:
      processUpgradeRepFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::INV_REQ:
      processInvReqFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::FLUSH_REQ:
      processFlushReqFromL2Cache(sender, shmem_msg);
      break;
      case ShmemMsg::WB_REQ: //aj: L2 send this message if other core needs data in S state
      processWbReqFromL2Cache(sender, shmem_msg);
      break;
   default:
      LOG_PRINT_ERROR("Unrecognized msg type: %u", shmem_msg_type);
      break;
   }

   if (  (shmem_msg_type == ShmemMsg::EX_REP)      || 
         (shmem_msg_type == ShmemMsg::SH_REP)      || 
         (shmem_msg_type == ShmemMsg::UPGRADE_REP)
      )
   {
      assert(_outstanding_shmem_msg_time <= getShmemPerfModel()->getCurrTime());
      
      // Reset the clock to the time the request left the tile is miss type is not modeled
      LOG_ASSERT_ERROR(_outstanding_shmem_msg.isModeled() == shmem_msg->isModeled(), "Request(%s), Response(%s)",
                       _outstanding_shmem_msg.isModeled() ? "MODELED" : "UNMODELED", shmem_msg->isModeled() ? "MODELED" : "UNMODELED");

      // If not modeled, set the time back to the original time message was sent out
      if (!_outstanding_shmem_msg.isModeled())
         getShmemPerfModel()->setCurrTime(_outstanding_shmem_msg_time);

      // Increment the clock by the time taken to update the L1-I/L1-D cache
      _memory_manager->incrCurrTime(shmem_msg->getReceiverMemComponent(), CachePerfModel::ACCESS_DATA_AND_TAGS);

      // There are no more outstanding memory requests
      _outstanding_shmem_msg = ShmemMsg();
     
      // Wake up the app thread and wait for it to complete one memory operation 
      _memory_manager->wakeUpAppThread();
      _memory_manager->waitForAppThread();
   }
}

void
L1CacheCntlr::processExRepFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   IntPtr address = shmem_msg->getAddress();
   Byte* data_buf = shmem_msg->getDataBuf();
   MemComponent::Type mem_component = shmem_msg->getReceiverMemComponent();
   
//   LOG_PRINT("L1CacheCntlr::processExRepFromL2Cache sender(%i) Address(%#llx) gonna insert address in M state", sender, address);


   assert(address == _outstanding_shmem_msg.getAddress());
   // Insert Cache Line in L1-I/L1-D Cache
   insertCacheLine(mem_component, address, CacheState::MODIFIED, data_buf);
}

void
L1CacheCntlr::processShRepFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   IntPtr address = shmem_msg->getAddress();
   Byte* data_buf = shmem_msg->getDataBuf();
   MemComponent::Type mem_component = shmem_msg->getReceiverMemComponent();

//   LOG_PRINT("L1CacheCntlr::processShRepFromL2Cache sender(%i) Address(%#llx) gonna insert address in S state", sender, address);

   assert(address == _outstanding_shmem_msg.getAddress());
   // Insert Cache Line in L1-I/L1-D Cache
   insertCacheLine(mem_component, address, CacheState::SHARED, data_buf);
}

void
L1CacheCntlr::processUpgradeRepFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   IntPtr address = shmem_msg->getAddress();
   LOG_ASSERT_ERROR(shmem_msg->getReceiverMemComponent() == MemComponent::L1_DCACHE,
                    "Unexpected mem component(%u)", shmem_msg->getReceiverMemComponent());

   assert(address == _outstanding_shmem_msg.getAddress());
   
   // Just change state from SHARED -> MODIFIED
   PrL1CacheLineInfo L1_cache_line_info;
   getCacheLineInfo(MemComponent::L1_DCACHE, address, &L1_cache_line_info);

   // Get cache line state
   __attribute__((unused)) CacheState::Type L1_cstate = L1_cache_line_info.getCState();
   LOG_ASSERT_ERROR(L1_cstate == CacheState::SHARED, "Address(%#lx), State(%u)", address, L1_cstate);

//   LOG_PRINT("L1CacheCntlr::processUpgradeRepFromL2Cache sender(%i) Address(%#llx) L1_CSTATE(%u) gonna set address in M state", sender, address, L1_cstate);

   
   L1_cache_line_info.setCState(CacheState::MODIFIED);

   // Set the meta-data in the L1-I/L1-D cache   
   setCacheLineInfo(MemComponent::L1_DCACHE, address, &L1_cache_line_info);
}

void
L1CacheCntlr::processInvReqFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   IntPtr address = shmem_msg->getAddress();
   MemComponent::Type mem_component = shmem_msg->getReceiverMemComponent();

   PrL1CacheLineInfo L1_cache_line_info;
   getCacheLineInfo(mem_component, address, &L1_cache_line_info);
   CacheState::Type cstate = L1_cache_line_info.getCState();

   // Update Shared Mem perf counters for access to L1-D Cache
   _memory_manager->incrCurrTime(mem_component, CachePerfModel::ACCESS_TAGS);
   
//   LOG_PRINT("L1CacheCntlr::processInvReqFromL2Cache sender(%i) Address(%#llx) CSTATE(%u)", sender, address, cstate);


   if (cstate != CacheState::INVALID)
   {
      LOG_ASSERT_ERROR(cstate == CacheState::SHARED, "cstate(%u)", cstate);

      // SHARED -> INVALID 

      // Invalidate the line in L1-D Cache
      invalidateCacheLine(mem_component, address);
      
      ShmemMsg send_shmem_msg(ShmemMsg::INV_REP, mem_component, MemComponent::L2_CACHE,
                              shmem_msg->getRequester(), shmem_msg->isReplyExpected(), address,
                              shmem_msg->isModeled());
      _memory_manager->sendMsg(sender, send_shmem_msg);
   }
   else
   {
      if (shmem_msg->isReplyExpected())
      {
         ShmemMsg send_shmem_msg(ShmemMsg::INV_REP, mem_component, MemComponent::L2_CACHE,
                                 shmem_msg->getRequester(), true, address,
                                 shmem_msg->isModeled());
         _memory_manager->sendMsg(sender, send_shmem_msg);
      }
   }
}

void
L1CacheCntlr::processFlushReqFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   assert(!shmem_msg->isReplyExpected());
   IntPtr address = shmem_msg->getAddress();
   LOG_ASSERT_ERROR(shmem_msg->getReceiverMemComponent() == MemComponent::L1_DCACHE,
                    "Unexpected mem component(%u)", shmem_msg->getReceiverMemComponent());

   PrL1CacheLineInfo L1_cache_line_info;
   getCacheLineInfo(MemComponent::L1_DCACHE, address, &L1_cache_line_info);
   CacheState::Type cstate = L1_cache_line_info.getCState();

//   LOG_PRINT("L1CacheCntlr::processFlushReqFromL2Cache sender(%i) Address(%#llx) CSTATE(%u)", sender, address, cstate);

   if (cstate != CacheState::INVALID)
   {
      // MODIFIED -> INVALID
      LOG_ASSERT_ERROR(cstate == CacheState::MODIFIED, "cstate(%u)", cstate);
      
      // Update Shared Mem perf counters for access to L1 Cache
      _memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_DATA_AND_TAGS);

      // Flush the line
      Byte data_buf[getCacheLineSize()];
      // First get the cache line to write it back
      readCacheLine(MemComponent::L1_DCACHE, address, data_buf);
   
      // Invalidate the cache line in the L1-I/L1-D cache
      invalidateCacheLine(MemComponent::L1_DCACHE, address);

      ShmemMsg send_shmem_msg(ShmemMsg::FLUSH_REP, MemComponent::L1_DCACHE, MemComponent::L2_CACHE,
                              shmem_msg->getRequester(), false, address,
                              data_buf, getCacheLineSize(),
                              shmem_msg->isModeled());
      _memory_manager->sendMsg(sender, send_shmem_msg);
   }
   else
   {
      // Update Shared Mem perf counters for access to L1-D cache tags
      _memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_TAGS);
   }
}

void
L1CacheCntlr::processWbReqFromL2Cache(tile_id_t sender, ShmemMsg* shmem_msg)
{
   assert(!shmem_msg->isReplyExpected());

   IntPtr address = shmem_msg->getAddress();
   LOG_ASSERT_ERROR(shmem_msg->getReceiverMemComponent() == MemComponent::L1_DCACHE,
                    "Unexpected mem component(%u)", shmem_msg->getReceiverMemComponent());
   // No requests can come externally to the L1-I cache since it contains
   // only instructions and no self modifying code can be handled
   // Also, no reading from a remote cache is allowed

   PrL1CacheLineInfo L1_cache_line_info;
   getCacheLineInfo(MemComponent::L1_DCACHE, address, &L1_cache_line_info);
   CacheState::Type cstate = L1_cache_line_info.getCState();

//   LOG_PRINT("L1CacheCntlr::processWbReqFromL2Cache sender(%i) Address(%#llx) CSTATE(%u)", sender, address, cstate);

   if (cstate != CacheState::INVALID)
   {
      // MODIFIED -> SHARED
      LOG_ASSERT_ERROR(cstate == CacheState::MODIFIED, "cstate(%u)", cstate);

      // Update shared memory performance counters for access to L1 Cache
      _memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_DATA_AND_TAGS);

      // Write-Back the line
      Byte data_buf[getCacheLineSize()];
      // Read the cache line into a local buffer
      readCacheLine(MemComponent::L1_DCACHE, address, data_buf);
      // set the state to SHARED
      L1_cache_line_info.setCState(CacheState::SHARED);

      // Write-back the new state in the L1 cache
      setCacheLineInfo(MemComponent::L1_DCACHE, address, &L1_cache_line_info);

      ShmemMsg send_shmem_msg(ShmemMsg::WB_REP, MemComponent::L1_DCACHE, MemComponent::L2_CACHE,
                              shmem_msg->getRequester(), false, address,
                              data_buf, getCacheLineSize(),
                              shmem_msg->isModeled());
      _memory_manager->sendMsg(sender, send_shmem_msg);
   }
   else
   {
      // Update Shared Mem perf counters for access to the L1-D Cache
      _memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_TAGS);
   }
}

Cache*
L1CacheCntlr::getL1Cache(MemComponent::Type mem_component)
{
   switch(mem_component)
   {
   case MemComponent::L1_ICACHE:
      return _L1_icache;

   case MemComponent::L1_DCACHE:
      return _L1_dcache;

   default:
      LOG_PRINT_ERROR("Unrecognized Memory Component(%u)", mem_component);
      return NULL;
   }
}

tile_id_t
L1CacheCntlr::getL2CacheHome(IntPtr address)
{
   return _L2_cache_home_lookup->getHome(address);
}

ShmemMsg::Type
L1CacheCntlr::getShmemMsgType(Core::mem_op_t mem_op_type)
{
   switch(mem_op_type)
   {
   case Core::READ:
      return ShmemMsg::SH_REQ;

   case Core::READ_EX:
   case Core::WRITE:
      return ShmemMsg::EX_REQ;

   default:
      LOG_PRINT_ERROR("Unsupported Mem Op Type(%u)", mem_op_type);
      return ShmemMsg::INVALID_MSG_TYPE;
   }
}

tile_id_t
L1CacheCntlr::getTileId()
{
   return _memory_manager->getTile()->getId();
}

UInt32
L1CacheCntlr::getCacheLineSize()
{ 
   return _memory_manager->getCacheLineSize();
}
 
ShmemPerfModel*
L1CacheCntlr::getShmemPerfModel()
{ 
   return _memory_manager->getShmemPerfModel();
}

bool 
L1CacheCntlr::processLeaseReqFromCore(IntPtr address, int lease_time)
{
   // Status is only used by Release requests
   // Status values:
   //    1. False: Lease was timed out
   //    2. True:  Lease was still valid and voluntarily released
   bool status = true;

   if(lease_time != 0){
      //printf("%s: Address(%lx), Lease_Time(%i) \n", (lease_time > 0)? "LEASE": "RELEASE", address, lease_time); fflush(stdout);
      bool lease = (lease_time > 0)? true : false;

      if(lease){
         LOG_ASSERT_ERROR( address , "Cannot lease NULL Address. Called Address(%#llx)", address);

         // Mark this request as "To be Leased"
         // Lease time will start after actual EX request is served
         _to_lease[address] = lease_time;
         LOG_PRINT("Marked for Lease: Address(%#llx), CurrentTime(%llu)", 
               address, getShmemPerfModel()->getCurrTime().toNanosec());
      }
      else
      {
         do{
            // If Address is NOT NULL then only release the passed address, otherwise Release ALL
            IntPtr curr_addr = (!address)? _lease_time_map.begin()->first : address;
            
            // Code for Release
            LOG_ASSERT_ERROR(_to_lease.find(curr_addr) == _to_lease.end(), 
                  "Address(%#llx) must be first leased before calling release!", curr_addr);
           
            if(_lease_time_map.find(curr_addr) != _lease_time_map.end()){

               // Remove from the Lease Map
               _lease_time_map.erase(curr_addr);

               LOG_PRINT("Removed from Lease Map: Address(%#llx), CurrentTime(%llu)", 
                     curr_addr, getShmemPerfModel()->getCurrTime().toNanosec());

               // Update the Lease Counters
               getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::RELEASE);
               
               if(_L1_outstanding_req_queue.find(curr_addr) != _L1_outstanding_req_queue.end()){
                  
                  LOG_PRINT("Sending message to serve outstanding request: Address(%#llx), CurrentTime(%llu)", 
                        curr_addr, getShmemPerfModel()->getCurrTime().toNanosec());
                  
                  _outstanding_reqs = true;

                  // Send out a message to the network thread to process any outstanding requests
                  ShmemMsg shmem_msg(ShmemMsg::RELEASE_REP, MemComponent::CORE, MemComponent::L1_DCACHE,
                                     getTileId(), false, curr_addr,
                                     false, false);
                  _memory_manager->sendMsg(getTileId(), shmem_msg);
                  
                  // Wait for the sim thread
                  _memory_manager->waitForSimThread();

                  // Wakeup Sim Thread
                  _memory_manager->wakeUpSimThread();
               }
            }
            else
            {
               // Lease was Expired
               // Update the status to FALSE
               status = false;
            }

         } while ( !address && !_lease_time_map.empty() );
      }
   }

   return status;
}

void
L1CacheCntlr::addToLeaseMap(IntPtr address, int lease_time)
{
   UInt64 lease_expiration_time = getShmemPerfModel()->getCurrTime().toNanosec();
 
   if(lease_time !=0)
   {   
      // Lease
      if(lease_time > 0){
         /*
         LOG_ASSERT_ERROR(_lease_time_map.find(address) == _lease_time_map.end(), 
               "Address(%#llx) must be first released before leasing again!", address);
         */
         lease_expiration_time += lease_time;
         _lease_time_map[address] = lease_expiration_time;
         LOG_ASSERT_ERROR(_lease_time_map.find(address) != _lease_time_map.end(), 
               "Failed to add to lease map: Address(%#llx), Core(%i)", address, getTileId() );
         LOG_PRINT("Added to Lease Map: Address(%#llx), LeaseExpTime(%llu)", address, lease_expiration_time);
      
         /*
         map<IntPtr, UInt64>::iterator iter;
         printf("Lease_Time_Map Core(%i)\n", getTileId());
         for(iter = _lease_time_map.begin(); iter != _lease_time_map.end(); iter++){
            printf("Address(%lx), LeaseExp(%lu)\n", iter->first, iter->second);
            fflush(stdout);
         }
         */
      }   
   }   
}

void 
L1CacheCntlr::cleanupExpiredLeases()
{
   if(!_lease_time_map.empty())
   {
      // Try to acquire the lock
      //if(_memory_manager->tryLock())
      _memory_manager->acquireLock();
      {
         // Pick the first entry
         map<IntPtr, UInt64>::iterator iter = _lease_time_map.begin();
         
         while( iter != _lease_time_map.end()){

            UInt64 current_time = getShmemPerfModel()->getCurrTime().toNanosec();
            IntPtr address  = iter->first;
            UInt64 exp_time = iter ->second; 
            
            // Move the iterator to Next element before removing the element
            iter++;
            
            // TODO: Fix it
            // Don't Remove it if it is part of an ongoing group lease
            /*
            if(_group_lease.find( address) != _group_lease.end() ){
               LOG_PRINT_WARNING("Lease of Address(%#llx) has expired during ongoing Group Lease! ", address);
               continue;
            }
            */

            // If lease time has expired
            if(current_time >= exp_time){

               // TODO: Fix it
               // Don't Remove it if it is part of an ongoing group lease
               LOG_ASSERT_ERROR(_group_lease.find( address) == _group_lease.end(), "Lease of Address(%#llx) has expired during ongoing Group Lease! ", address);
               
               // Remove from the Lease Map
               _lease_time_map.erase(address);

               LOG_PRINT("Lease Time Expired, Removed from Lease Map: Address(%#llx), CurrentTime(%llu)", 
                     address, getShmemPerfModel()->getCurrTime().toNanosec());
                  
               // Update the Lease Stats Counters
               getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::EXPIRED);

               // Check for outstanding requests
               if(_L1_outstanding_req_queue.find(address) != _L1_outstanding_req_queue.end()){
                  
                  _outstanding_reqs = true;

                  // Process the pending requests to this Leased Cacheline
                  processL2RequestToLeasedLine(address);
               }
               // Start over
               iter = _lease_time_map.begin();
            }
         }

         
         // Release the Lock
         _memory_manager->releaseLock();
      }
   }
}

void
L1CacheCntlr::processL2RequestToLeasedLine(IntPtr address)
{
   // Check for any Outstanding requests to Leased Lines 
   if(!_outstanding_reqs){
      return;
   }

   // Make sure the request is present in the queue
   assert(_L1_outstanding_req_queue.find(address) !=  _L1_outstanding_req_queue.end() );

   // Get the request
   map<IntPtr, ShmemReq*>::iterator req = _L1_outstanding_req_queue.find(address);
   ShmemReq* shmem_req = req->second;
   tile_id_t sender = shmem_req->getSender();
   ShmemMsg* shmem_msg = shmem_req->getShmemMsg();
   ShmemMsg::Type shmem_msg_type = shmem_msg->getType();

   // Make sure this Cache line has been RELEASED
   assert(_lease_time_map.find(address) == _lease_time_map.end());

   //  Now Process the request
   //
   // L2 Cache synchronization delay 
   if (sender == getTileId())
      getShmemPerfModel()->incrCurrTime(getL1Cache(shmem_msg->getReceiverMemComponent())->getSynchronizationDelay(L2_CACHE));
   else{
      getShmemPerfModel()->incrCurrTime(getL1Cache(shmem_msg->getReceiverMemComponent())->getSynchronizationDelay(NETWORK_MEMORY));
   }

   switch (shmem_msg_type)
   {
   case ShmemMsg::INV_REQ:
      processInvReqFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::FLUSH_REQ:
      processFlushReqFromL2Cache(sender, shmem_msg);
      break;
   case ShmemMsg::WB_REQ:
      processWbReqFromL2Cache(sender, shmem_msg);
      break;
   default:
      LOG_PRINT_ERROR("Unrecognized msg type: %u", shmem_msg_type);
      break;
   }

   // Dequeue this request
   _L1_outstanding_req_queue.erase(address);
   //printf("processL2RequestToLeasedLine(): Dequeued request for Address(%lx) Core(%i)\n", 
     //    address, getTileId());  fflush(stdout);

   // Update the Lease Counters
   if( shmem_msg_type == ShmemMsg::WB_REQ )
      getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::OUTSTANDING_RD_REQ);
   else
      getL1Cache(MemComponent::L1_DCACHE)->updateLeaseOpsCounters(Cache::OUTSTANDING_WR_REQ);
   
   LOG_PRINT("Finished serving outstanding request for Leased Cacheline: Address(%#llx) from Core(%i)", 
         address, sender);
   LOG_ASSERT_ERROR(_L1_outstanding_req_queue.find(address)==_L1_outstanding_req_queue.end(), 
         "Request for Address(%#llx) must be dequeued! Core(%i)", address, getTileId());

   // Clear the Outstanding Reqs Flag
   if( _L1_outstanding_req_queue.empty() ){
      _outstanding_reqs =false;
   }
}

bool
L1CacheCntlr::processWatchsetOpFromCore(IntPtr address, int operation)
{
   bool status = false;
   switch (operation)
   {
   case 0:  // Adding to Watchset
      /*
      printf("%i:To Add: %#lx in _to_watch: ", getTileId(), address); 
      print_set(_to_watch);
      printf("%i:Current _watchset: ", getTileId()); 
      print_set(_watchset);
      */
      
#ifdef DEBUG_SIM   
      // This address should not be already in to_be_watched set
      LOG_ASSERT_ERROR(_to_watch.find(address) == _to_watch.end(), "Attempted to Watch Cacheline while it is already queued to be added to Watchset! Address(%#lx)", address);   
      // This address should not be already watched
      LOG_ASSERT_ERROR(_watchset.find(address) == _watchset.end(), "Attempted to Watch Cacheline which is already in the Watchset! Address(%#lx). "
              "Could be due to adding address again without removing it first or two addresses sharing same cacheline.", address);   
#endif
      
#ifdef DEBUG_SIM   
      assert(_watchset.find(address) == _watchset.end());   // This address must not exist in watchset
#endif      
      LOG_PRINT("Adding to _to_watch set: Address(%#llx), Core(%i)", address, getTileId());
      _to_watch.insert(address);

#ifdef DEBUG_SIM   
      assert(_to_watch.find(address) != _to_watch.end());   // This address must exist in watchset
#endif
      status = true;
      /*
      printf("%i:Added. Current _to_watch: ", getTileId()); 
      print_set(_to_watch);
      */
      break;

   case 1: // Remove from watchset
      /*
      printf("%i:Removing %#lx: _watchset set: ", getTileId(), address); 
      print_set(_watchset);
      */
#ifdef DEBUG_SIM   
      LOG_ASSERT_ERROR(_to_watch.find(address) == _to_watch.end(), "Address (%#lx) cannot be removed as it still needs to be added to watchset! Read it first!"); 
      LOG_ASSERT_ERROR(_watchset.find(address) != _watchset.end(), "Attempted to Remove Cacheline which does not exist in the Watchset! Address(%#lx)", address);
#endif

#ifdef DEBUG_SIM         
      assert(_watchset.find(address) != _watchset.end());   // This address must exist in watchset
#endif
      
      LOG_PRINT("Removing from Watchset: Address(%#llx), Core(%i)", address, getTileId());
      _watchset.erase(address);
#ifdef DEBUG_SIM         
      assert(_watchset.find(address) == _watchset.end());   // This address should not be in watchset now
#endif
      // read the status
      status = (_downgraded_watchset.find(address) == _downgraded_watchset.end()) ? true : false;
      _downgraded_watchset.erase(address);
      
      // Update Shared Mem perf counters for access to L1 Cache
      //_memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_TAGS);
      
      // If really downgraded then add 1 to the counter
      if(!status)
         getL1Cache(MemComponent::L1_DCACHE)->updateWatchsetOpsCounters(Cache::DOWNGRADE, 1);
      
      
      /*      
      printf("%i:Removed: _watchset set: ", getTileId()); 
      print_set(_watchset);
      */    
      break;

   case 2: // Validate Watchset
      
      // Debugging
//      printf("%i:Validating: _to_watch set: ", getTileId()); 
//      print_set(_to_watch);
//      print_set(_watchset);
      
      LOG_PRINT("Validating Watchset: Core(%i)", getTileId());
#ifdef DEBUG_SIM   
      LOG_ASSERT_ERROR( _to_watch.empty(), "Watchset Validation cannot be called while some data still needs to be added to watchset! Please make read access to the watcset data before calling watchset validation!" );  
#endif

      //aj marking bit stack debug begin
      if(!_watchset.empty())
      {
         status = _downgraded_watchset.empty();
         if(!status)
         {
            // Add all the downgraded lines to stats
            getL1Cache(MemComponent::L1_DCACHE)->updateWatchsetOpsCounters(Cache::DOWNGRADE, _downgraded_watchset.size() );
//            printf("ValidateWatchset: Core(%i), _downgraded_watchset.size(%lu) before clearing\n", getTileId(), _downgraded_watchset.size());
            _downgraded_watchset.clear();
         }

//         printf("ValidateWatchset: Core(%i), _watchset.size(%lu) before clearing\n", getTileId(), _watchset.size());
         // Reset the watchset
         _watchset.clear();
      }

      //aj marking bit stack debug end

      // Update Shared Mem perf counters for access to L1 Cache
      //_memory_manager->incrCurrTime(MemComponent::L1_DCACHE, CachePerfModel::ACCESS_TAGS);
      
      break;
      
    case 3:
        LOG_PRINT("RemoveAllFromWatchset: Core(%i)", getTileId());
//        printf("processWatchsetOpFromCore:: RemoveAllFromWatchset Core(%i)\n", getTileId());
//        fflush(stdout);

#ifdef DEBUG_SIM           
        LOG_ASSERT_ERROR(_to_watch.empty(), "RemoveAllFromWatchset cannot be called while some data still needs to be added to watchset! Please make read access to the watchset data before calling watchset validation!");
        LOG_ASSERT_ERROR(!_watchset.empty(), "_watchset is empty nothing to remove this thread or other may have emptied the _watchset");
#endif
        if( !_watchset.empty() )
        {
            LOG_PRINT("RemoveAllFromWatchset: Core(%i), _watchset.size(%i) before clearing", getTileId(), _watchset.size());
//            printf("RemoveAllFromWatchset: Core(%i), _watchset.size(%lu) before clearing\n", getTileId(), _watchset.size());
            status = _downgraded_watchset.empty();
            if(!status) //if _downgraded_watchset not empty empty it.
            {
                // Add all the downgraded lines to stats
                getL1Cache(MemComponent::L1_DCACHE)->updateWatchsetOpsCounters(Cache::DOWNGRADE, _downgraded_watchset.size() );
                LOG_PRINT("RemoveAllFromWatchset: Core(%i), _downgraded_watchset.size(%i) before clearing", getTileId(), _downgraded_watchset.size());
//                printf("RemoveAllFromWatchset: Core(%i), _downgraded_watchset.size(%lu) before clearing\n", getTileId(), _downgraded_watchset.size());

                _downgraded_watchset.clear();
            }
            // Reset the watchset
            _watchset.clear();
        }
#ifdef DEBUG_SIM   
        LOG_ASSERT_ERROR(_watchset.empty(), "_watchset should be empty at this point");
        LOG_ASSERT_ERROR(_downgraded_watchset.empty(), "_downgraded_watchset should be empty at this point");
        LOG_ASSERT_ERROR(_to_watch.empty(), "_to_watch should be empty at this point");
#endif
//        printf("RemoveAllFromWatchset: Core(%i), _watchset.size(%lu) after clearing\n", getTileId(), _watchset.size());
//        printf("processWatchsetOpFromCore:: RemoveAllFromWatchset end\n");

        break;
    
    case 4:
         //assuming this sectiojn is run atomically. But I am not sure. May be this is cause of failure of multi thread test. FIXME.
         LOG_PRINT("ValidateAddressInWatchset: Core(%i)", getTileId());
#ifdef DEBUG_SIM            
         LOG_ASSERT_ERROR(_to_watch.empty(), "_to_watch should be empty?!");
#endif

/*
 * Single address based validation. Note previously tagged address during a operation may be untagged. does that impact correctness?
 * Why with stack_asm_hmr marking vwrote fails after successfull unlink in POP with concurrent PUSH?
 * POP has both top and firstnode in downgraded set thus fails marking vwrite(). What in push causes that? No idea.
 */
//         if (_watchset.find(address) != _watchset.end())
//         {
//            status = !(_downgraded_watchset.find(address) != _downgraded_watchset.end());
//         }
         

//         if (0 == getTileId())
//         {
//            printf("\n[ValidateAddressInWatchset: Core(%i) status(%i)\n", getTileId(), status);
//            print_set(_watchset);
//            print_set(_downgraded_watchset);
//            printf("]ValidateAddressInWatchset: Core(%i) status(%i)\n", getTileId(), status);
//            fflush(stdout);
//         }


//      LOG_PRINT(" Begin validatingWatchset empty based");
      LOG_ASSERT_ERROR( _to_watch.empty(), "Watchset Validation cannot be called while some data still needs to be added to watchset! Please make read access to the watcset data before calling watchset validation!" );  
/*
 * Fullwatchset validation all the addresses tagged so far in the operation will be validated.
 */      
      if(!_watchset.empty())
      {
         status = _downgraded_watchset.empty();
         if(!status)
         {
//            printf("\n[ValidateAddressInWatchset: Core(%i) status(%i)\n", getTileId(), status);
//            print_set(_watchset);
//            print_set(_downgraded_watchset);
//            printf("]ValidateAddressInWatchset: Core(%i) status(%i)\n", getTileId(), status);
//            fflush(stdout);

            // Add all the downgraded lines to stats
            getL1Cache(MemComponent::L1_DCACHE)->updateWatchsetOpsCounters(Cache::DOWNGRADE, _downgraded_watchset.size() );
         }
      }
//      LOG_PRINT("End validatingWatchset empty based status=%d", status);

      break;
#ifdef TRVOP
    case 8:  // TRV OP. IsAddTag Needed.
      /*
      printf("%i:To Add: %#lx in _to_watch: ", getTileId(), address); 
      print_set(_to_watch);
      printf("%i:Current _watchset: ", getTileId()); 
      print_set(_watchset);
      */
      // If address (cacheline) is not in watchset and also not in downgraded_watchset then we need to addTag
      // Otherwise we do not need to addTag as it was already added and may/may not have been invalidated since then.
      status = ( (_downgraded_watchset.find(address) == _downgraded_watchset.end()) &&
              (_watchset.find(address) == _watchset.end()) ) ;      
      /*
      printf("%i:Added. Current _to_watch: ", getTileId()); 
      print_set(_to_watch);
      */
      break;
#endif      
   default:
      LOG_ASSERT_ERROR(false, "Unsupported Watchset Operation!");
      break;
   }
   return status;
}

}
