#include <cstdlib>
#include <cassert>

#include "pin_memory_manager.h"

PinMemoryManager::PinMemoryManager(Core* core):
   m_core(core)
{
   // Allocate scratchpads (aligned at 4 * sizeof (void*) to correctly handle
   // memory access instructions that require addresses to be aligned such as
   // MOVDQA
   for (unsigned int i = 0; i < NUM_ACCESS_TYPES; i++)
   {
      __attribute__((unused)) int status = posix_memalign ((void**) &m_scratchpad[i], 4 * sizeof (void*), m_scratchpad_size);
      assert (status == 0);
   }
}

PinMemoryManager::~PinMemoryManager()
{}

#ifdef USEASMADDTAG
carbon_reg_t // is typedeffed as unsigned int
PinMemoryManager::redirectDummyMemOp (bool has_repandmov_prefix, IntPtr tgt_ea, IntPtr size, UInt32 op_num, bool is_read)
{
   assert (op_num < NUM_ACCESS_TYPES);
   char *scratchpad = m_scratchpad [op_num];
   if (is_read)
   {
        Core::mem_op_t mem_op_type;
        Core::lock_signal_t lock_signal;

        // When we DO NOT have a LOCK prefix, we do a normal READ
        mem_op_type = Core::READ;
        lock_signal = Core::NONE;
        
        m_core->accessMemory(lock_signal, mem_op_type, tgt_ea, scratchpad, size, true);
        m_core->handleWatchsetOperation(tgt_ea, size, 1); //removeFromwatchset, operation num = 1.        
   } //if (is_read)
   return (carbon_reg_t) scratchpad;
}
#endif

carbon_reg_t // is typedeffed as unsigned int
PinMemoryManager::redirectMemOp (bool has_lock_prefix, bool has_repandmov_prefix, IntPtr tgt_ea, IntPtr size, UInt32 op_num, bool is_read)
{
   assert (op_num < NUM_ACCESS_TYPES);
   char *scratchpad = m_scratchpad [op_num];
   if (is_read)
   {
      Core::mem_op_t mem_op_type;
      Core::lock_signal_t lock_signal;

      if (has_lock_prefix)
      {
         // When we have a LOCK prefix, we do an exclusive READ
         mem_op_type = Core::READ_EX;
         lock_signal = Core::LOCK;
      }
      else
      {
         // When we DO NOT have a LOCK prefix, we do a normal READ
         mem_op_type = Core::READ;
         lock_signal = Core::NONE;
      }
#ifdef TRVOP
      if (has_repandmov_prefix)
        m_core->addToWatchsetIfNeeded(tgt_ea, size/*NOTE SIZE DOESNT MATTER to add to watchset. ASSUMPTION: always les than cachelinesize*/);
#endif
      
      m_core->accessMemory(lock_signal, mem_op_type, tgt_ea, scratchpad, size, true);
      if (has_repandmov_prefix)
      {
         //acquires and release core lock
         bool validation_status = m_core->handleWatchsetOperation(tgt_ea, size/*FIXME:is it data_size?*/, 4); //call watchsetValidateAddress
#ifdef DEBUG_SIM            
         LOG_ASSERT_ERROR( *((carbon_reg_t*)scratchpad) != 0xffffffff
                 , "vread should not have 0xffffffff as a valid return value, ADDRINT and IntPtr are of type ulongint. But App downcasts that to uint");
#endif
         if (!validation_status)
         {
            // If validation fails then use next scratchpad[1] to write a special value (0xffffffff)
            // and return this (scratchpad[1]) instead of scratchpad[0] that has been read.
            char* scratchpad1 = m_scratchpad[1];
            *((carbon_reg_t*)scratchpad1) = 0xffffffff; 

            return (carbon_reg_t)scratchpad1; 
         }
      }
   } //if (is_read)
   return (carbon_reg_t) scratchpad;
}

bool 
PinMemoryManager::completeMemWrite (bool has_lock_prefix, bool has_repandmov_prefix, IntPtr tgt_ea, IntPtr size, UInt32 op_num)
{
   char *scratchpad = m_scratchpad [op_num];
   
   if (has_repandmov_prefix) //vwrite case
   {
#ifdef DEBUG_SIM   
      assert (op_num == 0);
#endif
      // call handleWatchsetMemOP
      bool validation_status = m_core->handleWatchsetMemoryOperation(
              tgt_ea, scratchpad, size, 6, true
              );

      return validation_status;
   }
   else // case: write ops that are not vwrite
   {
      Core::lock_signal_t lock_signal = (has_lock_prefix) ? Core::UNLOCK : Core::NONE;
      m_core->accessMemory (lock_signal, Core::WRITE, tgt_ea, scratchpad, size, true);
      return 0;
   }
}

#ifdef USEASMADDTAG
bool 
PinMemoryManager::completeDummyMemWrite (bool has_lock_prefix, bool has_repandmov_prefix, IntPtr tgt_ea, IntPtr size, UInt32 op_num)
{
    char *scratchpad = m_scratchpad [op_num];
    Core::lock_signal_t lock_signal = (has_lock_prefix) ? Core::UNLOCK : Core::NONE;
    
    //acquires and release core lock
   m_core->handleWatchsetOperation(tgt_ea, size, 1); //removeFromwatchset, operation num = 1.
   
    m_core->accessMemory (lock_signal, Core::WRITE, tgt_ea, scratchpad, size, true);
    return 0;
}
#endif

carbon_reg_t 
PinMemoryManager::redirectPushf ( IntPtr tgt_esp, IntPtr size )
{
   m_saved_esp = tgt_esp;
   return ((carbon_reg_t) m_scratchpad [0]) + size;
}

carbon_reg_t 
PinMemoryManager::completePushf ( IntPtr esp, IntPtr size )
{
   m_saved_esp -= size;
   completeMemWrite (false, false, (IntPtr) m_saved_esp, size, 0);
   return m_saved_esp;
}

carbon_reg_t 
PinMemoryManager::redirectPopf (IntPtr tgt_esp, IntPtr size)
{
   m_saved_esp = tgt_esp;
   // aj addded second param as false.
   return redirectMemOp (false, false, m_saved_esp, size, 0, true);
}

carbon_reg_t 
PinMemoryManager::completePopf (IntPtr esp, IntPtr size)
{
   return (m_saved_esp + size);
}
