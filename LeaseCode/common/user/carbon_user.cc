#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <pthread.h>
#include "simulator.h"
#include "thread_manager.h"
#include "tile_manager.h"
#include "tile.h"
#include "core_model.h"
#include "config_file.hpp"
#include "handle_args.h"
#include "carbon_user.h"
#include "thread_support_private.h"

static config::ConfigFile cfg;

tile_id_t CarbonGetTileId()
{
   return Sim()->getTileManager()->getCurrentTileID();
}

int CarbonStartSim(int argc, char **argv)
{
   string_vec args;

   // Set the default config path if it isn't 
   // overwritten on the command line.
   std::string config_path = "carbon_sim.cfg";

   // Parse the arguments that are relative
   // to the config file changes as well
   // as extracting the config_path
   parse_args(args, config_path, argc, argv);

   cfg.load(config_path);
   handle_args(args, cfg);

   Simulator::setConfig(&cfg);

   Simulator::allocate();
   Sim()->start();

   if (Config::getSingleton()->getCurrentProcessNum() == 0)
   {
      // Main process
      Sim()->getTileManager()->initializeThread(Tile::getMainCoreId(0));
  
      if (Sim()->getConfig()->getSimulationMode() == Config::FULL)
         CarbonSpawnThreadSpawner();

      LOG_PRINT("Returning to main()...");
      return 0;
   }
   else
   {
      LOG_PRINT("Replacing main()...");

      assert(Sim()->getConfig()->getSimulationMode() == Config::FULL);
      CarbonThreadSpawner (NULL);

      // Not main process
      while (!Sim()->finished())
         usleep(100);

      CarbonStopSim();

      // Otherwise we will run main ...
      exit(0);
   }
}

void CarbonStopSim()
{
   Simulator::release();
}

UInt64 CarbonGetTime()
{
   Core* core = Sim()->getTileManager()->getCurrentCore();
   UInt64 time = core->getModel()->getCurrTime().toNanosec();

   return time;
}

/* ===============================================================
// Lease/Release routines
// =============================================================== */
void* CarbonRequestLease(void* address, int size, int lease)
{
   // Graphite models this memory location as a Leased Load
   return address;
}
void CarbonRequestGroupLease(int arg_count, int lease_time, ...)
{
   return;
}

void CarbonRequestGroupLease_2(void* addr1, void* addr2, int lease_time){
   return;
}

bool CarbonReleaseLease(void* address, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

bool CarbonReleaseAllLease()
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

// ****************************************************************
// WatchSet Rutines
// ****************************************************************

void CarbonAddToWatchset(void* address, int size)
{
   return;
}

bool CarbonRemoveFromWatchset(void* address, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

bool CarbonRemoveFromWatchsetOpt(void* address)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

bool CarbonValidateWatchset()
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

bool CarbonRemoveAllFromWatchset()
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;    
}

bool CarbonValidateAddressInWatchset(void* address, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

// void* val is expected to out buffer where the value will be read into from the address.
// Currently, even though simulator side correctly writes the value at address init corectly.
// But that value is not reflected in app side. So this memop looks like a NOOP.
// Untill the day I understand the routine relacement API to do mem ops corectly I am using the alternative API.
bool CarbonValidateAndRead(void* address, void* val, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

// If returns ULONG_MAX that means  returned false.
// Any value < 0xffffffff (ULONG_MAX) means returned true and then the value could be used in app as a address to a node 
// or simple ulong int value if its a counter app.

IntPtr CarbonValidateAndReadTemp(void* address, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

//IntPtr is an fixed type value (unsigned long int).
bool CarbonValidateAndWrite(void* address, IntPtr val, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

bool CarbonValidateAndLock(void* address, IntPtr val, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

// In order to makeup for extra instruction generated by hmr API
// we will use CarbonRead to read mem addresses for reading addresses in other algos as well
IntPtr CarbonRead(void* address, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}

SInt32 CarbonGetVOPStatus()
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}
// In order to makeup for extra instruction generated by hmr API
// we will use CarbonWrite to write mem addresses for write ops other algos as well
//IntPtr is an fixed type value (unsigned long int).
bool CarbonWrite(void* address, IntPtr val, int size)
{
   // The actual return value comes from Graphite's Replacement Routine
   // The following return value is just to prevent the compiler from generating compilation error
   return false;
}