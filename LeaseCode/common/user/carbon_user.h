#ifndef CARBON_USER_H
#define CARBON_USER_H


#ifdef __cplusplus
extern "C"
{
#endif

#include <stdarg.h>
#include <pthread.h>
#include "fixed_types.h"
#include "capi.h"
#include "sync_api.h"
#include "thread_support.h"
#include "performance_counter_support.h"
#include "dvfs.h"

// Start the simulation (allocate the simulation models and create the simulation threads)
SInt32 CarbonStartSim(int argc, char **argv);
// Stop the simulation (join the simulation threads and release the simulation models)
void CarbonStopSim();
// Gets the tile-ID on which the thread is running
tile_id_t CarbonGetTileId();
// Get the current time (in nanoseconds)
UInt64 CarbonGetTime();


/* ======================================================================= */
/* CarbonRequestLease() and CarbonReleaseLease() functions MUST not be optimized by the compiler.
 * This is achieved by using the attribute optimize("O0") with the function definition.
 *  =========================================================================== */
// Request a Leased Load 
void* __attribute__((optimize("O0"))) CarbonRequestLease(void* address, int size, int lease);
void __attribute__((optimize("O0"))) CarbonRequestGroupLease_2(void* addr1, void* addr2, int lease_time);
void __attribute__((optimize("O0"))) CarbonRequestGroupLease(int arg_count, int lease_time, ...);

// Release a Leased Load
// Return value represents:
//    TRUE: The lease was still valid when Release function was called
//    FALSE: The lease timed out before Release function was called
bool  __attribute__((optimize("O0"))) CarbonReleaseLease(void* address, int size);
bool  __attribute__((optimize("O0"))) CarbonReleaseAllLease();

// Watchset
void /*__attribute__((optimize("O0")))*/ CarbonAddToWatchset(void* address, int size);
bool /*__attribute__((optimize("O0")))*/ CarbonRemoveFromWatchset(void* address, int size);
bool /*__attribute__((optimize("O0")))*/ CarbonRemoveFromWatchsetOpt(void* address);
bool /*__attribute__((optimize("O0")))*/ CarbonValidateWatchset();

bool /*__attribute__((optimize("O0")))*/ CarbonRemoveAllFromWatchset();
bool /*__attribute__((optimize("O0")))*/ CarbonValidateAddressInWatchset(void* address, int size);
bool /*__attribute__((optimize("O0")))*/ CarbonValidateAndRead(void* address, void* val, int size);
IntPtr /*__attribute__((optimize("O0")))*/ CarbonValidateAndReadTemp(void* address, int size);
bool /*__attribute__((optimize("O0")))*/ CarbonValidateAndWrite(void* address, IntPtr val, int size);
bool __attribute__((optimize("O0"))) CarbonValidateAndLock(void* address, IntPtr val, int size);

IntPtr /*__attribute__((optimize("O0")))*/ CarbonRead(void* address, int size);
bool /*__attribute__((optimize("O0")))*/ CarbonWrite(void* address, IntPtr val, int size);

SInt32 /*__attribute__((optimize("O0")))*/ CarbonGetVOPStatus();

#ifdef __cplusplus
}
#endif

#endif // CARBON_USER_H
