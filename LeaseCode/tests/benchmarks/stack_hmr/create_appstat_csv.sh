#!/bin/bash
#run this to output of run_exp.sh into csv format.
# example usage: ./create_csv.sh >> myfile.csv


out_dir="./exp_output"
echo "algo,threads,throughput
for stepfile in `ls $out_dir/stack*`
do
alg=`cat $stepfile | grep algo_type | cut -d"=" -f2| tr -d " "`
threads=`cat $stepfile | grep Threads | head -1 |cut -d"=" -f2| tr -d " "`
throughput=`cat $stepfile | grep Experiment_Throughput | cut -d"=" -f2| tr -d " "`
#maxkey=`cat $stepfile | grep Max_Keyrange | cut -d"=" -f2| tr -d " "`
#ins=`cat $stepfile | grep -e "ins" | head -1 |cut -d"=" -f2| tr -d " "`
#del=`cat $stepfile | grep -e del | head -1 |cut -d"=" -f2| tr -d " "`
#duration_ms=`cat $stepfile | grep Max_Duration | cut -d"=" -f2| tr -d " "`
#ds_size=`cat $stepfile | grep experiment_ds_size | cut -d"=" -f2| tr -d " "`
#epoch=`cat $stepfile | grep Epoch | cut -d"=" -f2| tr -d " "`
#tot_retired=`cat $stepfile | grep total_retired | cut -d"=" -f2| tr -d " "`
#tot_allocated=`cat $stepfile | grep total_allocated | cut -d"=" -f2| tr -d " "`
#tot_freed=`cat $stepfile | grep total_freed | cut -d"=" -f2| tr -d " "`
#walltime=`cat $stepfile | grep time_cmd_walltime | cut -d"=" -f2| tr -d " "`
#walltime=${walltime%.*}
#max_res_mem=`cat $stepfile | grep max_res_mem | cut -d"=" -f2| tr -d " "`
#tot_retries=`cat $stepfile | grep total_retries | cut -d"=" -f2| tr -d " "`
#
#finished=`cat $stepfile | grep "key_checksum validation:PASS"| head -1`
##echo $finished
#if [[ "$finished" == "key_checksum validation:PASS" ]]; then
#    succ=true
#else
#    succ=false
#fi

echo $alg,$((threads)),$((throughput))
done