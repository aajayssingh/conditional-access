#ifndef LOCK_H
#define LOCK_H
static void acquireLock(volatile unsigned int* lock)
{
    unsigned long long retrycount = 0;
    while(true)
    {

        if(*lock)
        {
            continue;
        }
//        if ((retrycount++)%100 == 0)
//            printf("retrycount=%llu ", retrycount);        
        
        if (__sync_bool_compare_and_swap(lock, 0, 1))
        {
            return;
        }
    }
}

static bool tryAcquireLock(volatile unsigned int* lock, const unsigned int tid)
{
#ifdef VLOCK
//    while(true)
//    {
//
////        if(*lock)
////        {
////            continue;
////        }
//
////        bool write_status = CarbonValidateAndWrite((void*)lock, (unsigned int long)1, sizeof(unsigned int));
//        volatile unsigned int long lock_val = 1;
//        bool write_status = CarbonValidateAndLock((void*)lock, lock_val, sizeof(unsigned int));
//        printf("tid(%u) tryAcquireLock: write res=%lu, *lock=%u\n", tid, write_status, *lock); 
//
//        if(!write_status)
//        {
//            //need to untag so dont retry here just return and attempt locking again.
////            printf("tid(%u) tryAcquireLock: write res=%lu, *lock=%u\n", tid, write_status, *lock); 
//            return false;
//        }
//        return true;
//    }
#endif
    
         unsigned int long read_val = CarbonValidateAndReadTemp((void*)lock, sizeof(unsigned int));
//         printf("tid=%d, read_val(*lock)=%lu *lock=%lu \n", tid, read_val, *lock); 

         if ((read_val == 0xffffffff) || (read_val != 0))
         {
//            printf ("tid=%d lock vread failed \n");
            return false;
         }
//         else if(read_val != 0)
//         {
////            printf ("tid=%d lock occupied\n");
//            return false;
//         }
         
         assert (read_val == 0 || read_val == 1);

         if (read_val == 0)
         {
            unsigned int long lock_val = 1;
            bool write_status = CarbonValidateAndWrite((void*)lock, lock_val, sizeof(unsigned int));

            if(!write_status)
            {
//                printf ("tid=%d lock(%p) vwrite failed\n",tid, lock);
                return false;
            }
         }
         
//        printf ("tid=%d acquired lock\n");
        return true;       
}

static void releaseLock(volatile unsigned int* lock, const unsigned int tid)
{
    // dont need vwrite here. As if the lock was acquired the node cannot be deleted. 
    // So neednt validate. Also I dont care if someone else also downgraded the cacheline
    *lock = 0;
//    printf("tid(%u) releaseLock: *lock=%u\n", tid, *lock); 
}

#endif //LOCK_H
