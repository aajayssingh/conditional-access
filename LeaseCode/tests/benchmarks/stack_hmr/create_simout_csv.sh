#!/bin/bash
####
# the script is to create tabular data from the sim output to analyse the simlator stats
# usage ./create_simout_csv.out  after running run_exp.sh that creates the needed input files. Make sure to delete old files in exp_output folder.
####
out_dir="./exp_output"


count=0
for stepfile in `ls $out_dir/sim*`
do
    #get algo name from file name
    algo_name=`echo $stepfile | cut -d"/" -f3 | cut -d"-" -f2`
    tot_walltime=`cat $stepfile | grep "Shutdown Time" | tr -s ' '| cut -d" " -f5`

#    echo $algo_name

    #get individual rows from simulator output begin
    tot_ins=`cat $stepfile | grep "Total Instructions" | sed 's/|/,/g'| head -c -3`

    tot_dataMemAcc=`cat $stepfile | grep "Total Data Memory Accesses" | sed 's/|/,/g'| head -c -3`
    tot_avgDataMemAccLatency=`cat $stepfile | grep "Average Data Memory Access Latency" | sed 's/|/,/g'| head -c -3`

    l1d_cache=`cat $stepfile | grep -a "Cache L1-D" -A3`
    dl1_cache_acc=$(echo $l1d_cache | awk -v FS="Cache Accesses|Cache Misses" '{print $2}'| sed 's/|/,/g' | head -c -3)
    str_dl1_cache_acc=`echo "Cache Accesses" $dl1_cache_acc`
    dl1_cache_miss=$(echo $l1d_cache | awk -v FS="Cache Misses|Miss Rate" '{print $2}' | sed 's/|/,/g' | head -c -3)
    str_dl1_cache_miss=`echo "Cache Misses" $dl1_cache_miss`
    dl1_missrate=$(echo $l1d_cache | awk -F"Miss Rate" '/Miss Rate/{print $2}' | sed 's/|/,/g'| head -c -2)
    str_dl1_missrate=`echo "Miss Rate" $dl1_missrate`



    br_pr=`cat $stepfile | grep -a "Branch Predictor Statistics" -A2`
    br_pr_correct=$(echo $br_pr | awk -v FS="Num Correct|Num Incorrect" '{print $2}' | sed 's/|/,/g'| head -c -3)
    str_br_pr_correct=`echo "Branch Num Correct" $br_pr_correct`
    br_pr_incorrect=$(echo $br_pr | awk -F"Num Incorrect" '/Num Incorrect/{print $2}'| sed 's/|/,/g'| head -c -2)
    str_br_pr_incorrect=`echo "Branch Num Incorrect" $br_pr_incorrect`

    exp_fence=`cat $stepfile | grep "Explicit LFENCE, SFENCE, MFENCE" | sed 's/,//g' | sed 's/|/,/g'| head -c -3`

    imp_fence=`cat $stepfile | grep "Implicit MFENCE" | sed 's/|/,/g'| head -c -3`

    l1d_cache=`cat $stepfile | grep -a "Cache L1-D" -A11`
    evics=$(echo $l1d_cache | awk -v FS="Evictions|Dirty Evictions" '{print $2}'| sed 's/|/,/g' | head -c -3)
    str_evics=`echo "Evictions" $evics`
    devics=$(echo $l1d_cache | awk -F"Dirty Evictions" '/Dirty Evictions/{print $2}' | sed 's/|/,/g'| head -c -2)
    #get complete comma strings
    str_devics=`echo "Dirty Evictions" $devics`

    #get individual rows from simulator output end: format:: tite, num1, num2, ...


    ##Add new stats prepared above when you add new stat
    arr=("$str_evics" "$str_devics" "$str_br_pr_correct" "$str_br_pr_incorrect" "$tot_ins" "$exp_fence" "$imp_fence" "$str_dl1_cache_acc" "$str_dl1_cache_miss" "$str_dl1_missrate" "$tot_dataMemAcc" "$tot_avgDataMemAccLatency")


    #Now prep rows to be sent to csv by averaging all nums
    head_row=() #header for csv
    stat_row=() #number content of the row which has per thread stat

    for item in "${arr[@]}"
    do 
        #echo "$item"| tr -d " "
        title=`echo $item | cut -d"," -f1`
        stats=`echo $item | cut -d"," -f2- | sed 's/,//g'`
        avg_stat=$(echo $stats | awk '{s=0; for (i=1;i<=NF;i++)s+=$i; print s/NF;}')
#        echo $title 
#        echo $stats
#        echo $avg_stat
        head_row+=("$title,")
        stat_row+=("$avg_stat,")
    done

    #add algo name too
if ((count==0))
then
    updated_head_row=("algo," "walltime in sec,")
    new_head_row=("${updated_head_row[@]}" "${head_row[@]}")
    echo ${new_head_row[*]}
#    echo ${head_row[*]}
fi
    updated_stat_row=("$algo_name," "$((tot_walltime/1000000)),")
    new_stat_row=("${updated_stat_row[@]}" "${stat_row[@]}")
    echo ${new_stat_row[*]}
#    echo ${stat_row[*]}
count=$count+1
done