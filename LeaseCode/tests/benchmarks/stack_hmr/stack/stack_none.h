/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stack_none.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_NONE_H
#define STACK_NONE_H

#include "lock.h"
#include "util.h"

class stack_none
{
private:
    struct node {
        int volatile	value;
        node* volatile	next;

        node(int inValue) : value(inValue), next(NULL) {}
    };
PAD;
node* _top;
node* sentinel_top;
PAD;

public:
   stack_none()
   {
        sentinel_top = new node(0);
        _top = sentinel_top;
   }
   ~stack_none()
   {
        node* next_node = _top;
        node* temp_node;
        while( next_node != NULL)
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::stack_none\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
   }
   
   bool push(const unsigned int tid, int inValue) 
   {
        bool success = false;
        node* nd = new node(inValue);
        num_allocs.add(tid, 1);
        do
        {
            // Request the lease, and Load the current _top value
            node* curr_top = _top;
            nd->next = curr_top;

            // Perform CAS Operation
            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);

            if(success)
            {
                return true;
            }
        } while(true);
    }

    int pop(const unsigned int tid)
    {
        bool success = false;
        do {
                node* curr_top = _top;
                if(curr_top == sentinel_top)
                {
                    return false;   // TODO: This should be a NULL value
                }
                // Perform CAS Operation
                success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->next);

                if( success ) 
                {
                    return curr_top->value;
                }
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      node* next_node = _top;

      while( next_node != sentinel_top){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      node* next_node = _top;

      while( next_node != sentinel_top){
         size ++;
         next_node = next_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node* next_node = _top;

        while( next_node != NULL)
        {
            printf("-->%d", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_NONE_H */

