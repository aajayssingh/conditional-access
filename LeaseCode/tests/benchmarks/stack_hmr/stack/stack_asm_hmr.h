/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stack_asm_hmr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_ASM_HMR_H
#define STACK_ASM_HMR_H

#include "lock.h"
#include "util.h"


#define DEBUG if(0)
//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \ 
        num_retries.add(tid, 1);\        
        goto retry; \
    }


struct asm_node 
{
    int 	 value;
    struct asm_node* next;
}__attribute__((aligned(PADDING_BYTES)));

        
class stack_asm_hmr
{
private:
    struct asm_node* _top;
    struct asm_node* sentinel_top;
    PAD;
public:
   stack_asm_hmr()
   {
        if(posix_memalign( (void**)&_top, 64, sizeof(struct asm_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        _top->value = 0;
        _top->next = NULL;
   }
   ~stack_asm_hmr()
   {
        struct  asm_node* next_asm_node = _top;
        struct  asm_node* temp_asm_node;
        while( next_asm_node != NULL)
        {
            temp_asm_node = next_asm_node;
            next_asm_node = next_asm_node->next;
            free(temp_asm_node);
        }
       
        printf("dtor::stack_asm_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
   }
   
   bool push(const unsigned int tid, int inValue) 
   {
//        printf ("tid=%d push\n", tid);

       bool success = false;
//        asm_node* nd = new asm_node(inValue);
        struct asm_node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(asm_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        nd->value = inValue;
        nd->next = NULL;
        
        num_allocs.add(tid, 1);

        int ops = 0;
        retry:
        {
            unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
            // Request the lease, and Load the current _top value
            asm_node* curr_top = _top;
            CarbonAddToWatchset((void*) curr_top, sizeof(struct asm_node));
//            printf ("tid=%d added (%p) (%p) to ws\n", tid, curr_top, &curr_top);

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (nd->next)
                : "m" (curr_top->next)
                ); 
            if ((unsigned int long)nd->next == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
                num_retries.add(tid, 1);
                goto retry; 
            }

            
//            try_vread(&curr_top->next, ret_val, sizeof(struct asm_node*))    
//            nd->next = (asm_node *)ret_val;

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (curr_top->next)
                          : "r" (nd)
                           );      
            int res = CarbonGetVOPStatus();
            if (res != 0)
            {
            //         printf("tid=%lu vwrite fail\n", tid);
               CarbonRemoveAllFromWatchset();
               return true;
            }
            
            CarbonRemoveAllFromWatchset();    
            goto retry;
        }
    }

    int pop(const unsigned int tid)
    {
//        printf ("tid=%d pop\n", tid);

        bool success = false;
        
        retry:
        {
            unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

            asm_node* curr_top = _top;
            CarbonAddToWatchset((void*) curr_top, sizeof(struct asm_node));

            try_vread(&curr_top->next, ret_val, sizeof(struct asm_node*))    
            asm_node* firstasm_node;

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (firstasm_node)
                : "m" (curr_top->next)
                ); 
            if ((unsigned int long)firstasm_node == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
                num_retries.add(tid, 1);
                goto retry; 
            }
            
            if(firstasm_node == NULL)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // TODO: This should be a NULL value
            }
            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->next);

//            if( success ) 
//            {
//                return curr_top->value;
//            }
//            bool write_status = CarbonValidateAndWrite((void*)&curr_top->next, 
//                    (unsigned int long)firstasm_node->next, sizeof(asm_node*));
////            bool write_status = CarbonValidateAndWrite((void*)&head->next, (unsigned int long)n, sizeof(int));
//
//            if(write_status)
//            {
////                printf ("tid=%d vwrite success popped key (%d)\n",tid, firstasm_node->value);
//                CarbonRemoveAllFromWatchset();    
////                printDebuggingDetails();
//                return firstasm_node->value;
//            }

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (curr_top->next)
                          : "r" (firstasm_node->next)
                           );      
            int res = CarbonGetVOPStatus();
            if (res != 0)
            {
            //         printf("tid=%lu vwrite fail\n", tid);
               CarbonRemoveAllFromWatchset();
               return true;
            }

            CarbonRemoveAllFromWatchset();    
            goto retry;
        }
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      asm_node* next_asm_node = _top;

      while( next_asm_node != sentinel_top){
         sum = sum + next_asm_node->value;
         next_asm_node = next_asm_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      asm_node* next_asm_node = _top;

      while( next_asm_node != NULL){
         size ++;
         next_asm_node = next_asm_node->next;
      }

      return size-1;
   }  

    void printDebuggingDetails()
    {
        asm_node* next_asm_node = _top->next;

        while( next_asm_node != NULL)
        {
            printf("-->%d", next_asm_node->value);
            next_asm_node = next_asm_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_ASM_HMR_H */

