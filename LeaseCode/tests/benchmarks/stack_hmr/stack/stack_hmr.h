/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   stack_hmr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_HMR_H
#define STACK_HMR_H

#include "lock.h"
#include "util.h"


#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \ 
        num_retries.add(tid, 1);\        
        goto retry; \
    }


struct node 
{
    int 	 value;
    struct node* next;
}__attribute__((aligned(PADDING_BYTES)));

        
class stack_hmr
{
private:
    struct node* _top;
    struct node* sentinel_top;
    PAD;
public:
   stack_hmr()
   {
        if(posix_memalign( (void**)&_top, 64, sizeof(struct node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        _top->value = 0;
        _top->next = NULL;
   }
   ~stack_hmr()
   {
        struct  node* next_node = _top;
        struct  node* temp_node;
        while( next_node != NULL)
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::stack_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
   }
   
   bool push(const unsigned int tid, int inValue) 
   {
//        printf ("tid=%d push\n", tid);

       bool success = false;
//        node* nd = new node(inValue);
        struct node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        nd->value = inValue;
        nd->next = NULL;
        
        num_allocs.add(tid, 1);
        int ops = 0;
        retry:
        {
            unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
            // Request the lease, and Load the current _top value
            node* curr_top = _top;
            CarbonAddToWatchset((void*) curr_top, sizeof(struct node));
//            printf ("tid=%d added (%p) (%p) to ws\n", tid, curr_top, &curr_top);
            
//            if (++ops >2)
//                assert(0);
//            nd->next = curr_top;
            try_vread(&curr_top->next, ret_val, sizeof(struct node*))    
            nd->next = (node *)ret_val;
//            printf ("tid=%d vread success value returned (%p)\n",tid, nd->next);

            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);
//
//            if(success)
//            {
//                return true;
//            }
            bool write_status = CarbonValidateAndWrite((void*)&curr_top->next, (unsigned int long)nd, sizeof(node*));
//            bool write_status = CarbonValidateAndWrite((void*)&head->next, (unsigned int long)n, sizeof(int));

            if(write_status)
            {
//                printf ("tid=%d vwrite success added key (%d)\n",tid, inValue);
                CarbonRemoveAllFromWatchset();    
//                printDebuggingDetails();
                return true;
            }

//            printf ("tid=%d vwrite fail key (%d)\n",tid, inValue);

            CarbonRemoveAllFromWatchset();    
            goto retry;
        }
    }

    int pop(const unsigned int tid)
    {
//        printf ("tid=%d pop\n", tid);

        bool success = false;
        
        retry:
        {
            unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

            node* curr_top = _top;
            CarbonAddToWatchset((void*) curr_top, sizeof(struct node));

            try_vread(&curr_top->next, ret_val, sizeof(struct node*))    
            node* firstnode = (node *)ret_val;
            
            if(firstnode == NULL)
            {
                CarbonRemoveAllFromWatchset();    

                return false;   // TODO: This should be a NULL value
            }
            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->next);

//            if( success ) 
//            {
//                return curr_top->value;
//            }
            bool write_status = CarbonValidateAndWrite((void*)&curr_top->next, 
                    (unsigned int long)firstnode->next, sizeof(node*));
//            bool write_status = CarbonValidateAndWrite((void*)&head->next, (unsigned int long)n, sizeof(int));

            if(write_status)
            {
//                printf ("tid=%d vwrite success popped key (%d)\n",tid, firstnode->value);
                free(firstnode);
                CarbonRemoveAllFromWatchset();    
//                printDebuggingDetails();
                return firstnode->value;
            }
            CarbonRemoveAllFromWatchset();    
            
            goto retry;
        }
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      node* next_node = _top;

      while( next_node != sentinel_top){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      node* next_node = _top;

      while( next_node != NULL){
         size ++;
         next_node = next_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node* next_node = _top->next;

        while( next_node != NULL)
        {
            printf("-->%d", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_HMR_H */

