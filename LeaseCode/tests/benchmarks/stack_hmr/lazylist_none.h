#ifndef LAZYLIST_NONE_H
#define LAZYLIST_NONE_H

#include <limits.h>
#include "lock.h"
#include "util.h"

class lazylist_none{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
PAD; // each node is posix_memalign allolcated so not needed
    struct node{
        unsigned int key;
        bool mark;
        pthread_mutex_t pt_lock;
        struct node * next;

        node(unsigned int key): key(key)
        {
            mark = false;
            next = NULL;
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
        }
    };

PAD;    
    struct node * head;
PAD;
    struct node * tail;    
PAD;
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_none(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_none();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_none::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
}

lazylist_none::lazylist_none(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_none\n");
//    head = new node(min_key);
//    tail = new node(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);

    head->next = tail;
}

lazylist_none::~lazylist_none()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_none\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());

}

bool lazylist_none::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_none::contains(const unsigned int tid, const unsigned int key)
{
    struct node * pred = head;
    struct node * curr = pred->next;
    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    return ((!curr->mark) && (curr->key == key));
}

bool lazylist_none::insert(const unsigned int tid, const unsigned int key)
{
    retry:

    struct node * pred = head;
    struct node * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key == key)
    {
        return false;
    }   
    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
    
    if (!validate(pred, curr))
    {
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
        goto retry;        
    }
    
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    new_node->next = curr;
    pred->next = new_node;
    num_allocs.add(tid, 1);
    
    pthread_mutex_unlock(&pred->pt_lock);
    pthread_mutex_unlock(&curr->pt_lock);

    return true;
}

bool lazylist_none::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    
    struct node * pred = head;
    struct node * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key != key)
    {
        return false;
    }   
    
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);

    if (!validate(pred, curr))
    {
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;
    
// Since simulator load/store are non faulty. a thread can access a null node and
// if members of a null node without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid nodes.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
    pthread_mutex_unlock(&pred->pt_lock);
    pthread_mutex_unlock(&curr->pt_lock);

    return true;
}

long long lazylist_none::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_none::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_none::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

#endif // LAZYLIST_NONE_H
