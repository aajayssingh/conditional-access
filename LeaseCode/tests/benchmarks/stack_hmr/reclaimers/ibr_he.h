/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   he.h
 * Author: mc
 *
 * Created on May 16, 2021, 2:00 PM
 */

#ifndef IBR_HE_H
#define IBR_HE_H
#include <list>
#include "concurrent_primitives.h"
#include "util.h"

#define MAX_HE 2

template <typename T>
class ibr_he{
private:
    const unsigned int numProcesses;
    unsigned int empty_freq;
    unsigned int epoch_freq;
    unsigned int max_he;
    
    padded<uint64_t> *retire_counters; //per thread
    padded<uint64_t> *alloc_counters; //per thread
    
    padded<std::list<T*>> *retired;    
    padded<std::atomic<uint64_t>*> *reservations; //per thread
    
    PAD;
    std::atomic<uint64_t> epoch;
    PAD;
            
public:

    inline uint64_t getEpoch()
    {
        return epoch.load(std::memory_order_acquire);
    }

    //reserve the current epoch when the obj is alive.
    // TODO come up with an example where HP like pathology will exist. Or prove
    // its free of HP pathology.
    // to reserve the epoch when the object read was alive or not freed.
    // I doubt why a thread will not miss epoch reservation and free the node whose epoch has been 
    // reserved here. TODO?? why cannt retiring of obj by some thread be reporder such that 
    // obj is freed while incorrectly it is reserved.
    T* read(const unsigned int tid, int index, std::atomic<T*>& obj)
    {
        uint64_t prev_epoch = reservations[tid].ui[index].load(std::memory_order_acquire);
        while (true)
        {
            T* ptr = obj.load(std::memory_order_acquire);
            //fence not needed after I changed obj (next field of node) to be of atomic type            
            // mem order acquire will ensure correct ordering.
            //            membarstoreload() 
            uint64_t curr_epoch = getEpoch();
            if (curr_epoch == prev_epoch)
            {
                return ptr; //fast path to avoid a store if epoch hasnt changed
            }
            else
            {
                // upper_reservs[tid].ui.store(curr_epoch, std::memory_order_release);
                reservations[tid].ui[index].store(curr_epoch, std::memory_order_seq_cst);
                prev_epoch = curr_epoch;
            }
        }
    }
    
    void clear(const unsigned int tid)
    {
        for (int i = 0; i < MAX_HE; i++)
        {
            reservations[tid].ui[i].store(0, std::memory_order_seq_cst);            
        }
    }
    
    uint64_t getReservation(const unsigned int tid, int idx)
    {
        return reservations[tid].ui[idx].load();
    }
  
    // to be called when a node is succesfully allocated.
    inline void incAllocCounter(const unsigned int tid)
    {
        alloc_counters[tid] = alloc_counters[tid] + 1;   
        if (alloc_counters[tid] % (epoch_freq) == 0)
        {
            epoch.fetch_add(1, std::memory_order_acq_rel);
//            printf("tid=%d epoch advanced=%lld\n", tid, epoch.load());
        }        
    }
    
    inline void retire(const unsigned int tid, T *obj)
    {
        if (obj == NULL)
        {
            return;
        }
        
        std::list<T*> *myTrash = &(retired[tid].ui);
        // for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
        // 	assert(it->obj!=obj && "double retire error");
        // }

        uint64_t retire_epoch = getEpoch();//epoch.load(std::memory_order_acquire);
        assert (obj->retire_epoch == UINT64_MAX 
                && "retire epoch shall be set here the first time!");
        obj->retire_epoch = retire_epoch; //set retire epoch
        
        myTrash->push_back(obj); // add obj in my limbo bag
        retire_counters[tid] = retire_counters[tid] + 1;
        if (retire_counters[tid] % empty_freq == 0)
        {
            empty(tid);
        }        
    }

    // FIXME: why epoc == 0?
    bool can_delete(uint64_t birth_epoch, uint64_t retire_epoch)
    {
        for (int i = 0; i < numProcesses; i++)
        {
            for (int j = 0; j < MAX_HE; j++)
            {
                const uint64_t epo = reservations[i].ui[j].load(std::memory_order_acquire); 
                if (epo < birth_epoch || epo > retire_epoch || epo == 0 
                        /*what if epo always remain 0 and be and re =0. 
                         * Then I may think of an unsafe records as safe?? */)
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
        }
        return true;
    }    
    
    void empty(const unsigned int tid)
    {
        std::list<T*> *myTrash = &(retired[tid].ui);

        int b4_empty_sz = myTrash->size();

        for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end;)
        {
            T* ptr = *iterator;
            if (can_delete(ptr->birth_epoch, ptr->retire_epoch))
            {
//                printf("tid=%d when retiring object has be=%lld re=%lld\n"
//                        , tid, ptr->birth_epoch, ptr->retire_epoch);
                free(ptr);
                
                total_freed.add(tid, 1);
                
                iterator = myTrash->erase(iterator); //return iterator corresponding to next of last erased item
            }
            else
            {
                ++iterator;
            }
        }

//        printf("ibr_he::tid=%d emptying: before sz=%d after sz=%d\n", tid, b4_empty_sz, myTrash->size());
    }
    
    ibr_he(const unsigned int _numProcesses) : numProcesses(_numProcesses)
    {
        empty_freq = EMPTY_FREQ; //30; //100
        epoch_freq = EPOCH_FREQ; //150; //50 //150;
        retired = new padded<std::list<T*>>[numProcesses];
        reservations = new padded<std::atomic<uint64_t>*>[numProcesses];        
        retire_counters = new padded<uint64_t>[numProcesses];
        alloc_counters = new padded<uint64_t>[numProcesses];
        
        for (int i = 0; i < numProcesses; i++)
        {
            reservations[i].ui = new std::atomic<uint64_t>[MAX_HE];
            for (int j = 0; j < MAX_HE; j++)
            {
                reservations[i].ui[j].store(0, memory_order_relaxed);
            }
            retired[i].ui.clear();
            retire_counters[i].ui = 0;
            alloc_counters[i].ui = 0;            
        }
        
        //since epoch 0 is treated as quiescence the epochs should start from 1.
        epoch.store(1, std::memory_order_release);
    }
    
    ~ibr_he()
    {
        unsigned int total_retired = 0;
        unsigned int total_allocated = 0;
        unsigned int total_reclaimed = 0;

        printf("\n pt_retired=");
        for (int i = 0; i < numProcesses; i++)
        {
            printf("%u ", retire_counters[i].ui);        
        }
        printf("\n pt_allocated=");
        for (int i = 0; i < numProcesses; i++)
        {
            total_retired += retire_counters[i].ui;
            total_allocated += alloc_counters[i].ui;
            printf("%u ", alloc_counters[i].ui);        
            delete [] reservations[i].ui;
        }
        printf("\n");
        
        printf("Epoch=%u \n", getEpoch());        
        printf("total_retired=%u\n", total_retired);
        printf("total_allocated=%u\n", total_allocated);
        printf("total_freed=%lld\n", total_freed.getTotal());

        delete [] reservations;
        delete [] retired;
        delete [] retire_counters;
        delete [] alloc_counters;
    }

};

#endif /* IBR_HE_H */

