/**
 * Copyright (C) 2015 Trevor Brown
 */

#ifndef HAZARDPTRS_H
#define HAZARDPTRS_H

#include "util.h"

#define MAX_HAZARDPTRS_PER_THREAD 2
template<typename T>
class hzrdptrs{

private:
    AtomicArrayList<T> **announce;
    ArrayList<T> **retired;    
    const int scanThreshold;
    const unsigned int numProcesses;

public:
    inline void incAllocCounter(const unsigned int tid)
    {
        num_allocs.add(tid, 1);
    }
    
    inline bool protect(const unsigned int tid, T * const obj, CallbackType notRetiredCallback
    , CallbackArg callbackArg, bool memoryBarrier = true) {
        int size = announce[tid]->size();
        assert (size <= 2 && "why hp max size exceeded??");
        announce[tid]->add(obj);
        if (memoryBarrier) __sync_synchronize(); // prevent retired from being read before we set a hazard pointer to obj
        
        if (notRetiredCallback(callbackArg)) 
        {
            assert(announce[tid]->size() <= MAX_HAZARDPTRS_PER_THREAD);
            return true;
        } 
        else 
        {
            return false;
        }
    }
    
    inline void unprotect(const unsigned int tid, T * const obj) {
        int sz = announce[tid]->size();
//        printf("tid(%d) hzrdptrs::unprotect before sz=%d(%p)\n", tid, announce[tid]->size(), obj);
//        announce[tid]->printDebug();
        announce[tid]->erase(obj);
//        announce[tid]->printDebug();
//        printf("tid(%d) hzrdptrs::unprotect before sz=%d after sz=%d(%p)\n", tid, sz, announce[tid]->size(), obj);

//        assert(announce[tid]->size() == sz-1);
    }

    inline void unprotectAll(const unsigned int tid) {
        announce[tid]->clear();
//        assert(announce[tid]->size() == 0);
    }

    inline void retire(const int tid, T* p) {
        retired[tid]->add(p);
        num_retired.add(tid, 1);

//        printf("retired[%u] size=%d \n", tid, retired[tid]->size());
        
        // if the retired bag is sufficiently large
        if (retired[tid]->isFull()) 
        {   
//            assert(0 && "verify my implementation");
            int b4_empty_sz = retired[tid]->size();
//            printf("before emptying retired[%u] size=%d \n", tid, retired[tid]->size());

            for (int ix=0;ix<retired[tid]->size();) 
            {
                auto ptr = retired[tid]->get(ix);
                bool is_safe = true;
                // check if retired[tid]->data[ix] is in any set of hazard pointers
                for (unsigned int otherTid=0; otherTid < numProcesses/*this->NUM_PROCESSES*/; ++otherTid) 
                {
                    if ((tid != otherTid) && announce[otherTid]->contains(ptr))
                    {
                        is_safe = false;
//                        printf("****hunted: this record is protected****\n");
                        break;
                    }
                }
                
                //VERIFYME
                if (is_safe)
                {
                    //free
                    free(ptr);         
                    retired[tid]->erase(ix);
                    total_freed.add(tid, 1);
                }
                else
                {
                   ++ix;
                }
            }
//            printf("tid=%d emptying: before sz=%d after sz=%d\n", tid, b4_empty_sz, retired[tid]->size());
        } //if (retired[tid]->isFull()) {
    }

    hzrdptrs(const unsigned int _numProcesses) : numProcesses(_numProcesses), 
            scanThreshold(EMPTY_FREQ/*2*_numProcesses*MAX_HAZARDPTRS_PER_THREAD*/) //to match ibr's retiring frequency.
    //Ans: to scott's doubt: My HP freq was too high but IBR retiring frq is low.
    {
//        numProcesses = _numProcesses;
        announce = new AtomicArrayList<T>*[numProcesses];
        retired = new ArrayList<T>*[numProcesses];
        for (int tid=0;tid<numProcesses;++tid) {
            announce[tid] = new AtomicArrayList<T>(MAX_HAZARDPTRS_PER_THREAD);
            retired[tid] = new ArrayList<T>(scanThreshold);
        }
    }
    
    ~hzrdptrs() 
    {
        printf("Epoch=%u\n", 0);        
        printf("total_retired=%u \n", num_retired.getTotal());
        printf("total_allocated=%u\n", num_allocs.getTotal());
        printf("total_freed=%lld\n", total_freed.getTotal());
        
        for (int tid=0; tid<numProcesses; ++tid) 
        {
            int sz = retired[tid]->size();
            for (int ix=0;ix<sz;++ix) {
                free(retired[tid]->get(ix));
            }
            delete announce[tid];
            delete retired[tid];
        }
        delete[] announce;
        delete[] retired;
    }    
    


};
#endif //HAZARDPTRS_H
