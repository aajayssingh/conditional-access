#!/bin/bash

#run this to get table data on terminal and create step files in exp_output.
# example usage: ./run_exp.sh
echo "started: " `date`
iter=1
out_dir="exp_output"

if [[ -d "${out_dir}" ]]
then
    echo "${out_dir} exists."
    rm -fr ${out_dir}/*.*
    echo cleaned ${out_dir}/
else
    mkdir ${out_dir}
    echo "created ${out_dir}"
fi

cols="\n%20s %10s %10s %10s %10s %15s %15s %15s %10s %15s %10s %5s %5s %10s %10s\n"
printf "$cols" alg threads tput dssize epoch tot_retired tot_allocated tot_freed walltime max_res_mem tot_ops ins del free_retire_ratio retries

div_valid(){ case $1 in 0) false;;esac; }

thread_arr=(1 6 12 18 24 30 36 42 48 54 60)
algo_arr=(stack_none stack_asm_hmr stack_hmr) #(lazylist_none lazylist_ibr lazylist_ibr_rcu lazylist_ibr_qsbr lazylist_ibr_he lazylist_hmr lazylist_asm_hmr) #lazylist_hp 
num_ins=50
num_del=50
range=100
dur=5
maxops=50

for n in "${thread_arr[@]}"
do
    for alg in "${algo_arr[@]}"
    do
        tput_sum=0; ds_size_sum=0; epoch_sum=0; tot_retired_sum=0; tot_allocated_sum=0; tot_freed_sum=0; walltime_sum=0; max_resmem_sum=0 
        for ((i=1; i<=$iter; i++))
        do
            tput=0
            fname=$out_dir/${alg}_n${n}_step${i}.txt;
            i_ds_size=0; i_epoch=0; i_retired=0; i_allocated=0; i_freed=0; i_walltime=0; i_max_resmem=0; i_tot_ops=0; i_ins=0; i_del=0; i_ret_free_ratio=0; i_retries=0;
            
            `/usr/bin/time -f "time_cmd_walltime=%e\nmax_res_mem=%M" -o $out_dir/temp_time.txt make INS=${num_ins} DEL=${num_del} MAX_DURATION=${dur} KEYRANGE=${range} THREADS=${n} ALG=${alg} MAXOPS=${maxops} {TRIAL}=$i &> $fname`
            
            if [[ $? -ne 0 ]]
            then
                echo "${alg}:${n}:step${i} crashed! saving in file crashed*.txt"
                i=$((i-1))
                mv $fname $out_dir/crashed_${alg}_n${n}_step${i}.txt
                continue
            fi

            cat $out_dir/temp_time.txt >> $fname
            tput=`cat $fname | grep Experiment_Throughput| cut -d"=" -f2| tr -d " "`
            i_ds_size=`cat $fname | grep experiment_ds_size| cut -d"=" -f2| tr -d " "`
            i_epoch=`cat $fname | grep Epoch| cut -d"=" -f2| tr -d " "`
            i_retired=`cat $fname | grep total_retired| cut -d"=" -f2| tr -d " "`
            i_allocated=`cat $fname | grep total_allocated| cut -d"=" -f2| tr -d " "`
            i_freed=`cat $fname | grep total_freed| cut -d"=" -f2| tr -d " "`
            i_walltime=`cat $fname | grep time_cmd_walltime| cut -d"=" -f2| tr -d " "`
            i_walltime=${i_walltime%.*}
            i_max_resmem=`cat $fname | grep max_res_mem| cut -d"=" -f2| tr -d " "`
            i_tot_ops=`cat $fname | grep "Total Exp Ops"| cut -d"=" -f2| tr -d " "`
            i_ins=`cat $fname | grep ins| cut -d"=" -f2| tr -d " "`
            i_del=`cat $fname | grep del| cut -d"=" -f2| head -1 | tr -d " "`
            i_retries=`cat $fname | grep total_retries| cut -d"=" -f2| head -1 | tr -d " "`
            
            ret_temp=${i_retired}
            div_valid "${ret_temp}" || ret_temp=1
            i_ret_free_ratio=$(echo "scale=2; ${i_freed}/${ret_temp}"|bc)
            
            printf "%20s %10d %10d %10d %10d %15d %15d %15d %10d %15d %10d %5d %5d %10f %10d\n" "$alg" "$n" "$tput" "$i_ds_size" "$i_epoch" "$i_retired" "$i_allocated" "$i_freed" "$i_walltime" "$i_max_resmem" "$i_tot_ops" "${i_ins}" "${i_del}"\
             "${i_ret_free_ratio}" "${i_retries}"
            
            # copy sim output from results dir to local exp_output folder.
            cp ../../../results/latest/sim.out $out_dir/
            mv $out_dir/sim.out $out_dir/sim-${alg}-${n}-step${i}.out
        done
    done
    echo " --------------- "
done

echo "finished: " `date`

#create csv for app stats
`./create_appstat_csv.sh > ${out_dir}/appstat_${num_ins}${num_ins}_${dur}_${range}.csv`
#create csv for simstats
`./simout_to_csv.sh > ${out_dir}/simstat_${num_ins}${num_ins}_${dur}_${range}.csv`

mv ${out_dir} ${out_dir}_${num_ins}${num_ins}_${dur}_${range}_test
echo "moved" ${out_dir} "--->" ${out_dir}_${num_ins}${num_ins}_${dur}_${range}
