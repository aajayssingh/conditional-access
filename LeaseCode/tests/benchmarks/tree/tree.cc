/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#include <fstream>
#include <sstream>
#include <sched.h>
#include <cstdlib>
#include <cstring>
#include <string>
#include <thread>
#include <ctime>
#include <set>
#include <chrono>
#include <typeinfo>
#include <pthread.h>
#include <atomic>

#include "random.h"
#include "globals.h"

#include "node_and_scx_record_manager.h"
#include "chromatic.h"
#include "bst.h"

#define DEFAULT_NEUTRALIZE_SIGNAL SIGQUIT

using namespace std;

typedef long long test_type;

// parameters for a simple concurrent test
int INS;
int DEL;
int MAXKEY;// = 32;
int OPS_PER_THREAD;// = 10000000;
int MILLIS_TO_RUN;
bool PREFILL;
int NTHREADS;// = 8;
char * RECLAIM_TYPE;
char * ALLOC_TYPE;
char * POOL_TYPE;
char * DATA_STRUCTURE;

chrono::time_point<chrono::high_resolution_clock> startTime;
chrono::time_point<chrono::high_resolution_clock> endTime;
long elapsedMillis;

// shared variables used in the concurrent test
bool start = false;
bool done = false;
atomic_int running; // number of threads that are running

const test_type NO_KEY = -1;
const test_type NO_VALUE = -1;
const int RETRY = -2;

// cpu sets for binding threads to cores
#ifdef HAS_CPU_SETS
cpu_set_t *cpusets[PHYSICAL_PROCESSORS];
#endif

template <class DataStructure>
class ThreadInfo {
public:
    int tid;
    DataStructure * tree;
};

template <class DataStructure>
void prefill(DataStructure * tree) {
    const double PREFILL_THRESHOLD = 0.03;
    for (int tid=0;tid<NTHREADS;++tid) {
        tree->initThread(tid); // it must be okay that we do this with the main thread and later with another thread.
    }
    Random& rng = rngs[0];
    const double expectedFullness = (INS+DEL ? INS / (double)(INS+DEL) : 0.5); // percent full in expectation
    const int expectedSize = (int)(MAXKEY * expectedFullness);
    int sz = 0;
    int tid = 0;
    for (int i=0;;++i) {
        VERBOSE {
            if (i&&((i % 1000000) == 0)) COUTATOMIC("PREFILL op# "<<i<<" sz="<<sz<<" expectedSize="<<expectedSize<<endl);
        }
        
        int key = rng.nextNatural(MAXKEY);
        int op = rng.nextNatural(100);
        if (op < 100*expectedFullness) {
            if (tree->insert(tid, key, key) == NO_VALUE) ++sz;
        } else {
            if (tree->erase(tid, key).second) --sz;
        }
        int absdiff = (sz < expectedSize ? expectedSize - sz : sz - expectedSize);
        if (absdiff < expectedSize*PREFILL_THRESHOLD) {
            break;
        }
        
        ++tid; // cycle through tids so we have memory uniformly allocated by each thread
        if (tid >= NTHREADS) tid = 0;
    }
    tree->clearCounters();
    VERBOSE COUTATOMIC("finished prefilling to size "<<sz<<" for expected size "<<expectedSize<<endl);
}

template <class RecordMgr, class DataStructure>
void *thread_timed(void *arg) {
    const int OPS_BETWEEN_TIME_CHECKS = 500;
    ThreadInfo<DataStructure> * info = (ThreadInfo<DataStructure> *) arg;
    int tid = info->tid;
    DataStructure * tree = info->tree;
#ifdef HAS_CPU_SETS
    sched_setaffinity(0, sizeof(cpusets[tid%PHYSICAL_PROCESSORS]), cpusets[tid%PHYSICAL_PROCESSORS]); // bind thread to core
    VERBOSE COUTATOMICTID("binding to cpu "<<(tid%PHYSICAL_PROCESSORS)<<endl);
#endif
    Random *rng = &rngs[tid*WORDS_PER_CACHE_LINE];
    tree->initThread(tid);
    running.fetch_add(1);
    __sync_synchronize();
    while (!start) { __sync_synchronize(); TRACE COUTATOMICTID("waiting to start"<<endl); } // wait to start
    int cnt = 0;
    while (!done) {
        if (((++cnt) % OPS_BETWEEN_TIME_CHECKS) == 0) {
            chrono::time_point<chrono::high_resolution_clock> __endTime = chrono::high_resolution_clock::now();
            if (chrono::duration_cast<chrono::milliseconds>(__endTime-startTime).count() >= MILLIS_TO_RUN) {
                done = true;
                __sync_synchronize();
                break;
            }
        }
        
        VERBOSE if (cnt&&((cnt % 1000000) == 0)) COUTATOMICTID("op# "<<cnt<<endl);
        int key = rng->nextNatural(MAXKEY);
        int op = rng->nextNatural(100);
        if (op < INS) {
            tree->insert(tid, key, key);
        } else if (op < INS+DEL) {
            tree->erase(tid, key);
        } else {
            tree->find(tid, key);
        }
    }
    running.fetch_add(-1);
    VERBOSE COUTATOMICTID("termination"<<endl);
    return NULL;
}

template <class RecordMgr, class DataStructure>
void test_mixed_operations(DataStructure * tree) {
    // get random number generator seeded with time
    // we use this rng to seed per-thread rng's that use a different algorithm
    srand(time(NULL));

    pthread_t *threads[NTHREADS];
    ThreadInfo<DataStructure> info[NTHREADS];
    for (int i=0;i<NTHREADS;++i) {
        threads[i] = new pthread_t;
        info[i].tid = i;
        info[i].tree = tree;
        rngs[i*WORDS_PER_CACHE_LINE].setSeed(rand());
    }
    
    if (PREFILL) prefill<DataStructure>(tree);

    for (int i=0;i<NTHREADS;++i) {
        if (pthread_create(threads[i], NULL, thread_timed<RecordMgr, DataStructure>, &info[i])) {
            cerr<<"ERROR: could not create thread"<<endl;
            exit(-1);
        }
    }
    
    while (running.load() < NTHREADS) {
        TRACE COUTATOMIC("main thread: waiting for threads to START running="<<running.load()<<endl);
    } // wait for all threads to be ready
    COUTATOMIC("main thread: starting timer..."<<endl);
    startTime = chrono::high_resolution_clock::now();
    __sync_synchronize();
    start = true;
    for (int i=0;i<NTHREADS;++i) {
        VERBOSE COUTATOMIC("main thread: attempting to join thread "<<i<<endl);
        if (pthread_join(*threads[i], NULL)) {
            cerr<<"ERROR: could not join thread"<<endl;
            exit(-1);
        }
        VERBOSE COUTATOMIC("main thread: joined thread "<<i<<endl);
    }
    endTime = chrono::high_resolution_clock::now();
    elapsedMillis = chrono::duration_cast<chrono::milliseconds>(endTime-startTime).count();
    COUTATOMIC((elapsedMillis/1000.)<<"s"<<endl);

    for (int i=0;i<NTHREADS;++i) {
        delete threads[i];
    }
}

void sighandler(int signum) {
    printf("Process %d got signal %d\n", getpid(), signum);

    if (signum == SIGUSR1) {
        TRACE_TOGGLE;
    } else {
        // tell all threads to terminate
        done = true;
        __sync_synchronize();

        // wait up to 10 seconds for all threads to terminate
        auto t1 = chrono::high_resolution_clock::now();
        while (running.load() > 0) {
            auto t2 = chrono::high_resolution_clock::now();
            auto diffSecondsFloor = chrono::duration_cast<chrono::milliseconds>(t2-t1).count();
            if (diffSecondsFloor > 3000) {
                COUTATOMIC("WARNING: threads did not terminate after "<<diffSecondsFloor<<"ms; forcefully exiting; tree render may not be consistent... running="<<running.load()<<endl);
                break;
            }
        }

//        // print a tree file that we can later render an image from
//        test_datastructure<test_type, test_type> *tree = ((test_datastructure<test_type, test_type> *) __tree);
//        tree->debugPrintToFile("tree", 0, "", 0, "");
//        tree->debugPrintToFileWeight("tree", 0, "", 0, "weight");

        // handle the original signal to get a core dump / termination as necessary
        if (signum == SIGSEGV || signum == SIGABRT) {
            signal(signum, SIG_DFL);
            kill(getpid(), signum);
        } else {
            printf("exiting...\n");
            exit(-1);
        }
    }
}

template <class RecordMgr, class DataStructure>
void printOutput(DataStructure * tree) {
    COUTATOMIC(typeid(tree).name()<<endl);
    tree->debugPrintAllocatorStatus();
    RecordMgr *recordmgr = tree->debugGetRecordMgr();
    COUTATOMIC("total allocated nodes         : "<<recordmgr->getDebugInfoNode()->getTotalAllocated()<<endl);
    COUTATOMIC("total allocated scx records   : "<<recordmgr->getDebugInfoSCX()->getTotalAllocated()<<endl);
    COUTATOMIC("total deallocated nodes       : "<<recordmgr->getDebugInfoNode()->getTotalDeallocated()<<endl);
    COUTATOMIC("total deallocated scx records : "<<recordmgr->getDebugInfoSCX()->getTotalDeallocated()<<endl);
    COUTATOMIC("total retired nodes           : "<<recordmgr->getDebugInfoNode()->getTotalRetired()<<endl);
    COUTATOMIC("total retired scx records     : "<<recordmgr->getDebugInfoSCX()->getTotalRetired()<<endl);
    COUTATOMIC("total returned to pool nodes  : "<<recordmgr->getDebugInfoNode()->getTotalToPool()<<endl);
    COUTATOMIC("total returned to pool scx    : "<<recordmgr->getDebugInfoSCX()->getTotalToPool()<<endl);
    COUTATOMIC("total taken from pool nodes   : "<<recordmgr->getDebugInfoNode()->getTotalFromPool()<<endl);
    COUTATOMIC("total taken from pool scx     : "<<recordmgr->getDebugInfoSCX()->getTotalFromPool()<<endl);
    COUTATOMIC("total taken blocks of nodes   : "<<recordmgr->getDebugInfoNode()->getTotalTaken()<<endl);
    COUTATOMIC("total taken blocks of scx     : "<<recordmgr->getDebugInfoSCX()->getTotalTaken()<<endl);
    COUTATOMIC("total given blocks of nodes   : "<<recordmgr->getDebugInfoNode()->getTotalGiven()<<endl);
    COUTATOMIC("total given blocks of scx     : "<<recordmgr->getDebugInfoSCX()->getTotalGiven()<<endl);
    COUTATOMIC("bytes allocated for nodes/scx : "<<(recordmgr->getDebugInfoNode()->getTotalAllocated()*sizeof(Node<test_type, test_type>) + recordmgr->getDebugInfoSCX()->getTotalAllocated()*sizeof(SCXRecord<test_type, test_type>))<<endl);
    COUTATOMIC(endl);
    
    debugCounters * const counters = tree->debugGetCounters();
    COUTATOMIC("total llx true                : "<<counters->llxSuccess->getTotal()<<endl);
    COUTATOMIC("total llx false               : "<<counters->llxFail->getTotal()<<endl);
    COUTATOMIC("total insert succ             : "<<counters->insertSuccess->getTotal()<<endl);
    COUTATOMIC("total insert retry            : "<<counters->insertFail->getTotal()<<endl);
    COUTATOMIC("total erase succ              : "<<counters->eraseSuccess->getTotal()<<endl);
    COUTATOMIC("total erase retry             : "<<counters->eraseFail->getTotal()<<endl);
    COUTATOMIC("total find succ               : "<<counters->findSuccess->getTotal()<<endl);
    COUTATOMIC("total find retry              : "<<counters->findFail->getTotal()<<endl);
    const long totalSucc = counters->insertSuccess->getTotal()+counters->eraseSuccess->getTotal()+counters->findSuccess->getTotal();
    const long throughput = (long) (totalSucc / (elapsedMillis/1000.));
    COUTATOMIC("total succ insert+erase+find  : "<<totalSucc<<endl);
    COUTATOMIC("throughput (succ ops/sec)     : "<<throughput<<endl);
    COUTATOMIC("elapsed milliseconds          : "<<elapsedMillis<<endl);
    COUTATOMIC(endl);

    COUTATOMIC("neutralize signal receipts    : "<<countInterrupted.getTotal()<<endl);
    COUTATOMIC("siglongjmp count              : "<<countLongjmp.getTotal()<<endl);
    COUTATOMIC(endl);
    
    // free tree
    VERBOSE COUTATOMIC("main thread: deleting tree..."<<endl);
    delete tree;
}

template <class RecordMgr, class DataStructure>
void performExperiment(DataStructure * tree) {
    test_mixed_operations<RecordMgr, DataStructure>(tree);
    printOutput<RecordMgr, DataStructure>(tree);
}

template <class Reclaim, class Alloc, class Pool>
void performExperiment() {
    // determine the correct data structure to use
    typedef node_and_scx_record_manager<test_type, test_type, Reclaim, Alloc, Pool> RecordMgr;
    if (strcmp(DATA_STRUCTURE, "BST") == 0) {
        typedef BST<test_type, test_type, less<test_type>, RecordMgr> DataStructure;
        performExperiment<RecordMgr, DataStructure>(new DataStructure(NO_KEY, NO_VALUE, RETRY, NTHREADS, DEFAULT_NEUTRALIZE_SIGNAL));
    } else if (strcmp(DATA_STRUCTURE, "Chromatic") == 0) {
        typedef Chromatic<test_type, test_type, less<test_type>, RecordMgr> DataStructure;
        performExperiment<RecordMgr, DataStructure>(new DataStructure(NO_KEY, NO_VALUE, RETRY, NTHREADS, DEFAULT_NEUTRALIZE_SIGNAL));
    } else {
        cout<<"bad data structure type"<<endl;
        exit(1);
    }
}

template <class Reclaim, class Alloc>
void performExperiment() {
    // determine the correct pool class
    
    if (strcmp(POOL_TYPE, "perthread_and_shared") == 0) {
        performExperiment<Reclaim, Alloc, pool_perthread_and_shared<test_type> >();
    } else if (strcmp(POOL_TYPE, "none") == 0) {
        performExperiment<Reclaim, Alloc, pool_none<test_type> >();
    } else {
        cout<<"bad pool type"<<endl;
        exit(1);
    }
}

template <class Reclaim>
void performExperiment() {
    // determine the correct alloc class
    
    if (strcmp(ALLOC_TYPE, "once") == 0) {
        performExperiment<Reclaim, allocator_once<test_type> >();
    } else if (strcmp(ALLOC_TYPE, "bump") == 0) {
        performExperiment<Reclaim, allocator_bump<test_type> >();
    } else if (strcmp(ALLOC_TYPE, "new") == 0) {
        performExperiment<Reclaim, allocator_new<test_type> >();
    } else {
        cout<<"bad allocator type"<<endl;
        exit(1);
    }
}

void performExperiment() {
    // determine the correct reclaim class
    
    if (strcmp(RECLAIM_TYPE, "none") == 0) {
        performExperiment<reclaimer_none<test_type> >();
    } else if (strcmp(RECLAIM_TYPE, "debra") == 0) {
        performExperiment<reclaimer_debra<test_type> >();
    } else if (strcmp(RECLAIM_TYPE, "debraplus") == 0) {
        performExperiment<reclaimer_debraplus<test_type> >();
    } else if (strcmp(RECLAIM_TYPE, "hazardptr") == 0) {
        performExperiment<reclaimer_hazardptr<test_type> >();
    } else {
        cout<<"bad reclaimer type"<<endl;
        exit(1);
    }
}

int main(int argc, char** argv) {

#ifdef HAS_CPU_SETS
    // create cpu sets for binding threads to cores
    int size = CPU_ALLOC_SIZE(PHYSICAL_PROCESSORS);
    for (int i=0;i<PHYSICAL_PROCESSORS;++i) {
        cpusets[i] = CPU_ALLOC(PHYSICAL_PROCESSORS);
        CPU_ZERO_S(size, cpusets[i]);
        CPU_SET_S(i, size, cpusets[i]);
    }
#endif

//    signal(SIGQUIT, sighandler);
//    VERBOSE DEBUG COUTATOMIC("registered SIGQUIT\n");
    signal(SIGTERM, sighandler);
    VERBOSE DEBUG COUTATOMIC("registered SIGTERM\n");
    signal(SIGSEGV, sighandler);
    VERBOSE DEBUG COUTATOMIC("registered SIGABRT\n");
    signal(SIGABRT, sighandler);
    VERBOSE DEBUG COUTATOMIC("registered SIGSEGV\n");
    signal(SIGUSR1, sighandler);
    VERBOSE DEBUG COUTATOMIC("registered SIGUSR1\n");

    PREFILL = false;
    MILLIS_TO_RUN = -1;
    for (int i=1;i<argc;++i) {
        if (strcmp(argv[i], "-i") == 0) {
            INS = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-d") == 0) {
            DEL = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-k") == 0) {
            MAXKEY = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-n") == 0) {
            NTHREADS = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-o") == 0) {
            OPS_PER_THREAD = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-mr") == 0) {
            RECLAIM_TYPE = argv[++i];
        } else if (strcmp(argv[i], "-ma") == 0) {
            ALLOC_TYPE = argv[++i];
        } else if (strcmp(argv[i], "-mp") == 0) {
            POOL_TYPE = argv[++i];
        } else if (strcmp(argv[i], "-ds") == 0) {
            DATA_STRUCTURE = argv[++i];
        } else if (strcmp(argv[i], "-t") == 0) {
            MILLIS_TO_RUN = atoi(argv[++i]);
        } else if (strcmp(argv[i], "-p") == 0) {
            PREFILL = true;
        } else {
            cout<<"bad arguments"<<endl;
            //exit(1);
        }
    }
    
    performExperiment();
    
    return 0;
}
