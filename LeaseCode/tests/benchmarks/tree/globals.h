/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef GLOBALS_H
#define	GLOBALS_H

#include <atomic>
#include "recordmgr/machineconstants.h"
#include "debugcounters.h"

#include "random.h"
static Random rngs[MAX_TID*WORDS_PER_CACHE_LINE]; // create per-thread random number generators (padded to avoid false sharing)
extern Random rngs[MAX_TID*WORDS_PER_CACHE_LINE];

#define HAS_CPU_SETS

// some useful options for the chromatic tree

//#define NOREBALANCING

#endif	/* GLOBALS_H */