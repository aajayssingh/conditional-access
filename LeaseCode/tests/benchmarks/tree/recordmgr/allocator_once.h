/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef ALLOC_ONCE_H
#define	ALLOC_ONCE_H

#include "machineconstants.h"
#include "globals.h"
#include "allocator_interface.h"
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
using namespace std;

#define MIN(a, b) ((a) < (b) ? (a) : (b))

template<typename T>
class allocator_once : public allocator_interface<T> {
private:
    const int cachelines;    // # cachelines needed to store an object of type T
    // for bump allocation from a contiguous chunk of memory
    T ** mem;             // mem[tid] = pointer to current array to perform bump allocation from
    size_t * memBytes;       // memBytes[tid*WORDS_PER_CACHE_LINE] = size of mem in bytes
    T ** current;         // current[tid*WORDS_PER_CACHE_LINE] = pointer to current position in array mem

    T* bump_memory_next(const int tid) {
        T* result = current[tid*WORDS_PER_CACHE_LINE];
        current[tid*WORDS_PER_CACHE_LINE] = (T*) (((char*) current[tid*WORDS_PER_CACHE_LINE]) + (cachelines*BYTES_IN_CACHE_LINE));
        return result;
    }
    int bump_memory_bytes_remaining(const int tid) {
        return (((char*) mem[tid])+memBytes[tid*WORDS_PER_CACHE_LINE]) - ((char*) current[tid*WORDS_PER_CACHE_LINE]);
    }
    bool bump_memory_full(const int tid) {
        return (((char*) current[tid*WORDS_PER_CACHE_LINE])+cachelines*BYTES_IN_CACHE_LINE > ((char*) mem[tid])+memBytes[tid*WORDS_PER_CACHE_LINE]);
    }

public:
    template<typename _Tp1>
    struct rebind {
        typedef allocator_once<_Tp1> other;
    };

    // reserve space for ONE object of type T
    T* allocate(const int tid) {
        if (bump_memory_full(tid)) return NULL;
        return bump_memory_next(tid);
    }
    void static deallocate(const int tid, T * const p) {
        // no op for this allocator; memory is freed only by the destructor.
    }
    void deallocateAndClear(const int tid, blockbag<T> * const bag) {
        // the bag is cleared, which makes it seem like we're leaking memory,
        // but it will be freed in the destructor as we release the huge
        // slabs of memory.
        bag->clearWithoutFreeingElements();
    }

    void debugPrintStatus(const int tid) {}
    
    void initThread(const int tid) {
//        // touch each page of memory before our trial starts
//        long pagesize = sysconf(_SC_PAGE_SIZE);
//        int last = (int) (memBytes[tid*WORDS_PER_CACHE_LINE]/pagesize);
//        VERBOSE COUTATOMICTID("touching each page... memBytes="<<memBytes[tid*WORDS_PER_CACHE_LINE]<<" pagesize="<<pagesize<<" last="<<last<<endl);
//        for (int i=0;i<last;++i) {
//            TRACE COUTATOMICTID("    "<<tid<<" touching page "<<i<<" at address "<<(long)((long*)(((char*) mem[tid])+i*pagesize))<<endl);
//            *((long*)(((char*) mem[tid])+i*pagesize)) = 0;
//        }
//        VERBOSE COUTATOMICTID(" finished touching each page."<<endl);
    }

    allocator_once(const int numProcesses, debugInfo * const _debug)
            : allocator_interface<T>(numProcesses, _debug)
            , cachelines((sizeof(T)+(BYTES_IN_CACHE_LINE-1))/BYTES_IN_CACHE_LINE) {
        VERBOSE DEBUG COUTATOMIC("constructor allocator_once"<<endl);
        mem = new T*[numProcesses];
        memBytes = new size_t[numProcesses*WORDS_PER_CACHE_LINE];
        current = new T*[numProcesses*WORDS_PER_CACHE_LINE];
//        long long newSizeBytes = MAX_TID*BYTES_IN_CACHE_LINE*cachelines;
//        while (newSizeBytes < (7472424960L / numProcesses)) {
//            newSizeBytes <<= 1;
//        }
        for (int tid=0;tid<numProcesses;++tid) {
            long long newSizeBytes = 7472424960L / numProcesses; // divide several GB amongst all threads.
//            newSizeBytes = MIN(newSizeBytes, 1<<30); // max 1GB per thread.
            VERBOSE COUTATOMIC("newSizeBytes        = "<<newSizeBytes<<endl);
//            if ((newSizeBytes % (cachelines*BYTES_IN_CACHE_LINE)) != 0) {
//                newSizeBytes -= (newSizeBytes % (cachelines*BYTES_IN_CACHE_LINE));
//            }
//            VERBOSE COUTATOMIC("newSizeBytes after  = "<<newSizeBytes<<endl);
            assert((newSizeBytes % (cachelines*BYTES_IN_CACHE_LINE)) == 0);
//            VERBOSE COUTATOMIC("thread "<<tid<<" allocating "<<newSizeBytes<<" bytes."<<endl);

            mem[tid] = (T*) malloc((size_t) newSizeBytes);
            if (mem[tid] == NULL) {
                cerr<<"could not allocate memory"<<endl;
                exit(-1);
            }
            //COUTATOMIC("successfully allocated"<<endl);
            memBytes[tid*WORDS_PER_CACHE_LINE] = (size_t) newSizeBytes;
            current[tid*WORDS_PER_CACHE_LINE] = mem[tid];
            // align on cacheline boundary
            int mod = (int) (((long) mem[tid]) % BYTES_IN_CACHE_LINE);
            if (mod > 0) {
                // we are ignoring the first mod bytes of mem, because if we
                // use them, we will not be aligning objects to cache lines.
                current[tid*WORDS_PER_CACHE_LINE] = (T*) (((char*) mem[tid]) + BYTES_IN_CACHE_LINE - mod);
            } else {
                current[tid*WORDS_PER_CACHE_LINE] = mem[tid];
            }
            assert((((long) current[tid*WORDS_PER_CACHE_LINE]) % BYTES_IN_CACHE_LINE) == 0);
        }
    }
    ~allocator_once() {
        long allocated = 0;
        for (int tid=0;tid<this->NUM_PROCESSES;++tid) {
            allocated += (((char*) current[tid*WORDS_PER_CACHE_LINE]) - ((char*) mem[tid]));
        }
        VERBOSE COUTATOMIC("destructor allocator_once allocated="<<allocated<<" bytes, or "<<(allocated/(cachelines*BYTES_IN_CACHE_LINE))<<" objects of size "<<sizeof(T)<<" occupying "<<cachelines<<" cache lines"<<endl);
        // free all allocated blocks of memory
        for (int tid=0;tid<this->NUM_PROCESSES;++tid) {
            delete mem[tid];
        }
        delete[] mem;
        delete[] memBytes;
        delete[] current;
    }
};
#endif	/* ALLOC_ONCE_H */

