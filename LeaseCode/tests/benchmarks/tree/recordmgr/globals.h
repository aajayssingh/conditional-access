/* 
 * File:   globals.h
 * Author: trbot
 *
 * Created on July 31, 2015, 4:44 PM
 */

#ifndef RECORDMGR_GLOBALS_H
#define	RECORDMGR_GLOBALS_H

#include "machineconstants.h"

// don't touch these options for crash recovery

#define CRASH_RECOVERY_USING_SETJMP
#define SEND_CRASH_RECOVERY_SIGNALS
#define AFTER_NEUTRALIZING_SET_BIT_AND_RETURN_TRUE
#define PERFORM_RESTART_IN_SIGHANDLER
#define SIGHANDLER_IDENTIFY_USING_PTHREAD_GETSPECIFIC

// some useful, data structure agnostic definitions

typedef bool CallbackReturn;
typedef void* CallbackArg;
typedef CallbackReturn (*CallbackType)(CallbackArg);
#define SOFTWARE_BARRIER asm volatile("": : :"memory")

// set __trace to true if you want many paths through the code to be traced with cout<<"..." statements
static std::atomic_bool ___trace(0);
extern std::atomic_bool ___trace;
#define TRACE_TOGGLE {bool ___t = ___trace; ___trace = !___t;}
#define TRACE DEBUG if(___trace)

#include <sstream>
#define COUTATOMIC(coutstr) cout<<coutstr
//{ \
    stringstream ss; \
    ss<<coutstr; \
    cout<<ss.str(); \
}
#define COUTATOMICTID(coutstr) cout<<"tid="<<(tid<10?" ":"")<<tid<<": "<<coutstr
// { \
    stringstream ss; \
    ss<<"tid="<<tid<<(tid<10?" ":"")<<": "<<coutstr; \
    cout<<ss.str(); \
}

#endif	/* GLOBALS_H */

