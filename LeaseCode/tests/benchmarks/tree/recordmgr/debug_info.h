/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef DEBUG_INFO_H
#define	DEBUG_INFO_H

#include "machineconstants.h"

class debugInfo {
private:
    const int NUM_PROCESSES;
    long *allocated;
    long *deallocated;
    long *fromPool;
    long *toPool; // toPool[tid*WORDS_PER_CACHE_LINE] = how many objects have been added to this pool
    long *given; // given[tid*WORDS_PER_CACHE_LINE] = how many blocks have been moved from this pool to a shared pool
    long *taken; // taken[tid*WORDS_PER_CACHE_LINE] = how many blocks have been moved from a shared pool to this pool
    long *retired; // retired[tid*WORDS_PER_CACHE_LINE] = how many objects have been retired
public:
    void clear() {
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            allocated[tid*WORDS_PER_CACHE_LINE] = 0;
            deallocated[tid*WORDS_PER_CACHE_LINE] = 0;
            fromPool[tid*WORDS_PER_CACHE_LINE] = 0;
            toPool[tid*WORDS_PER_CACHE_LINE] = 0;
            given[tid*WORDS_PER_CACHE_LINE] = 0;
            taken[tid*WORDS_PER_CACHE_LINE] = 0;
            retired[tid*WORDS_PER_CACHE_LINE] = 0;
        }
    }
    void addAllocated(const int tid, const int val) {
        allocated[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addDeallocated(const int tid, const int val) {
        deallocated[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addFromPool(const int tid, const int val) {
        fromPool[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addToPool(const int tid, const int val) {
        toPool[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addGiven(const int tid, const int val) {
        given[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addTaken(const int tid, const int val) {
        taken[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void addRetired(const int tid, const int val) {
        retired[tid*WORDS_PER_CACHE_LINE] += val;
    }
    long getAllocated(const int tid) {
        return allocated[tid*WORDS_PER_CACHE_LINE];
    }
    long getDeallocated(const int tid) {
        return deallocated[tid*WORDS_PER_CACHE_LINE];
    }
    long getFromPool(const int tid) {
        return fromPool[tid*WORDS_PER_CACHE_LINE];
    }
    long getToPool(const int tid) {
        return toPool[tid*WORDS_PER_CACHE_LINE];
    }
    long getGiven(const int tid) {
        return given[tid*WORDS_PER_CACHE_LINE];
    }
    long getTaken(const int tid) {
        return taken[tid*WORDS_PER_CACHE_LINE];
    }
    long getRetired(const int tid) {
        return retired[tid*WORDS_PER_CACHE_LINE];
    }
    long getTotalAllocated() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getAllocated(tid);
        }
        return result;
    }
    long getTotalDeallocated() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getDeallocated(tid);
        }
        return result;
    }
    long getTotalFromPool() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getFromPool(tid);
        }
        return result;
    }
    long getTotalToPool() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getToPool(tid);
        }
        return result;
    }
    long getTotalGiven() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getGiven(tid);
        }
        return result;
    }
    long getTotalTaken() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getTaken(tid);
        }
        return result;
    }
    long getTotalRetired() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += getRetired(tid);
        }
        return result;
    }
    debugInfo(int numProcesses) : NUM_PROCESSES(numProcesses) {
        allocated = new long[numProcesses*WORDS_PER_CACHE_LINE];
        deallocated = new long[numProcesses*WORDS_PER_CACHE_LINE];
        fromPool = new long[numProcesses*WORDS_PER_CACHE_LINE];
        toPool = new long[numProcesses*WORDS_PER_CACHE_LINE];
        given = new long[numProcesses*WORDS_PER_CACHE_LINE];
        taken = new long[numProcesses*WORDS_PER_CACHE_LINE];
        retired = new long[numProcesses*WORDS_PER_CACHE_LINE];
        clear();
    }
    ~debugInfo() {
        delete[] allocated;
        delete[] deallocated;
        delete[] fromPool;
        delete[] toPool;
        delete[] given;
        delete[] taken;
        delete[] retired;
    }
};

#endif	/* DEBUG_INFO_H */

