/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef MACHINECONSTANTS_H
#define	MACHINECONSTANTS_H

#define MAX_TID 64 // MUST BE A POWER OF TWO, since this is used for some bitwise operations
#define WORDS_PER_CACHE_LINE 8 // on intel i7 4770
#define BYTES_IN_CACHE_LINE 64 // on intel i7 4770
#define PHYSICAL_PROCESSORS 8  // on intel i7 4770
    
// set this to if(1) if you want verbose status messages
#define VERBOSE if(1)

#endif	/* MACHINECONSTANTS_H */

