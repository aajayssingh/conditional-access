/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef DEBUGCOUNTERS_H
#define	DEBUGCOUNTERS_H

#include "recordmgr/machineconstants.h"

class debugCounter {
private:
    const int NUM_PROCESSES;
    volatile long * data; // data[tid*WORDS_PER_CACHE_LINE] = count for thread tid (padded to avoid false sharing)
public:
    void add(const int tid, const int val) {
        data[tid*WORDS_PER_CACHE_LINE] += val;
    }
    void inc(const int tid) {
        add(tid, 1);
    }
    long get(const int tid) {
        return data[tid*WORDS_PER_CACHE_LINE];
    }
    long getTotal() {
        long result = 0;
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            result += get(tid);
        }
        return result;
    }
    void clear() {
        for (int tid=0;tid<NUM_PROCESSES;++tid) {
            data[tid*WORDS_PER_CACHE_LINE] = 0;
        }
        
    }
    debugCounter(const int numProcesses) : NUM_PROCESSES(numProcesses) {
        data = new long[numProcesses*WORDS_PER_CACHE_LINE];
        clear();
    }
    ~debugCounter() {
        delete[] data;
    }
};

class debugCounters {
public:
    const int NUM_PROCESSES;
    debugCounter * const llxSuccess;
    debugCounter * const llxFail;
    debugCounter * const insertSuccess;
    debugCounter * const insertFail;
    debugCounter * const eraseSuccess;
    debugCounter * const eraseFail;
    debugCounter * const findSuccess;
    debugCounter * const findFail;
    void clear() {
        llxSuccess->clear();
        llxFail->clear();
        insertSuccess->clear();
        insertFail->clear();
        eraseSuccess->clear();
        eraseFail->clear();
        findSuccess->clear();
        findFail->clear();
    }
    debugCounters(const int numProcesses) :
            NUM_PROCESSES(numProcesses),
            llxSuccess(new debugCounter(numProcesses)),
            llxFail(new debugCounter(numProcesses)),
            insertSuccess(new debugCounter(numProcesses)),
            insertFail(new debugCounter(numProcesses)),
            eraseSuccess(new debugCounter(numProcesses)),
            eraseFail(new debugCounter(numProcesses)),
            findSuccess(new debugCounter(numProcesses)),
            findFail(new debugCounter(numProcesses))
    {}
    ~debugCounters() {
        delete llxSuccess;
        delete llxFail;
        delete insertSuccess;
        delete insertFail;
        delete eraseSuccess;
        delete eraseFail;
        delete findSuccess;
        delete findFail;
    }
};

#endif	/* DEBUGCOUNTERS_H */

