/**
 * Preliminary C++ implementation of chromatic tree using LLX/SCX.
 * 
 * Copyright (C) 2015 Trevor Brown
 * This preliminary implementation is CONFIDENTIAL and may not be distributed.
 */

#ifndef NODE_AND_SCX_RECORD_MANAGER_H
#define	NODE_AND_SCX_RECORD_MANAGER_H

#include "recordmgr/record_manager.h"
#include "scxrecord.h"
#include "node.h"

template <typename K, typename V, class Reclaim = reclaimer_interface<K>, class Alloc = allocator_interface<K>, class Pool = pool_interface<K> >
class node_and_scx_record_manager {
protected:
    // to add more record types managed by this class, extend the following,
    // and then add the appropriate functions, below.
    typedef Node<K,V> * node_pointer;
    typedef SCXRecord<K,V> * scx_pointer;
    
    typedef record_manager<Node<K,V>,Reclaim,Alloc,Pool> NodeMgr;
    typedef record_manager<SCXRecord<K,V>,Reclaim,Alloc,Pool> SCXRecordMgr;
    
    NodeMgr * nodeMgr;
    SCXRecordMgr * scxrecordMgr;

    // no need to modify the following    
    typedef node_and_scx_record_manager<K,V,Reclaim,Alloc,Pool> SelfType;
    
public:
    const int NUM_PROCESSES;
    RecoveryMgr<SelfType> * recoveryMgr;
    
    void clearCounters() {
        nodeMgr->clearCounters();
        scxrecordMgr->clearCounters();
    }
    
    debugInfo * getDebugInfoNode() {
        return &nodeMgr->debugInfoRecord;
    }
    debugInfo * getDebugInfoSCX() {
        return &scxrecordMgr->debugInfoRecord;
    }
    
    inline bool isProtected(const int tid, node_pointer obj) {
        return nodeMgr->isProtected(tid, obj);
    }
    inline bool isProtected(const int tid, scx_pointer obj) {
        return scxrecordMgr->isProtected(tid, obj);
    }
    
    // for hazard pointers (and reference counting)
    inline bool protect(const int tid, node_pointer obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool hintMemoryBarrier = true) {
        return nodeMgr->protect(tid, obj, notRetiredCallback, callbackArg, hintMemoryBarrier);
    }
    inline bool protect(const int tid, scx_pointer obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool hintMemoryBarrier = true) {
        return scxrecordMgr->protect(tid, obj, notRetiredCallback, callbackArg, hintMemoryBarrier);
    }
    
    inline void unprotect(const int tid, node_pointer obj) {
        nodeMgr->unprotect(tid, obj);
    }
    inline void unprotect(const int tid, scx_pointer obj) {
        scxrecordMgr->unprotect(tid, obj);
    }
    
    // warning: qProtect must be reentrant and lock-free (=== async-signal-safe)
    inline bool qProtect(const int tid, scx_pointer obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool hintMemoryBarrier = true) {
        return scxrecordMgr->qProtect(tid, obj, notRetiredCallback, callbackArg, hintMemoryBarrier);
    }
    inline bool qProtect(const int tid, node_pointer obj, CallbackType notRetiredCallback, CallbackArg callbackArg, bool hintMemoryBarrier = true) {
        return nodeMgr->qProtect(tid, obj, notRetiredCallback, callbackArg, hintMemoryBarrier);
    }
    
    inline bool isQProtected(const int tid, scx_pointer obj) {
        return scxrecordMgr->isQProtected(tid, obj);
    }
    inline bool isQProtected(const int tid, node_pointer obj) {
        return nodeMgr->isQProtected(tid, obj);
    }
    
    inline void qUnprotectAll(const int tid) {
        assert(isQuiescent(tid));
        scxrecordMgr->qUnprotectAll(tid);
        nodeMgr->qUnprotectAll(tid);
    }

    // for epoch based reclamation
    inline bool isQuiescent(const int tid) {
        return nodeMgr->isQuiescent(tid);
    }
    inline void enterQuiescentState(const int tid) {
//        VERBOSE DEBUG2 COUTATOMIC("record_manager::enterQuiescentState(tid="<<tid<<")"<<endl);
        if (NodeMgr::usesEpochs()) {
            nodeMgr->enterQuiescentState(tid);
        } else {
            nodeMgr->enterQuiescentState(tid);
            scxrecordMgr->enterQuiescentState(tid); // not needed for EBR / DEBRA, but needed for HPs...
        }
    }
    inline void leaveQuiescentState(const int tid) {
        assert(isQuiescent(tid));
//        VERBOSE DEBUG2 COUTATOMIC("record_manager::leaveQuiescentState(tid="<<tid<<")"<<endl);
        // it turns out that Nodes and SCX records retired in the same
        // epoch can be reclaimed together, so we don't actually need
        // separate epochs for each object type.
        // instead, we pass all reclaimers to nodeMgr->leaveQuiescentState,
        // and it rotates the epoch bags for SCX records and nodes at the same time.
        if (NodeMgr::usesEpochs()) {
            void * const reclaimers[] = {(void * const) nodeMgr->reclaim, (void * const) scxrecordMgr->reclaim};
            nodeMgr->leaveQuiescentState(tid, reclaimers, 2);
            __sync_synchronize();// memory barrier needed (only) for epoch based schemes at the moment...
        } else {
            nodeMgr->leaveQuiescentState(tid, NULL, 0);
            scxrecordMgr->leaveQuiescentState(tid, NULL, 0);
        }
    }

    // for all schemes except reference counting
    inline void retire(const int tid, node_pointer p) {
        assert(isQuiescent(tid));
        nodeMgr->retire(tid, p);
    }
    inline void retire(const int tid, scx_pointer p) {
        assert(isQuiescent(tid));
        scxrecordMgr->retire(tid, p);
    }

    // for all schemes
    inline node_pointer allocateNode(const int tid) {
        assert(isQuiescent(tid));
        return nodeMgr->allocate(tid);
    }
    inline scx_pointer allocateSCXRecord(const int tid) {
        assert(isQuiescent(tid));
        return scxrecordMgr->allocate(tid);
    }
    
    inline void deallocate(const int tid, node_pointer p) {
        assert(isQuiescent(tid));
        nodeMgr->deallocate(tid, p);
    }
    inline void deallocate(const int tid, scx_pointer p) {
        assert(isQuiescent(tid));
        scxrecordMgr->deallocate(tid, p);
    }

    node_and_scx_record_manager(const int numProcesses, const int _neutralizeSignal)
            : NUM_PROCESSES(numProcesses) {
        recoveryMgr = new RecoveryMgr<SelfType>(numProcesses, _neutralizeSignal, this);
        nodeMgr = new NodeMgr(numProcesses, (RecoveryMgr<void *> *) recoveryMgr);
        scxrecordMgr = new SCXRecordMgr(numProcesses, (RecoveryMgr<void *> *) recoveryMgr);
    }
    ~node_and_scx_record_manager() {
        delete nodeMgr;
        delete scxrecordMgr;
    }
    
    // FUNCTIONS THAT NEED NOT CHANGE TO ACCOMMODATE ADDITIONAL RECORD TYPES
    inline static bool shouldHelp() { // FOR DEBUGGING PURPOSES
        return NodeMgr::shouldHelp();
    }
    inline static bool supportsCrashRecovery() {
        return NodeMgr::supportsCrashRecovery();
    }
    
    void printStatus(void) {
        nodeMgr->printStatus();
        scxrecordMgr->printStatus();
    }
    
    void initThread(const int tid) {
        nodeMgr->initThread(tid);
        scxrecordMgr->initThread(tid);
        recoveryMgr->initThread(tid);
        enterQuiescentState(tid);
    }
};

#endif
