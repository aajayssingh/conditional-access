#!/bin/sh

##
# Preliminary C++ implementation of chromatic tree using LLX/SCX.
# 
# Copyright (C) 2015 Trevor Brown
# This preliminary implementation is CONFIDENTIAL and may not be distributed.
#
# Run this to perform some experiments, and then run create-csv.sh to create
# a CSV file from the results.
##

ms=2000

cnt=0
i=0

datastructure="BST"
#datastructure="Chromatic"

for mode in 0 1; do
for ratio in 0_0 25_25 50_50; do
for maxkey in 10000 1000000; do
for allocator in once; do
    for threads in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16; do
        for alg in debra.perthread_and_shared debraplus.perthread_and_shared hazardptr.perthread_and_shared; do
            reclaimer=`echo $alg | cut -d"." -f1`
            pool=`echo $alg | cut -d"." -f2`
            for trial in 1 2 3 4 5 6 7 8; do #6 7 8 9 10; do
                ins=`echo $ratio | cut -d"_" -f1`
                del=`echo $ratio | cut -d"_" -f2`
                cmd="./harness-o3 -p -i $ins -d $del -k $maxkey -n $threads -t $ms -mr $reclaimer -ma $allocator -mp $pool -ds $datastructure"
                filename="perf-p-i$ins-d$del-k$maxkey-n$threads-t$ms-$reclaimer-$allocator-$pool-trial${trial}.out"
                if [ "$mode" -eq "0" ]; then
                        cnt=`expr $cnt + 1`
                        echo previewing step $cnt: $filename
                else
                        i=`expr $i + 1`
                        echo step $i of ${cnt}: $filename
                        echo step $i of ${cnt}: $cmd > $filename
                        $cmd >> $filename
                fi
            done
        done
    done
done
done
done
done
