#ifndef __STACK__
#define __STACK__

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <math.h>
#include "carbon_user.h"


class Stack
{
private:

	struct Node {
		int volatile	_value;
		Node* volatile	_next;

		Node(int inValue) : _value(inValue), _next(NULL) {}
	};

	Node* _top;

public:
   Stack()
      : _top(NULL)
   {
	}
   
   bool push(int inValue, int lease_time, unsigned long& failures) 
   {
      bool success = false;
		Node* nd = new Node(inValue);
		do{
         // Request the lease, and Load the current _top value
         if(lease_time > 0)
            CarbonRequestLease((void*) &_top, sizeof(_top), lease_time);
         Node* curr_top = _top;
         nd->_next = curr_top;

			/*
			UInt64 crt_time = CarbonGetTime();
			
			float random_number = ( float( rand() ) / (float) RAND_MAX;
			random_number *= 200;
			
			
			while ( ( CarbonGetTime() - crt_time ) < random_number ) ;
			*/

         // Perform CAS Operation
         success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);

         // Release
         CarbonReleaseLease((void*) &_top, sizeof(_top));
         
         if(success){
				return true;
			}
         else{
            failures++;
         }
		} while(true);
	}

	int pop(int lease_time, unsigned long& failures)
   {
      bool success = false;
		do {
         // Request the lease, and Load the current _top value
         CarbonRequestLease((void*) &_top, sizeof(_top), lease_time);
			Node* curr_top = _top;
			
         // if NULL
			if(!curr_top){
            // Release
            CarbonReleaseLease((void*) &_top, sizeof(_top));
				return 0;   // TODO: This should be a NULL value
         }

         // Perform CAS Operation
         success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->_next);
			
         // Release
         CarbonReleaseLease((void*) &_top, sizeof(_top));
         
         if( success ) {
				return curr_top->_value;
			}
         else{
            failures++;
         }

		} while(true);
	}

   int get_current_size(){
      int size = 0;
      Node* next_node = _top;

      while( next_node != NULL){
         size ++;
         next_node = next_node->_next;
      }

      return size;
   }
   
};



#endif
