#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <time.h>
#include <sched.h>
#include <errno.h>
#include <string.h>
#include <map>
#include <unistd.h>
#include <math.h>
#include <sys/syscall.h>
#include "stack.h"
#include "carbon_user.h"

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define DEFAULT_TIME                100
#define DEFAULT_PIN                 0
#define DEFAULT_OP_TYPE             0
#define DEFAULT_LEASE_TIME          100

#define HUNDRED                     100
#define MAX_COUNT                   1000000


class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_)
      : tid(tid_), num_threads(num_threads_)
      , lcount(0), tries(0), failures(0), total(0), worst_latency(0), avg_latency(0)
   {}
   ~ThreadData()
   {}

   int tid;
   int num_threads;

   unsigned lcount;
   unsigned long tries;
   unsigned long failures;
    
   unsigned total;  
   uint32_t worst_latency;
   long double avg_latency;

   std::vector <int> try_count;
   std::vector <int> consec; 
};

// barrier synchronization object
pthread_barrier_t   barrier; 

// Global Variables
Stack* _LF_Stack;
unsigned *counter;
int thread_count;
int op_type;
int lease_time;
int max_duration = 1000000;


// Prototypes 
void* threadMain(void* arg);
unsigned thread_main_fetch ( ThreadData* data );
unsigned thread_main_cas ( ThreadData* data );


int main(int argc, char *argv[])
{
   thread_count = DEFAULT_THREAD_COUNT;
   op_type = DEFAULT_OP_TYPE;
   lease_time = DEFAULT_LEASE_TIME;

   if(argc > 3){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      lease_time     = atoi(argv[3]);
      max_duration   = atoi(argv[4]) * 1000000;     // Duration entered in Milliseconds converted to Nanoseconds

      printf("\nstack_banch_test: Threads(%i) OpType(%i) Lease_time(%i) Max_Duration(%ims) Start.\n" , 
            thread_count, op_type, lease_time, max_duration/1000000);
   }
   else if(argc > 2){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      printf("\nstack_banch_test: Threads(%i) OpType(%i) Start.\n" , 
            thread_count, op_type);
   }
   else if(argc > 1){
      thread_count = atoi(argv[1]);
      printf("\nstack_banch_test: Threads(%i) Start.\n" , thread_count);
   }


   _LF_Stack = new Stack();

   //_LF_Stack->push(5, 500);
   //_LF_Stack->pop(500);


   // Initialize & allocate counter
   //int ret_val = posix_memalign( (void**) &counter, 64, sizeof(unsigned));
   //int ret_val = posix_memalign( (void**) &counter, 64, 64);
   
   /*
   if(ret_val != 0){ 
      printf("Error: Could not allocatei Counter!!! \n");
      if(ret_val == EINVAL)
         printf("The alignment argument was not a power of two, or was not a multiple of sizeof(void *). \n");
      else if(ret_val == ENOMEM)
         printf("Insufficient Memory to fulfil the request. \n");
      exit(EXIT_FAILURE);
   }
   */
   


   // Initialize Barrier
   pthread_barrier_init (&barrier, NULL, thread_count);
   printf("Initialized Barrier\n");

   // Allocate Thread data array
   ThreadData* thread_args[thread_count];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i] = new ThreadData(i, thread_count);
   }
   pthread_t thread_handles[thread_count];

   
   // Initialize the Stack
   ThreadData* dummy = new ThreadData(0, thread_count);
   printf("Initializing Stack with 1000 nodes! \n");
   for (int i = 1; i <= 1000; i++){
      _LF_Stack->push( (unsigned) i, -1, dummy->tries );
   }
   printf("Initialized...\n");
   printf("Initial Size = %d \n", _LF_Stack->get_current_size() );

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   // Enable performance and energy models
   CarbonEnableModels();

   // Create Threads
   for (int i = 1; i < thread_count; i++)
   {
      int ret = pthread_create(&thread_handles[i], NULL, threadMain, (void*) thread_args[i]);
      //printf("Created Thread %d\n", i);
      if (ret != 0)
      {
         printf("ERROR spawning thread %i\n", i);
         exit(EXIT_FAILURE);
      }
   }
   threadMain((void*) thread_args[0]);

   //printf("Threads Finished\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   //printf("Threads Joined\n");

   // Disable performance and energy models
   CarbonDisableModels();

/* ======================================================================================== */
/* END PARALLEL SECTION */
/* ======================================================================================== */
   
   // ---------------------------------------------------------
   // Output Summary
   //----------------------------------------------------------
   printf("Done.\n\n");

   unsigned long total_tries =0;
   unsigned long total_failures =0;
   unsigned long total_successes =0;
   volatile uint64_t overall_worst_latency = 0;
   volatile double worst_average_latency = 0;

   printf("Summary:\n");
   for (int i = 0; i < thread_count; i++)
   {
      ThreadData* data = thread_args[i];
      printf("Thread(%d): Attempts(%lu) Failures(%lu)\n", 
            i, data->tries, data->failures);
      
      total_tries += data->tries;
      total_failures += data->failures;
      total_successes += data->lcount;
      
      if(data->worst_latency > overall_worst_latency )
        overall_worst_latency = data->worst_latency;

      if(data->avg_latency > worst_average_latency  )
        worst_average_latency = data->avg_latency;
   }

   printf("\nTotal Attempts:\t %lu \n", total_tries);
   printf("  Successful Attempts:\t %lu (%f/s)\n", total_successes,
        (float)(total_successes)*1000000000/max_duration );
   printf("  Failed Attempts:\t %lu \n", total_failures );
   printf("\n\n");
   
   printf("Successful Ops = %lu \n", total_successes );
   printf("Throughput (Ops/s) = %f \n", (float)(total_successes)*1000000000/max_duration );
   printf("Overall Worst case latency (per operation)  = %lu \n", overall_worst_latency );
   printf("Average latency (per operation)  = %lf \n", worst_average_latency );
   printf("\n\n");
   
   return 0;
}

// ---------------------------------------------------------
// THREAD FUNCTIONS
//----------------------------------------------------------

void* threadMain(void* data) {
    
    if( op_type == 0 ) // CAS Operation
    {
        thread_main_cas( (ThreadData*)data );
    }
    else
    {
        printf( "wrong op_type parameter!" );
        exit(0);
    }
    return NULL;
}


unsigned thread_main_cas ( ThreadData* data )
{
   uint64_t start_time, latency;
   uint32_t latency_arary[2000];
   uint32_t latency_arary_index = 0;
   pthread_barrier_wait (&barrier); 
   //printf("Thread %d entered cas\n", data->tid);
   //fflush(stdout);

   while ( CarbonGetTime() < max_duration )
   {
      // PUSH
      data->tries++;
      start_time = CarbonGetTime();
      _LF_Stack->push( data->lcount, lease_time, data->tries );
      latency = CarbonGetTime() - start_time;
      data->lcount++;
      if(latency_arary_index < 2000){
         latency_arary[latency_arary_index] = latency;
         latency_arary_index++;
      }
      if( latency > data->worst_latency )
         data->worst_latency = latency;
      
      // POP
      data->tries++;    
      start_time = CarbonGetTime();
      _LF_Stack->pop( lease_time, data->tries );
      latency = CarbonGetTime() - start_time;
      data->lcount++;
      if(latency_arary_index < 2000){
         latency_arary[latency_arary_index] = latency;
         latency_arary_index++;
      }
      if( latency > data->worst_latency )
         data->worst_latency = latency;
      
   }
   data->failures = data->tries - data->lcount;

   // Sorting
   for(int i=0; i<latency_arary_index; i++){
     for(int j=0; j<latency_arary_index; j++){
        if(latency_arary[i] < latency_arary[j]){
           uint32_t temp = latency_arary[i];
           latency_arary[i] = latency_arary[j];
           latency_arary[j] = temp;
        }
     }
   }

   // 90th percentile 
   for(int i=0; i<(latency_arary_index/10); i++){
     data->avg_latency += latency_arary[i];
   }
   data->avg_latency = data->avg_latency / (latency_arary_index/10);
   pthread_barrier_wait (&barrier); 
   
   return 0;
}

