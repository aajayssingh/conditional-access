#ifndef UTIL_H
#define UTIL_H
/*
 * Helper code shamelessly copied from Trevor's setbench.
 */
#pragma once

#include <iostream>
#include <atomic>
#include <climits>
#include <math.h>
#include <assert.h>

using namespace std;

#define ASM __asm__ __volatile__
#define membarstoreload() { ASM ("mfence;") ; }
#define SOFTWARE_BARRIER asm volatile ("": : :"memory")
#define CPU_RELAX asm volatile("pause\n": : :"memory");

#define PADDING_BYTES 64
#define MAX_THREADS 64

#define CAT2(x,y) x##y
#define CAT(x,y) CAT2(x,y)
#define PAD volatile char CAT(___padding, __COUNTER__)[PADDING_BYTES]

//for IBR based begin
//epoch to empty ratio= 1:5
#define EMPTY_FREQ 15 //30 //500  // every 10 retire
#define EPOCH_FREQ 75 //150 //100 // every 50 alloc
//for IBR based end

//for HP begin
typedef bool CallbackReturn;
typedef void* CallbackArg;
typedef CallbackReturn (*CallbackType)(CallbackArg);
//for HP end

//for hashtable begin
#define HASHTABLE_CAPACITY 128
//for hashtable end

//globally shared rng very slow cause other algo tput to be ultra slow
uint64_t x = 1;
uint64_t xorshift64(void) {
	x ^= x >> 12; // a
	x ^= x << 25; // b
	x ^= x >> 27; // c
	return x * UINT64_C(2685821657736338717);
}
//globally shared rng very slow cause other algo tput to be ultra slow

class PaddedRandom{
private:
    volatile char padding[PADDING_BYTES-sizeof(unsigned int)];
    unsigned int seed;
public:
    PaddedRandom()
    {
        this->seed = 0;
        assert(0);
        printf("PaddedRandom()\n");
    }
    
    PaddedRandom(int seed)
    {
        this->seed = seed;
    }
    
    void setSeed(int seed)
    {
        this->seed = seed;
    }
    
    unsigned int nextNatural(int n){
        seed ^= seed << 6;
        seed ^= seed >> 21;
        seed ^= seed << 7;
        int retVal = (int)(seed % n);
        
        return (retVal < 0 ? -retVal: retVal);
    }
    
}__attribute__((aligned(PADDING_BYTES)));

class PaddedU64Random{
private:
    volatile char padding[PADDING_BYTES-sizeof(uint64_t)];
    uint64_t seed;
public:
    PaddedU64Random()
    {
        this->seed = 0;
    }
    
    PaddedU64Random(uint64_t seed)
    {
        this->seed = seed;
    }
    
    void setSeed(uint64_t seed)
    {
        this->seed = seed;
    }
    
    uint64_t nextNatural(uint64_t n){
        assert (seed && "seed cannot be 0");
        
	seed ^= seed >> 12; // a
	seed ^= seed << 25; // b
	seed ^= seed >> 27; // c
	uint64_t res = seed * UINT64_C(2685821657736338717);
//        return res;
        return ((res / (double) ULLONG_MAX) * n);
    }
    
};


class DebugCounter{
private:
    struct paddedVLL{
        volatile long long v;
        volatile char padding[PADDING_BYTES-sizeof(long long)];
    };
    paddedVLL data[MAX_THREADS+1];
public:
    void add (const unsigned long int tid, const long long val){
        data[tid].v += val;
    }
    
    void inc (const unsigned long int tid)
    {
        add(tid, 1);
    }
    
    long long get (const unsigned long int tid)
    {
        return data[tid].v;
    }
    
    long long getTotal ()
    {
        long sum = 0;
        for (unsigned long int tid=0; tid < MAX_THREADS; ++tid ){
            sum += get(tid);
        }
        
        return sum;
    }
    
    void clear()
    {
        for (unsigned long int tid=0; tid < MAX_THREADS; ++tid ){
            data[tid].v = 0;
        }
    }
    
    DebugCounter()
    {
        clear();
    }
}__attribute__((aligned(PADDING_BYTES)));

class DebugData{
private:
    struct paddedVLL{
        volatile unsigned long long v;
        volatile char padding[PADDING_BYTES-sizeof(long long)];
    };
    paddedVLL data[MAX_THREADS+1];
public:
    void set (const unsigned long int tid, const unsigned long long val){
        data[tid].v = val;
    }
    
    unsigned long long get (const unsigned long int tid)
    {
        return data[tid].v;
    }
    
    unsigned long long getTotal ()
    {
        long sum = 0;
        for (int tid=0; tid < MAX_THREADS; ++tid ){
            sum += get(tid);
        }
        
        return sum;
    }
    
    unsigned long long getMin (int num_threads)
    {
        unsigned long long min = ULLONG_MAX;
        for (int tid=0; tid < num_threads; ++tid ){
            if (min > get(tid))
            {
                min = get(tid);
            }
        }        
        return min;
    }
    
    unsigned long long getMax (int num_threads)
    {
        unsigned long long max = 0; //LONG_MIN;

        for (int tid=0; tid < num_threads; ++tid ){
            if (max < get(tid))
                max = get(tid);
        }        
        return max;
    }
    
    unsigned long long variance(int num_threads)
    {
        // Compute mean (average of elements)
        unsigned long long sum = 0;
        for (int i = 0; i < num_threads; i++)
            sum += get(i);
        long double mean = (long double)sum / (long double)num_threads;

        // Compute sum squared
        // differences with mean.
        long double sqDiff = 0;
        for (int i = 0; i < num_threads; i++)
            sqDiff += (get(i) - mean) * (get(i) - mean);
        return sqDiff / num_threads;
    }

    long double standardDeviation(int num_threads)
    {
        return sqrt(variance(num_threads));
    }
        
    void clear()
    {
        for (unsigned long int tid=0; tid < MAX_THREADS; ++tid ){
            data[tid].v = 0;
        }
    }
    
    DebugData()
    {
        clear();
    }
}__attribute__((aligned(PADDING_BYTES)));


//NOTE: data is not padded that is difference over original version in setbench.
template <typename T>
class ArrayList{
private:
    PAD;
    int __size;
    T **data;
public:
    const int capacity;
    PAD;
    ArrayList(const int _capacity):capacity(_capacity)
    {
        __size = 0;
        data = new T*[capacity]; //TODO not padded.
    }
    
    ~ArrayList()
    {
        delete[] data;
    }
    
    inline T* get(const int ix)
    {
        return data[ix];
    }
    
    inline int size()
    {
        return __size;
    }
    
    inline void add(T* const obj)
    {
//        if (! (__size < capacity))
//        {
//            printf("%d %d\n", __size , capacity);
//            assert (__size < capacity);
//        }
        assert (__size < capacity);
        data[__size++] = obj;
    }

    inline void erase(const int ix)
    {
        assert (ix >= 0 && ix < __size);
        data[ix] = data[--__size];
    }
    
    inline void erase(T * const obj)
    {
        int ix = getIndex(obj);
        if (ix != -1) erase(ix);
    }
    
    inline int getIndex(T * const obj) {
        for (int i=0;i<__size;++i) {
            if (data[i] == obj) return i;
        }
        return -1;
    }
    
    inline bool contains(T * const obj) {
        return (getIndex(obj) != -1);
    }
    
    inline void clear() {
        __size = 0;
    }
    
    inline bool isFull() {
        return __size == capacity;
    }
    
    inline bool isEmpty() {
        return __size == 0;
    }
};

//NOTE: data is not padded that is difference over original version in setbench.
template<typename T>
class AtomicArrayList{
private:
    PAD;
    std::atomic_int __size;
    std::atomic_uintptr_t *data;
public:
    const int capacity;
    AtomicArrayList(const int _capacity) : capacity(_capacity){
        __size.store(0, memory_order_relaxed);
        data = new atomic_uintptr_t[capacity];        
    }
    
    ~AtomicArrayList(){
        delete[] data;
    }
    
    inline T* get(const int ix){
        return (T*) data[ix].load(memory_order_relaxed);
    }
    
    inline int size(){
        return __size.load(memory_order_relaxed);
    }
    
    inline void add(T* const obj){
        int sz = __size.load(memory_order_relaxed);
//        printf("AtomicArrayList:add:: size=%d, capacity=%d\n", sz, capacity);
        assert (sz < capacity);
        SOFTWARE_BARRIER;
        data[sz].store((uintptr_t)obj, memory_order_relaxed);
        SOFTWARE_BARRIER;
        __size.store(sz+1, memory_order_relaxed);
    }
    
    inline void erase(const int ix){
        int sz = __size.load(memory_order_relaxed);
        assert(ix >= 0 && ix < sz);
        if (ix != sz-1) // to handle the decreasing size in loop in hp retire? 
        {
//            printf("ix(%d) != sz-1(%d)\n", ix, sz-1);
            data[ix].store(data[sz-1].load(memory_order_relaxed), memory_order_relaxed);
        }
        __size.store(sz-1, memory_order_relaxed);
    }
    
    inline void erase(T * const obj) {
        int ix = getIndex(obj);
//        printf("ix(%d)\n", ix);

        if (ix != -1) erase(ix);
    }
    
    inline int getIndex(T * const obj) {
        int sz = __size.load(memory_order_relaxed);
        for (int i=0;i<sz;++i) {
            if (data[i].load(memory_order_relaxed) == (uintptr_t) obj) return i;
        }
        return -1;
    }
    
    inline bool contains(T * const obj) {
        return (getIndex(obj) != -1);
    }
    
    inline void clear() {
        SOFTWARE_BARRIER;
        __size.store(0, memory_order_relaxed);
        SOFTWARE_BARRIER;
    }
    
    inline bool isFull() {
        return __size.load(memory_order_relaxed) == capacity;
    }
    
    inline bool isEmpty() {
        return __size.load(memory_order_relaxed) == 0;
    }
    
    inline void printDebug() {
        int sz = __size.load(memory_order_relaxed);
        for (int i=0;i<sz;++i) {
            printf("data[%d]=%p ", i, data[i].load(memory_order_relaxed)) ;
        }
        
        printf("\n");
    }    
};


// note: HASHSET_TABLE_SIZE must be a power of two for bitwise operations below to work
#define HASHSET_TABLE_SIZE 32
#define HASHSET_FIRST_INDEX(key) (hash((key)) & (HASHSET_TABLE_SIZE-1))
#define HASHSET_NEXT_INDEX(ix) ((ix)+1 % HASHSET_TABLE_SIZE)
#define HASHSET_EMPTY_CELL 0

template<typename K>
class hashset_new {
    private:
        PAD;
        int tableSize;
        K** keys;
        int __size;
        PAD;
        inline int hash(K * const key) {
            // MurmurHash3's integer finalizer
            uint64_t k = (uint64_t) key;
            k ^= k >> 33;
            k *= 0xff51afd7ed558ccd;
            k ^= k >> 33;
            k *= 0xc4ceb9fe1a85ec53;
            k ^= k >> 33;
            return (int) k;
        }
        inline int getIndex(K * const key) {
            int ix = firstIndex(key);
            assert(ix >= 0);
            assert(ix < tableSize);
            while (true) {
                if (keys[ix] == HASHSET_EMPTY_CELL || keys[ix] == key) {
                    return ix;
                }
                ix = nextIndex(ix);
                assert(ix >= 0);
                assert(ix < tableSize);
            }
        }
        inline int firstIndex(K * const key) {
            return (hash(key) & (tableSize-1));
        }
        inline int nextIndex(const int ix) {
            return ((ix+1) & (tableSize-1));
        }
    public:
        hashset_new(const int numberOfElements) {
            tableSize = 32;
            while (tableSize < numberOfElements*4) {
                tableSize *= 2;
            }
            
//            std::cout<<"constructor hashset_new capacity="<<tableSize<<std::endl;
            keys = (new K*[tableSize+2*PADDING_BYTES]) + PADDING_BYTES; /* HACKY OVER-ALLOCATION AND POINTER SHIFT TO ADD PADDING ON EITHER END WITH MINIMAL ARITHEMTIC OPS */
            __size = -1;
            clear();
        }
        ~hashset_new() {
//            VERBOSE DEBUG std::cout<<"destructor hashset_new"<<std::endl;
            delete[] (keys - PADDING_BYTES); /* HACKY OVER-ALLOCATION AND POINTER SHIFT TO ADD PADDING ON EITHER END WITH MINIMAL ARITHEMTIC OPS */
        }
        void clear() {
            if (__size) {
                memset(keys, HASHSET_EMPTY_CELL, tableSize*sizeof(K*));
                __size = 0;
            }
        }
        bool contains(K * const key) {
            return get(key) != HASHSET_EMPTY_CELL;
        }
        K* get(K * const key) {
            return keys[getIndex(key)];
        }
        void insert(K * const key) {
            int ix = getIndex(key);
            if (keys[ix] == HASHSET_EMPTY_CELL) {
                keys[ix] = key;
                ++__size;
                assert(__size < tableSize);
            }
        }
        int size() {
            return __size;
        }
    };

    
uint32_t murmur3(uint32_t key) 
{
    constexpr uint32_t seed = 0x1a8b714c;
    constexpr uint32_t c1 = 0xCC9E2D51;
    constexpr uint32_t c2 = 0x1B873593;
    constexpr uint32_t n = 0xE6546B64;
    
    uint32_t k = key;
    k = k * c1;
    k = (k << 15) | (k >> 17);
    k *= c2;
    
    uint32_t h = k ^ seed;
    h = (h << 13) | (h >> 19);
    h = h*5 + n;
    
    h ^= 4;
    
    h ^= (h>>16);
    h *= 0x85EBCA6B;
    h ^= (h>>13);
    h *= 0xC2B2AE35;
    h ^= (h>>16);
    return h;
}
    
//debug counters
DebugCounter total_freed;
DebugCounter num_allocs;
DebugCounter num_retired;
DebugCounter num_retries;

DebugCounter num_vreadfails;
DebugCounter num_vwritefails;


DebugCounter timeslimbofreed;

//save time in nano secs
DebugData start_time;
DebugData finish_time;

#include "random_xoshiro256p.h"

#endif //UTIL_H