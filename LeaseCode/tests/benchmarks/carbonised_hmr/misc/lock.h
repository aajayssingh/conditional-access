#ifndef LOCK_H
#define LOCK_H

static void acquireLock(volatile unsigned int* lock)
{
#ifdef TASLOCK
    while(__sync_lock_test_and_set(lock, 1)){}
#elif TTASLOCK
    while(true)
    {
        while(*lock == 1){
            __asm__ __volatile__("pause;");
        }
        if(! (__sync_lock_test_and_set(lock, 1)))
        {
            return;
        }
    }
#else
    while(true)
    {
        if(*lock)
        {
            __asm__ __volatile__("pause;");
            continue;
        }
        
        if (__sync_bool_compare_and_swap(lock, 0, 1))
        {
            return;
        }
    }
#endif
}

static bool tacquireLock(volatile unsigned int* lock)
{
    bool res = __sync_lock_test_and_set(lock, 1);
    return !(res);
}

#ifndef DISABLECARBON 

#define vread(field_addr, ret_val, field_size) \
    ret_val = CarbonRead((void*)field_addr, field_size); \


//static bool __attribute__((optimize("O0")))tryAcquireLock(volatile unsigned int* lock, const unsigned int tid)
static bool tryAcquireLock(volatile unsigned int* lock, const unsigned int tid)
{
#ifdef ASMVLOCK
//repnz prefix isRePne
#define ASF_F2COPY\
   ".byte 0xF2\n\t"
    
    unsigned int long read_val = 0;    
    __asm__ __volatile__ (ASF_F2COPY "mov %1, %0"
            :"=r" (read_val)
            : "m" (*lock)
            ); 
    
         if ((read_val == 0xffffffff) || (read_val != 0))
         {
//            printf ("tid=%d lock vread failed \n", tid);
            return false;
         }
         
         assert (read_val == 0 || read_val == 1);

         if (read_val == 0)
         {
            unsigned int long lock_val = 1;
//            printf ("tid=%d lock(%p) GONNA vwrite\n",tid, lock);

            __asm__ __volatile__ (ASF_F2COPY "mov %1, %0"
                    :"=m" (*lock)
                    : "r" (lock_val)
                     );
            
            int write_status = CarbonGetVOPStatus();

            if(!write_status)
            {
//                printf ("tid=%d lock(%p) vwrite failed\n",tid, lock);
                return false;
            }
//            printf ("tid=%d lock(%p) vwrite success\n",tid, lock);
         }
         
//        printf ("tid=%d acquired lock\n");
        return true;  
#else
        unsigned int long read_val = CarbonValidateAndReadTemp((void*)lock, sizeof(unsigned int));
//        printf("tid=%d, read_val(*lock)=%lu *lock=%lu \n", tid, read_val, *lock); 

         if ((read_val == 0xffffffff) || (read_val != 0))
         {
//            printf ("tid=%d lock vread failed \n", tid);
            return false;
         }
//         else if(read_val != 0)
//         {
////            printf ("tid=%d lock occupied\n");
//            return false;
//         }
         
         assert (read_val == 0 || read_val == 1);

         if (read_val == 0)
         {
            unsigned int long lock_val = 1;
//            printf ("tid=%d lock(%p) GONNA vwrite\n",tid, lock);

            bool write_status = CarbonValidateAndWrite((void*)lock, lock_val, sizeof(unsigned int));

            if(!write_status)
            {
//                printf ("tid=%d lock(%p) vwrite failed\n",tid, lock);
                return false;
            }
//            printf ("tid=%d lock(%p) vwrite success\n",tid, lock);
         }
         
//        printf ("tid=%d acquired lock\n");
        return true;               
#endif        
}
#endif

//static void __attribute__((optimize("O0")))releaseLock(volatile unsigned int* lock, const unsigned int tid)
static void releaseLock(volatile unsigned int* lock, const unsigned int tid)
{

    // dont need vwrite here. As if the lock was acquired the node cannot be deleted. 
    // So neednt validate. Also I dont care if someone else also downgraded the cacheline
#ifdef TASLOCK
    __sync_lock_release(lock);
#elif TTASLOCK
    __sync_lock_release(lock);
#else
    *lock = 0;
#endif
//    printf("tid(%u) releaseLock: *lock=%u\n", tid, *lock); 
}

#endif //LOCK_H
