#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <sys/time.h>

#include "carbon_user.h"
#include "util.h"

#include "stack_hmr.h"
#include "stack_asm_hmr.h"
#include "stack_none.h"
//#include "lazylist_hp.h"
#include "stack_ibr_qsbr.h"
#include "stack_ibr_rcu.h"
#include "stack_ibr.h"
#include "stack_ibr_he.h"
#include "stack_ibr_hp.h"

#define DEBUG_PRINT if (0)

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define DEFAULT_TIME                5 //ms
#define DEFAULT_KEYRANGE            100
#define DEFAULT_INS                 50
#define DEFAULT_DEL                 50
#define DEFAULT_CON                 0
#define DEFAULT_TRIAL               1
#define DEFAULT_MAXOPS              5000
#define DEFAULT_ALGOTYPE

#define OPB // to activate operabation based experiment

/****************************************
 * Globals
 ****************************************/
// barrier synchronization object
PAD;
pthread_barrier_t   barrier; 
PAD;
volatile bool done = false;
PAD;

int thread_count;
int max_duration; //real experiment duration
int op_type;
int max_keyrange;
int ins, del, con;
int trial_num;
int max_ops;
PAD;
char* algo_type;
PAD;
volatile bool isPrefillExpectedSizeReached = false;
PAD;
DebugCounter size_checksum;
PAD;
DebugCounter key_checksum;
PAD;
DebugCounter num_size_checks;
PAD;
DebugCounter num_cas;
PAD;
PaddedRandom rng[MAX_THREADS];
PAD;
unsigned long long op_based_exp_begtime = 0;
PAD;
unsigned long long op_based_exp_endtime = 0;
PAD;

template <class AlgoType>
class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_, AlgoType * ds_)
      : tid(tid_), num_threads(num_threads_), _ds(ds_)
   {
   }

   ~ThreadData()
   {}

   int tid;
   int num_threads;
   AlgoType* _ds;
   PAD; //padded each element while allocating
   unsigned long ops;
   PAD;
   //to know how many times a thread did costly getTotal() ops during prefill
   unsigned long times_checked_prefillds_size; 
   unsigned long num_cas_to_check_prefillsize;
}__attribute__((aligned(PADDING_BYTES)));

// return time diff  in msecs
long double diff_time(struct timeval start, struct timeval end)
{
   return ((end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0) + 0.5; //milli sec
}

/****************************************
 * Function prototypes
 ****************************************/
template <class AlgoType>
void* threadMain(void* arg);

template <class AlgoType>
void prefillInsertOnlyWorker ( ThreadData<AlgoType>* data );

template <class AlgoType>
unsigned opsBasedWorker ( ThreadData<AlgoType>* data );

template <class AlgoType>
void printSummary(ThreadData<AlgoType>**,long int, AlgoType*, bool isprefill = false);

template <class AlgoType>
void bootstrapExperiment();


/****************************************
 * Function definitions
 ****************************************/
#define PRINT(x) (#x)

template <class AlgoType>
void printSummary(ThreadData<AlgoType>** thread_args, long int exec_duration_ns, AlgoType* ds, bool isprefill = false){
      isprefill ? printf("prefill_ds_size=%lld\n", ds->getDSSize()) : printf("experiment_ds_size= %lld\n", ds->getDSSize());         

      // size checksum validation
      printf("size_checksum validation:");
      long long total_ds_size_pt = size_checksum.getTotal();
      long long total_ds_size_t = ds->getDSSize();

      if (total_ds_size_t == total_ds_size_pt){
         printf("PASS\n");
      }
      else
      {
         printf("FAIL:: total_size:%lld total_size_pt:%lld\n", total_ds_size_t, total_ds_size_pt);
         for (unsigned int t=0; t < thread_count; ++t)
            printf("tid(%u) size:%lld\n", t, size_checksum.get(t));

         ds->printDebuggingDetails();
         assert (0 && "size_checksum validation failed");
      }

      // key checksum validation
      printf("key_checksum validation:");
      long long total_ds_keysum_pt = key_checksum.getTotal();
      long long total_ds_keysum_t = ds->getSumOfKeys();

      if (total_ds_keysum_t == total_ds_keysum_pt){
         printf("PASS\n");
      }
      else
      {
         printf("FAIL:: total_keysum:%lld total_keysum_pt:%lld\n", total_ds_keysum_t, total_ds_keysum_pt);
         ds->printDebuggingDetails();
         assert (0 && "key_checksum validation failed");
      }
//         ds->printDebuggingDetails();
      
      unsigned long total_tries = 0;
      printf("per thread #ops=");
      for (int i = 0; i < thread_count; i++)
      {
         ThreadData<AlgoType>* data = thread_args[i*PADDING_BYTES];
         total_tries += data->ops;
         printf("%lu ", data->ops);
      }
      printf("\n");

      if(isprefill)
      {
         printf("Total Prefill Ops=%lu \n", total_tries);
         printf("Total num_size_checks=%lld \n", num_size_checks.getTotal());
         printf("Total num_cas=%lld \n", num_cas.getTotal());
         // not printing prefill tput as that requires simulated time.
         // Since I am not using enableCarbonModel() in prefill phase therefore
         // cannot use CarbonGetTime() to get simulated time.
      }
      else
      {
         printf("Total Exp Ops=%lu \n", total_tries);
         printf("duration used ns=%ld\n", exec_duration_ns);
         printf("Experiment_Throughput (Ops/s)=%lu \n", (total_tries)*1000000000/(exec_duration_ns) );
      
         //reset per thread ops fopr next phase:
         printf("thread_start_time (ms)=");
         for (int i = 0; i < thread_count; i++)
         {
            printf("%Lf ", (long double)start_time.get(i)/(long double)1000000.0);
         }
         printf("\nthread_finish_time (ms)=");
         for (int i = 0; i < thread_count; i++)
         {
            printf("%Lf ", (long double)finish_time.get(i)/(long double)1000000.0);
         }
         
         printf("\nper thread exec phase duration (ms)=");
         for (int i = 0; i < thread_count; i++)
         {
            printf("%Lf ", (long double)(finish_time.get(i) - start_time.get(i))/(long double)1000000.0 );
         }
         printf("\nmin exec duration ms=%Lf", (long double)(finish_time.getMin(thread_count) - start_time.getMin(thread_count))/(long double)1000000.0 );
         printf("\nmax exec duration ms=%Lf", (long double)(finish_time.getMax(thread_count) - start_time.getMin(thread_count))/(long double)1000000.0 );
         
//         printf("\nmin_thread_start_time ns=%lld", start_time.getMin(thread_count));
//         printf("\nmax_thread_start_time ns=%lld", start_time.getMax(thread_count));
//         printf("\nstart_time variance ns=%lld", start_time.variance(thread_count));
//         printf("\nstart_time std dev ns=%Lf\n", start_time.standardDeviation(thread_count));
//         
//         printf("\nmin_thread_finish_time ns=%lld", finish_time.getMin(thread_count));
//         printf("\nmax_thread_finish_time ns=%lld", finish_time.getMax(thread_count));
//         printf("\nfinish_time variance ns=%lld", finish_time.variance(thread_count));
//         printf("\nfinish_time std dev ns=%Lf", finish_time.standardDeviation(thread_count));
      }
      printf("\n");
      
      //reset per thread ops fopr next phase:
      for (int i = 0; i < thread_count; i++)
      {
         ThreadData<AlgoType>* data = thread_args[i*PADDING_BYTES];
         data->ops = 0;
      }
}

template <class AlgoType>
void prefillInsertOnlyWorker ( ThreadData<AlgoType>* data )
{
   int prefill_attempt_cnt = 0;

   int tid = data->tid;
   AlgoType* ds = data->_ds;
   PaddedRandom my_rng = rng[tid];
   
   //expected size of ds in percent
   const double expectedFullness = (ins + del ? (ins/(double)(ins+del)) : 0.5);
   const int expectedSize = (int)(max_keyrange * expectedFullness);
      
   pthread_barrier_wait (&barrier); 
   // repeatedly invoke ins del contains of lazylist based on some distribution
   // till the expected size is reached.
   while (!isPrefillExpectedSizeReached)
   {
      if (tid == 0)
      {
         // check if expected prefill ds size reached every certian number of ops.
         // if any one thread sees that desired size is reached then set the loop exit 
         // condition for all threads.
         //randomly choose exit threshold from (50, 100, 150, 200) ops.
         if (data->ops > (0.5*max_keyrange)) 
         {
            if (!isPrefillExpectedSizeReached)
            {
               num_size_checks.add(tid, 1);
               int ds_size = size_checksum.getTotal();
               if (ds_size >= expectedSize)
               {
                  printf ("tid=%d reached expected ds_size=%d\n", tid, ds_size);

                  __sync_bool_compare_and_swap(&isPrefillExpectedSizeReached, false, true); //only one thread would succeed
                  num_cas.add(tid, 1);
               }
               else
               {
                  printf ("tid=%d prefilling completion attempt #%d ds_size=%d\n", tid, ++prefill_attempt_cnt, ds_size);
               }
            }
            else
               break; // if expected size reached stop ins ops and return.
         }

         unsigned long int key = my_rng.nextNatural(max_keyrange) + 1; //key in [1, max_keyrange]
         bool res = ds->push(tid, key);
         if (res)
         {
            size_checksum.add(tid, 1);
            key_checksum.add(tid, key);
         }
   //         if((data->ops)%1 == 0) printf("tid=%lu key=%lu OP=%s res=%d\n", tid, key, PRINT(INS), res);

         data->ops++;
      } // if (tid == 0)
   }
   pthread_barrier_wait (&barrier);   
}

template <class AlgoType>
unsigned opsBasedWorker ( ThreadData<AlgoType>* data )
{
//   int opcount = 0;
   int tid = data->tid;
   AlgoType* ds = data->_ds;
   PaddedRandom my_rng = rng[tid];

   pthread_barrier_wait (&barrier); 
   //warning: keep it after barrier. So that main thread could reset ops.
   assert (data->ops == 0 && "op count for throughput calculation should be 0 initially!");
//   assert(size_checksum.get(tid) == 0);
   
   start_time.set(tid, CarbonGetTime());
   
   if (!op_based_exp_begtime)
   {
      //NOTE: printing floats doesn't work correctly. Simulator may be does incorrectly not sure why. So not printing in millisec.
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("tid=%d opsBasedWorker:: Experiment Carbon beg_time (%llu)ns\n\n", tid, op_based_exp_begtime);
      }
   }

   while (data->ops < max_ops)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {

      if (done)
      {
         finish_time.set(tid, CarbonGetTime());
         break;
      }
      
      unsigned long int key = my_rng.nextNatural(max_keyrange) + 1; //key in [1, max_keyrange]
      assert(key != 0 && "assert key should not be 0. 0 is reserved for SENTINEL TOP");
      double rndNum = my_rng.nextNatural(100); //(xorshift64() / (double) ULLONG_MAX) * 100;

      if ( rndNum < ins )
      {
         bool res = ds->push(tid, key);
         if (res)
         {
            size_checksum.add(tid, 1);
            key_checksum.add(tid, key);
         }
//         if((opcount++)%1 == 0)
//         printf("tid=%u key=%lu OP=%s res=%d\n", tid, key, PRINT(INS), res);
      }
      else if ( rndNum < (ins + del))
      {
         long long res = ds->pop(tid); //NOTE: needs to be long to be sent to add() as negative.
         assert(res >= 0);
         assert(res <= max_keyrange);
         if (res)
         {
            size_checksum.add(tid, -1);
            key_checksum.add(tid, -res);            
         }
//         if((opcount++)%1 == 0)
//         printf("tid=%u key=%lu OP=%s res=%u\n", tid, key, PRINT(DEL), res);
      }
      else
      {
//         assert(0 && "stack has no contains");//bool res = true; //ds->contains(tid, key);
         long long res = ds->peek(tid); //NOTE: needs to be long to be sent to add() as negative.
//         printf("tid=%u key=%lu OP=%s res=%llu\n", tid, key, PRINT(CON), res);
         assert(res >= 0);

         if (res > max_keyrange)
         {
            printf("tid=%u OP=%s res=%llu\n", tid, PRINT(CON), res);
            fflush(stdout);
            assert(res <= max_keyrange && "MAY BE the value of deleted node is returned");
         }
         //         if((opcount++)%1== 0) printf("tid=%lu; key=%lu OP=%s res=%d\n", tid, key, PRINT(CON), res);
      }
//      if((tid == 0 || tid == 16) && (opcount++)%100 == 0) printf("tid=%lu key=%lu time (%lld)ms\n", tid, key, CarbonGetTime()/1000000);
      
      data->ops++;
//      ds->printDebuggingDetails();
   }
   finish_time.set(tid, CarbonGetTime());
   if (!done && (0 == tid))
   {
//      finish_time.set(tid, CarbonGetTime());
      __sync_bool_compare_and_swap(&done, false, true);   
      if (!op_based_exp_endtime){
         if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, finish_time.get(tid)) )
         {
            printf ("\ntid=%d opsBasedWorker:: Experiment Carbon end_time(%llu)ns duration=(%llu)ns\n",
                    tid, op_based_exp_endtime, (op_based_exp_endtime - op_based_exp_begtime));
         }
      }   
   }   
   pthread_barrier_wait (&barrier);
   return 0;
}

template <class AlgoType>
void* threadMain(void* data) {
#ifndef DISABLE_PREFILL   
   prefillInsertOnlyWorker ( (ThreadData<AlgoType> *) data );
#endif //#ifndef DISABLE_PREFILL
   
   opsBasedWorker( (ThreadData<AlgoType> *) data ); 
   return NULL;
}

/*
 * Creates threads for prefilling and experiment phase. At the end of each phase
 * summary of stats is printed.
 */
template <class AlgoType>
void bootstrapExperiment()
{
   /* initialize random seed: this rng is used to seed per thread rng*/
   srand(/*trial_num*/1);
  
   //decide algo to run
   AlgoType* ds = new AlgoType(thread_count, 0, UINT_MAX-1);// NOTE: UINTMAX should not clas with 0xffffffff=4294967295 used by VREAD/VWRITE
   
   // Allocate Thread data array
   ThreadData<AlgoType>* thread_args[thread_count*PADDING_BYTES];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i*PADDING_BYTES] = new ThreadData<AlgoType>(i, thread_count, ds);
      // seed per threa rng
      rng[i].setSeed(rand()+1);  
   }
   pthread_t thread_handles[thread_count];

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   // Create Threads
   for (int i = 1; i < thread_count; i++)
   {
      int ret = pthread_create(&thread_handles[i], NULL, threadMain<AlgoType>, (void*) thread_args[i*PADDING_BYTES]);
      //printf("Created Thread %d\n", i);
      if (ret != 0)
      {
         printf("ERROR spawning thread %i\n", i);
         exit(EXIT_FAILURE);
      }
   }

#ifndef DISABLE_PREFILL   
   printf("***************\n");
   printf(" PREFILL PHASE BEGIN:\n");
   printf("***************\n\n");
#endif //#ifndef DISABLE_PREFILL
   // Note: not enabling Carbon perf models for prefill to avoid 
   // polluting exp phase stats with prefill stats.
   // Also note wall time and simulation times are diff. To get simulation time
   // CarbonGetTime() shall be used within CarbonEnableModels() - CarbonDisableModel().
   // CarbonGetTime() wont work outside those these APIs.
   // To cal tput I shall use simulation time and not wall time as num ops are
   // from simulation and wall time is from host machine which are two diff things.

   struct timeval prefill_start_time, prefill_finish_time, exec_finish_time;
   gettimeofday(&prefill_start_time, NULL);

#ifndef DISABLE_PREFILL      
   // main thread also executes prefill worker so that all threads
   // waiting there could unleash prefilling :)
//   CarbonEnableModels();
   
   prefillInsertOnlyWorker((ThreadData<AlgoType>*) thread_args[0*PADDING_BYTES]);

   gettimeofday(&prefill_finish_time, NULL);
   long double prefill_timediff_msec = diff_time(prefill_start_time, prefill_finish_time);
 
   printf("\n***************\n");
   printf(" PREFILL PHASE END: host walltime duration(%Lf)s\n", ((long double)prefill_timediff_msec/(long double)1000.0) );
   printf("***************\n");

   printf("DS SUMMARY:\n");
   printSummary(thread_args, prefill_timediff_msec, ds, true);
   printf("\n\n");
#endif //#ifndef DISABLE_PREFILL
   
   // Enable performance and energy models
   printf("***************\n");
   printf("EXPERIMENT PHASE BEGIN:\n");
   printf("***************\n");   
 
   // enable event counter and stat monitoring
   CarbonEnableModels();
   unsigned long long exp_beg_time = CarbonGetTime();
   printf ("Experiment phase main thread Carbon beg_time (%llu)ns, (%Lf)ms\n",exp_beg_time, ((long double)exp_beg_time/(long double)1000000.0));

   // main thread will call worker function to unleash the
   // waiting threads to start the exp phase.
   opsBasedWorker((ThreadData<AlgoType>*) thread_args[0*PADDING_BYTES]);

   unsigned long long exp_end_time = CarbonGetTime();
   printf ("Experiment phase main thread Carbon end_time (%llu)ns duration=(%llu)ns duration(%Lf)ms \n",
           exp_end_time, (exp_end_time - exp_beg_time), ((long double)(exp_end_time - exp_beg_time)/(long double)1000000.0));

   printf("Threads Finished\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   printf("Threads Joined\n");

   // Disable performance and energy models
   // note: moving it before thread join causes graphite to yell about some dynamic
   // mem issue 
   CarbonDisableModels(); 

   // to calculate total wall time of whole execution=prefill + exp
   gettimeofday(&exec_finish_time, NULL);
   long double execution_timediff_msec = diff_time(prefill_start_time, exec_finish_time);
   printf("***************\n");
   printf("EXPERIMENT PHASE END: Carbon duration(%Lf) ms\n", ((long double)(exp_end_time - exp_beg_time)/(long double)1000000.0)); 
   printf("Total host (prefill+experiment) walltime duration(%Lf) s\n", ( (long double)execution_timediff_msec/(long double)1000.0) );

   printf("EXPERIMENT PHASE END: OPB Carbon duration (%llu)ns (%Lf) ms\n",
           (op_based_exp_endtime - op_based_exp_begtime), ((long double)(op_based_exp_endtime - op_based_exp_begtime)/(long double)1000000.0)); 
   
   printf("***************\n\n");
   
/* ======================================================================================== */
/* END PARALLEL SECTION */
/* ======================================================================================== */
   
   // ---------------------------------------------------------
   // Output Summary
   //----------------------------------------------------------
   printf("EXPERIMENT PHASE DS SUMMARY:\n");
   printSummary(thread_args, (op_based_exp_endtime - op_based_exp_begtime), ds); //OPB
   ds->printDebuggingDetails();

   
   //cleanup
   delete ds;
   for (int i = 0; i < thread_count; i++)
   {
      delete thread_args[i*PADDING_BYTES];
   }
}

int main(int argc, char *argv[])
{
   thread_count      = DEFAULT_THREAD_COUNT;
   max_duration      = DEFAULT_TIME * 1000000;     // Duration entered in Milliseconds converted to Nanoseconds
   max_keyrange      = DEFAULT_KEYRANGE;
   ins               = DEFAULT_INS;
   del               = DEFAULT_DEL;
   con               = DEFAULT_CON;
   trial_num         = DEFAULT_TRIAL;
   max_ops           = DEFAULT_MAXOPS;

   // ---------------------------------------------------------
   // Parameters extraction
   //----------------------------------------------------------
  
   if(argc > 3){
      thread_count   = atoi(argv[1]);
      max_duration   = atoi(argv[2]) * 1000000;     // Duration entered in Milliseconds converted to Nanoseconds
      max_keyrange   = atoi(argv[3]);
      ins            = atoi(argv[4]);
      del            = atoi(argv[5]);
      con            = 100 - (ins + del);
      algo_type      = argv[6];
      trial_num      = atoi(argv[7]);
      max_ops        = atoi(argv[8]);

      printf("\nMINIBENCH:\n");
      printf(" Threads=%i\n", thread_count); 
      printf(" Max_Duration(ms)=%d\n", max_duration/1000000 );
      printf(" Max_Keyrange=%i \n", max_keyrange); 
      printf(" ins=%i\n", ins); 
      printf(" del=%i\n", del); 
      printf(" algo_type=%s\n", algo_type); 
      printf(" trial_num=%i\n", trial_num); 
      printf(" max_ops=%i\n", max_ops); 
   }
   else 
   {
      printf("\nMINIBENCH: wrong arguments.\n");
      exit(EXIT_FAILURE);
   }
   
   // ---------------------------------------------------------
   // Operation specific initializations
   //----------------------------------------------------------
   assert (thread_count <= MAX_THREADS && "thread count should not be MAX_THREADS.");
   
   // Initialize Barrier
   pthread_barrier_init (&barrier, NULL, thread_count);
   printf("Initialized Barrier\n");

   if (strcmp(algo_type, "stack_none") == 0)
   {
      bootstrapExperiment<stack_none>();
   }
//   else if(strcmp(algo_type, "lazylist_hp") == 0)
//   {
//      bootstrapExperiment<lazylist_hp>();
//   }
   else if (strcmp(algo_type, "stack_hmr") == 0)
   {
      bootstrapExperiment<stack_hmr>();
   }
   else if (strcmp(algo_type, "stack_asm_hmr") == 0)
   {
      bootstrapExperiment<stack_asm_hmr>();
   }
   else if (strcmp(algo_type, "stack_ibr_qsbr") == 0)
   {
      bootstrapExperiment<stack_ibr_qsbr>();
   }
   else if (strcmp(algo_type, "stack_ibr_rcu") == 0)
   {
      bootstrapExperiment<stack_ibr_rcu>();
   }
   else if (strcmp(algo_type, "stack_ibr") == 0)
   {
      bootstrapExperiment<stack_ibr>();
   }
   else if (strcmp(algo_type, "stack_ibr_he") == 0)
   {
      bootstrapExperiment<stack_ibr_he>();
   }
   else if (strcmp(algo_type, "stack_ibr_hp") == 0)
   {
      bootstrapExperiment<stack_ibr_hp>();
   }
   else
   {
      printf("\nMINIBENCH: wrong algo type=%s!\n", algo_type);
      exit(EXIT_FAILURE);
   }
      

   return 0;
}
