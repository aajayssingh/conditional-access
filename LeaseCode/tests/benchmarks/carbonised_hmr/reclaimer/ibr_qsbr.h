/*
 * As per IBR (ppopp2018) readme it is an efficient variant of RCU.
 * Adapting from their git.
 * 
 * MECHANISM:
 * Each retired node is associated with current epoch. This tells which epoch
 * the node was freed in, other threads running in same epoch may have priv pointers 
 * Each thread at endOp announces the current epoch it ran in at swmr location.
 * Each retiring thread scann all announced epochs and frees all records that
 * were retired before the minim scanned epoch value
 * 
 * -----------e--------e+1--------e+2-------e+3----->
 * In above timeline if min epoch is e+1 then all nodes retired in e or earlier
 * could be freed.
 * NOT RUBUST --> A delayed thread can prevent records retired in the epoch it has reserved and those in later epochs. 
 */

/* 
 * File:   ibr_qsbr.h
 * Author: mc lab (@j)
 *
 * Created on May 13, 2021, 4:15 PM
 * issue: alloc counter is inc in startOP-> wrongly calculates the number of allocated nodes.
 * fixed: alloc counter only when a node is added.
 * 
 */

#ifndef IBR_QSBR_H
#define IBR_QSBR_H

#include <list>
#include "concurrent_primitives.h"
#include "util.h"

template <typename T>
class ibr_qsbr{
private:
    const unsigned int numProcesses;
    unsigned int empty_freq;
    unsigned int epoch_freq;
    
public:
    class RCUInfo{
    public:
        T* obj;
        uint64_t epoch; //unsigned long int
        RCUInfo(T* _obj, uint64_t _epoch) : obj(_obj), epoch(_epoch) {}
    };
private:
    paddedAtomic<uint64_t> *reservations; //per thread
    padded<std::list<RCUInfo>> *retired; //per thread
    padded<uint64_t> *retire_counters; //per thread
    padded<uint64_t> *alloc_counters; //per thread
    
    PAD;
    std::atomic<uint64_t> epoch; //FIXME: may be use padded atomic as two PADS may screw up perf due to prefetch
    PAD;
            
public:
    uint64_t getEpoch()
    {
        return epoch.load(std::memory_order_acquire);
    }
    
    bool startOp(const unsigned int tid, const bool read_only = false)
    {
        bool result = true;
        return result;
    }
    
    void endOp(const unsigned int tid)
    {
        uint64_t e = epoch.load(std::memory_order_acquire);
        reservations[tid].ui.store(e, std::memory_order_seq_cst);
    }
    
    // to be called when a node is succesfully allocated.
    void incAllocCounter(const unsigned int tid)
    {
        alloc_counters[tid] = alloc_counters[tid] + 1;
#if defined (HASHTABLE) or defined (RECORD_GARBAGE)
    num_allocs.add(tid, 1);
#endif        
        //PERFISSUE: also multiple threads may consecutivly advance epoch
        //One thread advancing global epoch is fine.
//        printf("tid=%d #allocs %lld\n", tid, alloc_counters[tid].ui);

        if (alloc_counters[tid] % (epoch_freq) == 0)
        {
            epoch.fetch_add(1, std::memory_order_acq_rel);
//            printf("tid=%d epoch advanced\n", tid);
        }

    }
    
    void retire(const unsigned int tid, T *obj)
    {
        if (obj == NULL)
        {
            return;
        }
        
        //get my limbo bag
        std::list<RCUInfo> *myTrash = &(retired[tid].ui);
        
        //get current epoch and wrap the retired object with the epoch value
        //this save the info of the epoch the obj is retired in to know if it 
        //safe to be freed at the time of freeing.
        uint64_t e = epoch.load(std::memory_order_acquire);
        RCUInfo info = RCUInfo(obj, e);
        myTrash->push_back(info);
        retire_counters[tid] = retire_counters[tid] + 1;
        
        // FIXME This threshold imp[act relative perf of the reclaimer
        // could skew results if the threshold of reclamation is not same across 
        // all the reclaimers.
        //Nice Idea to put a plot in paper on freq of reclamation across all compared
        //reclaimers.
#if defined (HASHTABLE) or defined (RECORD_GARBAGE)
    num_retired.add(tid, 1);
#endif        
        if (retire_counters[tid] % empty_freq == 0)
        {
            empty(tid);
//            printf("tid=%d emptying... \n", tid);
        }        
        
    }
    
    void empty(const unsigned int tid)
    {
        uint64_t minEpoch = UINT64_MAX;
        
        //Find the minimum epoch a thread announced.
        for (int i = 0; i < numProcesses; i++)
        {
            uint64_t res = reservations[i].ui.load(std::memory_order_acquire);
            if (res < minEpoch)
            {
                minEpoch = res;
            }
        }
        
        // erase safe objects that were retired before min epoch.
        std::list<RCUInfo> *myTrash = &(retired[tid].ui);
        
//        int b4_empty_sz = myTrash->size();
        
        for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end;)
        {
            RCUInfo res = *iterator;
            if (res.epoch < minEpoch)
            {
                //return iterator corresponding to next of last erased item
                iterator = myTrash->erase(iterator);
                free(res.obj);
                total_freed.add(tid, 1);
//                this->pool->add(tid, res.obj);
            }
            else
            {
                ++iterator;
            }
        }
//        printf("tid=%d emptying: before sz=%d after sz=%d\n", tid, b4_empty_sz, myTrash->size());

        //FIXME: Can reset alloc and retire counters here 
        // to avoid wrap around.
    }
    
    ibr_qsbr(const unsigned int _numProcesses) : numProcesses(_numProcesses)
    {
        empty_freq = EMPTY_FREQ; //30; //100
        epoch_freq = EPOCH_FREQ; //150; //50 //150;
        retired = new padded<std::list<RCUInfo>>[numProcesses];
        reservations = new paddedAtomic<uint64_t>[numProcesses];        
        retire_counters = new padded<uint64_t>[numProcesses];
        alloc_counters = new padded<uint64_t>[numProcesses];
        
        for (int i = 0; i < numProcesses; i++)
        {
            reservations[i].ui.store(UINT64_MAX, std::memory_order_release);
            retired[i].ui.clear();
            retire_counters[i].ui = 0;
            alloc_counters[i].ui = 0;            
        }
        epoch.store(0, std::memory_order_release);
    }
    
    ~ibr_qsbr(){
        
//        assert(alloc_counters[0] > 5 && "not enough epochs");
        
        unsigned int total_retired = 0;
        unsigned int total_allocated = 0;
//        unsigned int total_reclaimed = 0;
        
        printf("\n pt_retired=");
        for (int i = 0; i < numProcesses; i++)
        {
            printf("%lu ", retire_counters[i].ui);        
        }
        printf("\n pt_allocated=");
        for (int i = 0; i < numProcesses; i++)
        {
            total_retired += retire_counters[i].ui;
            total_allocated += alloc_counters[i].ui;
            printf("%lu ", alloc_counters[i].ui);        
        }
        printf("\n");

#ifndef HASHTABLE
        printf("Epoch=%lu \n", getEpoch());        
        printf("total_retired=%u \n", total_retired);
        printf("total_allocated=%u \n", total_allocated);
        printf("total_freed=%lld \n", total_freed.getTotal());

        printf("total_retries=%lld \n", num_retries.getTotal());   
        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());            
#endif
        delete [] retired;
        delete [] reservations;
        delete [] retire_counters;
        delete [] alloc_counters;
    }
};

#endif /* IBR_QSBR_H */

