/*

Copyright 2017 University of Rochester

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 

 */



#ifndef IBR_HP_H
#define IBR_HP_H

#include <queue>
#include <list>
#include <vector>
#include <atomic>
#include "concurrent_primitives.h"

#define MAX_HAZARDPTRS_PER_THREAD 2

template<class T>
class ibr_hp {
private:
    const unsigned int numProcesses;
    unsigned int slotsPerThread;
    unsigned int empty_freq;
    
    padded<uint64_t> *retire_counters; //per thread
    paddedAtomic<T*>* slots;
    padded<int>* cntrs;
    padded<std::list<T*>>*retired; // TODO: use different structure to prevent malloc locking....

    void empty(const unsigned int tid) {
        std::list<T*>* myTrash = &(retired[tid].ui);
        for (typename std::list<T*>::iterator iterator = myTrash->begin(), end = myTrash->end(); iterator != end;) {
            bool danger = false;
            auto ptr = *iterator;
            for (int i = 0; i < numProcesses * slotsPerThread; i++) {
                if (ptr == slots[i].ui) {
                    danger = true;
                    break;
                }
            }
            if (!danger) {
                free(ptr);
                total_freed.add(tid, 1);

                iterator = myTrash->erase(iterator);
            } else {
                ++iterator;
            }
        }
        return;
    }

public:

    T* read(const unsigned int tid, int idx, std::atomic<T*>& obj) {
        T* ret;
        T* realptr;
        while (true) {
            ret = obj.load(std::memory_order_acquire);
            realptr = (T*) ((size_t) ret & 0xfffffffffffffffc);
            reserve(realptr, idx, tid);
            if (ret == obj.load(std::memory_order_acquire)) {
                return ret;
            }
        }
    }

    void reserve(T* ptr, int slot, const unsigned int tid) {
        slots[tid * slotsPerThread + slot] = ptr;
    }

    void clearSlot(int slot, const unsigned int tid) {
        slots[tid * slotsPerThread + slot] = NULL;
    }

    void clear(const unsigned int tid) {
        for (int i = 0; i < slotsPerThread; i++) {
            slots[tid * slotsPerThread + i] = NULL;
        }
    }

    inline void incAllocCounter(const unsigned int tid)
    {
        num_allocs.add(tid, 1);
    }
    
    void retire(const unsigned int tid, T* ptr) {
        if (ptr == NULL) {
            return;
        }
        std::list<T*>* myTrash = &(retired[tid].ui);
        // for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
        // 	assert(*it !=ptr && "double retire error");
        // }
        myTrash->push_back(ptr);
        if (cntrs[tid] == empty_freq) {
            cntrs[tid] = 0;
            empty(tid);
            timeslimbofreed.add(tid, 1);
        }
        cntrs[tid].ui++;
        retire_counters[tid] = retire_counters[tid] + 1;
        #if defined (HASHTABLE) or defined (RECORD_GARBAGE)
            num_retired.add(tid, 1);
        #endif
    }

    ibr_hp(const unsigned int _numProcesses, const unsigned int maxhp) : numProcesses(_numProcesses), slotsPerThread(maxhp){
        empty_freq = EMPTY_FREQ; //30; //100
//        this->slotsPerThread = MAX_HAZARDPTRS_PER_THREAD;
        slots = new paddedAtomic<T*>[numProcesses * slotsPerThread];
        for (int i = 0; i < numProcesses * slotsPerThread; i++) {
            slots[i] = NULL;
        }
        retired = new padded<std::list < T*>>[numProcesses];
        cntrs = new padded<int>[numProcesses];
        retire_counters = new padded<uint64_t>[numProcesses];

        for (int i = 0; i < numProcesses; i++) {
            cntrs[i] = 0;
            retired[i].ui = std::list<T*>();
            retire_counters[i].ui = 0;
        }
    }

    ~ibr_hp() {
        unsigned int total_retired = 0;
//        unsigned int total_allocated = 0;
        unsigned int total_reclaimed = 0;

        printf("\n pt_retired=");
        for (int i = 0; i < numProcesses; i++) {
            printf("%lu ", retire_counters[i].ui);
            total_retired += retire_counters[i].ui;
        }
        printf("\n");
#ifndef HASHTABLE
        printf("Epoch=%lld \n", timeslimbofreed.getTotal());
        printf("total_retired=%u\n", total_retired);
        printf("total_allocated=%lld\n", num_allocs.getTotal());
        printf("total_freed=%lld\n", total_freed.getTotal());
        
        printf("total_retries=%lld \n", num_retries.getTotal());   
        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());            
#endif
        
        delete [] retired;
        delete [] retire_counters;
    }
};


#endif
