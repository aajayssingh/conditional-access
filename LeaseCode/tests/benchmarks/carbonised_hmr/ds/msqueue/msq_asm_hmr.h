#ifndef MSQ_ASM_HMR_H
#define MSQ_ASM_HMR_H

#include "util.h"


//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

DebugData freed_address;

struct asm_node 
{
    unsigned int value;
    bool mark; //to guarantee that a freed asm_node is modified so that watchset validation recognises.
    struct asm_node* next;
}; //__attribute__((aligned(PADDING_BYTES)));



class msq_asm_hmr
{
private:
    const unsigned int num_threads;
//    struct asm_node {
//        unsigned int volatile value;
//        asm_node* volatile	next;
//
//        asm_node(unsigned int inValue) : value(inValue), next(NULL) {}
//    };
PAD;
asm_node* _head;
asm_node* _tail;
asm_node* sentinal_node; // at the start _head points to this dummy asm_node with value 0
PAD;

    asm_node* allocateAndInit(const unsigned int val)
    {
        struct asm_node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(asm_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff);
        //init posix mem allocated asm_node
        nd->value = val;
        nd->mark = 0;
        //        printf("adding tid(%u) key=%u val=%u \n", tid, key, nd->value);
        nd->next = NULL;
        
        return nd;
    }

public:
   msq_asm_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("asm_node size=%lu \n", sizeof(asm_node));

        sentinal_node = allocateAndInit(0);
        _head = _tail = sentinal_node;
   }
   ~msq_asm_hmr()
   {
        asm_node* next_node = _head;
        asm_node* temp_node;
        while( next_node != NULL) // include sentinel asm_node as well
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::msq_asm_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());   
   }
 
    unsigned int peek(const unsigned int tid) 
    {
        return _head->value;
    }
   
   //push is for enqueue
   bool push(const unsigned int tid, const unsigned int key) 
   {
        bool success = false;
        asm_node* nd = allocateAndInit(key);
        asm_node* tail;
//        num_allocs.add(tid, 1);
        do
        {
            //read the current tail
            CarbonAddToWatchset((void*) _tail, sizeof(struct asm_node));
            tail = _tail;
            if ((unsigned int long)tail == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
                num_retries.add(tid, 1);
                continue; 
            }
            //            asm_node* next = tail->next;
            asm_node* next;
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (next)
                : "m" (tail->next)
                ); 
            if ((unsigned int long)next == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
                num_retries.add(tid, 1);
                continue; 
            }
            
            
            if (tail == _tail)
            {
                if (next == NULL)
                {
                    success =  __sync_bool_compare_and_swap(&tail->next, next, nd);
                    if (success)
                        break;
                }
                else
                {
                    //advance tail if it point to old tail 
                    //and new nodes have been added after.
                    __sync_bool_compare_and_swap(&_tail, tail, next);
                }
            }
//            num_retries.add(tid, 1);
            CarbonRemoveAllFromWatchset();
        } while(true);
        
        //advance the _tail
        __sync_bool_compare_and_swap(&_tail, tail, nd);
        CarbonRemoveAllFromWatchset();
        return success;
    }

    unsigned int pop(const unsigned int tid)
    {
        bool success = false;
        unsigned int h_val=0;
        asm_node *next, *head, *tail;
        do {

                CarbonAddToWatchset((void*) _head, sizeof(struct asm_node));
                head = _head;
                if ((unsigned int long)head == 0xffffffff)
                { 
                    CarbonRemoveAllFromWatchset();
                    num_retries.add(tid, 1);
                    continue; 
                }

                CarbonAddToWatchset((void*) _tail, sizeof(struct asm_node));
                tail = _tail;
                if ((unsigned int long)tail == 0xffffffff)
                { 
                    CarbonRemoveAllFromWatchset();
                    num_retries.add(tid, 1);
                    continue; 
                }                

                next = head->next;
                if(head == _head)
                {
                    if (head == tail)
                    {
                        if (next == NULL)
                        {
                            CarbonRemoveAllFromWatchset();
                            return false;
                        }
                        else
                           __sync_bool_compare_and_swap(&_tail, tail, next); 
                    }
                    else
                    {
                        success =  __sync_bool_compare_and_swap(&_head, head, next);
                        if (success)
                        {
                            h_val = head->value;
                            head->mark = 1;
                            break;
                        }
                    }
                }
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
        } while(true);
        free(head);
        CarbonRemoveAllFromWatchset();
        return h_val;
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      asm_node* next_node = _head;

      while( next_node != NULL){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      asm_node* next_node = _head;

      while( next_node != NULL){
         size ++;
         next_node = next_node->next;
      }
//      printDebuggingDetails();
//      assert(size != 0);
//      printf("size = %lld", size);

      return size -1; //removing sentinel asm_node
   }  

    void printDebuggingDetails()
    {
        asm_node* next_node = _head;

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* MSQ_ASM_HMR_H */
