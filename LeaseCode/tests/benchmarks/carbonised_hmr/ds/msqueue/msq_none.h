/* 
 * File:   msq_none.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef MSQ_NONE_H
#define MSQ_NONE_H

#include "util.h"

class msq_none
{
private:
    const unsigned int num_threads;
    struct node {
        unsigned int volatile value;
        node* volatile	next;

        node(unsigned int inValue) : value(inValue), next(NULL) {}
    };
PAD;
node* _head;
node* _tail;
node* sentinal_node; // at the start _head points to this dummy node with value 0
PAD;

public:
   msq_none(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("node size=%lu \n", sizeof(node));

        sentinal_node = new node(0);
        _head = _tail = sentinal_node;
   }
   ~msq_none()
   {
        node* next_node = _head;
        node* temp_node;
        while( next_node != NULL) // include sentinel node as well
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::msq_none\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());   
   }
 
    unsigned int peek(const unsigned int tid) 
    {
        return _head->value;
    }
   
   //push is for enqueue
   bool push(const unsigned int tid, const unsigned int key) 
   {
        bool success = false;
        node* nd = new node(key);
        node* tail;
//        num_allocs.add(tid, 1);
        do
        {
            //read the current tail
            tail = _tail;
            node* next = tail->next;
            if (tail == _tail)
            {
                if (next == NULL)
                {
                    success =  __sync_bool_compare_and_swap(&tail->next, next, nd);
                    if (success)
                        break;
                }
                else
                {
                    //advance tail if it point to old tail 
                    //and new nodes have been added after.
                    __sync_bool_compare_and_swap(&_tail, tail, next);
                }
            }
//            num_retries.add(tid, 1);
        } while(true);
        
        //advance the _tail
        __sync_bool_compare_and_swap(&_tail, tail, nd);
        return success;
    }

    unsigned int pop(const unsigned int tid)
    {
        bool success = false;
        unsigned int h_val=0;
        node *next, *head, *tail;
        do {
                head = _head;
                tail = _tail;
                next = head->next;
                if(head == _head)
                {
                    if (head == tail)
                    {
                        if (next == NULL)
                            return false;
                        else
                           __sync_bool_compare_and_swap(&_tail, tail, next); 
                    }
                    else
                    {
                        success =  __sync_bool_compare_and_swap(&_head, head, next);
                        if (success)
                        {
                            h_val = head->value;
                            break;
                        }
                    }
                }
//                num_retries.add(tid, 1);
        } while(true);
//        free(head);
        return h_val;
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node* next_node = _head;

      while( next_node != NULL){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node* next_node = _head;

      while( next_node != NULL){
         size ++;
         next_node = next_node->next;
      }
//      printDebuggingDetails();
//      assert(size != 0);
//      printf("size = %lld", size);

      return size -1; //removing sentinel node
   }  

    void printDebuggingDetails()
    {
        node* next_node = _head;

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* MSQ_NONE_H */

