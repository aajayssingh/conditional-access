/* 
 * File:   asm_vwrite.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef ASM_VWRITE_H
#define ASM_VWRITE_H

#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

class asm_vwrite
{
private:
    unsigned int** counter_array;
    PAD;
    const unsigned int num_threads;


public:
   asm_vwrite(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("ctor::asm_vwrite\n");

        for(int i=0; i < 32; i++)
        {
           if( posix_memalign((void**) &counter_array[i], 64, sizeof(unsigned  int) ) != 0){
              printf("Error: posix_memalign could not allocate memory!!! \n");
              exit(EXIT_FAILURE);
           }
           
           *(counter_array[i]) = 0;
//           printf("counter_array[%i]=%lx *(counter_array[i])=%u\n", i, counter_array[i], *(counter_array[i]));
        }
   }
   ~asm_vwrite()
   {
        printf("dtor::asm_vwrite\n");
        unsigned int tsum = 0;
        for(int i=0; i < 32; i++)
        {
            tsum += *(counter_array[i]);
            printf("counter_array[%i]=%lx *(counter_array[i])=%u\n", i, counter_array[i], *(counter_array[i]));
        }
        printf("tsum=%u \n", tsum);
        
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());  
        
        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());  
        
   }
 
    bool contains(const unsigned int tid, const unsigned int key)
    {
        unsigned int res = 0;
        unsigned int keycopy = key;
        int sharedSlots = 1;
        while(sharedSlots--)
        {
//            printf("tid=%u chose key=%u sharedSlots(%d)\n", tid, keycopy, sharedSlots);
            retry:
    //b        
//            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//            :"=r" (res)
//            : "m" (*(counter_array[keycopy]))
//            ); 
//
//            if ((unsigned int long)res == 0xffffffff)
//            { 
//                CarbonRemoveAllFromWatchset();
////                num_vreadfails.add(tid, 1);
//                goto retry; 
//            }
            res = *(counter_array[keycopy]);

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
              :"=m" (*(counter_array[keycopy]))
              : "r" (res+1)
               );

            if (!CarbonGetVOPStatus())
            { 
                CarbonRemoveAllFromWatchset();
                num_vwritefails.add(tid, 1);
                goto retry; 
            }

    //a        
    //        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
    //          :"=m" (*(counter_array[tid]))
    //          : "r" (res)
    //           );    

            CarbonRemoveAllFromWatchset();
            keycopy = (keycopy+1)%32;
        }

        return true;
    }
    
//#ifdef EXPA1
//    bool contains(const unsigned int tid, const unsigned int key)
//    {
//        unsigned int res = 0;
//        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//        :"=r" (res)
//        : "m" (*(counter_array[tid]))
//        ); 
//
//        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//          :"=m" (*(counter_array[tid]))
//          : "r" (res)
//           );
//
//        return true;
//    }
//#elif EXPA2
//    bool contains(const unsigned int tid, const unsigned int key)
//    {
//        unsigned int res = 0;
////        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
////        :"=r" (res)
////        : "m" (*(counter_array[tid]))
////        ); 
//
//        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//          :"=m" (*(counter_array[tid]))
//          : "r" (res)
//           );
//
////        CarbonRemoveAllFromWatchset();
//        return true;
//    }
//#endif
   
    bool insert(const unsigned int tid, const unsigned int key)
    {
        unsigned int res = 0;
        for(int i=0; i < 32; i++)
        {
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
              :"=m" (*(counter_array[i]))
              : "r" (res)
               );     
            CarbonRemoveAllFromWatchset();
        }
        
        return false;
    }

    bool erase(const unsigned int tid, const unsigned int key)
    {
        return false;
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;

      return size;
   }  

    void printDebuggingDetails()
    {

    }   

};

#endif /* ASM_VWRITE_H */

