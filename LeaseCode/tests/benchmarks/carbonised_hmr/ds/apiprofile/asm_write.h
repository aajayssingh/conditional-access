/* 
 * File:   asm_write.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef ASM_WRITE_H
#define ASM_WRITE_H

#include "util.h"

#define ASF_NOP\
   ".byte 0x90\n\t"

class asm_write
{
private:
    unsigned int** counter_array;
    PAD;
    const unsigned int num_threads;


public:
   asm_write(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("ctor::asm_write\n");

        for(int i=0; i < 32; i++)
        {
           if( posix_memalign((void**) &counter_array[i], 64, sizeof(unsigned  int) ) != 0){
              printf("Error: posix_memalign could not allocate memory!!! \n");
              exit(EXIT_FAILURE);
           }
           
           *(counter_array[i]) = 0;
//           printf("counter_array[%i]=%lx *(counter_array[i])=%u\n", i, counter_array[i], *(counter_array[i]));
        }
   }
   ~asm_write()
   {
        printf("dtor::asm_write\n");
        unsigned int tsum = 0;
        for(int i=0; i < 32; i++)
        {
           tsum += *(counter_array[i]);
           printf("counter_array[%i]=%lx *(counter_array[i])=%u\n", i, counter_array[i], *(counter_array[i]));
        }
        printf("tsum=%u \n", tsum);
        
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal()); 

        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());          
   }
 
    bool contains(const unsigned int tid, const unsigned int key)
    {
        unsigned int res = 0;

        int sharedSlots = 1;
        unsigned int keycopy = key;
        while(sharedSlots--)
        {
            retry:
            unsigned int readval = *(counter_array[keycopy]);
//            __sync_fetch_and_add((counter_array[keycopy]), 1);

            if(! __sync_bool_compare_and_swap((counter_array[keycopy]), readval, readval+1))
            {
                num_vwritefails.add(tid, 1);
                goto retry;
            }
            keycopy = (keycopy+1)%32;
        }
//            __asm__ __volatile__ ("mov %1, %0"
//            :"=m" (*(counter_array[tid]))
//            : "r" (res)
//             );     
        
        return true;
    }
   
//    bool contains(const unsigned int tid, const unsigned int key)
//    {
//        unsigned int res = 0;
////        __asm__ __volatile__ ("mov %1, %0"
////            :"=r" (res)
////            : "m" (*(counter_array[tid]))
////            ); 
//        
//        __asm__ __volatile__ ("mov %1, %0"
//          :"=m" (*(counter_array[tid]))
//          : "r" (res)
//           );   
//
//        return true;
//    }
    
    bool insert(const unsigned int tid, const unsigned int key)
    {
        unsigned int res = 0;
        for(int i=0; i < 32; i++)
        {
            __asm__ __volatile__ ("mov %1, %0"
              :"=m" (*(counter_array[i]))
              : "r" (res)
               );     
        }
        
        return false;
    }

    bool erase(const unsigned int tid, const unsigned int key)
    {
        return false;
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;

      return size;
   }  

    void printDebuggingDetails()
    {

    }   

};

#endif /* ASM_WRITE_H */

