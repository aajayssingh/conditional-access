/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hashtable_ibr_hp.h
 * Author: ajay
 *
 * Created on December 24, 2021, 1:23 AM
 */

#ifndef HASHTABLE_IBR_HP_H
#define HASHTABLE_IBR_HP_H

#include "lazylist_ibr_hp.h"

class hashtable_ibr_hp
{
private:
    lazylist_ibr_hp** htb;
    const unsigned int num_threads;
    unsigned int capacity;


public:
   hashtable_ibr_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("ctor::hashtable_ibr_hp\n");
        capacity = HASHTABLE_CAPACITY;
        initHPReclaimer(num_threads);
        
        if(posix_memalign((void**) &htb, 64, sizeof(lazylist_ibr_hp*)*capacity ) != 0){
           printf("Error: posix_memalign could not allocate memory!!! \n");
           exit(EXIT_FAILURE);
        }
        
        for(int i=0; i < capacity; i++)
        {
           htb[i] = new lazylist_ibr_hp(num_threads, min_key, max_key);
//           printf ("htb[i]=%p htb[i]->head=%p\n", htb[i], htb[i]->head);
        }        
   }
   ~hashtable_ibr_hp()
   {
        printf("dtor::hashtable_ibr_hp\n");
//        printDebuggingDetails();
        for(int i=0; i < capacity; i++)
        {
           delete htb[i];
        }        
        free (htb);
        
//        printDebuggingDetails();
        printf("Epoch=%lu \n", timeslimbofreed.getTotal());

        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   

        printf("total_retries=%lld \n", num_retries.getTotal());     
        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());        

        deInitHPReclaimer();        
   }
 
    bool contains(const unsigned int tid, const unsigned int key)
    {
        bool res = false;
        //get bucket
        unsigned int bucket_idx = murmur3(key) % capacity;
//        printf("bucket_idx=%d\n", bucket_idx);
        res = htb[bucket_idx]->contains(tid, key);
        return res;
    }
   
    bool insert(const unsigned int tid, const unsigned int key)
    {
        bool res = false;
        //get bucket
        unsigned int bucket_idx = murmur3(key) % capacity;
//        printf("ins bucket_idx=%d\n", bucket_idx);
        res = htb[bucket_idx]->insert(tid, key);
        return res;
    }

    bool erase(const unsigned int tid, const unsigned int key)
    {
        bool res = false;
        //get bucket
        unsigned int bucket_idx = murmur3(key) % capacity;
//        printf("del bucket_idx=%d\n", bucket_idx);
        res = htb[bucket_idx]->erase(tid, key);
        return res;
    }
    
    long long getSumOfKeys()
    {
        long long sum = 0;
        for(int i=0; i < capacity; i++)
        {
            sum += htb[i]->getSumOfKeys();
        }        
        return sum;
    }

   long long getDSSize()
   {
        long long size = 0;
        for(int i=0; i < capacity; i++)
        {
            size += htb[i]->getDSSize();
        }        
        return size;
   }  

    void printDebuggingDetails()
    {
        for(int i=0; i < capacity; i++)
        {
            printf("BUCKET:%d %u\n", i, htb[i]->getDSSize());
//            htb[i]->printDebuggingDetails();
        }        
    }   

};
#endif /* HASHTABLE_IBR_HP_H */

