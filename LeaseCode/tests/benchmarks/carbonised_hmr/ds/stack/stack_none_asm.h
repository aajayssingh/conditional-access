/* 
 * File:   stack_none_asm.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_NONE_ASM_H
#define STACK_NONE_ASM_H

#include "util.h"

class stack_none_asm
{
private:
    const unsigned int num_threads;
    struct node {
        unsigned int volatile value;
        node* volatile	next;

        node(unsigned int inValue) : value(inValue), next(NULL) {}
    };
PAD;
node* _top;
node* sentinal_node; // at the start _top points to this dummy node with value 0
PAD;

public:
   stack_none_asm(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        printf("node size=%lu \n", sizeof(node));

        sentinal_node = new node(0);
        _top = sentinal_node;
   }
   ~stack_none_asm()
   {
        node* next_node = _top;
        node* temp_node;
        while( next_node != NULL) // include sentinel node as well
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::stack_none_asm\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());   
   }
 
    unsigned int peek(const unsigned int tid) 
    {
        unsigned int top_value = 0;
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (top_value)
            : "m" (_top->value)
            ); 
        return top_value;
    }
   
   bool push(const unsigned int tid, const unsigned int key) 
   {
        bool success = false;
        node* nd = new node(key);
        num_allocs.add(tid, 1);
        do
        {

            node* curr_top;
            __asm__ __volatile__ ("mov %1, %0"
                :"=r" (curr_top)
                : "m" (_top)
                ); 
            
            __asm__ __volatile__ ("mov %1, %0"
                :"=r" (nd->next)
                : "m" (curr_top)
                ); 

            // Perform CAS Operation
            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);

            if(success)
            {
                return true;
            }
            num_retries.add(tid, 1);
        } while(true);
    }

    unsigned int pop(const unsigned int tid)
    {
        bool success = false;
        do {
                node* curr_top;
                __asm__ __volatile__ ("mov %1, %0"
                    :"=r" (curr_top)
                    : "m" (_top)
                    ); 

                if(curr_top == sentinal_node)
                {
                    return false;   // TODO: This should be a NULL value
                }
                // Perform CAS Operation
                success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->next);

                if( success ) 
                {
                    return curr_top->value;
                }
                num_retries.add(tid, 1);
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node* next_node = _top;

      while( next_node != sentinal_node){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node* next_node = _top;

      while( next_node != sentinal_node){
         size ++;
         next_node = next_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node* next_node = _top;

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_NONE_ASM_H */

