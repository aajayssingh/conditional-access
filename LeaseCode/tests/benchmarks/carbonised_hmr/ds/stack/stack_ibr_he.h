/* 
 * File:   stack_ibr_he.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_IBR_HE_H
#define STACK_IBR_HE_H

#include "ibr_he.h"
#define MAX_HE 1

struct node_he; //fwd declaration to satiate complaining compiler.
ibr_he<struct node_he> *rec_he = NULL;

struct node_he{
    unsigned int value;
    std::atomic<struct node_he*> next;

    //rec based members
    uint64_t birth_epoch;
    uint64_t retire_epoch;

    node_he(unsigned int key): value(key)
    {
        next = NULL;
        assert(rec_he !=  NULL);
        
        birth_epoch = rec_he->getEpoch();
        retire_epoch = UINT64_MAX;
    }
};


class stack_ibr_he
{
private:
    const unsigned int num_threads;
    PAD;
    std::atomic<struct node_he*> _top;
    PAD;
    node_he* sentinal_node; // at the start _top points to this dummy node_he with value 0
//    PAD;
//    rec_he<struct node_he> *rec_he;

public:
   stack_ibr_he(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        rec_he = new ibr_he<struct node_he>(num_threads, MAX_HE);
 
        sentinal_node = new node_he(0);
        _top.store(sentinal_node, std::memory_order_relaxed);
   }
   ~stack_ibr_he()
   {
        node_he* next_node = _top.load(std::memory_order_relaxed);
        node_he* temp_node;
        while( next_node != NULL) // include sentinel node_he as well
        {
            temp_node = next_node;
            next_node = next_node->next.load(std::memory_order_relaxed);
            free(temp_node);
        }
       
        printf("dtor::stack_ibr_he\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
//        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete rec_he;
   }
   
   unsigned int peek(const unsigned int tid) 
   {
//        rec_he->startOp(tid);    
        
        node_he* curr_top = rec_he->read(tid, 0, _top);
        unsigned int top_val = curr_top->value;
        rec_he->clear(tid);
        return top_val;
    }
   
   bool push(const unsigned int tid, const unsigned int key) 
   {
//        rec_he->startOp(tid);    
        bool success = false;
        node_he* nd = new node_he(key);
//        num_allocs.add(tid, 1);
        rec_he->incAllocCounter(tid);
        do
        {
            node_he* curr_top = rec_he->read(tid, 0, _top);
            nd->next = curr_top;

            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);
            success = _top.compare_exchange_strong(curr_top, nd);
            if(success)
            {
                rec_he->clear(tid);
                return true;
            }
            num_retries.add(tid, 1);
        } while(true);
    }

    unsigned int pop(const unsigned int tid)
    {
//        rec_he->startOp(tid);    

        bool success = false;
        do {
                node_he* curr_top = rec_he->read(tid, 0, _top);
                if(curr_top == sentinal_node)
                {
                    rec_he->clear(tid);
                    return false;   //empty stack
                }
                // Perform CAS Operation
                success =  _top.compare_exchange_strong(curr_top, curr_top->next);

                if( success ) 
                {
                    unsigned int top_val = curr_top->value;
                    rec_he->retire(tid, curr_top);
                    rec_he->clear(tid);
                    return top_val;
                }
                num_retries.add(tid, 1);
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node_he* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         sum = sum + next_node->value;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node_he* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         size ++;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node_he* next_node = _top.load(std::memory_order_relaxed);

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next.load(std::memory_order_relaxed);
        }
        printf("\n");
    }   

};

#endif /* STACK_IBR_HE_H */

