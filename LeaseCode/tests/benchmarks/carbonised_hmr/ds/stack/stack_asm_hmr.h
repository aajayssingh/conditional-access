/* 
 * File:   stack_asm_hmr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 * NOTE: Works only with empty watchset based vread/vwrite validation NOT with addres based validation.
 * With addres based validation peek may return value from delete node. As vread in peek may succeed if addr of a node added after freeing of the node.
 */

#ifndef STACK_ASM_HMR_H
#define STACK_ASM_HMR_H

#include "lock.h"
#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

DebugData freed_address;

struct asm_node 
{
    unsigned int value;
    bool mark; //to guarantee that a freed node is modified so that watchset validation recognises.
    struct asm_node* next;
}; //__attribute__((aligned(PADDING_BYTES)));

        
class stack_asm_hmr
{
private:
    const unsigned int num_threads;
    PAD;
    struct asm_node* _top_sentinel;
    struct asm_node* _bottom_sentinel;
    PAD;
    ArrayList<asm_node> **retired;   
    
    const unsigned int min_key;
    const unsigned int max_key;
    
    asm_node* allocateAndInit(const unsigned int val)
    {
        struct asm_node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(asm_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff);
        //init posix mem allocated node
        nd->value = val;
        nd->mark = 0;
        //        printf("adding tid(%u) key=%u val=%u \n", tid, key, nd->value);
        nd->next = NULL;
        
        return nd;
    }

public:
   stack_asm_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key), max_key(max_key)
   {

        printf("node size=%lu \n", sizeof(asm_node));

        _bottom_sentinel = allocateAndInit(min_key);
        _top_sentinel = allocateAndInit(max_key);
            
        _top_sentinel->next = _bottom_sentinel;
        
        printDebuggingDetails();

//        retired = new ArrayList<asm_node>*[num_threads];
//        for (int tid=0;tid<num_threads;++tid) {
//            retired[tid] = new ArrayList<asm_node>(5000);
//        }
    }
   ~stack_asm_hmr()
   {
        struct  asm_node* next_asm_node = _top_sentinel;
        struct  asm_node* temp_asm_node;
        while( next_asm_node != NULL)
        {
            temp_asm_node = next_asm_node;
            next_asm_node = next_asm_node->next;
            free(temp_asm_node);
        }
       
        printf("dtor::stack_asm_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   
        printf("total_retries=%lld \n", num_retries.getTotal());        
   }
  
   void findAddr(asm_node* p)
   {
        printf("\n\n*******");

        for (unsigned int tid=0;tid<num_threads;++tid) {
            printf("tid=%d ", tid);
            for (int ix=0;ix<retired[tid]->size();++ix)
            {
//                printf ("%p ", retired[tid]->get(ix));
                if(retired[tid]->get(ix) == p)
                {
                    printf("found %p\n", p);
                    break;
                }
            }
            printf("\n");
        }
        printf("\n\n*******");       
   }
    
   
   //BUG: Why peek accesses a deleted node?
   //
    unsigned int peek(const unsigned int tid) 
    {
        do{
            asm_node* firstasm_node;
            unsigned int top_value = 0;
            int mark_value = 0;
            
            //read add to ws the sentinel TOP and read its next field which is TOP.
            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct asm_node));
            
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (firstasm_node)
                : "m" (_top_sentinel->next)
                ); 
            if ((unsigned int long)firstasm_node == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
                continue; 
            }
            
            if(firstasm_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // STack empty
            }

            //else read the TOP node's value
            CarbonAddToWatchset((void*) firstasm_node, sizeof(struct asm_node));
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (top_value)
                : "m" (firstasm_node->value)
                ); 

            if ((unsigned int long)top_value == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
//                printf("tid=%u read concurrently popped node val(%llu) restarting...\n", tid, top_value);
                continue; 
            }
            
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (mark_value)
                : "m" (firstasm_node->mark)
                ); 

            if ((unsigned int long)mark_value == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
//                printf("tid=%u read concurrently popped node val(%llu) restarting...\n", tid, top_value);
                continue; 
            }
            
//            if (mark_value == 1)
//            {
//                printf("tid=%u val(%u) mark(%d) firstasm_node(%p)\n", tid, top_value, mark_value, firstasm_node);
//                findAddr(firstasm_node);
//                fflush(stdout);
//            }
//            assert (mark_value != 1);
//            assert (top_value <= 100);
            
            CarbonRemoveAllFromWatchset();
            return top_value;
        }while (true);  
    }

   //NOTE: The way VREAD works is you add a node to ws and then you vread its field.
   // vreading the node itself fails validation. 
   // Later may check whether that is correct behaviour of API spec or not.
   // Therefore I cannt validate top and have to use top->next as real pointer to top.
    bool push(const unsigned int tid, const unsigned int key) 
    {
        struct asm_node* nd;
//        if(posix_memalign( (void**)&nd, 64, sizeof(asm_node)) != 0)
//        {
//            printf("Error: posix_memalign could not allocate memory!!! \n");
//            exit(EXIT_FAILURE);
//        }
//        
//        assert (((unsigned int long)nd) != 0xffffffff);
//        //init posix mem allocated node
//        nd->value = key;
//        nd->mark = 0;
//        //        printf("adding tid(%u) key=%u val=%u \n", tid, key, nd->value);
//        nd->next = NULL;        

        nd = allocateAndInit(key);
        num_allocs.add(tid, 1);

        do
        {
            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct asm_node));

            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (nd->next)
                : "m" (_top_sentinel->next)
                ); 
            if ((unsigned int long)nd->next == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
                continue; 
            }
    
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (_top_sentinel->next)
                          : "r" (nd)
                           );      
            int res = CarbonGetVOPStatus();
            if (res)//succeeded
            {
               CarbonRemoveAllFromWatchset();
//               printf("tid:%u key:%u PUSHING %p \n", tid, key, nd);
//               fflush(stdout);
               return true;
            }

            CarbonRemoveAllFromWatchset();
//            num_retries.add(tid, 1);
        }while(true);
     }
    
    //returs value in node. 0 < value < max_key
    unsigned int pop(const unsigned int tid)
    {
        do
        {
//            printf("tid=%u pop\n", tid);
            asm_node* firstasm_node;
            asm_node* secasm_node;

            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct asm_node));
            
            //read _top_sentinel->next in firstasm_node
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (firstasm_node)
                : "m" (_top_sentinel->next)
                ); 
            if ((unsigned int long)firstasm_node == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
                continue; 
            }
            
            if(firstasm_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // stack is empty.
            }            
            
            //else read the next field of firstasmnode
            CarbonAddToWatchset((void*) firstasm_node, sizeof(struct asm_node));
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (secasm_node)
                : "m" (firstasm_node->next)
                ); 
            if ((unsigned int long)secasm_node == 0xffffffff)
            { 
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
                continue; 
            }

            //Now write top.next= secasmnode
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (_top_sentinel->next)
                          : "r" (secasm_node)
                           );      
            int res = CarbonGetVOPStatus();
            unsigned int deleted_val;
            if (res) //succeeded in unlinking the firstnode
            {

                // Here, only this tid should have access to firstnode as it has been unlinked
                // by tid. Other push will not have firstasmnode in watchset.
                // other peeks could have it in watchset to read the value of firstasmnode.
                // So either peek or this marking in pop will succeed. Meaning marking could fail due to concurrent peeks.
                // BUG: But marking should not fail due to concurrent push ---> But this is happenning.
                // two pop also could not cause fail in marking as only one should succed the unlink and this the succeding one
                // will invalidate the watchset of others causing others to restart.
                        
//                printf("tid:%u GONNA mark %p\n", tid, firstasm_node);
//                fflush(stdout);
                // When concurrent PUSH are present marking the unlinked node in POP fails.
                // Happens because somehow firstnode also gets into downgrade watchset due to concurrent PUSH.
                // Solution: remove all from watchset to clear watchsets and then add the unlinked node again and retry marking.
                // Marking is imp to avoid peek reading a deleted node's garbage data and hence failing max_key assert
//                int cntr = 0;
//                while (true){
//                    bool markbit=1;
//                    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//                      :"=m" (firstasm_node->mark)
//                      : "r" (markbit)
//                       );      
//                    int setmark_res = CarbonGetVOPStatus();
//                    if (!setmark_res)
//                    {
////                        printf("tid:%u FAILED to mark %p %u %d\n", tid, firstasm_node, firstasm_node->value, firstasm_node->mark);
////                        fflush(stdout);
//                        if (cntr > 100)
//                        {
//                            printf("tid:%u FAILED to mark %p %u %d cntr=%d\n", tid, firstasm_node, firstasm_node->value, firstasm_node->mark, cntr);
////                            printf("cntr=%d\n", cntr);
//                            fflush(stdout);
//                            assert(cntr == 0 && "marking failed multiple times");
//                        }
//                        CarbonRemoveAllFromWatchset();
//                        CarbonAddToWatchset((void*) firstasm_node, sizeof(struct asm_node));
//                        cntr++;
//                        continue;
////                        assert(0);
//                    }
//
////                    printf("tid:%u SUCCESS to mark %p\n", tid, firstasm_node);
//                    break;
//                }

                // since no node will have access to this node it is safe to write directly. This write will
                // invalidate this address at all other cores causing concurrent peek to fail.
                // Why not vwrite? As vwrite may fail so have to retry at any rate.                
//                firstasm_node->mark = 1;
//                firstasm_node->next = NULL;

                //safe as no thread would be accessing teh firstnode as it has been removed atomically
                // any thread that is accessing this node should fail validation as first node has been unlinked.                
                deleted_val = firstasm_node->value; 
//                printf("removed %u==%u\n", firstasm_node->value, deleted_val);
//                printf("tid:%u key:%u freeing %p \n", tid, deleted_val, firstasm_node);
                free(firstasm_node);
//                retired[tid]->add(firstasm_node);
                num_retired.add(tid, 1);
                total_freed.add(tid, 1);

//                assert(deleted_val <= max_key && "accessing deleted node's field");
                CarbonRemoveAllFromWatchset();
                return deleted_val; //firstasm_node->value;
            }
            
            CarbonRemoveAllFromWatchset();    
//            num_retries.add(tid, 1);
        }while(true);
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      asm_node* next_asm_node = _top_sentinel->next;

      while( next_asm_node != _bottom_sentinel){
         sum = sum + next_asm_node->value;
         next_asm_node = next_asm_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      asm_node* next_asm_node = _top_sentinel->next;

      while( next_asm_node != _bottom_sentinel){
         size ++;
         next_asm_node = next_asm_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        asm_node* next_asm_node = _top_sentinel;       
        
        while( next_asm_node != NULL)
        {
//            printf("-->%u(%p)(%d) ", next_asm_node->value, next_asm_node, next_asm_node->mark);
            assert(next_asm_node->mark == 0 && "mark bit should be false");

            next_asm_node = next_asm_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_ASM_HMR_H */

