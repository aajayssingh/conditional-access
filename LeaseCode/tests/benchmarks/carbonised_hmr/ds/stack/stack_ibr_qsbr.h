/* 
 * File:   stack_ibr_qsbr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_QSBR_H
#define STACK_QSBR_H

#include "ibr_qsbr.h"

class stack_ibr_qsbr
{
private:
    const unsigned int num_threads;
    struct node {
        unsigned int volatile value;
        node* volatile	next;

        node(unsigned int inValue) : value(inValue), next(NULL) {}
    };

PAD;
node* _top;
PAD;
node* sentinal_node; // at the start _top points to this dummy node with value 0
PAD;
ibr_qsbr<struct node> *qsbr;

public:
   stack_ibr_qsbr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        sentinal_node = new node(0);
        _top = sentinal_node;
        qsbr = new ibr_qsbr<struct node>(num_threads);
   }
   ~stack_ibr_qsbr()
   {
        node* next_node = _top;
        node* temp_node;
        while( next_node != NULL) // include sentinel node as well
        {
            temp_node = next_node;
            next_node = next_node->next;
            free(temp_node);
        }
       
        printf("dtor::stack_ibr_qsbr\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
//        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete qsbr;
   }
   
   unsigned int peek(const unsigned int tid) 
   {
        qsbr->startOp(tid);    
        
        node* curr_top = _top;
        unsigned int top_val = curr_top->value;
        qsbr->endOp(tid);
        return top_val;
    }
   
   bool push(const unsigned int tid, const unsigned int key) 
   {
        qsbr->startOp(tid);    
        bool success = false;
        node* nd = new node(key);
        qsbr->incAllocCounter(tid);
        do
        {
            node* curr_top = _top;
            nd->next = curr_top;

            // Perform CAS Operation
            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);

            if(success)
            {
                qsbr->endOp(tid);
                return true;
            }
            num_retries.add(tid, 1);
        } while(true);
    }

    unsigned int pop(const unsigned int tid)
    {
        qsbr->startOp(tid);    

        bool success = false;
        do {
                node* curr_top = _top;
                if(curr_top == sentinal_node)
                {
                    qsbr->endOp(tid);
                    return false;   // TODO: This should be a NULL value
                }
                // Perform CAS Operation
                success =  __sync_bool_compare_and_swap(&_top, curr_top, curr_top->next);

                if( success ) 
                {
                    unsigned int top_val = curr_top->value;
                    qsbr->retire(tid, curr_top);
                    qsbr->endOp(tid);
                    return top_val;
                }
                num_retries.add(tid, 1);
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node* next_node = _top;

      while( next_node != sentinal_node){
         sum = sum + next_node->value;
         next_node = next_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node* next_node = _top;

      while( next_node != sentinal_node){
         size ++;
         next_node = next_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node* next_node = _top;

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_QSBR_H */

