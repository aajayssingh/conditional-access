/* 
 * File:   stack_asmhmr_trv_opt.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 * NOTE: Works only with empty watchset based vread/vwrite validation NOT with addres based validation.
 * With addres based validation peek may return value from delete node. As vread in peek may succeed if addr of a node added after freeing of the node.
 * This is an attempt to create ds version where only top is accessed like in none and doesn't require sentinel nodes.
 */

#ifndef STACK_ASMHMR_TRVOPT_H
#define STACK_ASMHMR_TRVOPT_H

#include "lock.h"
#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

struct asm_nodetrvopt 
{
    unsigned int value;
    bool mark; //to guarantee that a freed node is modified so that watchset validation recognises.
    struct asm_nodetrvopt* next;
}; //__attribute__((aligned(PADDING_BYTES)));

        
class stack_asmhmr_trvopt
{
private:
    const unsigned int num_threads;
    PAD;
    struct asm_nodetrvopt* _top;
    struct asm_nodetrvopt* _bottom_sentinel;
    PAD;
    ArrayList<asm_nodetrvopt> **retired;   
    
    const unsigned int min_key;
    const unsigned int max_key;
    
    asm_nodetrvopt* allocateAndInit(const unsigned int val)
    {
        struct asm_nodetrvopt* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(asm_nodetrvopt)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff);
        //init posix mem allocated node
        nd->value = val;
        nd->mark = 0;
        //        printf("adding tid(%u) key=%u val=%u \n", tid, key, nd->value);
        nd->next = NULL;
        
        return nd;
    }

public:
   stack_asmhmr_trvopt(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key), max_key(max_key)
   {

        printf("node size=%lu \n", sizeof(asm_nodetrvopt));

        _bottom_sentinel = allocateAndInit(min_key);
//        _top = allocateAndInit(max_key);            
        _top = _bottom_sentinel;
        
        printDebuggingDetails();

//        retired = new ArrayList<asm_nodetrvopt>*[num_threads];
//        for (int tid=0;tid<num_threads;++tid) {
//            retired[tid] = new ArrayList<asm_nodetrvopt>(5000);
//        }
    }
   ~stack_asmhmr_trvopt()
   {
//        printDebuggingDetails();
        
        struct  asm_nodetrvopt* next_asm_node = _top;
        struct  asm_nodetrvopt* temp_asm_node;
        while( next_asm_node != NULL)
        {
            temp_asm_node = next_asm_node;
            next_asm_node = next_asm_node->next;
            free(temp_asm_node);
        }
       
        printf("dtor::stack_asmhmr_trvopt\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   

        printf("total_retries=%lld \n", num_retries.getTotal());
        printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
        printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());               }
  
   void findAddr(asm_nodetrvopt* p)
   {
        printf("\n\n*******");

        for (unsigned int tid=0;tid<num_threads;++tid) {
            printf("tid=%d ", tid);
            for (int ix=0;ix<retired[tid]->size();++ix)
            {
//                printf ("%p ", retired[tid]->get(ix));
                if(retired[tid]->get(ix) == p)
                {
                    printf("found %p\n", p);
                    break;
                }
            }
            printf("\n");
        }
        printf("\n\n*******");       
   }
    
   
   //BUG: Why peek accesses a deleted node?
   //
    unsigned int peek(const unsigned int tid) 
    {
        asm_nodetrvopt* firstasm_node;
        unsigned int top_value = 0;

        do{
//            int mark_value = 0;
            
            //read add to ws the sentinel TOP and read its next field which is TOP.
//            CarbonAddToWatchset((void*) _top, sizeof(struct asm_nodetrvopt));
            
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (firstasm_node)
                : "m" (_top)
                ); 
            if ((unsigned int long)firstasm_node == 0xffffffff)
            { 
                num_vreadfails.add(tid, 1);
                CarbonRemoveAllFromWatchset();
                continue; 
            }
            
            if(firstasm_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // STack empty
            }

            //else read the TOP node's value
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (top_value)
                : "m" (firstasm_node->value)
                ); 

            if ((unsigned int long)top_value == 0xffffffff)
            { 
                num_vreadfails.add(tid, 1);
                CarbonRemoveAllFromWatchset();
//                printf("tid=%u read concurrently popped node val(%llu) restarting...\n", tid, top_value);
                continue; 
            }
            
//            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//                :"=r" (mark_value)
//                : "m" (firstasm_node->mark)
//                ); 
//
//            if ((unsigned int long)mark_value == 0xffffffff)
//            { 
//                num_vreadfails.add(tid, 1);
//                CarbonRemoveAllFromWatchset();
////                num_retries.add(tid, 1);
////                printf("tid=%u read concurrently popped node val(%llu) restarting...\n", tid, top_value);
//                continue; 
//            }
            
//            if (mark_value == 1)
//            {
//                printf("tid=%u val(%u) mark(%d) firstasm_node(%p)\n", tid, top_value, mark_value, firstasm_node);
//                findAddr(firstasm_node);
//                fflush(stdout);
//            }
//            assert (mark_value != 1);
//            assert (top_value <= 100);
            
            CarbonRemoveAllFromWatchset();
            return top_value;
        }while (true);  
    }

   //NOTE: The way VREAD works is you add a node to ws and then you vread its field.
   // vreading the node itself fails validation. 
   // Later may check whether that is correct behaviour of API spec or not.
   // Therefore I cannt validate top and have to use top->next as real pointer to top.
    bool push(const unsigned int tid, const unsigned int key) 
    {
        struct asm_nodetrvopt* nd;
        nd = allocateAndInit(key);
//        num_allocs.add(tid, 1);

        do
        {
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (nd->next)
                : "m" (_top)
                ); 
            if ((unsigned int long)nd->next == 0xffffffff)
            { 
                num_vreadfails.add(tid, 1);
                CarbonRemoveAllFromWatchset();
                //                num_retries.add(tid, 1);
                continue; 
            }
    
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (_top)
                          : "r" (nd)
                           );      
            int res = CarbonGetVOPStatus();
            if (res)//succeeded
            {
               CarbonRemoveAllFromWatchset();
//               printf("tid:%u key:%u PUSHING %p \n", tid, key, nd);
//               fflush(stdout);
               return true;
            }

            CarbonRemoveAllFromWatchset();
            num_vwritefails.add(tid, 1);
//            num_retries.add(tid, 1);
        }while(true);
     }
    
    //returs value in node. 0 < value < max_key
    unsigned int pop(const unsigned int tid)
    {

        asm_nodetrvopt* firstasm_node;
        asm_nodetrvopt* secasm_node;
        unsigned int deleted_val;

        do
        {
//            printf("tid=%u pop\n", tid);

//            CarbonAddToWatchset((void*) _top, sizeof(struct asm_nodetrvopt));
            
            //read _top->next in firstasm_node
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (firstasm_node)
                : "m" (_top)
                ); 
            if ((unsigned int long)firstasm_node == 0xffffffff)
            { 
                num_vreadfails.add(tid, 1);
                CarbonRemoveAllFromWatchset();
//                num_retries.add(tid, 1);
                continue; 
            }
            
            if(firstasm_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // stack is empty.
            }            
            
//            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//              :"=m" (_top->mark)
//              : "r" (1)
//               );      
//            int mres = CarbonGetVOPStatus();
//            if (!mres)
//            { 
//                CarbonRemoveAllFromWatchset();
//                num_vwritefails.add(tid, 1);
////                num_retries.add(tid, 1);
//                continue; 
//            }
            
            //Now write top.next= secasmnode
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                          :"=m" (_top)
                          : "r" (firstasm_node->next)
                           );      
            int res = CarbonGetVOPStatus();
            if (res) //succeeded in unlinking the firstnode
            {
                deleted_val = firstasm_node->value; 
//                printf("removed %u==%u\n", firstasm_node->value, deleted_val);
//                printf("tid:%u key:%u freeing %p \n", tid, deleted_val, firstasm_node);

                free(firstasm_node);
                num_retired.add(tid, 1);
                total_freed.add(tid, 1);

//                assert(deleted_val <= max_key && "accessing deleted node's field");
                CarbonRemoveAllFromWatchset();
                return deleted_val; //firstasm_node->value;
            }
            
            CarbonRemoveAllFromWatchset();  
            num_vwritefails.add(tid, 1);
//            num_retries.add(tid, 1);
        }while(true);
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      asm_nodetrvopt* next_asm_node = _top;

      while( next_asm_node != _bottom_sentinel){
         sum = sum + next_asm_node->value;
         next_asm_node = next_asm_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      asm_nodetrvopt* next_asm_node = _top;

      while( next_asm_node != _bottom_sentinel){
         size ++;
         next_asm_node = next_asm_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        asm_nodetrvopt* next_asm_node = _top;       
        
        while( next_asm_node != NULL)
        {
            printf("-->%u(%p)(%d) ", next_asm_node->value, next_asm_node, next_asm_node->mark);
            assert(next_asm_node->mark == 0 && "mark bit should be false");

            next_asm_node = next_asm_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_ASMHMR_TRVOPT_H */

