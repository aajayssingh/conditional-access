/* 
 * File:   stack_ibr_hp.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_IBR_HP_H
#define STACK_IBR_HP_H

#include "ibr_hp.h"
#define MAX_HP 1

struct node_hp; //fwd declaration to satiate complaining compiler.
ibr_hp<struct node_hp> *rec_hp = NULL;

struct node_hp{
    unsigned int value;
    std::atomic<struct node_hp*> next;

    node_hp(unsigned int key): value(key)
    {
        next = NULL;
        assert(rec_hp !=  NULL);
    }
};


class stack_ibr_hp
{
private:
    const unsigned int num_threads;
    PAD;
    std::atomic<struct node_hp*> _top;
    PAD;
    node_hp* sentinal_node; // at the start _top points to this dummy node_hp with value 0
//    PAD;
//    rec_hp<struct node_hp> *rec_hp;

public:
   stack_ibr_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        rec_hp = new ibr_hp<struct node_hp>(num_threads, MAX_HP);
 
        sentinal_node = new node_hp(0);
        _top.store(sentinal_node, std::memory_order_relaxed);
   }
   ~stack_ibr_hp()
   {
        node_hp* next_node = _top.load(std::memory_order_relaxed);
        node_hp* temp_node;
        while( next_node != NULL) // include sentinel node_hp as well
        {
            temp_node = next_node;
            next_node = next_node->next.load(std::memory_order_relaxed);
            free(temp_node);
        }
       
        printf("dtor::stack_ibr_hp\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
//        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete rec_hp;
   }
   
   unsigned int peek(const unsigned int tid) 
   {
//        rec_hp->startOp(tid);    
        
        node_hp* curr_top = rec_hp->read(tid, 0, _top);
        unsigned int top_val = curr_top->value;
        rec_hp->clear(tid);
        return top_val;
    }
   
   bool push(const unsigned int tid, const unsigned int key) 
   {
//        rec_hp->startOp(tid);    
        bool success = false;
        node_hp* nd = new node_hp(key);
//        num_allocs.add(tid, 1);
        rec_hp->incAllocCounter(tid);
        do
        {
            node_hp* curr_top = rec_hp->read(tid, 0, _top);
            nd->next = curr_top;

            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);
            success = _top.compare_exchange_strong(curr_top, nd);
            if(success)
            {
                rec_hp->clear(tid);
                return true;
            }
            num_retries.add(tid, 1);
        } while(true);
    }

    unsigned int pop(const unsigned int tid)
    {
//        rec_hp->startOp(tid);    

        bool success = false;
        do {
                node_hp* curr_top = rec_hp->read(tid, 0, _top);
                if(curr_top == sentinal_node)
                {
                    rec_hp->clear(tid);
                    return false;   //empty stack
                }
                // Perform CAS Operation
                success =  _top.compare_exchange_strong(curr_top, curr_top->next);

                if( success ) 
                {
                    unsigned int top_val = curr_top->value;
                    rec_hp->retire(tid, curr_top);
                    rec_hp->clear(tid);
                    return top_val;
                }
                num_retries.add(tid, 1);
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node_hp* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         sum = sum + next_node->value;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node_hp* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         size ++;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node_hp* next_node = _top.load(std::memory_order_relaxed);

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next.load(std::memory_order_relaxed);
        }
        printf("\n");
    }   

};

#endif /* STACK_IBR_HP_H */

