#ifndef LAZYLIST_NONE_HMR_H
#define LAZYLIST_NONE_HMR_H

#include <limits.h>
#include "lock.h"
#include "util.h"

class lazylist_none_hmr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
PAD; // each node is posix_memalign allolcated so not needed
    struct node{
        unsigned int key;
        bool mark;

#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif

        struct node * next;

        node(unsigned int key): key(key)
        {
            mark = false;
            next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        }
    };

PAD;    
    struct node * head;
PAD;
    struct node * tail;    
PAD;
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_none_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_none_hmr();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_none_hmr::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
}

lazylist_none_hmr::lazylist_none_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_none_hmr\n");
    printf("node size=%lu \n", sizeof(node));

//    head = new node(min_key);
//    tail = new node(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);

    head->next = tail;
    
}

lazylist_none_hmr::~lazylist_none_hmr()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_none_hmr\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());
}

bool lazylist_none_hmr::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

//noneCarbon

bool lazylist_none_hmr::contains(const unsigned int tid, const unsigned int key)
{
    unsigned int long ret_val = 0;    
    struct node * pred = head;
    vread(&pred->next, ret_val, sizeof(struct node*))    
    struct node * curr = (struct node *)ret_val;
    
    while((unsigned int)CarbonRead((void*)&curr->key, sizeof(unsigned int)) < key)
    {
        pred = curr;
        vread(&curr->next, ret_val, sizeof(struct node*))    
        curr = (struct node *)ret_val;
    }
    
    vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val;
    
    vread(&curr->mark, ret_val, sizeof(bool))    
    bool curr_mark = (unsigned int)ret_val;

    return ((!curr_mark) && (curr_key == key));
}

bool lazylist_none_hmr::insert(const unsigned int tid, const unsigned int key)
{
    retry:

    unsigned int long ret_val = 0;    
    struct node * pred = head;
    vread(&pred->next, ret_val, sizeof(struct node*))    
    struct node * curr = (struct node *)ret_val;
    
    while((unsigned int)CarbonRead((void*)&curr->key, sizeof(unsigned int)) < key)
    {
        pred = curr;
        vread(&curr->next, ret_val, sizeof(struct node*))    
        curr = (struct node *)ret_val;
    }

    vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val;

    if (curr_key == key)
    {
        return false;
    }   
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        goto retry;        
    }
    
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    new_node->next = curr;
    pred->next = new_node;
    num_allocs.add(tid, 1);
    
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);    
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    return true;
}

bool lazylist_none_hmr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int long ret_val = 0;    
    struct node * pred = head;
    vread(&pred->next, ret_val, sizeof(struct node*))    
    struct node * curr = (struct node *)ret_val;
    
    while((unsigned int)CarbonRead((void*)&curr->key, sizeof(unsigned int)) < key)
    {
        pred = curr;
        vread(&curr->next, ret_val, sizeof(struct node*))    
        curr = (struct node *)ret_val;
    }
    
    vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val;
    
    if (curr_key != key)
    {
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);    
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;
    
// Since simulator load/store are non faulty. a thread can access a null node and
// if members of a null node without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid nodes.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);    
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    return true;
}

long long lazylist_none_hmr::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_none_hmr::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_none_hmr::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u (%p)", curr->key, curr);
        curr = curr->next;
    }
    printf("\n");
}
#endif // LAZYLIST_NONE_HMR_H
