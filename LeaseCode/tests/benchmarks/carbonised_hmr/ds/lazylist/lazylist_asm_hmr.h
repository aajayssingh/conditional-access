#ifndef LAZYLISTASMHMR_H
#define LAZYLISTASMHMR_H

#include <limits.h>
#include "lock.h"
#include "util.h"
//#define OPT

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

//not part of class as node_asmhmr type is needed to write MACROS or cleanUP function.
// could make inline functions part of class as well.
struct node_asmhmr{
    unsigned int key;
    bool mark;
    volatile unsigned int lock;
    struct node_asmhmr * next;

    node_asmhmr(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
        lock = 0; 
    }
};//__attribute__((aligned(PADDING_BYTES)));

/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void asmcleanUp(const unsigned int tid, struct node_asmhmr * pred = NULL, struct node_asmhmr * curr = NULL) __attribute__((always_inline));


/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void asmcleanUp(const unsigned int tid, struct node_asmhmr * pred = NULL, struct node_asmhmr * curr = NULL)
{
//    if (usepred)
//    {
//        assert (pred && "usepred is true but pred == NULL" );
//    }
//    if (usecurr)
//    {
//        assert (curr && "usecurr is true but curr == NULL" );
//    }
    if (usepred && pred) releaseLock(&pred->lock, tid);
    if (usecurr && curr) releaseLock(&curr->lock, tid);
    CarbonRemoveAllFromWatchset();    
}

class lazylist_asm_hmr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    PAD;
    struct node_asmhmr * head;
    PAD;
    struct node_asmhmr * tail;
    PAD;
    
    bool validate(struct node_asmhmr * pred, struct node_asmhmr * curr);
    void initNode(struct node_asmhmr * n, unsigned int key);
    void printNode(struct node_asmhmr * n);

public:
    lazylist_asm_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_asm_hmr();
    
    std::pair<struct node_asmhmr *, struct node_asmhmr *> locate(const unsigned int tid, const unsigned int key);

    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_asm_hmr::initNode(struct node_asmhmr * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    n->lock = 0; 
}

void lazylist_asm_hmr::printNode(struct node_asmhmr * n)
{
    printf("n(%p)::  ", n);
    printf("n->key(%u), ", n->key);
    printf("n->mark(%u), ", n->mark);
    printf("n->next(%p), ", n->next);
    printf("n->lock(%u)\n", n->lock);
}

lazylist_asm_hmr::lazylist_asm_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("node size=%lu \n", sizeof(node_asmhmr));

    printf("ctor::lazylist_asm_hmr\n");

    if(posix_memalign( (void**)&head, 64, sizeof(struct node_asmhmr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_asmhmr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);
    head->next = tail;
}

lazylist_asm_hmr::~lazylist_asm_hmr()
{
    struct node_asmhmr * curr = head;
    struct node_asmhmr * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_asm_hmr\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());   
    printf("total_retries=%lld \n", num_retries.getTotal());        

//    printf("~lazylist_asm_hmr: sizeof(struct node_asmhmr)=%d, sizeof(struct node_asmhmr*)=%d\n", sizeof(struct node_asmhmr), sizeof(struct node_asmhmr*));
}

bool lazylist_asm_hmr::validate(struct node_asmhmr * pred, struct node_asmhmr * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \    
        goto retry; \
    }

//#define try_vread(field_addr, ret_val, field_size) \
//    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
//    DEBUG printf("vread field (%p)\n",(void*)ret_val);
//    if (ret_val == 0xffffffff) \
//    { \
//        CarbonRemoveAllFromWatchset(); \    
//        DEBUG printf("retry (%p)\n", (void*)ret_val); \
//        goto retry; \
//    }

std::pair<struct node_asmhmr *, struct node_asmhmr *> lazylist_asm_hmr::locate(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
    
    struct node_asmhmr * pred = head;
    CarbonAddToWatchset((void*) pred, sizeof(struct node_asmhmr));
//How to use: just deref the pointer you would pass to CarbonAddToWatchset
// NOT USING asm-read version of addTag becuase some how read its next field in VREAD fails.
// Any way since its outside the loop it doesn't matter that much for num of instruction counts.
//    __asm__ __volatile__ (ASF_F3 "mov %1, %0"
//        :"=r" (dummypred)
//        : "m" (*pred)
//        ); 

    struct node_asmhmr * curr;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr)
        : "m" (pred->next)
        ); 
    if ((unsigned int long)curr == 0xffffffff)
    { 
//        printf("lazylist_asm_hmr::locate read of pred next failed\n");
//        fflush(stdout);

        CarbonRemoveAllFromWatchset();
//        num_retries.add(tid, 1);
        goto retry; 
    }
    
    CarbonAddToWatchset((void*) curr, sizeof(struct node_asmhmr));  
    
    //DISCUSSME: DO we need to vread the key field of a node?
    unsigned int curr_key = 0xffffffff;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    
    while(curr_key < key)
    {
        CarbonRemoveFromWatchset((void*) pred, sizeof(struct node_asmhmr));        
        pred = curr;
       
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr)
            : "m" (curr->next)
            ); 
        if ((unsigned int long)curr == 0xffffffff)
        {
            CarbonRemoveAllFromWatchset();
//            num_retries.add(tid, 1);
            goto retry; 
        }
        CarbonAddToWatchset((void*) curr, sizeof(struct node_asmhmr));  
    
        //DISCUSSME: DO we need to vread the key field of a node?
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr_key)
            : "m" (curr->key)
            ) ;
    }
    //check if loop was exited due to failed validation for curr->key read
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset(); 
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;
    }
    return std::make_pair (pred, curr);
}       

bool lazylist_asm_hmr::contains(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_asmhmr *, struct node_asmhmr *> dest_nodes = locate(tid, key);
    struct node_asmhmr *pred = dest_nodes.first;
    struct node_asmhmr *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;
    }
    
    bool curr_mark;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_mark)
        : "m" (curr->mark)
        ) ;
    if (curr_mark == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;
    }    
    
    bool res = ((!curr_mark) && (curr_key == key));
    asmcleanUp<>(tid);
    return res;
}

//UINFO: remove watched address before returning from all return points of the functions.
bool lazylist_asm_hmr::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_asmhmr *, struct node_asmhmr *> dest_nodes = locate(tid, key);
    struct node_asmhmr *pred = dest_nodes.first;
    struct node_asmhmr *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;
    }

    if (curr_key == key)
    {
        asmcleanUp<>(tid);
        return false;
    }   
    
    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

    if (!tryAcquireLock(&pred->lock, tid))
    {
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        asmcleanUp<true, false>(tid, pred);
//        num_retries.add(tid, 1);
        goto retry;        
    }

    if (!validate(pred, curr))
    {
        asmcleanUp<true, true>(tid, pred, curr);
//        num_retries.add(tid, 1);
        goto retry;        
    }
    
    struct node_asmhmr * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_asmhmr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    num_allocs.add(tid, 1);
    
    new_node->next = curr;
    pred->next = new_node;

    asmcleanUp<true, true>(tid, pred, curr);
    return true;
}

bool lazylist_asm_hmr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_asmhmr *, struct node_asmhmr *> dest_nodes = locate(tid, key);
    struct node_asmhmr *pred = dest_nodes.first;
    struct node_asmhmr *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;
    }

    if (curr_key != key)
    {        
      asmcleanUp<>(tid);
      return false;
    }
    
    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0);

    if (!tryAcquireLock(&pred->lock, tid))
    {
        asmcleanUp<>(tid);
//        num_retries.add(tid, 1);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        asmcleanUp<true, false>(tid, pred);
//        num_retries.add(tid, 1);
        goto retry;        
    }

    if (!validate(pred, curr))
    {
        asmcleanUp<true, true>(tid, pred, curr);
//        num_retries.add(tid, 1);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;    
    
    asmcleanUp<true, true>(tid, pred, curr);
    free (curr);
    num_retired.add(tid, 1);
    total_freed.add(tid, 1);

    return true;
}

long long lazylist_asm_hmr::getSumOfKeys()
{
    long long sum = 0;
    struct node_asmhmr * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_asm_hmr::getDSSize()
{
    long long count = 0;
    struct node_asmhmr * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_asm_hmr::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node_asmhmr * curr = head;
    while(curr !=  NULL)
    {
//        printf("--> %u(%p)", curr->key, curr);
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());
    printf("total_retries=%lld \n", num_retries.getTotal());
}

#endif // LAZYLISTASMHMR_H
