/* 
 * File:   lazylist_ibr_rcu.h
 * Author: mc
 *
 * Created on May 14, 2021, 12:56 PM
 */

#ifndef LAZYLIST_IBR_RCU_H
#define LAZYLIST_IBR_RCU_H
#include "lock.h"
#include "ibr_rcu.h"

struct node_rcu; //fwd declaration to satiate complaining compiler.
ibr_rcu<struct node_rcu> *rcu = NULL;

struct node_rcu{
    unsigned int key;
    bool mark;
#ifdef SETBENCHLOCK
    volatile unsigned int lock;
#else
    pthread_mutex_t pt_lock;
#endif
    struct node_rcu * next;

    node_rcu(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
#ifdef SETBENCHLOCK            
        lock = 0;
#else            
        if (pthread_mutex_init(&pt_lock, NULL) != 0){
            printf("Mutex Initialization failed!");
            exit(EXIT_FAILURE);
        }
#endif
    }
};

void initRCUReclaimer(unsigned int nthreads)
{
    rcu = new ibr_rcu<struct node_rcu>(nthreads);
}

void deInitRCUReclaimer()
{
    delete rcu;
}

class lazylist_ibr_rcu{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    //pad??
    struct node_rcu * head;
    struct node_rcu * tail;    
    bool validate(struct node_rcu * pred, struct node_rcu * curr);
    void initNode(struct node_rcu * n, unsigned int key);
    void printNode(struct node_rcu * n);

public:
    lazylist_ibr_rcu(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr_rcu();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr_rcu::initNode(struct node_rcu * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(rcu !=  NULL);
}

lazylist_ibr_rcu::lazylist_ibr_rcu(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
#ifndef HASHTABLE
    printf("ctor::lazylist_ibr_rcu\n");
    printf("node_rcu size=%lu \n", sizeof(node_rcu));
    rcu = new ibr_rcu<struct node_rcu>(num_threads);
#endif
    
    head = new node_rcu(min_key);
    tail = new node_rcu(max_key);
    head->next = tail;
}

lazylist_ibr_rcu::~lazylist_ibr_rcu()
{
    struct node_rcu * curr = head;
    struct node_rcu * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
#ifndef HASHTABLE        
    printf("dtor::lazylist_ibr_rcu\n");
    delete rcu;
#endif
}

bool lazylist_ibr_rcu::validate(struct node_rcu * pred, struct node_rcu * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

//#ifdef EXP_ASMHMR
//
//    bool lazylist_ibr_rcu::contains(const unsigned int tid, const unsigned int key)
//    {
//        rcu->startOp(tid);
//        
//        unsigned int long ret_val = 0;    
//        struct node_rcu * pred = head;
//
//        struct node_rcu * curr;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr)
//            : "m" (pred->next)
//            ); 
//
//        unsigned int curr_key = 0xffffffff;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr_key)
//            : "m" (curr->key)
//            ) ;    
//
//        while(curr_key < key)
//        {
//            pred = curr;
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr)
//                : "m" (curr->next)
//                ); 
//
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr_key)
//                : "m" (curr->key)
//                ) ;
//        }
//
//        bool curr_mark;
//        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//            :"=r" (curr_mark)
//            : "m" (curr->mark)
//            ) ;
//
//        bool res =  ((!curr_mark) && (curr_key == key));
//        rcu->endOp(tid);
//
//        return res;
//    }
//
//    bool lazylist_ibr_rcu::insert(const unsigned int tid, const unsigned int key)
//    {
//        retry:
//        rcu->startOp(tid);
//
//        unsigned int long ret_val = 0;    
//        struct node_rcu * pred = head;
//        struct node_rcu * curr;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr)
//            : "m" (pred->next)
//            ); 
//
//        unsigned int curr_key = 0xffffffff;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr_key)
//            : "m" (curr->key)
//            ) ;    
//
//        while(curr_key < key)
//        {
//            pred = curr;
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr)
//                : "m" (curr->next)
//                ); 
//
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr_key)
//                : "m" (curr->key)
//                ) ;
//        }
//
//        if (curr_key == key)
//        {
//            return false;
//        }       
//
//    #ifdef SETBENCHLOCK   
////        acquireLock(&pred->lock);
////        acquireLock(&curr->lock);
//        if (!tacquireLock(&pred->lock))
//        {
//            num_retries.add(tid, 1);
//            goto retry;
//        }
//        
//        if (!tacquireLock(&curr->lock))
//        {
//            num_retries.add(tid, 1);
//            releaseLock(&pred->lock, tid);
//            goto retry;
//        }
//    #else
//        pthread_mutex_lock(&pred->pt_lock);
//        pthread_mutex_lock(&curr->pt_lock);
//    #endif
//
//        if (!validate(pred, curr))
//        {
//    #ifdef SETBENCHLOCK 
//            releaseLock(&pred->lock, tid);
//            releaseLock(&curr->lock, tid);
//    #else        
//            pthread_mutex_unlock(&pred->pt_lock);
//            pthread_mutex_unlock(&curr->pt_lock);
//    #endif
//            num_vwritefails.add(tid, 1);
//            rcu->endOp(tid);
//            goto retry;        
//        }
//
//    //    struct node_rcu * new_node = new node_rcu(key);
//        struct node_rcu * new_node;
//        if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_rcu)) != 0)
//        {
//            printf("Error: posix_memalign could not allocate memory!!! \n");
//            exit(EXIT_FAILURE);
//        }
//        initNode(new_node, key);
//
//        new_node->next = curr;
//        pred->next = new_node;
//
//        rcu->incAllocCounter(tid);
//
//    #ifdef SETBENCHLOCK 
//            releaseLock(&pred->lock, tid);
//            releaseLock(&curr->lock, tid);
//    #else        
//            pthread_mutex_unlock(&pred->pt_lock);
//            pthread_mutex_unlock(&curr->pt_lock);
//    #endif
//
//        rcu->endOp(tid);
//
//        return true;
//    }
//
//    bool lazylist_ibr_rcu::erase(const unsigned int tid, const unsigned int key)
//    {
//        retry:
//        rcu->startOp(tid);
//
//        unsigned int long ret_val = 0;    
//        struct node_rcu * pred = head;
//        struct node_rcu * curr;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr)
//            : "m" (pred->next)
//            ); 
//
//        unsigned int curr_key = 0xffffffff;
//        __asm__ __volatile__ ("mov %1, %0"
//            :"=r" (curr_key)
//            : "m" (curr->key)
//            ) ;    
//
//        while(curr_key < key)
//        {
//            pred = curr;
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr)
//                : "m" (curr->next)
//                ); 
//
//            __asm__ __volatile__ ("mov %1, %0"
//                :"=r" (curr_key)
//                : "m" (curr->key)
//                ) ;
//        }
//
//        if (curr_key != key)
//        {
//            return false;
//        }       
//    #ifdef SETBENCHLOCK   
////        acquireLock(&pred->lock);
////        acquireLock(&curr->lock);
//        if (!tacquireLock(&pred->lock))
//        {
//            num_retries.add(tid, 1);
//            goto retry;
//        }
//        
//        if (!tacquireLock(&curr->lock))
//        {
//            num_retries.add(tid, 1);
//            releaseLock(&pred->lock, tid);
//            goto retry;
//        }        
//    #else
//        pthread_mutex_lock(&pred->pt_lock);
//        pthread_mutex_lock(&curr->pt_lock);
//    #endif
//
//        if (!validate(pred, curr))
//        {
//    #ifdef SETBENCHLOCK 
//            releaseLock(&pred->lock, tid);
//            releaseLock(&curr->lock, tid);
//    #else        
//            pthread_mutex_unlock(&pred->pt_lock);
//            pthread_mutex_unlock(&curr->pt_lock);
//    #endif
//            num_vwritefails.add(tid, 1);
//            rcu->endOp(tid);
//            goto retry;        
//        }
//
//        curr->mark = true;
//        pred->next = curr->next;
//
//        // Since simulator load/store are non faulty. a thread can access a null node_rcu and
//    // if members of a null node_rcu without seg faults. This would only cause inf loop or 
//    //    threads appearing to be freezed etc.. Watch out for such errors in future
//    // If such errors occur I might very well be accessing invalid nodes.
//    //    printf("freed\n");
//    //    free(curr);
//    //    curr = NULL;
//    #ifdef SETBENCHLOCK 
//            releaseLock(&pred->lock, tid);
//            releaseLock(&curr->lock, tid);
//    #else        
//            pthread_mutex_unlock(&pred->pt_lock);
//            pthread_mutex_unlock(&curr->pt_lock);
//    #endif
//
//        rcu->retire(tid, curr);    
//
//        rcu->endOp(tid);
//        return true;
//    }
//
//#else
    bool lazylist_ibr_rcu::contains(const unsigned int tid, const unsigned int key)
    {
        rcu->startOp(tid);

        struct node_rcu * pred = head;
        struct node_rcu * curr = pred->next;
        while(curr->key < key)
        {
            pred = curr;
            curr = curr->next;
        }

        bool res = ((!curr->mark) && (curr->key == key)); 
        rcu->endOp(tid);

        return res;
    }

    bool lazylist_ibr_rcu::insert(const unsigned int tid, const unsigned int key)
    {
        retry:
        rcu->startOp(tid);

        struct node_rcu * pred = head;
        struct node_rcu * curr = pred->next;

        while(curr->key < key)
        {
            pred = curr;
            curr = curr->next;
        }

        if (curr->key == key)
        {
            rcu->endOp(tid);
            return false;
        }   


    #ifdef SETBENCHLOCK   
        acquireLock(&pred->lock);
        acquireLock(&curr->lock);
    #else
        pthread_mutex_lock(&pred->pt_lock);
        pthread_mutex_lock(&curr->pt_lock);
    #endif

        if (!validate(pred, curr))
        {
    #ifdef SETBENCHLOCK 
            releaseLock(&pred->lock, tid);
            releaseLock(&curr->lock, tid);
    #else        
            pthread_mutex_unlock(&pred->pt_lock);
            pthread_mutex_unlock(&curr->pt_lock);
    #endif
            rcu->endOp(tid);
            goto retry;        
        }

    //    struct node_rcu * new_node = new node_rcu(key);
        struct node_rcu * new_node;
        if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_rcu)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        initNode(new_node, key);

        new_node->next = curr;
        pred->next = new_node;

        rcu->incAllocCounter(tid);

    #ifdef SETBENCHLOCK 
            releaseLock(&pred->lock, tid);
            releaseLock(&curr->lock, tid);
    #else        
            pthread_mutex_unlock(&pred->pt_lock);
            pthread_mutex_unlock(&curr->pt_lock);
    #endif

        rcu->endOp(tid);

        return true;
    }

    bool lazylist_ibr_rcu::erase(const unsigned int tid, const unsigned int key)
    {
        retry:
        rcu->startOp(tid);

        struct node_rcu * pred = head;
        struct node_rcu * curr = pred->next;

        while(curr->key < key)
        {
            pred = curr;
            curr = curr->next;
        }

        if (curr->key != key)
        {
            rcu->endOp(tid);
            return false;
        }   

    #ifdef SETBENCHLOCK   
        acquireLock(&pred->lock);
        acquireLock(&curr->lock);
    #else
        pthread_mutex_lock(&pred->pt_lock);
        pthread_mutex_lock(&curr->pt_lock);
    #endif

        if (!validate(pred, curr))
        {
    #ifdef SETBENCHLOCK 
            releaseLock(&pred->lock, tid);
            releaseLock(&curr->lock, tid);
    #else        
            pthread_mutex_unlock(&pred->pt_lock);
            pthread_mutex_unlock(&curr->pt_lock);
    #endif
            rcu->endOp(tid);
            goto retry;        
        }

        curr->mark = true;
        pred->next = curr->next;

        // Since simulator load/store are non faulty. a thread can access a null node_rcu and
    // if members of a null node_rcu without seg faults. This would only cause inf loop or 
    //    threads appearing to be freezed etc.. Watch out for such errors in future
    // If such errors occur I might very well be accessing invalid nodes.
    //    printf("freed\n");
    //    free(curr);
    //    curr = NULL;
    #ifdef SETBENCHLOCK 
            releaseLock(&pred->lock, tid);
            releaseLock(&curr->lock, tid);
    #else        
            pthread_mutex_unlock(&pred->pt_lock);
            pthread_mutex_unlock(&curr->pt_lock);
    #endif

        rcu->retire(tid, curr);    

        rcu->endOp(tid);
        return true;
    }
//#endif

long long lazylist_ibr_rcu::getSumOfKeys()
{
    long long sum = 0;
    struct node_rcu * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr_rcu::getDSSize()
{
    long long count = 0;
    struct node_rcu * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr_rcu::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node_rcu * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

#endif /* LAZYLIST_IBR_RCU_H */

