#ifndef LAZYLIST_HMRTRV_H
#define LAZYLIST_HMRTRV_H

#include <limits.h>
#include "lock.h"
#include "util.h"
//#define OPT
#define DEBUG if(0)
//use this to enablke default pthread mutex lock.
//#define LOCK_DEBUG

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"


//not part of class as node_hmrtrv type is needed to write MACROS or cleanUP function.
// could make inline functions part of class as well.
struct node_hmrtrv{
    unsigned int key;
    bool mark;
    volatile unsigned int lock;
    struct node_hmrtrv * next;

    node_hmrtrv(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
        lock = 0; 
    }
};//__attribute__((aligned(PADDING_BYTES)));

/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void cleanUpHMRTRV(const unsigned int tid, struct node_hmrtrv * pred = NULL, struct node_hmrtrv * curr = NULL) __attribute__((always_inline));


/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void cleanUpHMRTRV(const unsigned int tid, struct node_hmrtrv * pred = NULL, struct node_hmrtrv * curr = NULL)
{
//    if (usepred)
//    {
//        assert (pred && "usepred is true but pred == NULL" );
//    }
//    if (usecurr)
//    {
//        assert (curr && "usecurr is true but curr == NULL" );
//    }
    if (usepred && pred) releaseLock(&pred->lock, tid);
    if (usecurr && curr) releaseLock(&curr->lock, tid);
    CarbonRemoveAllFromWatchset();    
}

class lazylist_hmr_trv{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    PAD;
    struct node_hmrtrv * head;
    PAD;
    struct node_hmrtrv * tail;
    PAD;
    
    bool validate(struct node_hmrtrv * pred, struct node_hmrtrv * curr);
    void initNode(struct node_hmrtrv * n, unsigned int key);
    void printNode(struct node_hmrtrv * n);

public:
    lazylist_hmr_trv(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_hmr_trv();
    
    std::pair<struct node_hmrtrv *, struct node_hmrtrv *> locate(const unsigned int tid, const unsigned int key);

    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_hmr_trv::initNode(struct node_hmrtrv * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    n->lock = 0; 
}

void lazylist_hmr_trv::printNode(struct node_hmrtrv * n)
{
    printf("n(%p)::  ", n);
    printf("n->key(%u), ", n->key);
    printf("n->mark(%u), ", n->mark);
    printf("n->next(%p), ", n->next);
    printf("n->lock(%u)\n", n->lock);
}

lazylist_hmr_trv::lazylist_hmr_trv(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("node size=%lu \n", sizeof(node_hmrtrv));
    printf("ctor::lazylist_hmr_trv\n");

    if(posix_memalign( (void**)&head, 64, sizeof(struct node_hmrtrv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_hmrtrv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);
    head->next = tail;
}

lazylist_hmr_trv::~lazylist_hmr_trv()
{
    struct node_hmrtrv * curr = head;
    struct node_hmrtrv * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_hmr_trv\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());   
    printf("total_retries=%lld \n", num_retries.getTotal());        

//    printf("~lazylist_hmr_trv: sizeof(struct node_hmrtrv)=%d, sizeof(struct node_hmrtrv*)=%d\n", sizeof(struct node_hmrtrv), sizeof(struct node_hmrtrv*));
}

bool lazylist_hmr_trv::validate(struct node_hmrtrv * pred, struct node_hmrtrv * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \ 
        goto retry; \
    }

//#define try_vread(field_addr, ret_val, field_size) \
//    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
//    DEBUG printf("vread field (%p)\n",(void*)ret_val);
//    if (ret_val == 0xffffffff) \
//    { \
//        CarbonRemoveAllFromWatchset(); \    
//        DEBUG printf("retry (%p)\n", (void*)ret_val); \
//        goto retry; \
//    }

std::pair<struct node_hmrtrv *, struct node_hmrtrv *> lazylist_hmr_trv::locate(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
    
    struct node_hmrtrv * pred = head;
//    CarbonAddToWatchset((void*) pred, sizeof(struct node_hmrtrv));

    //tryvread and type cast ret_val and addto watchset if its node_hmrtrv address that is read.
    try_vread(&pred->next, ret_val, sizeof(struct node_hmrtrv*))    
    struct node_hmrtrv * curr = (struct node_hmrtrv *)ret_val; //will this cause read of the address in read_val?

//    CarbonAddToWatchset((void*) curr, sizeof(struct node_hmrtrv));    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))
    unsigned int curr_key = (unsigned int)ret_val;
    while(curr_key < key)
    {
        
        CarbonRemoveFromWatchsetOpt((void*) pred);        
//        CarbonRemoveFromWatchset((void*) pred, sizeof(struct node_hmrtrv));     
        pred = curr;
       
        try_vread(&curr->next, ret_val, sizeof(struct node_hmrtrv*))    
        curr = (struct node_hmrtrv *)ret_val; //will this cause read of the address in read_val?
//        CarbonAddToWatchset((void*) curr, sizeof(struct node_hmrtrv)); 
        try_vread(&curr->key, ret_val, sizeof(unsigned int))
        curr_key = (unsigned int)ret_val;    
    }
    
    //check if loop was exited due to failed validation for curr->key read
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        cleanUpHMRTRV<>(tid);
        num_retries.add(tid, 1);
        goto retry;
    }
    return std::make_pair (pred, curr);;
}       

bool lazylist_hmr_trv::contains(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_hmrtrv *, struct node_hmrtrv *> dest_nodes = locate(tid, key);
    struct node_hmrtrv *pred = dest_nodes.first;
    struct node_hmrtrv *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting

    try_vread(&curr->mark, ret_val, sizeof(bool))    
    bool curr_mark = (bool)ret_val; //UINFO: downcasting
    bool res = ((!curr_mark) && (curr_key == key));
//    CarbonRemoveAllFromWatchset();    
    cleanUpHMRTRV<>(tid);
    
    return res;
}

//UINFO: remove watched address before returning from all return points of the functions.
bool lazylist_hmr_trv::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_hmrtrv *, struct node_hmrtrv *> dest_nodes = locate(tid, key);
    struct node_hmrtrv *pred = dest_nodes.first;
    struct node_hmrtrv *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting
    if (curr_key == key)
    {
        cleanUpHMRTRV<>(tid);
        return false;
    }   
    
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

    if (!tryAcquireLock(&pred->lock, tid))
    {
        cleanUpHMRTRV<>(tid);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        cleanUpHMRTRV<true, false>(tid, pred);
        goto retry;        
    }

//    if (!validate(pred, curr))
//    {
//        cleanUpHMRTRV<true, true>(tid, pred, curr);
//        goto retry;        
//    }
    
    struct node_hmrtrv * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_hmrtrv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    num_allocs.add(tid, 1);
    
    new_node->next = curr;
    pred->next = new_node;

    cleanUpHMRTRV<true, true>(tid, pred, curr);
    return true;
}

bool lazylist_hmr_trv::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_hmrtrv *, struct node_hmrtrv *> dest_nodes = locate(tid, key);
    struct node_hmrtrv *pred = dest_nodes.first;
    struct node_hmrtrv *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting
    if (curr_key != key)
    {        
      cleanUpHMRTRV<>(tid);
      return false;
    }
    
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0);

    if (!tryAcquireLock(&pred->lock, tid))
    {
        cleanUpHMRTRV<>(tid);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        cleanUpHMRTRV<true, false>(tid, pred);
        goto retry;        
    }

//    if (!validate(pred, curr))
//    {
//        cleanUpHMRTRV<true, true>(tid, pred, curr);
//        goto retry;        
//    }
    
    curr->mark = true;
    pred->next = curr->next;    
    
    cleanUpHMRTRV<true, true>(tid, pred, curr);
    free (curr);
    num_retired.add(tid, 1);
    total_freed.add(tid, 1);

    return true;
}

long long lazylist_hmr_trv::getSumOfKeys()
{
    long long sum = 0;
    struct node_hmrtrv * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_hmr_trv::getDSSize()
{
    long long count = 0;
    struct node_hmrtrv * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_hmr_trv::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node_hmrtrv * curr = head;
    while(curr !=  NULL)
    {
//        printf("--> %u(%p)", curr->key, curr);
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());
    printf("total_retries=%lld \n", num_retries.getTotal());
}

#endif // LAZYLIST_HMRTRV_H
