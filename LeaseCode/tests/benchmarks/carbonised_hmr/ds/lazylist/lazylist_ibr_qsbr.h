#ifndef LAZYLIST_IBR_QSBR_H
#define LAZYLIST_IBR_QSBR_H

#include "lock.h"
#include "ibr_qsbr.h"

struct node_qsbr; //fwd declaration to satiate complaining compiler.
ibr_qsbr<struct node_qsbr> *qsbr = NULL;

//pad??
struct node_qsbr{
    unsigned int key;
    bool mark;
#ifdef SETBENCHLOCK
    volatile unsigned int lock;
#else
    pthread_mutex_t pt_lock;
#endif
    struct node_qsbr * next;

    node_qsbr(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
#ifdef SETBENCHLOCK            
        lock = 0;
#else            
        if (pthread_mutex_init(&pt_lock, NULL) != 0){
            printf("Mutex Initialization failed!");
            exit(EXIT_FAILURE);
        }
#endif
    }
};

void initQSBRReclaimer(unsigned int nthreads)
{
    qsbr = new ibr_qsbr<struct node_qsbr>(nthreads);
}

void deInitQSBRReclaimer()
{
    delete qsbr;
}

class lazylist_ibr_qsbr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    struct node_qsbr * head;
    struct node_qsbr * tail;    
    bool validate(struct node_qsbr * pred, struct node_qsbr * curr);
    void initNode(struct node_qsbr * n, unsigned int key);
    void printNode(struct node_qsbr * n);

public:
    lazylist_ibr_qsbr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr_qsbr();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr_qsbr::initNode(struct node_qsbr * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(qsbr !=  NULL);
}

lazylist_ibr_qsbr::lazylist_ibr_qsbr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
#ifndef HASHTABLE
    printf("ctor::lazylist_ibr_qsbr\n");
    qsbr = new ibr_qsbr<struct node_qsbr>(num_threads);
#endif
    
    head = new node_qsbr(min_key);
    tail = new node_qsbr(max_key);
    head->next = tail;
}

lazylist_ibr_qsbr::~lazylist_ibr_qsbr()
{
    struct node_qsbr * curr = head;
    struct node_qsbr * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
#ifndef HASHTABLE        
    printf("dtor::lazylist_ibr_qsbr\n");
    delete qsbr;
#endif
}

bool lazylist_ibr_qsbr::validate(struct node_qsbr * pred, struct node_qsbr * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_ibr_qsbr::contains(const unsigned int tid, const unsigned int key)
{
    qsbr->startOp(tid);
    
    struct node_qsbr * pred = head;
    struct node_qsbr * curr = pred->next;
    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    bool res = ((!curr->mark) && (curr->key == key)); 
    qsbr->endOp(tid);

    return res;
}

bool lazylist_ibr_qsbr::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    qsbr->startOp(tid);

    struct node_qsbr * pred = head;
    struct node_qsbr * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key == key)
    {
        qsbr->endOp(tid);
        return false;
    }   
    

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
//        num_vwritefails.add(tid, 1);
        qsbr->endOp(tid);
        goto retry;        
    }
    
//    struct node_qsbr * new_node = new node_qsbr(key);
    struct node_qsbr * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_qsbr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    new_node->next = curr;
    pred->next = new_node;
    
    qsbr->incAllocCounter(tid);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    qsbr->endOp(tid);

    return true;
}

bool lazylist_ibr_qsbr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    qsbr->startOp(tid);
    
    struct node_qsbr * pred = head;
    struct node_qsbr * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key != key)
    {
        qsbr->endOp(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif

    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
//        num_vwritefails.add(tid, 1);
        qsbr->endOp(tid);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;

    // Since simulator load/store are non faulty. a thread can access a null node_qsbr and
// if members of a null node_qsbr without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid nodes.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    qsbr->retire(tid, curr);    
    qsbr->endOp(tid);
    return true;
}

long long lazylist_ibr_qsbr::getSumOfKeys()
{
    long long sum = 0;
    struct node_qsbr * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr_qsbr::getDSSize()
{
    long long count = 0;
    struct node_qsbr * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr_qsbr::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node_qsbr * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

#endif // LAZYLIST_IBR_QSBR_H
