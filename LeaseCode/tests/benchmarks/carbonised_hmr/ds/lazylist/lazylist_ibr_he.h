/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lazylist_ibr_he.h
 * Author: mc
 *
 * Created on May 16, 2021, 2:01 PM
 */

#ifndef LAZYLIST_IBR_HE_H
#define LAZYLIST_IBR_HE_H

#include "lock.h"
#include "ibr_he.h"
#define MAX_HE 2

struct node_he; //fwd declaration to satiate complaining compiler.
ibr_he<struct node_he> *rec_he = NULL;

struct node_he{
    unsigned int key;
    bool mark;
#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif

//    struct node_he * next;
    std::atomic<struct node_he*> next;
    //rec based members
    uint64_t birth_epoch;
    uint64_t retire_epoch;

    node_he(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        assert(rec_he !=  NULL);
        
        birth_epoch = rec_he->getEpoch();
        retire_epoch = UINT64_MAX; //vulnerablept
    }
};

void initHEReclaimer(unsigned int nthreads)
{
    rec_he = new ibr_he<struct node_he>(nthreads, MAX_HE); //WARNING: NEEDS TO BE THE FIRST THING
}

void deInitHEReclaimer()
{
    delete rec_he;
}

class lazylist_ibr_he{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    //pad??

    struct node_he * head;
    struct node_he * tail;    
    bool validate(struct node_he * pred, struct node_he * curr);
    void initNode(struct node_he * n, unsigned int key);
    void printNode(struct node_he * n);

public:
    lazylist_ibr_he(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr_he();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr_he::initNode(struct node_he * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(rec_he !=  NULL);

    n->birth_epoch = rec_he->getEpoch();
    n->retire_epoch = UINT64_MAX; //vulnerablept
}

lazylist_ibr_he::lazylist_ibr_he(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
#ifndef HASHTABLE
    printf("ctor::lazylist_ibr_he\n");
    rec_he = new ibr_he<struct node_he>(num_threads, MAX_HE); //WARNING: NEEDS TO BE THE FIRST THING
#endif
    
//    head = new node_he(min_key);
//    tail = new node_he(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node_he)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_he)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);    


    head->next = tail;
}

lazylist_ibr_he::~lazylist_ibr_he()
{
    struct node_he * curr = head;
    struct node_he * temp_node_ibr = NULL;
    
    while(curr != NULL)
    {
        temp_node_ibr = curr;
        curr = curr->next;
        
        free(temp_node_ibr);
    }
#ifndef HASHTABLE        
    printf("dtor::lazylist_ibr_he\n");
    delete rec_he;
#endif
}

bool lazylist_ibr_he::validate(struct node_he * pred, struct node_he * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_ibr_he::contains(const unsigned int tid, const unsigned int key)
{
    unsigned int count_idx = 0;
    struct node_he * pred = head;    
    struct node_he * curr = rec_he->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_he->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_he->getReservation(tid, 0) == rec_he->getReservation(tid, 1));
//    printf("cont:: (%lld, %lld)", rec_he->getReservation(tid, 0), rec_he->getReservation(tid, 1));
    bool res = ((!curr->mark) && (curr->key == key)); 
    
    rec_he->clear(tid);
    return res;
}

bool lazylist_ibr_he::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int count_idx = 0;
    struct node_he * pred = head;    
    struct node_he * curr = rec_he->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_he->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_he->getReservation(tid, 0) == rec_he->getReservation(tid, 1));
//    printf("insert:: (%lld, %lld)", rec_he->getReservation(tid, 0), rec_he->getReservation(tid, 1));
    
    if (curr->key == key)
    {
        rec_he->clear(tid);
        return false;
    }   
    

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        num_vwritefails.add(tid, 1);
        rec_he->clear(tid);
        goto retry;        
    }
    
//    struct node_he * new_node_ibr = new node_he(key);
    struct node_he * new_node_ibr;
    if(posix_memalign( (void**)&new_node_ibr, 64, sizeof(struct node_he)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node_ibr, key);

    new_node_ibr->next = curr;
    pred->next = new_node_ibr;
    rec_he->incAllocCounter(tid);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    rec_he->clear(tid);
    return true;
}

bool lazylist_ibr_he::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int count_idx = 0;
    struct node_he * pred = head;    
    struct node_he * curr = rec_he->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_he->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_he->getReservation(tid, 0) == rec_he->getReservation(tid, 1));
//    printf("erase:: (%lld, %lld)", rec_he->getReservation(tid, 0), rec_he->getReservation(tid, 1));
    
    if (curr->key != key)
    {
        rec_he->clear(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif

    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        num_vwritefails.add(tid, 1);
        rec_he->clear(tid);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next.store(curr->next, std::memory_order_seq_cst);// = curr->next;

    // Since simulator load/store are non faulty. a thread can access a null node_he and
// if members of a null node_he without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid node_ibrs.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif    
    rec_he->retire(tid, curr);    

    rec_he->clear(tid);
    return true;
}

long long lazylist_ibr_he::getSumOfKeys()
{
    long long sum = 0;
    struct node_he * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr_he::getDSSize()
{
    long long count = 0;
    struct node_he * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr_he::printDebuggingDetails()
{
    //print list    
    printf("LIST: \n");
    struct node_he * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u (be=%lu, re=%lu)", curr->key, curr->birth_epoch, curr->retire_epoch);
        curr = curr->next;
    }
    printf("\n");
}


#endif /* LAZYLIST_IBR_HE_H */

