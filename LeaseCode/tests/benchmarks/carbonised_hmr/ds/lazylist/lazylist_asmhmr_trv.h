#ifndef LAZYLISTASMHMRTRV_H
#define LAZYLISTASMHMRTRV_H

#include <limits.h>
#include "lock.h"
#include "util.h"
//#define OPT

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

//operand size override prefix
#define ASF_66\
   ".byte 0x66\n\t"

//not part of class as node_trv type is needed to write MACROS or cleanUP function.
// could make inline functions part of class as well.
struct node_trv{
    unsigned int key;
    bool mark;
    volatile unsigned int lock;
    struct node_trv * next;

    node_trv(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
        lock = 0; 
    }
};//__attribute__((aligned(PADDING_BYTES)));

/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void trvcleanUp(const unsigned int tid, struct node_trv * pred = NULL, struct node_trv * curr = NULL) __attribute__((always_inline));


/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void trvcleanUp(const unsigned int tid, struct node_trv * pred = NULL, struct node_trv * curr = NULL)
{
//    if (usepred)
//    {
//        assert (pred && "usepred is true but pred == NULL" );
//    }
//    if (usecurr)
//    {
//        assert (curr && "usecurr is true but curr == NULL" );
//    }
    if (usepred && pred) releaseLock(&pred->lock, tid);
    if (usecurr && curr) releaseLock(&curr->lock, tid);
    CarbonRemoveAllFromWatchset();    
}

class lazylist_asmhmr_trv{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    PAD;
    struct node_trv * head;
    PAD;
    struct node_trv * tail;
    PAD;
    
    bool validate(struct node_trv * pred, struct node_trv * curr);
    void initNode(struct node_trv * n, unsigned int key);
    void printNode(struct node_trv * n);

public:
    lazylist_asmhmr_trv(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_asmhmr_trv();
    
    std::pair<struct node_trv *, struct node_trv *> locate(const unsigned int tid, const unsigned int key);

    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_asmhmr_trv::initNode(struct node_trv * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    n->lock = 0; 
}

void lazylist_asmhmr_trv::printNode(struct node_trv * n)
{
    printf("n(%p)::  ", n);
    printf("n->key(%u), ", n->key);
    printf("n->mark(%u), ", n->mark);
    printf("n->next(%p), ", n->next);
    printf("n->lock(%u)\n", n->lock);
}

lazylist_asmhmr_trv::lazylist_asmhmr_trv(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
#ifndef HASHTABLE
    printf("node size=%lu \n", sizeof(node_trv));
    printf("ctor::lazylist_asmhmr_trv\n");
#endif
    
    if(posix_memalign( (void**)&head, 64, sizeof(struct node_trv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_trv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);
    head->next = tail;
}

lazylist_asmhmr_trv::~lazylist_asmhmr_trv()
{
    struct node_trv * curr = head;
    struct node_trv * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }

#ifndef HASHTABLE    
    printf("dtor::lazylist_asmhmr_trv\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());   

    printf("total_retries=%lld \n", num_retries.getTotal());     
    printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
    printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());    
#endif
//    printf("~lazylist_asmhmr_trv: sizeof(struct node_trv)=%d, sizeof(struct node_trv*)=%d\n", sizeof(struct node_trv), sizeof(struct node_trv*));
}

bool lazylist_asmhmr_trv::validate(struct node_trv * pred, struct node_trv * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \    
        goto retry; \
    }

//#define try_vread(field_addr, ret_val, field_size) \
//    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
//    DEBUG printf("vread field (%p)\n",(void*)ret_val);
//    if (ret_val == 0xffffffff) \
//    { \
//        CarbonRemoveAllFromWatchset(); \    
//        DEBUG printf("retry (%p)\n", (void*)ret_val); \
//        goto retry; \
//    }

std::pair<struct node_trv *, struct node_trv *> lazylist_asmhmr_trv::locate(const unsigned int tid, const unsigned int key)
{
#ifdef USEASMADDTAG
    struct node_asmhmr * dummypred;
#endif    
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
    unsigned int curr_key = 0xffffffff;
    retry:
    
    struct node_trv * pred = head;
//    CarbonAddToWatchset((void*) pred, sizeof(struct node_trv));

    //How to use: just deref the pointer you would pass to CarbonAddToWatchset
// NOT USING asm-read version of addTag becuase some how read its next field in VREAD fails.
// Any way since its outside the loop it doesn't matter that much for num of instruction counts.
//    __asm__ __volatile__ (ASF_F3 "mov %1, %0"
//        :"=r" (dummypred)
//        : "m" (*pred)
//        ); 

    //tags first node of list
    bool isMark;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (isMark)
        : "m" (pred->mark)
        ); 
    if (true == isMark)
    { 
//        printf("lazylist_asmhmr_trv::locate read of pred next failed\n");
//        fflush(stdout);
        num_vreadfails.add(tid, 1);
        CarbonRemoveAllFromWatchset();
//        num_retries.add(tid, 1);
        goto retry; 
    }
    
    struct node_trv * curr;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr)
        : "m" (pred->next)
        ); 
    if ((unsigned int long)curr == 0xffffffff)
    { 
//        printf("lazylist_asmhmr_trv::locate read of pred next failed\n");
//        fflush(stdout);
        num_vreadfails.add(tid, 1);
        CarbonRemoveAllFromWatchset();
//        num_retries.add(tid, 1);
        goto retry; 
    }
    
//    CarbonAddToWatchset((void*) curr, sizeof(struct node_trv));  
    
    //DISCUSSME: DO we need to vread the key field of a node?
//tags the second node of list
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    
    while(curr_key < key)
    {
#ifdef USEASMADDTAG
        //asm version of removeFromWatchset
        __asm__ __volatile__ (ASF_F3 "mov %1, %0"
            :"=r" (dummypred)
            : "m" (*pred)
            ); 
#else        
        CarbonRemoveFromWatchset((void*) pred, sizeof(struct node_trv));        
#endif
        pred = curr;
       
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr)
            : "m" (curr->next)
            ); 
        if ((unsigned int long)curr == 0xffffffff)
        {
            CarbonRemoveAllFromWatchset();
            num_vreadfails.add(tid, 1);
//            num_retries.add(tid, 1);
            goto retry; 
        }

    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (isMark)
        : "m" (curr->mark)
        ); 
    if (true == isMark)
    { 
//        printf("lazylist_asmhmr_trv::locate read of pred next failed\n");
//        fflush(stdout);
        num_vreadfails.add(tid, 1);
        CarbonRemoveAllFromWatchset();
//        num_retries.add(tid, 1);
        goto retry; 
    }        
        //        CarbonAddToWatchset((void*) curr, sizeof(struct node_trv));  
    
        //DISCUSSME: DO we need to vread the key field of a node?
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr_key)
            : "m" (curr->key)
            ) ;
    }
    //check if loop was exited due to failed validation for curr->key read
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset(); 
        trvcleanUp<>(tid);
        num_vreadfails.add(tid, 1);
//        num_retries.add(tid, 1);
        goto retry;
    }
    return std::make_pair (pred, curr);
}       

bool lazylist_asmhmr_trv::contains(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node_trv *, struct node_trv *> dest_nodes = locate(tid, key);
    struct node_trv *pred = dest_nodes.first;
    struct node_trv *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
        trvcleanUp<>(tid);
        num_vreadfails.add(tid, 1);
//        num_retries.add(tid, 1);
        goto retry;
    }
    
//    bool curr_mark;
//    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//        :"=r" (curr_mark)
//        : "m" (curr->mark)
//        ) ;
//    if (curr_mark == 0xffffffff)
//    { 
////        CarbonRemoveAllFromWatchset();   
//        num_vreadfails.add(tid, 1);
//        trvcleanUp<>(tid);
////        num_retries.add(tid, 1);
//        goto retry;
//    }    
    
    bool res = ((curr_key == key));
    trvcleanUp<>(tid);
    return res;
}

//UINFO: remove watched address before returning from all return points of the functions.
bool lazylist_asmhmr_trv::insert(const unsigned int tid, const unsigned int key)
{
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    retry:

    std::pair<struct node_trv *, struct node_trv *> dest_nodes = locate(tid, key);
    struct node_trv *pred = dest_nodes.first;
    struct node_trv *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        trvcleanUp<>(tid);
        num_vreadfails.add(tid, 1);
//        num_retries.add(tid, 1);
        goto retry;
    }

    if (curr_key == key)
    {
        trvcleanUp<>(tid);
        return false;
    }   
    
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

    if (!tryAcquireLock(&pred->lock, tid))
    {
        trvcleanUp<>(tid);
//        num_vreadfails.add(tid, 1);
        num_retries.add(tid, 1);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        trvcleanUp<true, false>(tid, pred);
//        num_vreadfails.add(tid, 1);
        num_retries.add(tid, 1);
        goto retry;        
    }

//    if (!validate(pred, curr))
//    {
//        trvcleanUp<true, true>(tid, pred, curr);
////        num_vwritefails.add(tid, 1);
//        goto retry;        
//    }
    
    struct node_trv * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node_trv)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
//    num_allocs.add(tid, 1);
    
    new_node->next = curr;
    pred->next = new_node;

    trvcleanUp<true, true>(tid, pred, curr);
    return true;
}

bool lazylist_asmhmr_trv::erase(const unsigned int tid, const unsigned int key)
{
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
    retry:

    std::pair<struct node_trv *, struct node_trv *> dest_nodes = locate(tid, key);
    struct node_trv *pred = dest_nodes.first;
    struct node_trv *curr = dest_nodes.second;
    
    unsigned int curr_key;
    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        trvcleanUp<>(tid);
        num_vreadfails.add(tid, 1);
//        num_retries.add(tid, 1);
        goto retry;
    }

    if (curr_key != key)
    {        
      trvcleanUp<>(tid);
      return false;
    }
    
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0);

    if (!tryAcquireLock(&pred->lock, tid))
    {
        trvcleanUp<>(tid);
//        num_vreadfails.add(tid, 1);
        num_retries.add(tid, 1);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        trvcleanUp<true, false>(tid, pred);
//        num_vreadfails.add(tid, 1);
        num_retries.add(tid, 1);
        goto retry;        
    }

//    if (!validate(pred, curr))
//    {
//        trvcleanUp<true, true>(tid, pred, curr);
////        num_vwritefails.add(tid, 1);
//        goto retry;        
//    }
    
    curr->mark = true;
    pred->next = curr->next;    
    
    trvcleanUp<true, true>(tid, pred, curr);
    free (curr);
//    num_retired.add(tid, 1);
//    total_freed.add(tid, 1);

    return true;
}

long long lazylist_asmhmr_trv::getSumOfKeys()
{
    long long sum = 0;
    struct node_trv * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_asmhmr_trv::getDSSize()
{
    long long count = 0;
    struct node_trv * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_asmhmr_trv::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node_trv * curr = head;
    while(curr !=  NULL)
    {
//        printf("--> %u(%p)", curr->key, curr);
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
//    printf("total_retired=%lld \n", num_retired.getTotal());
//    printf("total_allocated=%lld \n", num_allocs.getTotal());
//    printf("total_freed=%lld \n", total_freed.getTotal());
//    printf("total_retries=%lld \n", num_retries.getTotal());
}

#endif // LAZYLISTASMHMRTRV_H
