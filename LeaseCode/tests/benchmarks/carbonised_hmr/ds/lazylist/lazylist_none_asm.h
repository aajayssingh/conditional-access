#ifndef LAZYLIST_NONE_ASM_H
#define LAZYLIST_NONE_ASM_H

#include <limits.h>
#include "lock.h"
#include "util.h"

class lazylist_none_asm{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
PAD; // each node is posix_memalign allolcated so not needed
    struct node{
        unsigned int key;
        bool mark;

#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif

        struct node * next;

        node(unsigned int key): key(key)
        {
            mark = false;
            next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        }
    };

PAD;    
    struct node * head;
PAD;
    struct node * tail;    
PAD;
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_none_asm(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_none_asm();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_none_asm::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
}

lazylist_none_asm::lazylist_none_asm(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
#ifndef HASHTABLE
    printf("ctor::lazylist_none_asm\n");
    printf("node size=%lu \n", sizeof(node));
#endif
    
//    head = new node(min_key);
//    tail = new node(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);

    head->next = tail;

}

lazylist_none_asm::~lazylist_none_asm()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
#ifndef HASHTABLE
    printf("dtor::lazylist_none_asm\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());
    printf("total_retries=%lld \n", num_retries.getTotal());
    printf("num_vreadfails=%lld \n", num_vreadfails.getTotal());  
    printf("num_vwritefails=%lld \n", num_vwritefails.getTotal());    
#endif
}

bool lazylist_none_asm::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

//noneasm
bool lazylist_none_asm::contains(const unsigned int tid, const unsigned int key)
{
    unsigned int long ret_val = 0;    
    struct node * pred = head;
    
    struct node * curr;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr)
        : "m" (pred->next)
        ); 
    
    unsigned int curr_key = 0xffffffff;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;    
    
    while(curr_key < key)
    {
        pred = curr;
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr)
            : "m" (curr->next)
            ); 
    
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr_key)
            : "m" (curr->key)
            ) ;
    }
    
    bool curr_mark;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr_mark)
        : "m" (curr->mark)
        ) ;

    return ((!curr_mark) && (curr_key == key));
}

bool lazylist_none_asm::insert(const unsigned int tid, const unsigned int key)
{
    retry:

    unsigned int long ret_val = 0;    
    struct node * pred = head;
    struct node * curr;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr)
        : "m" (pred->next)
        ); 
    
    unsigned int curr_key = 0xffffffff;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;    
    
    while(curr_key < key)
    {
        pred = curr;
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr)
            : "m" (curr->next)
            ); 
    
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr_key)
            : "m" (curr->key)
            ) ;
    }

    if (curr_key == key)
    {
        return false;
    }   
//    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
//        if (!tacquireLock(&pred->lock))
//        {
////            num_retries.add(tid, 1);
//            goto retry;
//        }
//        
//        if (!tacquireLock(&curr->lock))
//        {
////            num_retries.add(tid, 1);
//            releaseLock(&pred->lock, tid);
//            goto retry;
//        }    
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    
    if (!validate(pred, curr))
    {

#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        num_retries.add(tid, 1);
        //        num_vwritefails.add(tid, 1);
        goto retry;        
    }
    
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    new_node->next = curr;
    pred->next = new_node;
    num_allocs.add(tid, 1);
    
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);    
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    return true;
}

bool lazylist_none_asm::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int long ret_val = 0;    
    struct node * pred = head;
    struct node * curr;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr)
        : "m" (pred->next)
        ); 
    
    unsigned int curr_key = 0xffffffff;
    __asm__ __volatile__ ("mov %1, %0"
        :"=r" (curr_key)
        : "m" (curr->key)
        ) ;    
    
    while(curr_key < key)
    {
        pred = curr;
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr)
            : "m" (curr->next)
            ); 
    
        __asm__ __volatile__ ("mov %1, %0"
            :"=r" (curr_key)
            : "m" (curr->key)
            ) ;
    }
    
    if (curr_key != key)
    {
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);    

//        if (!tacquireLock(&pred->lock))
//        {
////            num_retries.add(tid, 1);
//            goto retry;
//        }
//        
//        if (!tacquireLock(&curr->lock))
//        {
////            num_retries.add(tid, 1);            
//            releaseLock(&pred->lock, tid);
//            goto retry;
//        }    
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
//        num_vwritefails.add(tid, 1);
        num_retries.add(tid, 1);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;

//    CarbonWrite((void*)&curr->mark, 1, sizeof(unsigned int));
//    CarbonWrite((void*)&pred->next, (unsigned int long)curr->next, sizeof(struct node *));
    
// Since simulator load/store are non faulty. a thread can access a null node and
// if members of a null node without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid nodes.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK   
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);    
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    return true;
}

long long lazylist_none_asm::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_none_asm::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_none_asm::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u (%p)", curr->key, curr);
        curr = curr->next;
    }
    printf("\n");
}
#endif // LAZYLIST_NONE_ASM_H
