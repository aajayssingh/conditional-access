/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   extbst_ibr_hp.h
 * Author: mc
 *
 * Created on September 26, 2021, 11:28 AM
 */

#ifndef EXTBST_IBR_HP_H
#define EXTBST_IBR_HP_H

#include "lock.h"
#include "ibr_hp.h"
#define MAX_HP 3

struct node_hp; //fwd declaration to satiate complaining compiler.
ibr_hp<struct node_hp> *rec_hp = NULL;

struct node_hp{
    unsigned int key;
    unsigned int lock;
    bool mark;

//    node_hp* left;
//    node_hp* right;
    std::atomic<struct node_hp*> left;    
    std::atomic<struct node_hp*> right;  

    node_hp(unsigned int key): key(key)
    {
        mark = false;
        lock = 0;
        left = NULL;
        right = NULL;
//        if (pthread_mutex_init(&pt_lock, NULL) != 0){
//            printf("Mutex Initialization failed!");
//            exit(EXIT_FAILURE);
//        }
        assert(rec_hp !=  NULL);
    }    

    bool isLeaf()
    {
        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;
    }

    bool isParentOf(node_hp * other)
    {
        return ((left == other) || (right == other));
    }    
};


class extbst_ibr_hp
{
private:
    struct searchRecord{
        node_hp* volatile	gp; //grand parent
        node_hp* volatile	p; //parent
        node_hp* volatile	n; //leaf node_hp
        
        searchRecord(node_hp* _gp, node_hp* _p, node_hp* _n): gp(_gp), p(_p), n(_n){}
    };    
    
    node_hp* volatile root;
    const unsigned int num_threads;
    const unsigned int min_key, max_key;
    
    node_hp* createLeafNode(const unsigned int tid, const unsigned int key)
    {
        return createInternalNode(tid, key, NULL, NULL);
    }
    
    node_hp* allocateNode(const unsigned int tid)
    {
//        struct asm_node* nd =  new asm_node();
        struct node_hp* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(node_hp)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            fflush(stdout);
            assert(0 && "posix_memalign could not allocate memory");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff && "allocated address cannot be this reserved value");
        
        assert(rec_hp !=  NULL);
        rec_hp->incAllocCounter(tid);
        return nd;
    }
    
    node_hp* createInternalNode(const unsigned int tid, const unsigned int key, node_hp * left, node_hp * right)
    {
        node_hp* new_node = allocateNode(tid); //new node_hp();
        new_node->key = key;
        new_node->lock = 0;
        new_node->mark = 0;
        new_node->left = left;
        new_node->right = right;
        
        return new_node;     
    }
    
public:
    extbst_ibr_hp(const unsigned int _num_threads, const unsigned int _min_key, const unsigned int _max_key): num_threads(_num_threads), min_key(_min_key), max_key(_max_key)
    {
        const unsigned int tid = 0;
        rec_hp = new ibr_hp<struct node_hp>(num_threads, MAX_HP);

        node_hp* leftRoot = createLeafNode(tid, _min_key);
        node_hp* rightRoot = createLeafNode(tid, _max_key);
        root = createInternalNode(tid, _min_key, leftRoot, rightRoot);
    }
    
    ~extbst_ibr_hp()
    {
        //free all nodes
        freeTree(root);

        printf("dtor::extbst_ibr_hp\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
//        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete rec_hp;
    }
    
    void freeTree (struct node_hp* nd)
    {
        if(nd==NULL)
            return;

        freeTree(nd->right);
        freeTree(nd->left);

        free(nd);
    }
    
    struct searchRecord search(const unsigned int tid, const unsigned int key)
    {
        unsigned int count_idx = 0;
        node_hp * gp = NULL;
        node_hp * p = NULL;
        node_hp * n = root;
        
        while(!n->isLeaf())
        {
            gp = p;
            p = n;
//            n = (key <= n->key) ? n->left : n->right;
            n = (key <= n->key) ? rec_hp->read(tid, count_idx%3, n->left) : rec_hp->read(tid, count_idx%3, n->right);
            count_idx++;
        }
        return searchRecord(gp, p, n);
    }
    
    bool validateIns(node_hp* p, node_hp* n)
    {
        return ( (!(n->mark)) && (!(p->mark)) && (p->isParentOf(n)) );
    }
    
    bool validateErase(node_hp* gp, node_hp* p, node_hp* n)
    {
        bool res =  ( (!(gp->mark)) && (gp->isParentOf(p)) && (!(p->mark)) && (p->isParentOf(n)) && (!(n->mark)) );
//        printf("%d %d %d %d %d\n", (!gp->mark), (gp->isParentOf(p)), (!p->mark), (p->isParentOf(n)), (!n->mark));
        return res;
    }
    
    bool insert(const unsigned int tid, const unsigned int key){
        
        while(true)
        {
            struct searchRecord ret = search(tid, key);

            //decide childs for new newnode
            node_hp* newLeaf = createLeafNode(tid, key);
            bool leftdir = (key < ret.n->key);
            node_hp * leftChild = (leftdir) ? newLeaf : ret.n;
            node_hp * rightChild = (leftdir) ? ret.n: newLeaf;
            node_hp* newInternalNode = (leftdir)? createInternalNode(tid, key, leftChild, rightChild) : createInternalNode(tid, ret.n->key, leftChild, rightChild);

            //atomically insert newInternalNode
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            //check if key already present after locking to avoid derefing freed node_hp.
            if (ret.n->key == key)
            {
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
                rec_hp->clear(tid);
                return false; //key already present
            }

            {
                if ( validateIns(ret.p, ret.n) )
                {
                    if(ret.n == ret.p->left)
                    {
                        ret.p->left = newInternalNode;
                    }
                    else{
                        ret.p->right = newInternalNode;
                    }
                    releaseLock(&ret.p->lock, tid);
                    releaseLock(&ret.n->lock, tid);
                    rec_hp->clear(tid);
                    return true;
                }
                //validation failed
#ifdef COLLECTSTAT                
//                num_vwritefails.add(tid, 1);
#endif
                free(newLeaf);
                free(newInternalNode);
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
            }
            rec_hp->clear(tid);
        } //while true
    }
    
    bool erase(const unsigned int tid, const unsigned int key){

        while(true)
        {
            struct searchRecord ret = search(tid, key);
            acquireLock(&ret.gp->lock);
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            if (ret.n->key != key)
            {
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);  
                rec_hp->clear(tid);
                return false;
            }
            {
                if ( validateErase(ret.gp, ret.p, ret.n) )
                {
                    //mark n and p
                    ret.p->mark =  true;
                    ret.n->mark =  true;

                    node_hp * sibling = (ret.p->left == ret.n) ? ret.p->right : ret.p->left;
                    if (ret.gp->left == ret.p)
                    {
                        ret.gp->left = sibling;
                    }
                    else
                    {
                        ret.gp->right = sibling;
                    }

                    releaseLock(&ret.gp->lock, tid);
                    releaseLock(&ret.p->lock, tid);       
                    releaseLock(&ret.n->lock, tid);    
                    
                    rec_hp->retire(tid, ret.p);
                    rec_hp->retire(tid, ret.n);

                    rec_hp->clear(tid);
                    return true;

                }
#ifdef COLLECTSTAT                
                num_vwritefails.add(tid, 1);
#endif
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);       
            }
            rec_hp->clear(tid);
        }//while
    }
    
    bool contains(const unsigned int tid, const unsigned int key){
        struct searchRecord ret = search(tid, key);
        
        bool res = ( !(ret.n->mark) && (ret.n->key == key) );
        rec_hp->clear(tid);
        return res;
    }
    
    long long sumLeafKeys(node_hp *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key) )
            return nd->key;
        else
            return sumLeafKeys(nd->left) + sumLeafKeys(nd->right);                       
    }    
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      sum = sumLeafKeys(root);
      return sum;
    }

    void printLeafs(node_hp *nd)
    {
        if (nd == NULL)
            return;
        if (nd->isLeaf())
        {
            printf("%u ", nd->key);
        }
        printLeafs(nd->left);
        printLeafs(nd->right);                       
    }
    
    long long countLeafs(node_hp *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key)  )
            return 1;
        else
            return countLeafs(nd->left) + countLeafs(nd->right);                       
    }
  
    
   long long getDSSize()
   {
      long long size = 0;
      size = countLeafs(root);
      return size;
   }      
    
    void printTree(node_hp * nd)
    {
        if (nd == NULL)
            return;
        printTree(nd->left);
        std::cout << nd->key<<"("<<nd->mark<<")" <<" ";
        assert(!nd->mark);
        printTree(nd->right);               
    }
    void printDebuggingDetails()
    {
//        std::cout<<"Printing tree:\n";
//        printTree(root);
//        std::cout<<std::endl;
        
        std::cout<<"Printing leafs:\n";
        printLeafs(root);
        std::cout<<std::endl;
    }       
};

#endif /* EXTBST_IBR_HP_H */

