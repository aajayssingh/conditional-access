/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   extbst_hmr.h
 * Author: mc
 *
 * Created on September 27, 2021, 6:50 PM
 */
#ifndef EXTBST_HMR_H
#define EXTBST_HMR_H

#include "lock.h"
#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

#define VERROR 0xffffffff

struct hmr_node{
    unsigned int key;
    volatile unsigned int lock;
    bool mark;

    struct hmr_node* left;
    struct hmr_node* right;

    bool isLeaf()
    {
        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;
    }

    bool isParentOf(struct hmr_node * other)
    {
        return ((left == other) || (right == other));
    }    
};

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size);\
    if (ret_val == 0xffffffff)\
    {\
        CarbonRemoveAllFromWatchset();\
        goto retry;\
    }

#define vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size);

class extbst_hmr
{
private:

    struct searchRecord{
        hmr_node* volatile	gp; //grand parent
        hmr_node* volatile	p; //parent
        hmr_node* volatile	n; //leaf hmr_node
        unsigned int leaf_key;
        
        searchRecord(hmr_node* _gp, hmr_node* _p, hmr_node* _n, unsigned int _leaf_key): gp(_gp), p(_p), n(_n), leaf_key(_leaf_key){}
    };    
    
    hmr_node* volatile root;
    const unsigned int num_threads;
    const unsigned int min_key, max_key;
    
    hmr_node* allocateNode(const unsigned int tid)
    {
//        struct hmr_node* nd =  new hmr_node();
        struct hmr_node* nd;
        int res = posix_memalign( (void**)&nd, 64, sizeof(hmr_node));
        if( res != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            fflush(stdout);
            assert(0 && "posix_memalign could not allocate memory");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff && "allocated address cannot be this reserved value");
        assert(((unsigned long int)nd)%64 == 0 && "not cachelinesize aligned");

        num_allocs.add(tid, 1);
        return nd;
    }
    
    hmr_node* createLeafNode(const unsigned int tid, const unsigned int key)
    {
        return createInternalNode(tid, key, NULL, NULL);
    }
    
    hmr_node* createInternalNode(const unsigned int tid, const unsigned int key, hmr_node * left, hmr_node * right)
    {
        hmr_node* new_node = allocateNode(tid);
        new_node->key = key;
        new_node->lock = 0;
        new_node->mark = 0;
        new_node->left = left;
        new_node->right = right;
        return new_node;     
    }
    
public:
    extbst_hmr(const unsigned int _num_threads, const unsigned int _min_key, const unsigned int _max_key): num_threads(_num_threads), min_key(_min_key), max_key(_max_key)
    {
        printf("node size=%lu \n", sizeof(hmr_node));
        fflush(stdout);
        hmr_node* leftRoot = createLeafNode(0, _min_key);
        hmr_node* rightRoot = createLeafNode(0, _max_key);
        root = createInternalNode(0, _min_key, leftRoot, rightRoot);
    }
    
    ~extbst_hmr()
    {
        //free all nodes
        freeTree(root);

        printf("dtor::extbst_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   
        printf("total_retries=%lld \n", num_retries.getTotal());        

    }
    
    void freeTree (struct hmr_node* nd)
    {
        if(nd==NULL)
            return;

        freeTree(nd->right);
        freeTree(nd->left);

        free(nd);
    }

    
    unsigned int long isLeaf(struct hmr_node* nd, const unsigned int tid)
    {
        hmr_node * left = NULL;
        hmr_node * right = NULL;
        unsigned int long ret_val = 0;

        CarbonAddToWatchset((void*) nd, sizeof(struct hmr_node));

        vread(&nd->left, ret_val, sizeof(struct hmr_node*))  
        left = (struct hmr_node*)ret_val;
        if ((unsigned int long)left == VERROR)
        { 
            CarbonRemoveAllFromWatchset();
            return VERROR; 
        }
        
        vread(&nd->right, ret_val, sizeof(struct hmr_node*))  
        right = (struct hmr_node*)ret_val;
 
        if ((unsigned int long)right == VERROR)
        { 
            CarbonRemoveAllFromWatchset();
            return VERROR; 
        }

        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;        
    }
    
    //USAGENOTE: Ensure that you invoke CarbonRemoveAllFromWatchset() at the function 
    // that calls search(). Search return gp, p and n added in watchset.   
    struct searchRecord search(const unsigned int tid, const unsigned int key)
    {
        retry:

        hmr_node * gp = NULL;
        hmr_node * p = NULL;
        hmr_node * n = root;
        unsigned int long ret_val = 0;
        
        do
        {
            unsigned int long is_leaf = isLeaf(n, tid);
            if (is_leaf == VERROR)
            {
                //num_retries.add(tid, 1);
//                CarbonRemoveAllFromWatchset(); //already removed by the function returning VERROR
                goto retry;
            }
            else if (1 == is_leaf)
            {
                break; //successfully reached the location
            }
            
            //not a leaf and all vreads succeeded so far continue tree traversal
            // n is in watchset get next node
            
            //get current node key
            unsigned int curr_key;
            try_vread(&n->key, ret_val, sizeof(unsigned int))
            curr_key = (unsigned int)ret_val;
            
            //read left node
            hmr_node * curr_left = NULL;            
            try_vread(&n->left, ret_val, sizeof(struct hmr_node*))    
            curr_left = (struct hmr_node *)ret_val;
            
            //read right node
            hmr_node * curr_right = NULL;            
            try_vread(&n->right, ret_val, sizeof(struct hmr_node*))    
            curr_right = (struct hmr_node *)ret_val;
            
            if (gp)
            {
                CarbonRemoveFromWatchsetOpt((void*) gp);
//                CarbonRemoveFromWatchset((void*) gp, sizeof(struct hmr_node));
            }

            gp = p;
            p = n;
            n = (key <= curr_key) ? curr_left : curr_right;            
        }while(true);
        
        //get current node key
        unsigned int curr_key;
        try_vread(&n->key, ret_val, sizeof(unsigned int))
        curr_key = (unsigned int)ret_val;
        
        // INV: at this point This thread have gp, p and n in watchset
        // I mean there was a time when gp, p and n were in watchset.
        assert(p);
        assert(n);
        return searchRecord(gp, p, n, curr_key);
    }
    
    // NOTE for HMR needn't check for mark and next pointers Validation is not needed as VLOCK has implicit validation.
    bool validateIns(hmr_node* p, hmr_node* n)
    {
        assert(!p->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!n->mark && "Since the node is VLOCKED it cannot be marked.");

        return ( (!(n->mark)) &&(!(p->mark)) && (p->isParentOf(n)) );
    }
    
    // NOTE for HMR needn't check for mark and next pointers Validation is not needed as VLOCK has implicit validation.
    bool validateErase(hmr_node* gp, hmr_node* p, hmr_node* n)
    {
        bool res =  ( (!gp->mark) && (gp->isParentOf(p)) && (!p->mark) && (p->isParentOf(n)) && (!n->mark));
//        printf("%d %d %d %d %d\n", (!gp->mark), (gp->isParentOf(p)), (!p->mark), (p->isParentOf(n)), (!n->mark));
        assert(!gp->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!p->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!n->mark && "Since the node is VLOCKED it cannot be marked.");
        return res;
    }
    
    bool insert(const unsigned int tid, const unsigned int key){
        retry :
        {
            struct searchRecord ret = search(tid, key);

            if (ret.leaf_key == key)
            {
                CarbonRemoveAllFromWatchset();
                return false; //key already present
            }            

            if (!tryAcquireLock(&ret.p->lock, tid))
            {
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }

            if (!tryAcquireLock(&ret.n->lock, tid))
            {
                releaseLock(&ret.p->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }            

            assert(!(ret.p->mark));
            assert(!(ret.n->mark));
            assert((ret.p->lock));
            assert((ret.n->lock));
            
            //decide childs for new newnode
            hmr_node* newLeaf = createLeafNode(tid, key);
            bool leftdir = (key < ret.leaf_key);
            hmr_node * leftChild = (leftdir) ? newLeaf : ret.n;
            hmr_node * rightChild = (leftdir) ? ret.n: newLeaf;
//            hmr_node* newInternalNode = createInternalNode(tid, key, leftChild, rightChild);
            assert(ret.leaf_key == ret.n->key);
            hmr_node* newInternalNode = (leftdir)? createInternalNode(tid, key, leftChild, rightChild) : createInternalNode(tid, ret.leaf_key, leftChild, rightChild);

            {
                // FIXME: Validation not needed as retry vlocks have implicit validation
                if ( validateIns(ret.p, ret.n) )
                {
                    if(ret.n == ret.p->left)
                    {
                        ret.p->left = newInternalNode;
                    }
                    else{
                        ret.p->right = newInternalNode;
                    }
                    
                    assert(!ret.p->mark);
                    assert(!ret.n->mark);

                    releaseLock(&ret.p->lock, tid);
                    releaseLock(&ret.n->lock, tid);
                    CarbonRemoveAllFromWatchset();
                    return true;
                }
                //validation failed
                assert(0 && "one thread case should not fail validation");
                free(newLeaf);
                free(newInternalNode);
                
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
                CarbonRemoveAllFromWatchset();
                goto retry;
            }
        } //retry
    }
    
    bool erase(const unsigned int tid, const unsigned int key){
        retry:
        {
            //INV: when search returns gp. p and n are in watchset.
            struct searchRecord ret = search(tid, key);
            
            if (ret.leaf_key != key)
            {
                CarbonRemoveAllFromWatchset();
                return false; //key already present
            }            

            // at this point if other thread has marked any of gp, p & n or 
            // in any way modified gp, p,n then this thread would fail its watchset 
            // validation. Hence safe.
            assert(ret.gp);
            if (!tryAcquireLock(&ret.gp->lock, tid))
            {
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }


            if (!tryAcquireLock(&ret.p->lock, tid))
            {
                releaseLock(&ret.gp->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }
           
            if (!tryAcquireLock(&ret.n->lock, tid))
            {
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }            

            assert(!(ret.gp->mark));
            assert(!(ret.p->mark));
            assert(!(ret.n->mark));
            
            assert((ret.gp->lock));
            assert((ret.p->lock));
            assert((ret.n->lock));
            
            // after all gp, p and n have been locked then no other thread could modify
           // these nodes by virtue of mutual exclusion provided by VLOCKs
           // That means I can assert that gp p and n are never seen as marked.
            {
                // FIXME: Validation not needed as retry vlocks have implicit validation
                if ( validateErase(ret.gp, ret.p, ret.n) )
                {
                    //mark n and p any thread doing concurrent vread/vwrite will fail
                    // as marking will cause watchset to be downgraded.
                    assert(!(ret.gp->mark));
                    assert(!(ret.p->mark));
                    assert(!(ret.n->mark));

                    ret.p->mark =  true;
                    ret.n->mark =  true;

                    hmr_node * sibling = (ret.p->left == ret.n) ? ret.p->right : ret.p->left;
                    if (ret.gp->left == ret.p)
                    {
                        ret.gp->left = sibling;
                    }
                    else
                    {
                        ret.gp->right = sibling;
                    }

                    releaseLock(&ret.gp->lock, tid);
                    releaseLock(&ret.p->lock, tid);       
                    releaseLock(&ret.n->lock, tid);
                    CarbonRemoveAllFromWatchset();
                    free (ret.p);
                    free (ret.n);

                    num_retired.add(tid, 1);
                    total_freed.add(tid, 1);
                    
                    return true;

                }
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);  
                CarbonRemoveAllFromWatchset();
            }
        }//while
        
        return false;
    }
    
    bool contains(const unsigned int tid, const unsigned int key){
        struct searchRecord ret = search(tid, key);
        // It safe and correct to return leaf_key as there was a time
        // when this thread vread leaf node and read the leaf_key during search operation.
        assert ((ret.leaf_key <= max_key) && (ret.leaf_key > min_key));
        CarbonRemoveAllFromWatchset();
        return (key == ret.leaf_key);
    }
    
    long long sumLeafKeys(hmr_node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key) )
            return nd->key;
        else
            return sumLeafKeys(nd->left) + sumLeafKeys(nd->right);                       
    }    
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      sum = sumLeafKeys(root);
      return sum;
    }
    
    long long countLeafs(hmr_node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key)  )
            return 1;
        else
            return countLeafs(nd->left) + countLeafs(nd->right);                       
    }
  
    
   long long getDSSize()
   {
      long long size = 0;
      size = countLeafs(root);
      return size;
   }      
    
    void printTree(hmr_node * nd)
    {
        if (nd == NULL)
            return;
        printTree(nd->left);
//        printf("%u(%p) ", nd->key, nd);
        std::cout << nd->key<<"-"<<nd<<" ";
        assert(!nd->mark);

        printTree(nd->right);               
    }

    void printLeafs(hmr_node *nd)
    {
        if (nd == NULL)
            return;
        if (nd->isLeaf())
        {
            printf("%u ", nd->key);
        }
        printLeafs(nd->left);
        printLeafs(nd->right);                       
    }
    
    
    void printDebuggingDetails()
    {
//        std::cout<<"Printing tree:\n";
//        printTree(root);
//        std::cout<<std::endl;

        std::cout<<"Printing leafs:\n";
        printLeafs(root);
        std::cout<<std::endl;        
    }       
};

#endif /* EXTBST_HMR_H */

