/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   extbst_none_asm.h
 * Author: mc
 *
 * Created on September 26, 2021, 11:28 AM
 */
#include "lock.h"
#include "util.h"


#ifndef EXTBST_NONE_ASM_H
#define EXTBST_NONE_ASM_H

class extbst_none_asm
{
private:
    struct node{
        unsigned int key;
        volatile unsigned int lock;
        bool mark;

        node* left;
        node* right;
        
        bool isLeaf()
        {
            bool res = (left == NULL);
            assert (!res || (right == NULL));            
            return res;
        }
        
        bool isParentOf(node * other)
        {
            return ((left == other) || (right == other));
        }    
    };

    struct searchRecord{
        node* volatile	gp; //grand parent
        node* volatile	p; //parent
        node* volatile	n; //leaf node
        
        searchRecord(node* _gp, node* _p, node* _n): gp(_gp), p(_p), n(_n){}
    };    
    
    node* volatile root;
    const unsigned int num_threads;
    const unsigned int min_key, max_key;
    

    
    node* createLeafNode(const unsigned int tid, const unsigned int key)
    {
        return createInternalNode(tid, key, NULL, NULL);
    }
    
    node* allocateNode(const unsigned int tid)
    {
        struct node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            assert(0 && "posix_memalign could not allocate memory");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff && "allocated address cannot be this reserved value");

        num_allocs.add(tid, 1);

        return nd;
    }
    
    node* createInternalNode(const unsigned int tid, const unsigned int key, node * left, node * right)
    {
        node* new_node = allocateNode(tid); //new node();
        new_node->key = key;
        new_node->lock = 0;
        new_node->mark = 0;
        new_node->left = left;
        new_node->right = right;
        return new_node;     
    }
    
public:
    extbst_none_asm(const unsigned int _num_threads, const unsigned int _min_key, const unsigned int _max_key): num_threads(_num_threads), min_key(_min_key), max_key(_max_key)
    {
        printf("node size=%lu \n", sizeof(node));

        node* leftRoot = createLeafNode(0, _min_key);
        node* rightRoot = createLeafNode(0, _max_key);
        root = createInternalNode(0, _min_key, leftRoot, rightRoot);
    }
    
    ~extbst_none_asm()
    {
        //free all nodes
        freeTree(root);

        printf("dtor::extbst_none_asm\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());   

    }
    
    void freeTree (struct node* nd)
    {
        if(nd==NULL)
            return;

        freeTree(nd->right);
        freeTree(nd->left);

        free(nd);
    }
    
    struct searchRecord search(const unsigned int tid, const unsigned int key)
    {
        node * gp = NULL;
        node * p = NULL;
        node * n = root;
        unsigned int long ret_val = 0;
        while(!n->isLeaf())
        {
            gp = p;
            p = n;
//            //get current node key
            unsigned int curr_key;
            __asm__ __volatile__ ("mov %1, %0"
                :"=r" (curr_key)
                : "m" (n->key)
                );             

            //read left node
            node * curr_left = NULL;            
            __asm__ __volatile__ ("mov %1, %0"
                :"=r" (curr_left)
                : "m" (n->left)
                );             

            node * curr_right = NULL;            
            __asm__ __volatile__ ("mov %1, %0"
                :"=r" (curr_right)
                : "m" (n->right)
                );             
            
            n = (key <= curr_key) ? curr_left : curr_right;
        }
        return searchRecord(gp, p, n);
    }
    
    bool validateIns(node* p, node* n)
    {
        return ( (!(n->mark)) && (!(p->mark)) && (p->isParentOf(n)) );
    }
    
    bool validateErase(node* gp, node* p, node* n)
    {
        bool res =  ( (!(gp->mark)) && (gp->isParentOf(p)) && (!(p->mark)) && (p->isParentOf(n)) && (!(n->mark)) );
        return res;
    }
    
    bool insert(const unsigned int tid, const unsigned int key){
        
        while(true)
        {

            struct searchRecord ret = search(tid, key);
            //decide childs for new newnode
            node* newLeaf = createLeafNode(tid, key);
            bool leftdir = (key < ret.n->key);
            node * leftChild = (leftdir) ? newLeaf : ret.n;
            node * rightChild = (leftdir) ? ret.n: newLeaf;
            node* newInternalNode = (leftdir)? createInternalNode(tid, key, leftChild, rightChild) : createInternalNode(tid, ret.n->key, leftChild, rightChild);

            //atomically insert newInternalNode
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            //check if key already present after locking to avoid derefing freed node.
            if (ret.n->key == key)
            {
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
                return false; //key already present
            }

            {
                if ( validateIns(ret.p, ret.n) )
                {
                    if(ret.n == ret.p->left)
                    {
                        ret.p->left = newInternalNode;
                    }
                    else{
                        ret.p->right = newInternalNode;
                    }
                    releaseLock(&ret.p->lock, tid);
                    releaseLock(&ret.n->lock, tid);

                    return true;
                }
                //validation failed
#ifdef COLLECTSTAT                
//                num_vwritefails.add(tid, 1);
#endif
                free(newLeaf);
                free(newInternalNode);
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
            }
        } //while true
    }
    
    bool erase(const unsigned int tid, const unsigned int key){

        while(true)
        {
            struct searchRecord ret = search(tid, key);
            acquireLock(&ret.gp->lock);
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            if (ret.n->key != key)
            {
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);       
                return false;
            }
            {
                if ( validateErase(ret.gp, ret.p, ret.n) )
                {
                    //mark n and p
                    ret.p->mark =  true;
                    ret.n->mark =  true;

                    node * sibling = (ret.p->left == ret.n) ? ret.p->right : ret.p->left;
                    if (ret.gp->left == ret.p)
                    {
                        ret.gp->left = sibling;
                    }
                    else
                    {
                        ret.gp->right = sibling;
                    }

                    releaseLock(&ret.gp->lock, tid);
                    releaseLock(&ret.p->lock, tid);       
                    releaseLock(&ret.n->lock, tid);       
                    return true;

                }
#ifdef COLLECTSTAT
//                num_vwritefails.add(tid, 1);
#endif
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);       
            }
        }//while
    }
    
    bool contains(const unsigned int tid, const unsigned int key){
        struct searchRecord ret = search(tid, key);
        return ( !(ret.n->mark) && (ret.n->key == key) );
    }
    
    long long sumLeafKeys(node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key) )
            return nd->key;
        else
            return sumLeafKeys(nd->left) + sumLeafKeys(nd->right);                       
    }    
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      sum = sumLeafKeys(root);
      return sum;
    }

    void printLeafs(node *nd)
    {
        if (nd == NULL)
            return;
        if (nd->isLeaf())
        {
            printf("%u ", nd->key);
        }
        printLeafs(nd->left);
        printLeafs(nd->right);                       
    }
    
    long long countLeafs(node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key)  )
            return 1;
        else
            return countLeafs(nd->left) + countLeafs(nd->right);                       
    }
  
    
   long long getDSSize()
   {
      long long size = 0;
      size = countLeafs(root);
      return size;
   }      
    
    void printTree(node * nd)
    {
        if (nd == NULL)
            return;
        printTree(nd->left);
        std::cout << nd->key<<"("<<nd<<")" <<" ";
        assert(!nd->mark);
        printTree(nd->right);               
    }
    void printDebuggingDetails()
    {
//        std::cout<<"Printing tree:\n";
//        printTree(root);
//        std::cout<<std::endl;
        
        std::cout<<"Printing leafs:\n";
        printLeafs(root);
        std::cout<<std::endl;
    }       
};

#endif /* EXTBST_NONE_ASM_H */

