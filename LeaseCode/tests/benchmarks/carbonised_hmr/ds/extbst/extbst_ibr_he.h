/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   extbst_ibr_he.h
 * Author: mc
 *
 * Created on September 26, 2021, 11:28 AM
 */

#ifndef EXTBST_IBR_HE_H
#define EXTBST_IBR_HE_H

#include "lock.h"
#include "ibr_he.h"
#define MAX_HE 3

struct node_he; //fwd declaration to satiate complaining compiler.
ibr_he<struct node_he> *rec_he = NULL;

struct node_he{
    unsigned int key;
    unsigned int lock;
    bool mark;

//    node_he* left;
//    node_he* right;
    std::atomic<struct node_he*> left;    
    std::atomic<struct node_he*> right;  

    //rec based members
    uint64_t birth_epoch;
    uint64_t retire_epoch;

    node_he(unsigned int key): key(key)
    {
        mark = false;
        lock = 0;
        left = NULL;
        right = NULL;
//        if (pthread_mutex_init(&pt_lock, NULL) != 0){
//            printf("Mutex Initialization failed!");
//            exit(EXIT_FAILURE);
//        }
        assert(rec_he !=  NULL);
        
        birth_epoch = rec_he->getEpoch();
        retire_epoch = UINT64_MAX;
    }    

    bool isLeaf()
    {
        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;
    }

    bool isParentOf(node_he * other)
    {
        return ((left == other) || (right == other));
    }    
};


class extbst_ibr_he
{
private:
    struct searchRecord{
        node_he* volatile	gp; //grand parent
        node_he* volatile	p; //parent
        node_he* volatile	n; //leaf node_he
        
        searchRecord(node_he* _gp, node_he* _p, node_he* _n): gp(_gp), p(_p), n(_n){}
    };    
    
    node_he* volatile root;
    const unsigned int num_threads;
    const unsigned int min_key, max_key;
    
    node_he* createLeafNode(const unsigned int tid, const unsigned int key)
    {
        return createInternalNode(tid, key, NULL, NULL);
    }
    
    node_he* allocateNode(const unsigned int tid)
    {
//        struct asm_node* nd =  new asm_node();
        struct node_he* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(node_he)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            fflush(stdout);
            assert(0 && "posix_memalign could not allocate memory");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff && "allocated address cannot be this reserved value");
        
        assert(rec_he !=  NULL);
        rec_he->incAllocCounter(tid);
        return nd;
    }
    
    node_he* createInternalNode(const unsigned int tid, const unsigned int key, node_he * left, node_he * right)
    {
        node_he* new_node = allocateNode(tid); //new node_he();
        new_node->key = key;
        new_node->lock = 0;
        new_node->mark = 0;
        new_node->left = left;
        new_node->right = right;
        new_node->birth_epoch = rec_he->getEpoch();
        new_node->retire_epoch = UINT64_MAX;        
        
        return new_node;     
    }
    
public:
    extbst_ibr_he(const unsigned int _num_threads, const unsigned int _min_key, const unsigned int _max_key): num_threads(_num_threads), min_key(_min_key), max_key(_max_key)
    {
        const unsigned int tid = 0;
        rec_he = new ibr_he<struct node_he>(num_threads, MAX_HE);

        node_he* leftRoot = createLeafNode(tid, _min_key);
        node_he* rightRoot = createLeafNode(tid, _max_key);
        root = createInternalNode(tid, _min_key, leftRoot, rightRoot);
    }
    
    ~extbst_ibr_he()
    {
        //free all nodes
        freeTree(root);

        printf("dtor::extbst_ibr_he\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
//        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete rec_he;
    }
    
    void freeTree (struct node_he* nd)
    {
        if(nd==NULL)
            return;

        freeTree(nd->right);
        freeTree(nd->left);

        free(nd);
    }
    
    struct searchRecord search(const unsigned int tid, const unsigned int key)
    {
        unsigned int count_idx = 0;
        node_he * gp = NULL;
        node_he * p = NULL;
        node_he * n = root;
        
        while(!n->isLeaf())
        {
            gp = p;
            p = n;
//            n = (key <= n->key) ? n->left : n->right;
            n = (key <= n->key) ? rec_he->read(tid, count_idx%3, n->left) : rec_he->read(tid, count_idx%3, n->right);
            count_idx++;
        }
        return searchRecord(gp, p, n);
    }
    
    bool validateIns(node_he* p, node_he* n)
    {
        return ( (!(n->mark)) && (!(p->mark)) && (p->isParentOf(n)) );
    }
    
    bool validateErase(node_he* gp, node_he* p, node_he* n)
    {
        bool res =  ( (!(gp->mark)) && (gp->isParentOf(p)) && (!(p->mark)) && (p->isParentOf(n)) && (!(n->mark)) );
//        printf("%d %d %d %d %d\n", (!gp->mark), (gp->isParentOf(p)), (!p->mark), (p->isParentOf(n)), (!n->mark));
        return res;
    }
    
    bool insert(const unsigned int tid, const unsigned int key){
        
        while(true)
        {
            struct searchRecord ret = search(tid, key);

            //decide childs for new newnode
            node_he* newLeaf = createLeafNode(tid, key);
            bool leftdir = (key < ret.n->key);
            node_he * leftChild = (leftdir) ? newLeaf : ret.n;
            node_he * rightChild = (leftdir) ? ret.n: newLeaf;
            node_he* newInternalNode = (leftdir)? createInternalNode(tid, key, leftChild, rightChild) : createInternalNode(tid, ret.n->key, leftChild, rightChild);

            //atomically insert newInternalNode
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            //check if key already present after locking to avoid derefing freed node_he.
            if (ret.n->key == key)
            {
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
                rec_he->clear(tid);
                return false; //key already present
            }

            {
                if ( validateIns(ret.p, ret.n) )
                {
                    if(ret.n == ret.p->left)
                    {
                        ret.p->left = newInternalNode;
                    }
                    else{
                        ret.p->right = newInternalNode;
                    }
                    releaseLock(&ret.p->lock, tid);
                    releaseLock(&ret.n->lock, tid);
                    rec_he->clear(tid);
                    return true;
                }
                //validation failed
#ifdef COLLECTSTAT                
                num_vwritefails.add(tid, 1);
#endif
                free(newLeaf);
                free(newInternalNode);
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
            }
            rec_he->clear(tid);
        } //while true
    }
    
    bool erase(const unsigned int tid, const unsigned int key){

        while(true)
        {
            struct searchRecord ret = search(tid, key);
            acquireLock(&ret.gp->lock);
            acquireLock(&ret.p->lock);
            acquireLock(&ret.n->lock);

            if (ret.n->key != key)
            {
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);  
                rec_he->clear(tid);
                return false;
            }
            {
                if ( validateErase(ret.gp, ret.p, ret.n) )
                {
                    //mark n and p
                    ret.p->mark =  true;
                    ret.n->mark =  true;

                    node_he * sibling = (ret.p->left == ret.n) ? ret.p->right : ret.p->left;
                    if (ret.gp->left == ret.p)
                    {
                        ret.gp->left = sibling;
                    }
                    else
                    {
                        ret.gp->right = sibling;
                    }

                    releaseLock(&ret.gp->lock, tid);
                    releaseLock(&ret.p->lock, tid);       
                    releaseLock(&ret.n->lock, tid);    
                    
                    rec_he->retire(tid, ret.p);
                    rec_he->retire(tid, ret.n);

                    rec_he->clear(tid);
                    return true;

                }
#ifdef COLLECTSTAT
                num_vwritefails.add(tid, 1);
#endif                
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);       
                releaseLock(&ret.n->lock, tid);       
            }
            rec_he->clear(tid);
        }//while
    }
    
    bool contains(const unsigned int tid, const unsigned int key){
        struct searchRecord ret = search(tid, key);
        
        bool res = ( !(ret.n->mark) && (ret.n->key == key) );
        rec_he->clear(tid);
        return res;
    }
    
    long long sumLeafKeys(node_he *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key) )
            return nd->key;
        else
            return sumLeafKeys(nd->left) + sumLeafKeys(nd->right);                       
    }    
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      sum = sumLeafKeys(root);
      return sum;
    }

    void printLeafs(node_he *nd)
    {
        if (nd == NULL)
            return;
        if (nd->isLeaf())
        {
            printf("%u ", nd->key);
        }
        printLeafs(nd->left);
        printLeafs(nd->right);                       
    }
    
    long long countLeafs(node_he *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key)  )
            return 1;
        else
            return countLeafs(nd->left) + countLeafs(nd->right);                       
    }
  
    
   long long getDSSize()
   {
      long long size = 0;
      size = countLeafs(root);
      return size;
   }      
    
    void printTree(node_he * nd)
    {
        if (nd == NULL)
            return;
        printTree(nd->left);
        std::cout << nd->key<<"("<<nd->mark<<")" <<" ";
        assert(!nd->mark);
        printTree(nd->right);               
    }
    void printDebuggingDetails()
    {
//        std::cout<<"Printing tree:\n";
//        printTree(root);
//        std::cout<<std::endl;
        
        std::cout<<"Printing leafs:\n";
        printLeafs(root);
        std::cout<<std::endl;
    }       
};

#endif /* EXTBST_IBR_HE_H */

