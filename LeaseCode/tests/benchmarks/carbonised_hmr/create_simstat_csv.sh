#!/bin/bash
####
# This script takes simulator result files (sim.out) renamed and saved by run_exp.sh in trial_data directory and renamed as sim_algname_th_stepi.out and produces tabular data in csv format for pivoting and plotting in excel.
#INPUT: pass path to directory where simulator output files are as first argument.
#Example usage:
#./create_simstat_csv.sh path to trial_data > mysimstats.csv
#INPUT: a csv file with simulator stats
####

out_dir="$1"

count=0
for stepfile in `ls $out_dir/sim_*step1.out`
do
    #get algo name from file name
    algo_name_threads=`echo $stepfile | cut -d"/" -f4 | cut -d"_" -f2,3`
    tot_walltime=`cat $stepfile | grep "Shutdown Time" | tr -s ' '| cut -d" " -f5`

    threads=`echo $algo_name_threads | cut -d"_" -f2`
    algo_name=`echo $algo_name_threads | cut -d"_" -f1`

    head_row=("algo, threads,") #header for csv
    stat_row=("$algo_name, $threads,") #number content of the row which has per thread stat

##    Get all | separated lines
    output=`cat $stepfile  | grep -a "Core Summary" -A200`
    while IFS= read -r line
    do
        #get comma separated line
        csv_line=`echo "${line}" | sed 's/,/ /g' | sed 's/|/,/g'`
#        echo csvline::"${csv_line}"
        echo "${csv_line}" >> ${out_dir}/raw_${algo_name}.csv
        title=`echo "${csv_line}" | cut -d"," -f1`
        stats=`echo "${csv_line}" | cut -d"," -f2-`
        editstat=$(echo $stats  | awk 'gsub(/,$/,x)') #remove last ,
#        echo $title stats::$stats
#        echo $title editstat::$editstat
        #simstats csv created for AMD machine (or simulator) will have stats averaged across all threads
        avg_stat=$(echo $editstat | awk -F',' '{s=0; for (i=1;i<=NF;i++)s+=$i; print s/NF;}')
#        echo title: "${title}"
#        echo AVG:: "${avg_stat}"
        head_row+=("${title},")
        stat_row+=("$avg_stat,")
    done <<< "${output}"

#    echo ${head_row[*]}
#    echo ${stat_row[*]}


if ((count==0))
then
    echo ${head_row[*]}
fi
    echo ${stat_row[*]}
count=$count+1
done
