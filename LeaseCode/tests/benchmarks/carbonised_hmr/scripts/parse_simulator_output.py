#!/usr/bin/env python

# only runs on system with proper python setup.
#called from simout_to_simtext.sh
#Warning: only call for one stepi file. calling for stei and stepj may overwrite older .txt file created by this python script

import re
import sys
import numpy
from numpy import *
from optparse import OptionParser

global graphiteResults

def rowSearch(heading, key, numColumns, reportError=True):
   global graphiteResults
   key += "(.*)"
   
   headingFound = False
   
   for line in graphiteResults:
      if headingFound:
         matchKey = re.search(key, line)
         if matchKey:
            counts = line.split('|')
            eventCounts = counts[1:numColumns+1]
            for i in range(0, numColumns):
               if (len(eventCounts[i].split()) == 0):
                  eventCounts[i] = "0.0"
               #elif (eventCounts[i].split()[0] == "nan"):
               #   eventCounts[i] = "0.0"
            #print eventCounts
            #print "%d" % size(eventCounts)
            return list(map(lambda x: float(x), eventCounts))
      else:
         if (re.search(heading, line)):
            headingFound = True

   if (reportError == True):
      print ("\n\n****  ERROR  ***** Could not Find", (heading, key), "\n\n") 
      # print "\n\n****  ERROR  ***** Could not Find [%s,%s]\n\n" % (heading, key)
      sys.exit(-1)
   else: # reportError == False
      return list([0.0] * numColumns)

parser = OptionParser()
parser.add_option("--results-dir", dest="results_dir", help="Sim output Results Directory")
parser.add_option("--file-name", dest="file_name", help="name of step file in trial_data")
parser.add_option("--algo-name", dest="algo_name", help="name of ds algo")
parser.add_option("--num-threads", dest="num_threads", type="int", help="Number of Application Threads")
parser.add_option("--num-ops", dest="num_ops", type="int", help="total ds operations executed by app")

(options,args) = parser.parse_args()

##default testing inputs
# options.results_dir = "../trial_data"
# options.file_name = "sim_lazylist_ibr_2_step1.out"
# options.algo_name = "lazylist_ibr"
# options.num_threads = 2

print ("python ./parse_simulator_output.py --results-dir", options.results_dir, "--file-name", options.file_name, "--algo-name", options.algo_name, "--num-threads", options.num_threads, "--num-ops", options.num_ops)


# Read Results Files
try:
   graphiteResults = open(options.results_dir + "/"+ options.file_name, 'r').readlines()
except IOError:
   # print "*ERROR* File: %s/sim.out not present" % (options.results_dir)
   print ("*ERROR* File:", options.results_dir + "/"+ options.file_name, " not present")
   sys.exit(-1)

numThreads = options.num_threads
tot_app_ops = options.num_ops

# Total Instructions
totalInstructions = sum(rowSearch("Core Summary", "Total Instructions", numThreads))

# Completion Time
completionTime = numpy.average(rowSearch("Core Summary", "Completion Time \(in nanoseconds\)", numThreads))
totalCompletionTime = sum(rowSearch("Core Summary", "Completion Time \(in nanoseconds\)", numThreads))
memoryStallTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Memory", numThreads))
computeTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Execution Unit", numThreads))
synchronizationTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Synchronization", numThreads))
netRecvTime = sum(rowSearch("Stall Time Breakdown \(in nanoseconds\)", "Network Recv", numThreads))

# Network Traffic
networkFlitsSent = sum(rowSearch("Network \(Memory\)", "Total Flits Sent", numThreads))
networkFlitsReceived = sum(rowSearch("Network \(Memory\)", "Total Flits Received", numThreads))

# Memory Access Latency
totalInstructionMemoryAccessesRow = rowSearch("Shared Memory Model Summary", "Total Instruction Memory Accesses", numThreads);
instructionMemoryAccessLatencyRow = rowSearch("Shared Memory Model Summary", "Average Instruction Memory Access Latency \(in nanoseconds\)", numThreads)
averageInstructionMemoryAccessLatency = sum(numpy.array(instructionMemoryAccessLatencyRow) * numpy.array(totalInstructionMemoryAccessesRow)) / sum(totalInstructionMemoryAccessesRow)
totalInstructionMemoryAccessLatency = sum(numpy.array(instructionMemoryAccessLatencyRow) * numpy.array(totalInstructionMemoryAccessesRow))
totalInstructionBufferHits = sum(rowSearch("Shared Memory Model Summary", "Instruction Buffer Hits", numThreads))
totalInstructionMemoryAccesses = sum(totalInstructionMemoryAccessesRow)

totalDataMemoryAccessesRow = rowSearch("Shared Memory Model Summary", "Total Data Memory Accesses", numThreads);
dataMemoryAccessLatencyRow = rowSearch("Shared Memory Model Summary", "Average Data Memory Access Latency \(in nanoseconds\)", numThreads)
averageDataMemoryAccessLatency = sum(numpy.array(dataMemoryAccessLatencyRow) * numpy.array(totalDataMemoryAccessesRow)) / sum(totalDataMemoryAccessesRow)
totalDataMemoryAccessLatency = sum(numpy.array(dataMemoryAccessLatencyRow) * numpy.array(totalDataMemoryAccessesRow))
totalDataMemoryAccesses = sum(totalDataMemoryAccessesRow)

l1IAccesses = sum(rowSearch("Cache L1-I","Cache Accesses", numThreads))
l1DAccesses = sum(rowSearch("Cache L1-D","Cache Accesses", numThreads))
l2Accesses = sum(rowSearch("Cache L2","Cache Accesses", numThreads))
l1IMisses = sum(rowSearch("Cache L1-I","Cache Misses", numThreads))
l1DMisses = sum(rowSearch("Cache L1-D","Cache Misses", numThreads))
l2Misses = sum(rowSearch("Cache L2","Cache Misses", numThreads))

# Cache miss types

#coldMisses = sum(rowSearch("Miss Type Counters","Cold Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Cold Misses \(Remote\)", numThreads))

#capacityMisses = sum(rowSearch("Miss Type Counters","Capacity Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Capacity Misses \(Remote\)", numThreads))

#upgradeMisses = sum(rowSearch("Miss Type Counters","Upgrade Misses \(Local\)", numThreads)) + \
#                sum(rowSearch("Miss Type Counters","Upgrade Misses \(Remote\)", numThreads))

#sharingMisses = sum(rowSearch("Miss Type Counters","Sharing Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Sharing Misses \(Remote\)", numThreads))

#wordMisses = sum(rowSearch("Miss Type Counters","Word Misses \(Local\)", numThreads)) + \
#      sum(rowSearch("Miss Type Counters","Word Misses \(Remote\)", numThreads))


# Replica/home read/write counters
#replica_read_accesses = sum(rowSearch("L2 Cache Cntlr","Replica Read Accesses", numCores))
#replica_read_misses = sum(rowSearch("L2 Cache Cntlr","Replica Read Misses", numCores))
#replica_write_accesses = sum(rowSearch("L2 Cache Cntlr","Replica Write Accesses", numCores))
#replica_write_misses = sum(rowSearch("L2 Cache Cntlr","Replica Write Misses", numCores))

#home_read_accesses = sum(rowSearch("L2 Cache Cntlr","Home Read Accesses", numCores))
#home_read_misses = sum(rowSearch("L2 Cache Cntlr","Home Read Misses", numCores))
#home_write_accesses = sum(rowSearch("L2 Cache Cntlr","Home Write Accesses", numCores))
#home_write_misses = sum(rowSearch("L2 Cache Cntlr","Home Write Misses", numCores))

# Total DRAM accesses
#dramAccesses = sum(rowSearch("ORam Performance Model Summary", "Total ORam Accesses", numCores))

# Cache Tag/Data array reads/writes
l1I_TagReads = sum(rowSearch("Cache L1-I","Tag Array Reads", numThreads))
l1I_TagWrites = sum(rowSearch("Cache L1-I","Tag Array Writes", numThreads))
#l1I_DataReads_Word = sum(rowSearch("Cache L1-I","Data Array Reads \(Word\)", numCores))
#l1I_DataWrites_Word = sum(rowSearch("Cache L1-I","Data Array Writes \(Word\)", numCores)) 
#l1I_DataReads_CacheLine = sum(rowSearch("Cache L1-I","Data Array Reads \(Cache-Line\)", numCores))
#l1I_DataWrites_CacheLine = sum(rowSearch("Cache L1-I","Data Array Writes \(Cache-Line\)", numCores))

l1D_TagReads = sum(rowSearch("Cache L1-D","Tag Array Reads", numThreads))
l1D_TagWrites = sum(rowSearch("Cache L1-D","Tag Array Writes", numThreads))
#l1D_DataReads_Word = sum(rowSearch("Cache L1-D","Data Array Reads \(Word\)", numCores))
#l1D_DataWrites_Word = sum(rowSearch("Cache L1-D","Data Array Writes \(Word\)", numCores)) 
#l1D_DataReads_CacheLine = sum(rowSearch("Cache L1-D","Data Array Reads \(Cache-Line\)", numCores))
#l1D_DataWrites_CacheLine = sum(rowSearch("Cache L1-D","Data Array Writes \(Cache-Line\)", numCores))

l2_TagReads = sum(rowSearch("Cache L2","Tag Array Reads", numThreads))
l2_TagWrites = sum(rowSearch("Cache L2","Tag Array Writes", numThreads))
#l2_DataReads_Word = sum(rowSearch("Cache L2","Data Array Reads \(Word\)", numCores))
#l2_DataWrites_Word = sum(rowSearch("Cache L2","Data Array Writes \(Word\)", numCores)) 
#l2_DataReads_CacheLine = sum(rowSearch("Cache L2","Data Array Reads \(Cache-Line\)", numCores))
#l2_DataWrites_CacheLine = sum(rowSearch("Cache L2","Data Array Writes \(Cache-Line\)", numCores))

# Network access counters
#routerBufferWrites = sum(rowSearch("Network \(Memory\)","Buffer Writes", numCores))
#routerBufferReads = sum(rowSearch("Network \(Memory\)","Buffer Reads", numCores))
#routerCrossbarTraversals = sum(rowSearch("Network \(Memory\)","Crossbar\[1\] Traversals", numCores)) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[2\] Traversals", numCores)) * 2) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[3\] Traversals", numCores)) * 3) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[4\] Traversals", numCores)) * 4) + \
#                           (sum(rowSearch("Network \(Memory\)","Crossbar\[5\] Traversals", numCores)) * 5)
#routerSwitchAllocatorRequests = sum(rowSearch("Network \(Memory\)","Switch Allocator Requests", numCores))
#linkTraversals = sum(rowSearch("Network \(Memory\)","Link Traversals", numCores))

# Instruction Memory Access Latency Breakdown
#total_IMAT_L1Cache = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache", numCores))
#total_IMAT_L1Cache_L2Replica = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Replica", numCores))
#total_IMAT_L2Replica_Waiting = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica Waiting", numCores))
#total_IMAT_L2Replica_Sharers = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica And Sharers", numCores, reportError=False))
#total_IMAT_L2Replica_L2Home = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Replica And L2 Home", numCores))
#total_IMAT_L1Cache_L2Home = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Home", numCores))
#total_IMAT_L2Home_Waiting = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home Waiting", numCores))
#total_IMAT_L2Home_Sharers = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home And Sharers", numCores))
#total_IMAT_L2Home_DRAM = sum(rowSearch("Total Instruction Memory Access Latency \(in nanoseconds\)","L2 Home And DRAM", numCores))

# Data Memory Access Latency Breakdown
#total_DMAT_L1Cache = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache", numCores))
#total_DMAT_L1Cache_L2Replica = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Replica", numCores))
#total_DMAT_L2Replica_Waiting = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica Waiting", numCores))
#total_DMAT_L2Replica_Sharers = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica And Sharers", numCores, reportError=False))
#total_DMAT_L2Replica_L2Home = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Replica And L2 Home", numCores))
#total_DMAT_L1Cache_L2Home = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L1 Cache And L2 Home", numCores))
#total_DMAT_L2Home_Waiting = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home Waiting", numCores))
#total_DMAT_L2Home_Sharers = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home And Sharers", numCores))
#total_DMAT_L2Home_DRAM = sum(rowSearch("Total Data Memory Access Latency \(in nanoseconds\)","L2 Home And DRAM", numCores))

#total_page_migration_stall_time = sum(rowSearch("Page Migration Stall Time \(in nanoseconds\)","Total", numCores))


# Branch predictor stats
numCorrectBranch = sum(rowSearch("Branch Predictor Statistics","Num Correct", numThreads))
numInCorrectBranch = sum(rowSearch("Branch Predictor Statistics","Num Incorrect", numThreads))

# Fence Instructions
ExplicitFence = sum(rowSearch("Fence Instructions","Explicit LFENCE, SFENCE, MFENCE", numThreads))
ImplicitFence = sum(rowSearch("Fence Instructions","Implicit MFENCE", numThreads))

#Detailed Stall Time Breakdown (in nanoseconds)
L1ICacheStallTime = sum(rowSearch("Detailed Stall Time Breakdown \(in nanoseconds\)","L1-I Cache", numThreads))
L1DCacheStallTime = sum(rowSearch("Detailed Stall Time Breakdown \(in nanoseconds\)","L1-D Cache", numThreads))


# Cache Miss type L1D
numL1ColdMisses = sum(rowSearch("Cache L1-D","Cold Misses", numThreads))
numL1CapacityMisses = sum(rowSearch("Cache L1-D","Capacity Misses", numThreads))
numL1SharingMisses = sum(rowSearch("Cache L1-D","Sharing Misses", numThreads))

# Cache Miss type L2
numL2ColdMisses = sum(rowSearch("Cache L2","Cold Misses", numThreads))
numL2CapacityMisses = sum(rowSearch("Cache L2","Capacity Misses", numThreads))
numL2SharingMisses = sum(rowSearch("Cache L2","Sharing Misses", numThreads))

starttime=0
stoptime=0
for line in graphiteResults:
    if (re.search('Start Time', line)):
        starttime=int(line.split(" ")[-1].strip())
    if (re.search('Stop Time', line)):
        stoptime=int(line.split(" ")[-1].strip())
        
################################################################################################################
# Write event counters to a file
################################################################################################################

stats_file = open(options.results_dir + "/parsedsim_" + options.algo_name + "_" + str(options.num_threads) + ".txt", 'w')
stats_file.write("Total-Instructions-PO = %f\n" % (totalInstructions/tot_app_ops))
stats_file.write("timetaken(usec) = %f\n" % (stoptime - starttime))
#stats_file.write("Completion-Time-PT-Millsec = %f\n" % (completionTime/1000000))

#stats_file.write("Network-Flits-Sent-PO = %f\n" % (networkFlitsSent/tot_app_ops))
#stats_file.write("Network-Flits-Received-PO = %f\n" % (networkFlitsReceived/tot_app_ops))

#stats_file.write("Average-Instruction-Memory-Access-Latency = %f\n" % (averageInstructionMemoryAccessLatency))
stats_file.write("Average-Data-Memory-Access-Latency = %f\n" % (averageDataMemoryAccessLatency))
stats_file.write("Total-Instruction-Memory-Access-Latency-PO = %f\n" % (totalInstructionMemoryAccessLatency/tot_app_ops))
stats_file.write("Total-Data-Memory-Access-Latency-PO = %f\n" % (totalDataMemoryAccessLatency/tot_app_ops))
#stats_file.write("Instruction-Fetch-Buffer-Hits-PO = %f\n" % (totalInstructionBufferHits/tot_app_ops))
stats_file.write("Total-Data-Memory-Access-PO = %f\n" % (totalDataMemoryAccesses/tot_app_ops))
stats_file.write("Total-Instruction-Memory-Access-PO = %f\n" % (totalInstructionMemoryAccesses/tot_app_ops))

#stats_file.write("Private-Cache-Hierarchy-Miss-Rate = %f\n" % ((l1IMisses + l1DMisses) / (l1IAccesses + l1DAccesses)))

#stats_file.write("L1I-Cache-Miss-Rate-percent = %f\n" % ((l1IMisses / l1IAccesses)*100))
stats_file.write("L1D-Cache-Miss-Rate-percent = %f\n" % ((l1DMisses / l1DAccesses)*100))
stats_file.write("L2-Cache-Miss-Rate-percent = %f\n" % ((l2Misses / l2Accesses)*100))
#stats_file.write("L1I-Cache-Accesses-PO = %f\n" % (l1IAccesses/tot_app_ops))
stats_file.write("L1D-Cache-Accesses-PO = %f\n" % (l1DAccesses/tot_app_ops))
stats_file.write("L2-Cache-Accesses-PO = %f\n" % (l2Accesses/tot_app_ops))
#stats_file.write("L1I-Cache-Misses-PO = %f\n" % (l1IMisses/tot_app_ops))
stats_file.write("L1D-Cache-Misses-PO = %f\n" % (l1DMisses/tot_app_ops))
stats_file.write("L2-Cache-Misses-PO = %f\n" % (l2Misses/tot_app_ops))
stats_file.write("L2-Cache-Misses-TOT = %f\n" % (l2Misses))
stats_file.write("L2-Cache-Misses-PT = %f\n" % (l2Misses/numThreads))
#stats_file.write("L1I-MPKI = %f\n" % (l1IMisses * 1000 /  totalInstructions))


# Core Counters
#stats_file.write("Total-Completion-Time-PO = %f\n" % (totalCompletionTime/tot_app_ops))
stats_file.write("Memory-Stall-Time-PO = %f\n" % (memoryStallTime/tot_app_ops))
#stats_file.write("Compute-Time-PO = %f\n" % (computeTime/tot_app_ops))
#stats_file.write("Synchronization-Stall-Time-PO = %f\n" % (synchronizationTime/tot_app_ops))
#stats_file.write("Network-Recv-Stall-Time-PO = %f\n" % (netRecvTime/tot_app_ops))
#Detailed Stall Time Breakdown (in nanoseconds)
#stats_file.write("L1I-CacheStall-Time-PO = %f\n" % (L1ICacheStallTime/tot_app_ops))
stats_file.write("L1D-CacheStall-Time-PO = %f\n" % (L1DCacheStallTime/tot_app_ops))

## Cache counters
#stats_file.write("L1I-Tag-Reads-PO = %f\n" % (l1I_TagReads/tot_app_ops))
#stats_file.write("L1I-Tag-Writes-PO = %f\n" % (l1I_TagWrites/tot_app_ops))
#
#stats_file.write("L1D-Tag-Reads-PO = %f\n" % (l1D_TagReads/tot_app_ops))
#stats_file.write("L1D-Tag-Writes-PO = %f\n" % (l1D_TagWrites/tot_app_ops))
#
#stats_file.write("L2-Tag-Reads-PO = %f\n" % (l2_TagReads/tot_app_ops))
#stats_file.write("L2-Tag-Writes-PO = %f\n" % (l2_TagWrites/tot_app_ops))
#
## Branch predictor counters
stats_file.write("Num-Correct-Branch-PO = %f\n" % (numCorrectBranch/tot_app_ops))
stats_file.write("Num-InCorrect-Branch-PO = %f\n" % (numInCorrectBranch/tot_app_ops))
#
## Fence Instructions
#stats_file.write("Explicit-Fence-PO = %f\n" % (ExplicitFence/tot_app_ops))
#stats_file.write("Implicit-Fence-PO = %f\n" % (ImplicitFence/tot_app_ops))
#
## Cache Miss type L1D
stats_file.write("L1D-Cold-Misses-PO = %f\n" % (numL1ColdMisses/tot_app_ops))
stats_file.write("L1D-Capacity-Misses-PO = %f\n" % (numL1CapacityMisses/tot_app_ops))
stats_file.write("L1D-Sharing-Misses-PO = %f\n" % (numL1SharingMisses/tot_app_ops))
#
## Cache Miss type L2
#stats_file.write("L2-Cold-Misses-PO = %f\n" % (numL2ColdMisses/tot_app_ops))
#stats_file.write("L2-Capacity-Misses-PO = %f\n" % (numL2CapacityMisses/tot_app_ops))
#stats_file.write("L2-Sharing-Misses-PO = %f\n" % (numL2SharingMisses/tot_app_ops))


stats_file.close()