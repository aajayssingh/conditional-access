#!/usr/bin/env python

'''
script to plot throughput from appstat csv. 
'''

import matplotlib.pyplot as plt
import pandas as pd
import os
import glob

#for file in glob.glob(os.path.join("../", 'appstat_*.csv')):
#    df = pd.read_csv(file)
#    sub_df = df.iloc[:, [0,1,2]]
#    stat_name = sub_df.columns[2]
#    sub_df = pd.pivot_table(sub_df, values=sub_df.columns[2], columns='algo', index=sub_df.columns[1], aggfunc='mean')
#    list_cols = list(sub_df.columns)
#    sub_df.plot(y=list_cols, kind="line", xticks=list(sub_df.index), marker='D', title=stat_name+" "+file.split("/")[1])
#    sub_df.plot(y=list_cols, kind="bar", title=stat_name+" "+file.split("/")[1])


import itertools
marker = itertools.cycle(('o', 'v', '^', 's', 'p', 'P', '*', 'D', 'x', 'd' )) 

algomap={'hashtable_none':'hashtable_none', 'hashtable_asmhmr_trv':'hashtable_hbr', 'hashtable_ibr':'hashtable_ibr', 'hashtable_ibr_he':'hashtable_he', 'hashtable_ibr_hp':'hashtable_hp', 'hashtable_ibr_qsbr':'hashtable_qsbr', 'hashtable_ibr_rcu':'hashtable_rcu',\
         'stack_none_asm':'stack_none', 'stack_asmhmr_trv':'stack_hbr', 'stack_ibr':'stack_ibr', 'stack_ibr_he':'stack_he', 'stack_ibr_hp':'stack_hp', 'stack_ibr_qsbr':'stack_qsbr', 'stack_ibr_rcu':'stack_rcu',\
         'lazylist_none_asm':'lazylist_none', 'lazylist_asmhmr_trv':'lazylist_hbr', 'lazylist_ibr':'lazylist_ibr', 'lazylist_ibr_he':'lazylist_he', 'lazylist_ibr_hp':'lazylist_hp', 'lazylist_ibr_qsbr':'lazylist_qsbr', 'lazylist_ibr_rcu':'lazylist_rcu',\
         'extbst_none_asm':'extbst_none', 'extbst_asmhmr_trv':'extbst_hbr', 'extbst_ibr':'extbst_ibr', 'extbst_ibr_he':'extbst_he', 'extbst_ibr_hp':'extbst_hp', 'extbst_ibr_qsbr':'extbst_qsbr', 'extbst_ibr_rcu':'extbst_rcu'\
         }


for file in glob.glob(os.path.join("../", 'appstat_*.csv')):
    df = pd.read_csv(file)
    orig_sub_df = df.iloc[:, [0,1,2]]
    stat_name = orig_sub_df.columns[2]
    
    for index in orig_sub_df.index:
        orig_sub_df.loc[index,'algo'] = algomap[orig_sub_df.loc[index,'algo']] 
    
    sub_df = pd.pivot_table(orig_sub_df, values=orig_sub_df.columns[2], columns='algo', index=orig_sub_df.columns[1], aggfunc='mean')
    std_sub_df = pd.pivot_table(orig_sub_df, values=orig_sub_df.columns[2], columns='algo', index=orig_sub_df.columns[1], aggfunc='std')
    
    ax = sub_df.plot(kind='line', title=stat_name+" "+file.split("/")[1], xticks=list(sub_df.index)) #, figsize=(10, 5)
    for i, line in enumerate(ax.get_lines()):
        line.set_marker(next(marker))
        line.set_linewidth(2)
        line.set_markersize(6)
        plt.errorbar(x=line.get_xdata(), y=line.get_ydata(), yerr=list(std_sub_df[std_sub_df.columns[i]]), color=line.get_color(), capsize=4)        
     
    # adding legend
    ax.legend(ax.get_lines(), sub_df.columns, loc='best')
    plt.grid()
#    plt.show()
    plt.savefig("../"+stat_name.strip()+file.split("/")[1].split(".")[0]+'.png')
#    plt.savefig("../"+stat_name.strip()+file.split("/")[1].split(".")[0]+'.png', dpi=300)
    plt.close('all')
