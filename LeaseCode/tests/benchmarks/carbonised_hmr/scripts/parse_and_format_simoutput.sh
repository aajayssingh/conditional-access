#!/bin/bash

data_dir="../trial_data"


count=0
# file_name=""
# algo_name=""
# threads=0
for stepfile in `ls $data_dir/sim_*step1.out`
do
    #get algo name from file name
    file_name=`echo $stepfile | cut -d"/" -f3`
    algo_name=`echo $file_name | cut -d"_" -f2`
    threads=`echo $file_name | cut -d"_" -f3`

    app_res_file=$data_dir"/app_"${algo_name}"_"${threads}"_step1.txt"
    tot_app_ops=`cat ${app_res_file} | grep "Total Exp Ops" | cut -d"=" -f2| tr -d " "`
    # echo $file_name $algo_name $threads $app_res_file $tot_app_ops

    python3 parse_simulator_output.py --results-dir ${data_dir} --file-name ${file_name} --algo-name ${algo_name} --num-threads ${threads} --num-ops ${tot_app_ops}
count=$count+1
done


echo "created parsedsim*.txt files"

./create_parsedsimstat_to_csv.sh >> simstat_po.csv


mv simstat_po.csv ../
echo "moved simstat_po outside scripts"

mkdir ../simplots
python3 plot.py
