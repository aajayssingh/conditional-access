#!/bin/bash
#run this to output of run_exp.sh into csv format.
# example usage: ./create_csv.sh >> myfile.csv
#!/bin/bash
####
# This script takes run_exp.sh app stats output saved in trial_data directory produces tabular data in csv format for pivoting and plotting app stats in excel.
#INPUT: pass path to directory where trial_data files are as first argument.
#Example usage:
#./create_appstat_csv.sh path to trial_data > myappstats.csv
#INPUT: a csv file with app stats.
####

ValidNum () {
    re='^[0-9]+$'
    if ! [[ $1 =~ $re ]] ; then
       echo "error: Not a number" >&2; exit 1
    fi
}

out_dir="$1"
echo "algo,threads,throughput,maxkey,ins,del,succins,succdel,attins,attdel,duration_ms,ds_size,epoch,tot_retired,tot_allocated,tot_freed,walltime,max_res_mem,tot_retries,tot_ops,vrfails,vwrfails,success"
for stepfile in `ls $out_dir/app_*`
do
alg=`cat $stepfile | grep algo_type | cut -d"=" -f2| tr -d " "`
threads=`cat $stepfile | grep Threads | head -1 |cut -d"=" -f2| tr -d " "`
throughput=`cat $stepfile | grep Experiment_Throughput | cut -d"=" -f2| tr -d " "`
maxkey=`cat $stepfile | grep Max_Keyrange | cut -d"=" -f2| tr -d " "`
ins=`cat $stepfile | grep -e "ins" | head -1 |cut -d"=" -f2| tr -d " "`
del=`cat $stepfile | grep -e del | head -1 |cut -d"=" -f2| tr -d " "`

i_succins=`cat $stepfile | grep -w "Total SUCC_INS"| cut -d"=" -f2| tail -1 | tr -d " "`
i_succdel=`cat $stepfile | grep -w "Total SUCC_DEL"| cut -d"=" -f2| tail -1 | tr -d " "`
i_attins=`cat $stepfile | grep -w "Total ATT_INS"| cut -d"=" -f2| tail -1 | tr -d " "`
i_attdel=`cat $stepfile | grep -w "Total ATT_DEL"| cut -d"=" -f2| tail -1 | tr -d " "`


duration_ms=`cat $stepfile | grep Max_Duration | cut -d"=" -f2| tr -d " "`
ds_size=`cat $stepfile | grep experiment_ds_size | cut -d"=" -f2| tr -d " "`
epoch=`cat $stepfile | grep Epoch | cut -d"=" -f2| tr -d " "`
tot_retired=`cat $stepfile | grep total_retired | cut -d"=" -f2| tr -d " "`
tot_allocated=`cat $stepfile | grep total_allocated | cut -d"=" -f2| tr -d " "`
tot_freed=`cat $stepfile | grep total_freed | cut -d"=" -f2| tr -d " "`
walltime=`cat $stepfile | grep time_cmd_walltime | cut -d"=" -f2| tr -d " "`
walltime=${walltime%.*}
max_res_mem=`cat $stepfile | grep max_res_mem | cut -d"=" -f2| tr -d " "`
tot_retries=`cat $stepfile | grep total_retries | cut -d"=" -f2| tail -1| tr -d " "`
i_vrfails=`cat $stepfile | grep num_vreadfails| cut -d"=" -f2| head -1 | tr -d " "`
i_vwrfails=`cat $stepfile | grep num_vwritefails| cut -d"=" -f2| head -1 | tr -d " "`

tot_ops=`cat $stepfile | grep "Total Exp Ops"| cut -d"=" -f2| tr -d " "`
finished=`cat $stepfile | grep "key_checksum validation:PASS"| head -1`
#echo $finished

ValidNum $tot_retries

if [[ "$finished" == "key_checksum validation:PASS" ]]; then
    succ=true
else
    succ=false
fi

echo $alg,$((threads)),$((throughput)),$((maxkey)),$((ins)),$((del)),$((i_succins)),$((i_succdel)),$((i_attins)),$((i_attdel)),$((duration_ms)),$((ds_size)),$((epoch)),$((tot_retired)),$((tot_allocated)),$((tot_freed)),$((walltime)),$((max_res_mem)),$((tot_retries)),$((tot_ops)),$((i_vrfails)),$((i_vwrfails)),$succ
done