#!/bin/bash

################################################################################
# set the configuration parameters for experiments:
#alg_arr=(lazylist_none_asm lazylist_asmhmr_trv lazylist_ibr lazylist_ibr_rcu lazylist_ibr_qsbr lazylist_ibr_hp lazylist_ibr_he) # lazylist_asm_hmr lazylist_hp)
#alg_arr=(lazylist_asmhmr_trv lazylist_none_asm) #lazylist_ibr_qsbr lazylist_ibr_rcu) # lazylist_ibr_qsbr lazylist_ibr_hp lazylist_ibr_he) # lazylist_asm_hmr lazylist_hp)
#alg_arr=(stack_none_asm stack_asmhmr_trvopt stack_ibr stack_ibr_rcu stack_ibr_qsbr stack_ibr_hp stack_ibr_he)
#alg_arr=(asm_vread asm_read) #(asm_write asm_vwrite)
#alg_arr=(stack_asmhmr_trvopt stack_none_asm)
#alg_arr=(stack_none_asm stack_asmhmr_trvopt stack_ibr stack_ibr_rcu stack_ibr_qsbr stack_ibr_hp stack_ibr_he)

#alg_arr=(extbst_none_asm extbst_asmhmr_trv extbst_ibr extbst_ibr_rcu extbst_ibr_qsbr extbst_ibr_hp extbst_ibr_he)
#alg_arr=(extbst_none_asm extbst_asm_hmr extbst_ibr extbst_ibr_rcu extbst_ibr_qsbr extbst_ibr_hp extbst_ibr_he)
#alg_arr=(extbst_asmhmr_trv extbst_none_asm)
#alg_arr=(hashtable_none hashtable_asmhmr_trv hashtable_ibr hashtable_ibr_rcu hashtable_ibr_qsbr hashtable_ibr_hp hashtable_ibr_he)
alg_arr=(hashtable_none)
threads_arr=(1 4 8 12 16 20 24 28 32)
#threads_arr=(1)

iterations=2
ins_arr=(0 5 50) #keep ins and del array same size
del_arr=(0 5 50) #keep ins and del array same size
maxops_arr=(3000)
dssize_arr=(1000) #(2000) #next 100 and 2000
#ds="stack"
#ds="lazylist" #"lazylist"
#ds="extbst"
ds="hashtable"   # for hashtable dssize_arr keys get distributed over capacity. 1000/128 = load factor.
#ds="apiprofile"

################################################################################
echo "make clean and running once before starting experiment"
`make clean > delme.txt 2>&1` 
`HMR_DS=$ds make THREADS=1 ALG=${ds}_none INS=50 DEL=50 KEYRANGE=100 MAXOPS=100 > delme2.txt 2>&1`
echo "make clean and running done check delme.txt delme2.txt"
################################################################################
for sz in "${dssize_arr[@]}"
do
    for mxops in "${maxops_arr[@]}"
    do
        for((idx=0; idx<${#ins_arr[@]}; ++idx))
        do
            echo "EXPERIMENT CONFIGURATION: "
            echo "dssize="${sz} "maxops="${mxops}
            echo ins=${ins_arr[idx]} del=${del_arr[idx]}
            echo iterations=${iterations}
            echo algos="${alg_arr[@]}" 
            echo threads="${threads_arr[@]}"
            echo ""
            source run_exp.sh ${sz} ${mxops} ${ins_arr[idx]} ${del_arr[idx]} #${iterations} #"${alg_arr[@]}" "${threads_arr[@]}"
            echo "#########"
            echo ""
        done #ins

    done #mxops

done #sz


#password="ajay"
#username="ajay"
#Ip="10.42.0.1"
#srcpath="./exp_result/"
#destpath="~/shdir/Day105/"
##scp -r ./exp_result/ ajay@10.42.0.1:~/shdir/Day105/
#scp -r ${srcpath} $username@$Ip:${destpath}


#sshpass -p "$password" scp /<PATH>/final.txt $username@$Ip:/root/<PATH>