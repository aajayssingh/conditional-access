#ifndef __THREADDATA__
#define __THREADDATA__

typedef struct ThreadData
{
   int tid;

   unsigned lcount;
   unsigned long tries;
   unsigned long failures;
   unsigned long type2_failures;
   unsigned long type3_failures;
} ThreadData_t ;


ThreadData_t* new_ThreadData(int id);

#endif
