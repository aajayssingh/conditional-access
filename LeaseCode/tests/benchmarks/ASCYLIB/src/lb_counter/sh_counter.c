
#include "sh_counter.h"
#include "utils.h"

unsigned int levelmax;
unsigned int size_pad_32;
__thread ssmem_allocator_t* alloc;


sh_counter_t*
sh_counter_new()
{
   sh_counter_t *counter;
   
   if ((counter = (sh_counter_t *)ssalloc_aligned(CACHE_LINE_SIZE, sizeof( sh_counter_t ))) == NULL){
      perror("malloc");
      exit(1);
   }
   counter->value = 0;

#if defined(LL_GLOBAL_LOCK)
  counter->lock = (volatile ptlock_t*) ssalloc_aligned(CACHE_LINE_SIZE, sizeof(ptlock_t));
  if (counter->lock == NULL){
      perror("malloc");
      exit(1);
   }
   GL_INIT_LOCK(counter->lock);
#endif

  return counter;
}

