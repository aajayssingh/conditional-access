
#include <assert.h>
#include <getopt.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <inttypes.h>
#include <sys/time.h>
#include <unistd.h>
#include <malloc.h>
#include "utils.h"
#include "atomic_ops.h"

#include "thread_data.h"


ThreadData_t* new_ThreadData(int id){
   ThreadData_t* data;

   if( posix_memalign( (void**) &data, 64, sizeof(ThreadData_t) ) != 0){
      printf("Error: posix_memalign could not allocate memory!!! \n");
      exit(EXIT_FAILURE);
   }

   data->tid = id;
   data->lcount = 0;
   data->tries = 0;
   data->failures = 0;
   data->type2_failures = 0;
   data->type3_failures = 0;

   return data;
}



