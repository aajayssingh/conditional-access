
#include <assert.h>
#include <getopt.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <inttypes.h>
#include <sys/time.h>
#include <unistd.h>
#include <malloc.h>
#include "utils.h"
#include "atomic_ops.h"
#include "rapl_read.h"
#ifdef __sparc__
#  include <sys/types.h>
#  include <sys/processor.h>
#  include <sys/procset.h>
#endif


#include "thread_data.h"
#include "sh_counter.h"
#include "carbon_user.h"

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define DEFAULT_DURATION            10000000
#define DEFAULT_LOCKS_COUNT         1
#define DEFAULT_LEASE_TIME          1000
#define DEFAULT_LEASE_COUNT         1

#define LEASES 1

#define HUNDRED                     100
#define MAX_COUNT                   40000

#define ASM __asm__ __volatile__
#define membarstoreload() { ASM ("mfence;") ; }
#define CPU_RELAX asm volatile("pause\n": : :"memory");


int MarsagliaXOR(int *p_seed) { 
   int seed = *p_seed; 
   
   if (seed == 0) { 
      seed = 1;  
   }

   seed ^= seed << 6; 
   seed ^= ((unsigned)seed) >> 21; 
   seed ^= seed << 7;  
   *p_seed = seed; 

   return seed & 0x7FFFFFFF; 
} 

// barrier synchronization object
pthread_barrier_t   barrier; 

// Lock objects & counters Array
sh_counter_t**    sh_counter_array;

// Global Variables
int thread_count = DEFAULT_THREAD_COUNT;
int lease_time = DEFAULT_LEASE_TIME;
int lease_count = DEFAULT_LEASE_COUNT;
int max_duration = DEFAULT_DURATION;
int locks_count = DEFAULT_LOCKS_COUNT;


// Prototypes 
void* threadMain(void* arg);
unsigned thread_main_multi_locks ( ThreadData_t* data );


int main(int argc, char **argv) 
{
   int i;
   ssalloc_init();
   
   // ---------------------------------------------------------
   // Parameters extraction
   //----------------------------------------------------------
   struct option long_options[] = {
      // These options don't set a flag
      {"help",                      no_argument,       NULL, 'h'},
      {"duration",                  required_argument, NULL, 'd'},
      {"num-threads",               required_argument, NULL, 'n'},
      {"lease_time",                required_argument, NULL, 'l'},
      {"lease_count",               required_argument, NULL, 'f'},
      {NULL, 0, NULL, 0}
   };


   int c;
   while(1) 
   {
      i = 0;
      c = getopt_long(argc, argv, "hd:n:l:f:k:", long_options, &i);
         
      if(c == -1)
         break;
      
      if(c == 0 && long_options[i].flag == 0)
         c = long_options[i].val;
      
      switch(c)
      {
         case 0:
            // Flag is automatically set 
            break;
         case 'h':
            printf("lb_counters test "
            "\n"
            "\n"
            "Usage:\n"
            "  %s [options...]\n"
            "\n"
            "Options:\n"
            "  -h, --help\n"
            "        Print this message\n"
            "  -d, --duration <int>\n"
            "        Test duration in milliseconds\n"
            "  -n, --num-threads <int>\n"
            "        Number of threads\n"
            "  -l, --lease_time <int>\n"
            "        Lease time in ns.\n"
            "  -f, --lease_count <int>\n"
            "        Number of leases taken in a group lease.\n"
            "  -k, --locks_count <int>\n"
            "        Number of total shared counters (locks) in the application.\n"
            , argv[0]);
            exit(0);
         case 'd':
           max_duration = atoi(optarg) * 1000000;
           break;
         case 'n':
           thread_count = atoi(optarg);
           break;
         case 'l':
           lease_time = atoi(optarg);
           break;
         case 'f':
           lease_count = atoi(optarg);
           break;
         case 'k':
           locks_count = atoi(optarg);
           break;
         case '?':
         default:
           printf("Use -h or --help for help\n");
           exit(1);
      }
   }

   
   if(locks_count < 1){
      printf("ERROR: Argument 'Total Locks' must be at lease 1. Exiting...!  \n");
      printf(" Total_Locks(%d) \n", locks_count);
      return 1;
   }

   printf("\ncounters_banch_test: \n");
   printf(" Threads(%i) \n", thread_count); 
   printf(" Max_Duration(%d ms) \n", max_duration/1000000 );
   printf(" Lease_time(%i) \n", lease_time);
   printf(" Lease_Count(%i) \n", lease_count);
   printf(" Total_Locks(%d) \n", locks_count);
   printf(" Start.\n");

   // ---------------------------------------------------------
   // Operation specific initializations
   //----------------------------------------------------------
   
   // Allocate space for locks and counters arrays
   if( posix_memalign( (void**) &sh_counter_array, 64, thread_count * sizeof(sh_counter_t*) ) != 0){
      printf("Error: posix_memalign could not allocate memory!!! \n");
      exit(EXIT_FAILURE);
   }

   // Now allocate locks & Counters
   for(i=0; i < locks_count; i++){
      sh_counter_array[i] = sh_counter_new();
   }

   /* initialize random seed: */
   srand(time(NULL));


   // ---------------------------------------------------------
   // Global initializations
   //----------------------------------------------------------
   
   // Initialize Barrier
   pthread_barrier_init (&barrier, NULL, thread_count);
   printf("Initialized Barrier\n");


   // Allocate Thread data array
   ThreadData_t* thread_args[thread_count];
   for (i = 0; i < thread_count; i++)
   {
      thread_args[i] = new_ThreadData(i);
   }
   pthread_t thread_handles[thread_count];

   RETRY_STATS_ZERO();

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   // Enable performance and energy models
   CarbonEnableModels();

   // Create Threads
   for (i = 1; i < thread_count; i++)
   {
      int ret = pthread_create(&thread_handles[i], NULL, threadMain, (void*) thread_args[i]);
      //printf("Created Thread %d\n", i);
      if (ret != 0)
      {
         printf("ERROR spawning thread %i\n", i);
         exit(EXIT_FAILURE);
      }
   }
   threadMain((void*) thread_args[0]);

   //printf("Threads Finished\n");
   for (i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   //printf("Threads Joined\n");

   // Disable performance and energy models
   CarbonDisableModels();

/* ======================================================================================== */
/* END PARALLEL SECTION */
/* ======================================================================================== */
   
   // ---------------------------------------------------------
   // Output Summary
   //----------------------------------------------------------
   printf("Done.\n\n");

   unsigned long total_tries =0;
   unsigned long total_failures =0;
   unsigned long total_type2_failures =0;

   printf("Summary:\n");
   for (i = 0; i < thread_count; i++)
   {
      ThreadData_t* data = thread_args[i];
      printf("Thread(%d): Total_Attempts(%lu) Type1 Failures(%lu) Type2 Failures(%lu) \n", 
            i, data->tries, data->failures, data->type2_failures );
      
      total_failures += data->failures;
      total_type2_failures += data->type2_failures;
      total_tries += data->tries;
   }
   printf("\nTotal Attempts:\t %lu \n", total_tries);
   printf("  Successful Attempts:\t %lu (%f/s)\n", total_tries - total_failures - total_type2_failures,
        (float)(total_tries - total_failures - total_type2_failures)*1000000000/max_duration );
   printf("  Failed Attempts:\t %lu \n", total_failures + total_type2_failures);
   printf("    Type1 Failures:\t %lu \n", total_failures);
   printf("    Type2 Failures:\t %lu \n", total_type2_failures);
   printf("\n\n");
   printf("Successful Ops = %lu \n", total_tries - total_failures - total_type2_failures );
   printf("Throughput (Ops/s) = %f \n", (float)(total_tries - total_failures - total_type2_failures)*1000000000/max_duration );

   return 0;
}


#define UINT64_C(val) (val##ULL)

uint64_t x; /* The state must be seeded with a nonzero value. */

uint64_t xorshift64(void) {
	x ^= x >> 12; // a
	x ^= x << 25; // b
	x ^= x >> 27; // c
	return x * UINT64_C(2685821657736338717);
}



// ---------------------------------------------------------
// THREAD FUNCTIONS
//----------------------------------------------------------

void* threadMain(void* data) {
    
   thread_main_multi_locks( (ThreadData_t*) data );
   return NULL;
}



unsigned thread_main_multi_locks ( ThreadData_t* data )
{
   pthread_barrier_wait (&barrier); 
   
   //printf("Thread(%d) passed barrier \n", data->tid);
   uint64_t l1 = 0;

   while ( CarbonGetTime() < max_duration )
   {
      data->tries++;

      // ================== LOCK ============================== // 
#if defined(MUTEX) || defined(TAS) || defined(TTAS)
      while(1){
         CarbonRequestLease((void*) sh_counter_array[l1]->lock, sizeof(pthread_mutex_t), lease_time);
#if defined(MUTEX)
         if( !GL_TRYLOCK(sh_counter_array[l1]->lock) )
            break;
#else
         if( GL_TRYLOCK(sh_counter_array[l1]->lock) )
            break;
#endif // End ifdef MUTEX 
         else
            CarbonReleaseAllLease();
      }
#else

      GL_LOCK(sh_counter_array[l1]->lock);
#endif /* end of if defined(MUTEX) || defined(TAS) || defined(TTAS) */

      // Increment Counters
      sh_counter_array[l1]->value++;
      data->lcount++;
      
      // Release the Lock
      GL_UNLOCK(sh_counter_array[l1]->lock);

#if defined(MUTEX) || defined(TAS) || defined(TTAS)
      CarbonReleaseAllLease();
#endif
         

   }
   
   pthread_barrier_wait (&barrier); 
   
   return 0;
}




