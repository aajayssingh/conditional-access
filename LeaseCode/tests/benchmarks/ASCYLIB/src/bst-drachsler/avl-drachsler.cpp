/*   
 *   File: avl-drachsler.cpp
 *   Author: Tudor David <tudor.david@epfl.ch>
 *   Description: Dana Drachsler, Martin Vechev, and Eran Yahav. 
 *   Practical Concurrent Binary Search Trees via Logical Ordering. PPoPP 2014.
 *   bst-drachsler.c is part of ASCYLIB
 *
 * Copyright (c) 2014 Vasileios Trigonakis <vasileios.trigonakis@epfl.ch>,
 * 	     	      Tudor David <tudor.david@epfl.ch>
 *	      	      Distributed Programming Lab (LPD), EPFL
 *
 * ASCYLIB is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */

#include "avl-drachsler.h"
#include "carbon_user.h"
#include <sys/syscall.h>

//#include <vector>

#define bool unsigned

//using namespace std;

RETRY_STATS_VARS;

__thread ssmem_allocator_t* alloc;

node_t* create_node(skey_t k, sval_t value, int initializing) {
    volatile node_t* new_node;
#if GC == 1
    if (unlikely(initializing)) {
        new_node = (volatile node_t*) ssalloc_aligned(CACHE_LINE_SIZE, sizeof(node_t));
    } else {
        new_node = (volatile node_t*) ssmem_alloc(alloc, sizeof(node_t));
    }
#else 
    new_node = (volatile node_t*) ssalloc(sizeof(node_t));
#endif
    if (new_node == NULL) {
        perror("malloc in bst create node");
        exit(1);
    }
    new_node->left = NULL;
    new_node->right = NULL;
    new_node->parent = NULL;
    new_node->succ = NULL;
    new_node->pred = NULL;
    INIT_LOCK(&(new_node->tree_lock));
    INIT_LOCK(&(new_node->succ_lock));
    new_node->key = k;
    new_node->value = value;
    new_node->mark = FALSE;
    asm volatile("" ::: "memory");
    return (node_t*) new_node;
}

node_t* initialize_tree(){
    node_t* parent = create_node(MIN_KEY, (sval_t) 0, 1); 
    node_t* root = create_node(MAX_KEY, (sval_t) 0, 1);
    root->pred = parent;
    root->succ = parent;
    root->parent = parent;
    parent->right = root;
    parent->succ = root;
    return root;
}

node_t* bst_search(skey_t k, node_t* root) {
  PARSE_TRY();

  node_t* n = root;
  node_t* child;
  skey_t curr_key;
  volatile skey_t dummy_key;
  //node_t dummy_node;
  
  
  node_t* first_node = (node_t *) root->parent; 
  node_t* last_parent = (node_t *) n->parent;
  
  
  //printf("App Adding %#lx \n", first_node); fflush(stdout);
   //not sure this is the case
   CarbonAddToWatchset( (void *) first_node, CACHE_LINE_SIZE );
   //dummy_key = first_node->key;   
      
  
  
  bool lastLeft = true; 
   float x, y;
  
  while (1) {
  
    curr_key = n->key;
    
    if (curr_key == k) {
    
      // Call ValidateWatchset just to reset the Watchset
      CarbonValidateWatchset();
      return n;

    }
    
    
    
    if (curr_key < k) {
    //going right
      child = (node_t*) n->right;
      if (lastLeft) {
			lastLeft = false;
			//clear the set
			CarbonValidateWatchset();
			
			first_node = (node_t *) n->parent;
		
        
        	CarbonAddToWatchset( (void *) first_node, CACHE_LINE_SIZE );
      	//dummy_key =  first_node->key;
      	
			
      }
    } 
    else {
    		//going left
      	child = (node_t*) n->left;
      	if (!lastLeft) {
				lastLeft = true;
				
				CarbonValidateWatchset();
			
				first_node = (node_t *) n->parent;
        	
        	       
        		CarbonAddToWatchset( (void *) first_node, CACHE_LINE_SIZE );
      		//dummy_key =  first_node->key;
      	
      	}
      	
    	}
    	
      //printf("App Adding %#lx \n", n); fflush(stdout);
    	CarbonAddToWatchset( (void *) n, CACHE_LINE_SIZE );
      //dummy_key =  n->key;
    
    	
    	if (child == NULL) {
    	
    		if (CarbonValidateWatchset()) 
      	{ 
      		return n;
      	}
      	
      	first_node = (node_t *) root->parent; 
      	child = root;
      	lastLeft = true;
      	
      	CarbonAddToWatchset( (void *) first_node, CACHE_LINE_SIZE );
  			//dummy_key = first_node->key;
      
    	}
   	n = child;
  }
}

sval_t bst_contains(skey_t k, node_t* root) {
    node_t* n = bst_search(k,root);
    //while (n->key > k){
    //    n = (node_t*) n->pred;
    //}
    //while (n->key < k){
    //    n = (node_t*) n->succ;
    //}
    if ((n->key == k) && (n->mark == FALSE)) {
        return n->value;
    }
    return 0;
}

bool_t bst_insert(skey_t k, sval_t v, node_t* root) {
    while(1) {
      UPDATE_TRY();
         node_t* node = bst_search(k, root);
        volatile node_t* p;
        if (node->key >= k) {
            p = (node_t*) node->pred;
        } else {
            p = (node_t*) node;
        }

#if DRACHSLER_RO_FAIL == 1
	 node_t* n = node;
	while (n->key > k)
	  {
	    n = (node_t*) n->pred;
	  }
	while (n->key < k)
	  {
	    n = (node_t*) n->succ;
	  }
	if ((n->key == k) && (n->mark == FALSE)) 
	  {
	    return FALSE;
	  }
#endif

        LOCK(&(p->succ_lock));
        volatile node_t* s = (node_t*) p->succ;
        if ((k > p->key) && (k <= s->key) && (p->mark == FALSE)) {
            if (s->key == k) {
                UNLOCK(&(p->succ_lock)); 
                return FALSE;
            }
            node_t* new_node = create_node(k,v,0);
            node_t* parent = choose_parent((node_t*) p, (node_t*) s, node);
            new_node->succ = s;
            new_node->pred = p;
            new_node->parent = parent;
#ifdef __tile__
            MEM_BARRIER;
#endif
            s->pred = new_node;
            p->succ = new_node;
            UNLOCK(&(p->succ_lock));
            insert_to_tree((node_t*) parent,(node_t*) new_node,(node_t*) root);
            return true;
        }
        UNLOCK(&(p->succ_lock));
    }
}

node_t* choose_parent(node_t* p, node_t* s, node_t* first_cand){
    node_t* candidate;
    if ((first_cand == p) || (first_cand == s)) {
        candidate = first_cand;
    } else {
        candidate = p;
    }
    while (1) {
        LOCK(&(candidate->tree_lock));
        if (candidate == p) {
            if (candidate->right == NULL) {
                return candidate;
            }
            UNLOCK(&(candidate->tree_lock));
            candidate = s;
        } else {
            if (candidate->left == NULL) {
                return candidate;
            }
            UNLOCK(&(candidate->tree_lock));
            candidate = p;
        }
    }
}


void rebalance(node_t* iter, node_t* iter_parent, node_t* root, bool is_left) {
	node_t* other_child = NULL;
	if (iter_parent == root) {
		UNLOCK(&(iter_parent->tree_lock));
		if (iter != NULL) UNLOCK(&(iter->tree_lock));
		return;
	}
	node_t* grand_parent = NULL;
	while (iter_parent != root) {
		bool updated_height = update_height(iter, iter_parent, is_left);
		int bf = iter_parent->left_height - iter_parent->right_height;
		if (!updated_height && abs(bf) < 2) {
			UNLOCK(&(iter->tree_lock));
			UNLOCK(&(iter_parent->tree_lock));
			return;
		}
		while (bf >= 2 || bf <= -2) {
			if ((is_left && bf <= -2) || (!is_left && bf >= 2)) {
				other_child = is_left? iter_parent->right : iter_parent->left;
				bool locked = TRYLOCK(other_child->tree_lock);
				if (!locked || !other_child->mark) {
					if (locked) UNLOCK(&(other_child->tree_lock));
					if (grand_parent != NULL) UNLOCK(&(grand_parent->tree_lock));
					grand_parent = NULL;
					iter = unlock_and_relock(iter, iter_parent);
					if (iter == NULL) {
						return;
					}
					is_left = iter_parent->left == iter;
					bf = iter_parent->left_height - iter_parent->right_height;
					continue;
				} 
				is_left = !is_left;
			} else {
				other_child = iter;
				iter = NULL;
			}
			if ((is_left && other_child->left_height - other_child->right_height < 0) || (!is_left && other_child->left_height - other_child ->right_height > 0)) {
				node_t* grand_child =  is_left? other_child->right : other_child->left;
				bool grand_locked = TRYLOCK(grand_child); 
				if (!grand_locked || !grand_child->mark) {
					if (grand_locked) UNLOCK(grand_child->tree_lock);
					UNLOCK(other_child->tree_lock);
					if (grand_parent != NULL) UNLOCK(grand_parent->tree_lock);
					grand_parent = NULL;
					iter = unlock_and_relock(iter, iter_parent);
					if (iter == NULL) {
						return;
					}
					is_left = iter_parent->left == iter;
					bf = iter_parent->left_height - iter_parent->right_height;
					continue;
				}
				rotate(grand_child, other_child, iter_parent, is_left);
				UNLOCK(other_child->tree_lock);
				other_child = grand_child;
			}
			if (grand_parent == NULL) {
				grand_parent = lock_parent(iter_parent);
			}
			rotate(other_child,  iter_parent, grand_parent, !is_left);
			if (iter != NULL) {
				UNLOCK(iter->tree_lock);
			}
			bf = iter_parent->left_height - iter_parent->right_height;
			if (bf >= 2 || bf <= -2) {
				UNLOCK(grand_parent->tree_lock);
				grand_parent = other_child;
				iter = NULL;
				is_left = bf >= 2? false: true; // enforces to lock child
				continue;
			}
			iter = iter_parent;
			iter_parent = other_child;
			is_left = iter_parent->left == iter;
			bf = iter_parent->left_height - iter_parent->right_height;
		}
		if (iter != NULL) {
			UNLOCK(iter->tree_lock);
		}
		iter = iter_parent;
		iter_parent = (grand_parent != NULL) ? grand_parent: lock_parent(iter_parent);
		is_left = iter_parent->left == iter;
		grand_parent = NULL;
	}
	UNLOCK(iter->tree_lock);
	UNLOCK(iter_parent->tree_lock);
}



void insert_to_tree(node_t* parent, node_t* new_node, node_t* root) {
    new_node->parent = parent;
    if (parent->key < new_node->key) {
        parent->right = new_node;
    } else {
        parent->left = new_node;
    }

	if (parent != root) {
		node_t* grand_parent = lock_parent(parent);
		rebalance(parent, grand_parent, root, grand_parent->left == parent);
	} else {
	    UNLOCK(&(parent->tree_lock));
         }
}


node_t* lock_parent(node_t* node) {
    node_t* p;
    while (1) {
        p = (node_t*) node->parent;
        LOCK(&(p->tree_lock));
        if ((node->parent == p) && (p->mark == FALSE)) {
            return p;
        }
        UNLOCK(&(p->tree_lock));
    }
}



bool update_height(node_t* iter, node_t* iter_parent, bool left) {
	int new_height = (iter == NULL) ? 0: max(iter->left_height, iter->right_height) + 1;
	int old_height = left? iter_parent->left_height : iter_parent->right_height;
	if (new_height == old_height) return false;
	if (left) {
		iter_parent->left_height = new_height;
	} else {
		iter_parent->right_height = new_height;
	}
	return true;
}


node_t* unlock_and_relock(node_t* iter, node_t* iter_parent) {
	if (iter != NULL) {
		UNLOCK(iter->tree_lock);
	}
	UNLOCK(iter_parent->tree_lock);
	while (true) { 
		LOCK(iter_parent->tree_lock);
		int bf = iter_parent->left_height - iter_parent->right_height;
		if (!iter_parent->mark) {
			UNLOCK(iter_parent->tree_lock);
			return NULL;
		}
		iter = (bf >= 2) ? iter_parent->left : iter_parent->right;
		if (iter == NULL) return NULL;
		if (TRYLOCK(iter->tree_lock)) return iter;
		UNLOCK(iter_parent->tree_lock);
	}
}


void rotate(node_t* iter, node_t* iter_parent, node_t* grand_parent, bool left) {
	if (grand_parent->left == iter_parent) {
		grand_parent->left = iter;
	} else {
		grand_parent->right = iter;
	}
	iter->parent = grand_parent;
	iter_parent->parent = iter;
	node_t* child = left? iter->left : iter->right;
	if (left) {
		iter_parent->right = child;
		if (child != NULL) {
			child->parent = iter_parent; 
		}
		iter->left = iter_parent;
		iter_parent->right_height = iter->left_height;
		iter->left_height = max(iter_parent->left_height, iter_parent->right_height) + 1;
	} else {
		iter_parent->left = child;
		if (child != NULL) {
			child->parent = iter_parent; 
		}
		iter->right = iter_parent;
		iter_parent->left_height = iter->right_height;
		iter->right_height = max(iter_parent->left_height, iter_parent->right_height) + 1;
	}
}


sval_t bst_remove(skey_t k, node_t* root) {
node_t* node;
while (1) {
UPDATE_TRY();
node = bst_search(k, root);
node_t* p;
if (node->key >= k) {
    p = (node_t*) node->pred;
        } else {
            p = (node_t*) node;
        }

#if DRACHSLER_RO_FAIL == 1
	node_t* n = node;
	while (n->key > k)
	  {
	    n = (node_t*) n->pred;
	  }
	while (n->key < k)
	  {
	    n = (node_t*) n->succ;
	  }
	if ((n->key != k) && (n->mark == FALSE)) 
	  {
	    return FALSE;
	  }
#endif

        LOCK(&(p->succ_lock));
        node_t* s = (node_t*) p->succ;
        if ((k > p->key) && (k <= s->key) && (p->mark == FALSE)) {
            if (s->key > k) {
                UNLOCK(&(p->succ_lock));
                return 0;
            }
            LOCK(&(s->succ_lock));
            bool_t has_two_children = acquire_tree_locks(s);
            lock_parent(s);
            s->mark = TRUE;
            node_t* s_succ = (node_t*) s->succ;
            s_succ->pred = p;
            p->succ = s_succ;
            UNLOCK(&(s->succ_lock));
            UNLOCK(&(p->succ_lock));
            sval_t v = s->value;
            remove_from_tree(s, has_two_children,root);
            return v; 
        }
        UNLOCK(&(p->succ_lock));
    }
}

bool_t acquire_tree_locks(node_t* n) {
  LOCK_TRY_ONCE_CLEAR();

    while (1) {
        LOCK(&(n->tree_lock));
        node_t* left = (node_t*) n->left;
        node_t* right = (node_t*) n->right;
        //lock_parent(n);
        if ((right == NULL) || (left == NULL)) {
            return FALSE;
        } else {
            node_t* s = (node_t*) n->succ;
            int l=0;
            node_t* parent;
            node_t* sp = (node_t*) s->parent;
            if (sp != n) {
                parent = sp;
                if (!TRYLOCK(&(parent->tree_lock))) {
                    UNLOCK(&(n->tree_lock));
                    //UNLOCK(&(n->parent->tree_lock));
                    continue;
                }
                l=1;
                if ((parent != s->parent) || (parent->mark==TRUE)) {
                    UNLOCK(&(n->tree_lock));
                    UNLOCK(&(parent->tree_lock));
                    //UNLOCK(&(n->parent->tree_lock));
                    continue;
                }
            }
            if (!TRYLOCK(&(s->tree_lock))) {
                UNLOCK(&(n->tree_lock));
                //UNLOCK(&(n->parent->tree_lock));
                if (l) { 
                    UNLOCK(&(parent->tree_lock));
                }
                continue;
            }
            return TRUE;
        }
    }
}

void remove_from_tree(node_t* n, bool_t has_two_children,node_t* root) {
    node_t* child;
    node_t* parent;
    node_t* s;
    //int l=0;
    if (has_two_children == FALSE) { 
        if ( n->right == NULL) {
            child = (node_t*) n->left;
        } else {
            child = (node_t*) n->right;
        }
        parent = (node_t*) n->parent;
        update_child(parent, n, child);
	UNLOCK(&(n->tree_lock));
	rebalance(child, parent, root, parent->left == child);
    } else {
        s = (node_t*) n->succ;
        child = (node_t*) s->right;
        parent = (node_t*) s->parent;
        //if (parent != n ) l=1;
        update_child(parent, s, child);
        s->left = n->left;
        s->right = n->right;
        n->left->parent = s;
        if (n->right != NULL) {
            n->right->parent = s;
        }
        update_child((node_t*) n->parent, n, s);
        if (parent == n) {
            parent = s;
        } else {
            UNLOCK(&(s->tree_lock));
        }
        //UNLOCK(&(parent->tree_lock));
	UNLOCK(&(n->tree_lock));
        UNLOCK(&(n->parent->tree_lock));
	rebalance(child, parent, root, parent->left == child);
    }

#if GC == 1
    ssmem_free(alloc, n);
#endif
}

void update_child(node_t* parent, node_t* old_ch, node_t* new_ch) {
    if (parent->left == old_ch) {
        parent->left = new_ch;
    } else {
        parent->right = new_ch;
    }
    if (new_ch != NULL) {
        new_ch->parent = parent;
    }
}


uint32_t bst_size(node_t* node) {
    if (node==NULL) return 0;
    uint32_t x = 0;
    if ((node->key != MAX_KEY) && (node->key != MIN_KEY)) {
        x = 1;
    }
    return x + bst_size((node_t*) node->right) + bst_size((node_t*) node->left);
}

