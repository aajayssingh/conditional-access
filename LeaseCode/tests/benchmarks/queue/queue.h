#ifndef __QUEUE__
#define __QUEUE__

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <math.h>
#include <pthread.h>
#include <assert.h> 
#include "thread_data.h"
#include "carbon_user.h"

#define _NULL_VALUE  0
#define ASM __asm__ __volatile__
#define membarstoreload() { ASM ("mfence;") ; }
#define CACHE_LINE_SIZE 64

#define TAIL_LEASE 1

class Queue
{
private:

	struct Node {
		
      Node* volatile	_next;
		int volatile	_value;

		Node(int inValue) : _next(NULL), _value(inValue) {}
	};

	volatile Node* _head;
   char dummy_1[CACHE_LINE_SIZE];
	volatile Node* _tail;
   char dummy_2[CACHE_LINE_SIZE];
   int _lease_count;

public:
   Queue(int lease_count)
   {
      _lease_count = lease_count;

      void* buffer;
      if (posix_memalign( (void**) &buffer, CACHE_LINE_SIZE, sizeof(Node) ) != 0) {
         printf("posix_memalign did not work! \n");
         exit(1);
      }
      Node* new_node = new(buffer) Node(_NULL_VALUE);
		//Node* const new_node = new Node(_NULL_VALUE);		// Allocate a free node. TODO: Fix NULL_VALUE

		new_node->_next = NULL;								      // Make it the only node in the linked list
      
      /*
      if( posix_memalign( (void**) &_head, CACHE_LINE_SIZE, sizeof(Node*) ) != 0){
         printf("posix_memalign did not work! \n");
         exit(1);
      }
      if( posix_memalign( (void**) &_tail, CACHE_LINE_SIZE, sizeof(Node*) ) != 0){
         printf("posix_memalign did not work! \n");
         exit(1);
      }
      */
		
      _head = new_node;
		_tail = new_node;
    
      /*
      printf("New Node Address(%lx) %s\n", new_node, ((unsigned long) new_node % CACHE_LINE_SIZE == 0 )? "ALIGNED" : "NOT_ALIGNED");
      printf("HEAD Address(%lx) %s\n", &_head, ((unsigned long) &_head % CACHE_LINE_SIZE == 0 )? "ALIGNED" : "NOT_ALIGNED");
      printf("TAIL Address(%lx) %s\n", &_tail, ((unsigned long) &_tail % CACHE_LINE_SIZE == 0 )? "ALIGNED" : "NOT_ALIGNED");
      printf("TAIL NEXT Address(%lx) %s\n", &(_tail->_next), ((unsigned long) &(_tail->_next) % CACHE_LINE_SIZE == 0 )? "ALIGNED" : "NOT_ALIGNED");
      printf("TAIL NEXT (%lx) \n", _tail->_next);
      */
      //exit(0);
	}
   
   bool __attribute__((optimize("O0"))) enqueue(int inValue, int lease_time, ThreadData* data) 
   {
      bool success = false;
      void* buffer;
      if (posix_memalign( (void**) &buffer, CACHE_LINE_SIZE, sizeof(Node) ) != 0) {
         printf("posix_memalign did not work! \n");
         exit(1);
      }
      Node* new_node = new(buffer) Node(_NULL_VALUE); // Allocate a new node from the free list
		new_node->_next = NULL;    					      // Set next pointer of node to NULL
		
      volatile Node* tail;
      volatile Node* dummy_node;
		Node* next_of_tail;

		do{
         data->tries++;

         // Request the lease, and read the 'next' pointer of tail node
         //CarbonRequestLease((void*) &(_tail), sizeof(_tail), lease_time);
#ifdef TAIL_LEASE
         if(lease_time > 0)
            CarbonRequestGroupLease( _lease_count, lease_time, &_tail, &(_tail->_next));
         // A dummy tail read to fix the lease problem
         dummy_node = _tail;
         tail = _tail;									// Read Tail ptr 
#else
         tail = _tail;									// Read Tail ptr 
         CarbonRequestGroupLease( 1, lease_time, &(tail->_next) );
         //dummy_node = _tail->_next;
#endif   

         next_of_tail = tail->_next;			   // Read next ptr of Tail node

			if(tail == _tail)	{							      // Are tail and next consistent?
				
            // Was Tail pointing to the last node?
				if (next_of_tail == NULL) {
					
               // Try to link node at the end of the linked list
               success =  __sync_bool_compare_and_swap( &(tail->_next), next_of_tail, new_node);

#ifndef TAIL_LEASE
               CarbonReleaseAllLease();
#endif
               if(success){
                  // Enqueue is done.  Try to swing Tail to the inserted node
                  __sync_bool_compare_and_swap(&_tail, tail, new_node);
                  
#ifdef TAIL_LEASE
                  CarbonReleaseAllLease();
#endif
                  break;	// Enqueue is done.  Exit loop
					
               } else {
#ifdef TAIL_LEASE
                  CarbonReleaseAllLease();
#endif
                  data->failures++; // Major failures type
					}
               
				} else {		// Tail was not pointing to the last node 
               
#ifndef TAIL_LEASE
               CarbonReleaseAllLease();
#endif
               // Try to swing Tail to the next node
               __sync_bool_compare_and_swap(&_tail, tail, next_of_tail);

#ifdef TAIL_LEASE
               CarbonReleaseAllLease();
#endif
               data->type2_failures++;
				}
			}
         else
         {
            // Release
            CarbonReleaseAllLease();
            data->type3_failures++;
         }

		} while(true);

		// Enqueue is done.  Try to swing Tail to the inserted node
      //__sync_bool_compare_and_swap(&_tail, tail, new_node);

		return true;

	}

   int dequeue(int lease_time, ThreadData* data) 
   {
      bool success = false;
		volatile Node* head;
		volatile Node* tail;
		Node* next_of_head;

		do{
         data->tries++;
			
         tail = _tail;									// Read Tail
        
         // Request the lease, and read Head pointer
         CarbonRequestLease((void*) &_head, sizeof(_head), lease_time);
			head = _head;
			
         next_of_head = head->_next;				      // Read next ptr of Head node

			if(head == _head)	{							      // Are head, tail and next consistent?
            if(head == tail){                         // Is queue empty or Tail falling behind?
					
               // Release ASAP
               CarbonReleaseLease((void*) &_head, sizeof(_head));
               
               if (next_of_head == NULL)	{					// Is queue empty?
						return _NULL_VALUE;							// Queue is empty, couldn't dequeue
					}

					// Tail is falling behind.  Try to advance it
               __sync_bool_compare_and_swap( &_tail, tail, next_of_head);

               data->type2_failures++;
				} 
            // No need to deal with Tail
            else {
					// Read value before CAS Otherwise, another dequeue might free the next node
					int rtrn_value = next_of_head->_value;

					// Try to swing Head to the next node
               success =  __sync_bool_compare_and_swap( &_head, head, next_of_head);
               
               // Release
               CarbonReleaseLease((void*) &_head, sizeof(_head));
					
               if(success){
						//free(head.ptr)		     // It is safe now to free the old node
						return rtrn_value;     // Queue was not empty, dequeue succeeded	
					} else {
						data->failures++;
					}
				}
         }
         else {	
            // Release
            CarbonReleaseLease((void*) &_head, sizeof(_head));
            data->type3_failures++;
         }
         
		} while(true);
	}

   int get_current_size(){
      int size = 0;
      Node* next_node = _head->_next;

      while( next_node != NULL){
         size ++;
         next_node = next_node->_next;
      }

      return size;
   }
   
};



#endif
