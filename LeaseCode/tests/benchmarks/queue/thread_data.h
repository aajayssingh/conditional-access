#ifndef __THREADDATA__
#define __THREADDATA__

class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_)
      : tid(tid_), num_threads(num_threads_)
      , lcount(0), tries(0)
      , failures(0)
      , type2_failures(0)
      , type3_failures(0)
   {}
   ~ThreadData()
   {}

   int tid;
   int num_threads;

   unsigned lcount;
   unsigned long tries;
   unsigned long failures;
   unsigned long type2_failures;
   unsigned long type3_failures;
};

#endif
