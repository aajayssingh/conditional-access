#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <time.h>
#include <sched.h>
#include <errno.h>
#include <string.h>
#include <map>
#include <unistd.h>
#include <math.h>
#include <sys/syscall.h>
#include "queue.h"
#include "thread_data.h"
#include "carbon_user.h"

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define DEFAULT_TIME                100
#define DEFAULT_PIN                 0
#define DEFAULT_OP_TYPE             0
#define DEFAULT_LEASE_TIME          100

#define HUNDRED                     100
#define MAX_COUNT                   1000


// barrier synchronization object
pthread_barrier_t   barrier; 

/* ================================================================ */
// Global Variables
/* ================================================================ */
Queue* _MS_Queue;
//unsigned *counter;
int thread_count;
int op_type;
int lease_time;
int lease_count = 1;
unsigned long max_duration = 1000000;
unsigned long max_count;
/* ================================================================ */

// Prototypes 
void* threadMain(void* arg);
unsigned thread_main_fetch ( ThreadData* data );
unsigned thread_main_cas ( ThreadData* data );


int main(int argc, char *argv[])
{
   thread_count   = DEFAULT_THREAD_COUNT;
   op_type        = DEFAULT_OP_TYPE;
   lease_time     = DEFAULT_LEASE_TIME;
   max_count      = MAX_COUNT;

   if(argc > 4){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      lease_time     = atoi(argv[3]);
      max_duration   = (unsigned long) atoi(argv[4]) * 1000000;     // Duration entered in Milliseconds converted to Nanoseconds
      lease_count    = atoi(argv[5]);
      
      printf("\ncounters_banch_test: Threads(%i) OpType(%i) Lease_time(%i) Max_Duration(%lums) Lease_count(%i) Start.\n" , 
            thread_count, op_type, lease_time, max_duration/1000000, lease_count);
   }
   else if(argc > 3){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      lease_time     = atoi(argv[3]);
      printf("\ncounters_banch_test: Threads(%i) OpType(%i) Lease_time(%i) Start.\n" , 
            thread_count, op_type, lease_time);
   }
   else if(argc > 2){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      printf("\ncounters_banch_test: Threads(%i) OpType(%i) Start.\n" , 
            thread_count, op_type);
   }
   else if(argc > 1){
      thread_count = atoi(argv[1]);
      printf("\ncounters_banch_test: Threads(%i) Start.\n" , thread_count);
   }


   _MS_Queue = new Queue(lease_count);



   // Initialize & allocate counter
   //int ret_val = posix_memalign( (void**) &counter, 64, sizeof(unsigned));
   //int ret_val = posix_memalign( (void**) &counter, 64, 64);
   
   /*
   if(ret_val != 0){ 
      printf("Error: Could not allocatei Counter!!! \n");
      if(ret_val == EINVAL)
         printf("The alignment argument was not a power of two, or was not a multiple of sizeof(void *). \n");
      else if(ret_val == ENOMEM)
         printf("Insufficient Memory to fulfil the request. \n");
      exit(EXIT_FAILURE);
   }
   */

   // Initialize Barrier
   pthread_barrier_init (&barrier, NULL, thread_count);
   printf("Initialized Barrier\n");

   // Allocate Thread data array
   ThreadData* thread_args[thread_count];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i] = new ThreadData(i, thread_count);
   }
   pthread_t thread_handles[thread_count];
  
   // Initialize the queue
   ThreadData* dummy = new ThreadData(0, thread_count);
   printf("Initializing Queue with 2000 nodes! \n");
   for (int i = 1; i <= 2000; i++){
      _MS_Queue->enqueue( i, -1, dummy );
   }
   printf("Initialized...\n");
   printf("Initial Size = %d \n", _MS_Queue->get_current_size() );



/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   // Enable performance and energy models
   CarbonEnableModels();

   /*
   unsigned long fails;
   _MS_Queue->enqueue(5, 500, fails);
   _MS_Queue->enqueue(5, 500, fails);
   _MS_Queue->dequeue(500, fails);
   _MS_Queue->dequeue(500, fails);
   */

   // Create Threads
   for (int i = 1; i < thread_count; i++)
   {
      int ret = pthread_create(&thread_handles[i], NULL, threadMain, (void*) thread_args[i]);
      //printf("Created Thread %d\n", i);
      if (ret != 0)
      {
         printf("ERROR spawning thread %i\n", i);
         exit(EXIT_FAILURE);
      }
   }
   threadMain((void*) thread_args[0]);

   //printf("Threads Finished\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   //printf("Threads Joined\n");

   // Disable performance and energy models
   CarbonDisableModels();

/* ======================================================================================== */
/* END PARALLEL SECTION */
/* ======================================================================================== */
   
   // ---------------------------------------------------------
   // Output Summary
   //----------------------------------------------------------
   printf("Done.\n\n");
   printf("Final Size = %d \n", _MS_Queue->get_current_size() );

   unsigned long total_tries =0;
   unsigned long total_failures =0;
   unsigned long total_successes =0;

   printf("Summary:\n");
   for (int i = 0; i < thread_count; i++)
   {
      ThreadData* data = thread_args[i];
      printf("Thread(%d): Attempts(%lu), Failures: Type1(%lu) Type2(%lu) Type3(%lu)\n", 
            i, data->tries, data->failures, data->type2_failures, data->type3_failures);

      total_tries += data->tries;
      total_failures += data->failures + data->type2_failures + data->type3_failures;
      total_successes += data->lcount;
   }
   printf("\nTotal Attempts:\t %lu \n", total_tries);
   printf("  Successful Attempts:\t %lu (%f/s)\n", total_successes,
        (float)(total_successes)*1000000000/max_duration );
   printf("  Failed Attempts:\t %lu \n", total_failures );
   printf("\n\n");
   
   printf("Successful Ops = %lu \n", total_successes );
   printf("Throughput (Ops/s) = %f \n", (float)(total_successes)*1000000000/max_duration );
   printf("\n\n");

   return 0;
}


void* threadMain(void* data) {
    
    if( op_type == 0 )
    {
        thread_main_cas( (ThreadData*)data );
    }
    else
    {
        printf( "wrong op_type parameter!" );
        exit(0);
    }
    return NULL;
}


unsigned thread_main_cas ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   //printf("Thread %d entered cas\n", data->tid);
   //fflush(stdout);

   //ThreadData* dummy = new ThreadData(data->tid, thread_count);

   while ( CarbonGetTime() < max_duration )
   {
      // ENQUEUE
      _MS_Queue->enqueue( data->lcount, lease_time, data );
      data->lcount++;
      
      // DEQUEUE
      _MS_Queue->dequeue( lease_time, data );
      data->lcount++;
   }

   pthread_barrier_wait (&barrier); 
   
   return 0;
}

