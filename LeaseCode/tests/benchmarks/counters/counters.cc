#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <time.h>
#include <sched.h>
#include <errno.h>
#include <string.h>
#include <map>
#include <unistd.h>
#include <math.h>
#include <sys/syscall.h>
#include <assert.h>
#include "carbon_user.h"

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define DEFAULT_LOCKS_COUNT         8
#define DEFAULT_TIME                100
#define DEFAULT_PIN                 0
#define DEFAULT_OP_TYPE             0
#define DEFAULT_LEASE_TIME          100
#define DEFAULT_LEASE_COUNT         1
#define DEFAULT_SNAP_SIZE			   2


#define LEASES 1

#define HUNDRED                     100
#define MAX_COUNT                   40000

#define ASM __asm__ __volatile__
#define membarstoreload() { ASM ("mfence;") ; }
#define CPU_RELAX asm volatile("pause\n": : :"memory");

#define LOCK                        1
#define UNLOCK                      0


class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_)
      : tid(tid_), num_threads(num_threads_)
      , lcount(0), tries(0), failures(0) 
      , type2_failures(0)
      , type3_failures(0)
      , total_latency(0)
      , worst_latency(0)
   {}
   ~ThreadData()
   {}

   int tid;
   int num_threads;
   int snap_size;
   
   //this is just a hack, to avoid allocating / de-allocating, which apparently upsets the simulator
   int snap[100];
   int indices[100];

   unsigned lcount;
   unsigned long tries;
   unsigned long failures;
   unsigned long type2_failures;
   unsigned long type3_failures;
    
   unsigned long total_latency;  
   unsigned long worst_latency;  

   std::vector <int> try_count;
   std::vector <int> consec; 
};

int MarsagliaXOR(int *p_seed) { 
   int seed = *p_seed; 
   
   if (seed == 0) { 
      seed = 1;  
   }

   seed ^= seed << 6; 
   seed ^= ((unsigned)seed) >> 21; 
   seed ^= seed << 7;  
   *p_seed = seed; 

   return seed & 0x7FFFFFFF; 
} 


bool __attribute__((optimize("O0"))) try_lock(int volatile* lock_var ){
//bool try_lock(int* lock_var ){
   int dummy = *(lock_var);   // To start the lease
   membarstoreload();
   bool success = __sync_bool_compare_and_swap(lock_var, UNLOCK, LOCK);
   membarstoreload();
   return success;
}

bool unlock(int volatile* lock_var ){
   membarstoreload();
   bool success = __sync_bool_compare_and_swap(lock_var, LOCK, UNLOCK);
   membarstoreload();
   if(!success){
      printf("Lock_Addr(%lx)\n", lock_var);
      fflush(stdout);
      assert(success);
   }
   return success;
}

// barrier synchronization object
pthread_barrier_t   barrier; 

// Lock object
pthread_mutex_t lock __attribute__((aligned(64)));

// Lock objects & counters Array
pthread_mutex_t**    lock_array;
int volatile**       lock_var_array;
unsigned**           counter_array;
int volatile         flag;

struct node{
   int key;
   int x[64];
   struct node* next;
};
struct node head[10000];
struct node tail[10000];


// Global Variables
unsigned *counter;
int thread_count;
int op_type;
int lease_time;
int lease_count;
int max_duration;
int locks_count = DEFAULT_LOCKS_COUNT;
int snap_size = DEFAULT_SNAP_SIZE;
unsigned long seeds[3];



// Prototypes 
void* threadMain(void* arg);
unsigned thread_main_fetch ( ThreadData* data );
unsigned thread_main_cas ( ThreadData* data );
unsigned thread_main_lock ( ThreadData* data );
unsigned thread_main_multi_locks ( ThreadData* data );
unsigned thread_main_multi_cas_locks ( ThreadData* data );
unsigned thread_main_multi_cas_locks_simple_leases ( ThreadData* data );
unsigned thread_main_cas_lock_var_CS ( ThreadData* data );
unsigned thread_main_snapshot_value_based ( ThreadData* data );
unsigned thread_main_snapshot_set_based ( ThreadData* data );
unsigned thread_main_testdummy ( ThreadData* data );
unsigned thread_main_test_counter ( ThreadData* data );

int main(int argc, char *argv[])
{
   thread_count = DEFAULT_THREAD_COUNT;
   op_type = DEFAULT_OP_TYPE;
   lease_time = DEFAULT_LEASE_TIME;
   lease_count = DEFAULT_LEASE_COUNT;
   flag = 0;


   // ---------------------------------------------------------
   // Parameters extraction
   //----------------------------------------------------------
   
   if(argc > 3){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      lease_time     = atoi(argv[3]);
      max_duration   = atoi(argv[4]) * 1000000;     // Duration entered in Milliseconds converted to Nanoseconds
      lease_count    = atoi(argv[5]);
     
      if(atoi(argv[6]) > 0)
         locks_count    = atoi(argv[6]);
      
      if(atoi(argv[7]) > 0 )
      {
      	snap_size = atoi( argv[ 7 ] );
      }
      
      
      if(locks_count < 2){
         printf("ERROR: Argument 'Total Locks' must be more than 1. Exiting...!  \n");
         printf(" Total_Locks(%d) \n", locks_count);
         return 1;
      }

      printf("\ncounters_banch_test: \n");
      printf(" Threads(%i) \n", thread_count); 
      printf(" OpType(%i) \n", op_type); 
      printf(" Lease_time(%i) \n", lease_time);
      printf(" Lease_Count(%i) \n", lease_count);
      printf(" Max_Duration(%d ms) \n", max_duration/1000000 );
      printf(" Total_Locks(%d) \n", locks_count);
      printf(" Snap_Size(%d) \n", snap_size );
      printf(" Start.\n");
   }
   else if(argc > 2){
      thread_count   = atoi(argv[1]);
      op_type        = atoi(argv[2]);
      printf("\ncounters_banch_test: Threads(%i) OpType(%i) Start.\n" , 
            thread_count, op_type);
   }
   else if(argc > 1){
      thread_count = atoi(argv[1]);
      printf("\ncounters_banch_test: Threads(%i) Start.\n" , thread_count);
   }

   // ---------------------------------------------------------
   // Operation specific initializations
   //----------------------------------------------------------
   
   if(op_type <= 2)
   {
      // Initialize & allocate counter
      int ret_val = posix_memalign( (void**) &counter, 64, sizeof(unsigned));
      //int ret_val = posix_memalign( (void**) &counter, 64, 64);
      
      if(ret_val != 0){ 
         printf("Error: Could not allocate Counter!!! \n");
         if(ret_val == EINVAL)
            printf("The alignment argument was not a power of two, or was not a multiple of sizeof(void *). \n");
         else if(ret_val == ENOMEM)
            printf("Insufficient Memory to fulfil the request. \n");
         exit(EXIT_FAILURE);
      }


      // Initialize Lock
      if (pthread_mutex_init(&lock, NULL) != 0){
         printf("Mutex Initialization failed!");
         exit(EXIT_FAILURE);
      }
      printf("LOCK_ADDR=%lx ", &lock);
      printf("counter_ADDR=%lx ", &counter);
      printf("counter=%lx ", counter);
      // Initialize counter
      *counter = 0;
   }

   else if (op_type == 3)
   {
      // Allocate the space for locks and counters arrays
      if( posix_memalign( (void**) &lock_array, 64, thread_count * sizeof(pthread_mutex_t*) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }
      if( posix_memalign( (void**) &counter_array, 64, thread_count * sizeof(unsigned*) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }

      // Now allocate locks
      for(int i=0; i < locks_count; i++){

         if( posix_memalign( (void**) &lock_array[i], 64, sizeof(pthread_mutex_t) ) != 0){
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
         }

         // Initialize Lock
         if (pthread_mutex_init(lock_array[i], NULL) != 0){
            printf("Mutex Initialization failed!");
            exit(EXIT_FAILURE);
         }
      }

      // Now allocate Counters
      for(int i=0; i < thread_count; i++){
         
         if( posix_memalign( (void**) &counter_array[i], 64, sizeof(unsigned) ) != 0){
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
         }

         // Initialize counter
         *( counter_array[i] ) = 0;
      }

      /* initialize random seed: */
      srand(time(NULL));
   }

   else if (op_type == 4 || op_type == 5 || op_type == 6 || op_type == 7 || op_type == 8 || op_type == 9|| op_type == 10)  // CAS based locks
   {
      // Allocate the space for locks and counters arrays
      if( posix_memalign( (void**) &lock_var_array, 64, locks_count * sizeof(int*) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }
      if( posix_memalign( (void**) &counter_array, 64, (thread_count+1) * sizeof(unsigned*) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }
      

      // Now allocate locks
      for(int i=0; i < locks_count; i++){

         if( posix_memalign( (void**) &lock_var_array[i], 64, sizeof(int) ) != 0){
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
         }

         // Initialize Lock
         *(lock_var_array[i]) = UNLOCK;
         if ( *(lock_var_array[i])  != UNLOCK ){
            printf("LOCK Initialization failed!");
            exit(EXIT_FAILURE);
         }
         printf("LOCK[%i]=%lx ", i, lock_var_array[i]);
      }
      printf("\n\n");

      // Now allocate Counters
      for(int i=0; i < thread_count+1; i++){
         
         if( posix_memalign( (void**) &counter_array[i], 64, sizeof(unsigned) ) != 0){
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
         }

         // Initialize counter
         *( counter_array[i] ) = 0;
      }

      /* initialize random seed: */
      srand(time(NULL));
   }

   else
   {
      printf("Invalid operation type(%d). \n", op_type);
      exit(EXIT_FAILURE);
   }


   // ---------------------------------------------------------
   // Global initializations
   //----------------------------------------------------------
   
   // Initialize Barrier
   pthread_barrier_init (&barrier, NULL, thread_count);
   printf("Initialized Barrier\n");


   // Allocate Thread data array
   ThreadData* thread_args[thread_count];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i] = new ThreadData(i, thread_count);
   }
   pthread_t thread_handles[thread_count];

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   // Enable performance and energy models
   CarbonEnableModels();

   // Create Threads
   for (int i = 1; i < thread_count; i++)
   {
      int ret = pthread_create(&thread_handles[i], NULL, threadMain, (void*) thread_args[i]);
      //printf("Created Thread %d\n", i);
      if (ret != 0)
      {
         printf("ERROR spawning thread %i\n", i);
         exit(EXIT_FAILURE);
      }
   }
   threadMain((void*) thread_args[0]);

   //printf("Threads Finished\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   //printf("Threads Joined\n");

   // Disable performance and energy models
   CarbonDisableModels();

/* ======================================================================================== */
/* END PARALLEL SECTION */
/* ======================================================================================== */
   
   // ---------------------------------------------------------
   // Output Summary
   //----------------------------------------------------------
   printf("Done.\n\n");

   if(op_type != 1){
      unsigned long total_tries =0;
      unsigned long total_failures =0;
      unsigned long total_type2_failures =0;
      unsigned long total_lcount =0;
      unsigned long total_lcountvalues =0;


      printf("Summary:\n");
      for (int i = 0; i < thread_count; i++)
      {
         ThreadData* data = thread_args[i];
         printf("Thread(%d): Total_Attempts(%lu) Type1 Failures(%lu) Type2 Failures(%lu), Average_Latency(%f), WosrtLatency(%lu)\n", 
               i, data->tries, data->failures, data->type2_failures, 
               (float) data->total_latency/data->tries, data->worst_latency );
         
         total_failures += data->failures;
         total_type2_failures += data->type2_failures;
         total_tries += data->tries;
         total_lcount += data->lcount;
         
      }
      
      for (int j = 0; j < locks_count; j++)
      {            
         printf("*l[%d]=%d\n", j, *lock_var_array[j]);
         total_lcountvalues +=  *lock_var_array[j];
      }


      printf("\nTotal Counter Val:\t %lu Count val in lock=%d\n", total_lcount, total_lcountvalues);
      printf("\nTotal Attempts:\t %lu \n", total_tries);
      printf("  Successful Attempts:\t %lu (%f/s)\n", total_tries - total_failures - total_type2_failures,
           (float)(total_tries - total_failures - total_type2_failures)*1000000000/max_duration );
      printf("  Failed Attempts:\t %lu \n", total_failures + total_type2_failures);
      printf("    Type1 Failures:\t %lu \n", total_failures);
      printf("    Type2 Failures:\t %lu \n", total_type2_failures);
      printf("\n\n");
      printf("Successful Ops = %lu \n", total_tries - total_failures - total_type2_failures );
      printf("Throughput (Ops/s) = %f \n", (float)(total_tries - total_failures - total_type2_failures)*1000000000/max_duration );
   }

   free(counter);

   return 0;
}


#define UINT64_C(val) (val##ULL)

uint64_t x; /* The state must be seeded with a nonzero value. */

uint64_t xorshift64(void) {
	x ^= x >> 12; // a
	x ^= x << 25; // b
	x ^= x >> 27; // c
	return x * UINT64_C(2685821657736338717);
}



// ---------------------------------------------------------
// THREAD FUNCTIONS
//----------------------------------------------------------

void* threadMain(void* data) {
    
    if( op_type == 0 ) // CAS Operation
    {
        thread_main_cas( (ThreadData*)data );
    }
    else if ( op_type == 1 )  // Fetch & Increment
    {
        thread_main_fetch( (ThreadData*) data );
    }
    else if ( op_type == 2 )  // Lock & Increment
    {
        thread_main_lock( (ThreadData*) data );
    }
    else if ( op_type == 3 )  // Multiple pthread Locks
    {
        thread_main_multi_locks( (ThreadData*) data );
    }
    else if ( op_type == 4 )  // Multiple CAS based locks
    {
        thread_main_multi_cas_locks( (ThreadData*) data );
    }
    else if ( op_type == 5 )  // Multiple CAS based locks with single lease at a time
    {
       thread_main_multi_cas_locks_simple_leases( (ThreadData*) data );
    }
    else if ( op_type == 6 )  // Single CAS based lock with single lease, variable Critical Section Time
    {
       thread_main_cas_lock_var_CS( (ThreadData*) data );
    }
    else if ( op_type == 7 ) // The snapshot test
    {
         thread_main_snapshot_value_based( (ThreadData*) data );
    }
    else if ( op_type == 8 )
    {
        thread_main_snapshot_set_based( (ThreadData *) data );
    }
    else if (op_type == 9)
    {
        thread_main_testdummy( (ThreadData *) data );
    }	
    else if (op_type == 10)
    {
        thread_main_test_counter( (ThreadData *) data );
    }	
    else
    {
        printf( "wrong op_type parameter!" );
        exit(EXIT_FAILURE);
    }
    return NULL;
}

	
unsigned thread_main_fetch ( ThreadData* data )
{
   uint64_t runtime = 0;
   uint64_t v1;
    
   //printf("Thread %d entered fetch\n", data->tid);
   pthread_barrier_wait (&barrier); 
    
   while (1)
   {
      //printf("running fetch1\n");
        
      v1 = __sync_fetch_and_add(  counter, 1);
      
      //printf("running fetch2\n");
        
      //We now add all the counter values
        
      //data->try_count.push_back( v1 );
      //printf("running fetch3\n");
        
      data->lcount++;
        
      if ( data->lcount >= MAX_COUNT) break;
     
      /*
      if(data->lcount % 10 == 0){
         printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
         fflush(stdout);
      }
      */
        
   }
    
   //printf("Thread %d returned\n", data->tid);
   pthread_barrier_wait (&barrier); 
       
   return runtime;
}


unsigned thread_main_cas ( ThreadData* data )
{
   int crt_value;
   bool success = 0;
   
   pthread_barrier_wait (&barrier); 
   //printf("Thread %d entered cas\n", data->tid);
   //fflush(stdout);

   while ( CarbonGetTime() < max_duration )
   {
      //crt_value = *((unsigned*) CarbonRequestLease((void*) counter, sizeof(unsigned), lease_time) );

      //CarbonRequestLease((void*) counter, sizeof(unsigned), lease_time);
      CarbonAddToWatchset((void*) counter, sizeof(unsigned));
      crt_value = *counter;
      data->tries++;    
      success = CarbonValidateWatchset();
      //success = CarbonRemoveFromWatchset((void*) counter, sizeof(unsigned));

      __sync_bool_compare_and_swap(counter, crt_value, crt_value+1 );

      //bool status = CarbonReleaseLease((void*) counter, sizeof(unsigned));
      
      /*
      if(status)
         printf("counters::thread_main_cas: Status = %s\n", (status)? "TRUE" : "FALSE");
      */


      if( success )
      {
         data->lcount++;
      }
      else
      {
         data->failures++;
      }

      //CarbonValidateWatchset();

      /*
      if(data->lcount % 100000 == 0){
         printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
         //fflush(stdout);
      }
      */
   }
    
   pthread_barrier_wait (&barrier); 
   
   return 0;
}


unsigned thread_main_lock ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   //printf("Thread %d entered cas\n", data->tid);
   //fflush(stdout);

   while ( CarbonGetTime() < max_duration )
   {
      // Calling LEASE on the Lock
      CarbonRequestLease((void*) &lock, sizeof(lock), lease_time);

      // Try to acquire the LOCK
      if( pthread_mutex_trylock(&lock) == 0){

         // Increment
         (*counter)++;
         
         // Release the Lock
         pthread_mutex_unlock(&lock);

         // RELEASE the Lease
         CarbonReleaseLease((void*) &lock, sizeof(lock));
         
         data->lcount++;
      }
      else{
         // RELEASE the Lease
         CarbonReleaseLease((void*) &lock, sizeof(lock));
         data->failures++;
         //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
      }
      
      data->tries++;    

      /*
      if(data->lcount % 100000 == 0){
         printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
         //fflush(stdout);
      }
      */
   }
    
   pthread_barrier_wait (&barrier); 
   
   return 0;
}


unsigned thread_main_multi_locks ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   //printf("Thread %d entered cas\n", data->tid);
   //fflush(stdout);

   int l1, l2;

   /*
   l1 = rand() % locks_count;
   l2 = rand() % locks_count;
   if(l1 == l2)
      l2 = (l1 + 1) % locks_count;
   */

   l1 = data->tid % locks_count;
   l2 = (l1+1) % locks_count;

   if(lock_array[l1] > lock_array[l2]){
      int temp = l1;
      l1 = l2;
      l2 = temp;
   }
   assert(lock_array[l1] < lock_array[l2]);
   
   unsigned long start_time, end_time;
   
   while (CarbonGetTime() < max_duration )
   {
      data->tries++;    
      
      // Hardcoding the Locks and Counter numbers for creating high contention 
      //l1 = 0;
      //l2 = 1;
      
      //printf("Thread(%d): l1(%d), l2(%d) \n", data->tid, l1, l2);

      //start_time = CarbonGetTime();
      // Calling LEASE on the Lock
      //CarbonRequestLease((void*) lock_array[l1], sizeof(*lock_array[l1]), lease_time);
      CarbonRequestGroupLease(lease_count, lease_time, lock_array[l1], lock_array[l2]);

      //printf("%lx ", lock_array[l1]);
      //assert(lock_array[l1]);

      // Try to acquire the LOCK
      if( pthread_mutex_trylock(lock_array[l1]) == 0){

         //printf("Thread(%d): Got l1(%d) \n", data->tid, l1);
         if( pthread_mutex_trylock(lock_array[l2]) == 0){
         
            //printf("Thread(%d): Got l2(%d) \n", data->tid, l2);

            // Increment Counters
            *( counter_array[data->tid] ) ++;
            *( counter_array[data->tid + 1] ) ++;
            
            // Release both the Locks
            pthread_mutex_unlock(lock_array[l1]);
            pthread_mutex_unlock(lock_array[l2]);

            // RELEASE the Lease
            CarbonReleaseAllLease();
            
            //end_time = CarbonGetTime();
            
            data->lcount++;
            if(data->lcount % 100000 == 0){
               printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
               fflush(stdout);
            }

            /*
            l1 = rand() % locks_count;
            l2 = rand() % locks_count;
            if(l1 == l2)
               l2 = (l1 + 1) % locks_count;
            */
         }
         else
         {
            // Release External Lock
            pthread_mutex_unlock(lock_array[l1]);

            // RELEASE the Lease
            CarbonReleaseAllLease();
            //end_time = CarbonGetTime();
            data->type2_failures++;
            //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
         }
      }
      else{
         //printf("Thread(%d): Lost l1(%d) \n", data->tid, l1);
         // RELEASE the Lease
         CarbonReleaseAllLease();
         //end_time = CarbonGetTime();
         data->failures++;
         //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
      }

      //data->total_latency += (end_time - start_time);
      //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   }

   // For the last iteration
   //data->total_latency += (end_time - start_time);
   //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
    
   pthread_barrier_wait (&barrier); 
   
   return 0;
}


unsigned thread_main_multi_cas_locks ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   
   //printf("Thread(%d) passed barrier \n", data->tid);
   uint64_t n1, n2, l1, l2;
   x = time(0);

		   


   n1 = xorshift64(  );
   l1 = n1 % locks_count;
   n2 = xorshift64( );
   l2 = n2 % locks_count;
   if(l1 == l2)
      l2 = (l1 + 1) % locks_count;
   
   /*
   l1 = data->tid % locks_count;
   l2 = (l1+1) % locks_count;
   */

   if(lock_var_array[l1] > lock_var_array[l2]){
      int temp = l1;
      l1 = l2;
      l2 = temp;
   }
   assert(lock_var_array[l1] < lock_var_array[l2]);

   unsigned long start_time, end_time;

   //start_time = CarbonGetTime();
   
   while ( CarbonGetTime() < max_duration )
   {
      data->tries++;
      
      // Calling LEASE on the Lock
      //CarbonRequestLease((void*) lock_var_array[l1], sizeof(*lock_var_array[l1]), lease_time);
      //#ifdef LEASES
      CarbonRequestGroupLease(lease_count, lease_time, lock_var_array[l1], lock_var_array[l2]);
      //#endif

      bool success = try_lock(lock_var_array[l1]);
      

      //printf("Thread(%d) Tries = %d \n", data->tid, data->tries);
      
      //start_time = CarbonGetTime();
      // Try to acquire the LOCK
      if( success == true ){

         //CarbonRequestLease((void*) lock_var_array[l1], sizeof(*lock_var_array[l1]), lease_time);
         //assert(*(lock_var_array[l1]) == LOCK );

         //printf("Thread(%d): Got l1(%d) \n", data->tid, l1);
         if( try_lock(lock_var_array[l2]) == true ){
         
            //printf("Thread(%d): Got l2(%d) \n", data->tid, l2);

            //printf("%lx %lx   ", lock_var_array[l1], lock_var_array[l2]);
            //printf("%i %i   ", l1, l2);
            
            // Increment Counters
            *( counter_array[data->tid] ) ++;
            //*( counter_array[data->tid + 1] ) ++;
            
            // Release both the Locks
            unlock(lock_var_array[l1]);
            unlock(lock_var_array[l2]);

            
            // RELEASE the Lease
	    //#ifdef LEASES
            CarbonReleaseAllLease();
            //#endif
            
            data->lcount++;
            /*
            if(data->lcount % 10000 == 0){
               printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
               fflush(stdout);
            }
            */

            // Next locks
            n1 = xorshift64( );
            l1 = n1 % locks_count;
            n2 = xorshift64( );
            l2 = n2 % locks_count;
            if(l2 == l2)
               l2 = (l1 + 1) % locks_count;
            
            if(lock_var_array[l1] > lock_var_array[l2]){
               int temp = l1;
               l1 = l2;
               l2 = temp;
            }
            //assert(lock_var_array[l1] < lock_var_array[l2]);
         
         }
         else
         {
            // Release External Lock
            unlock(lock_var_array[l1]);

            // RELEASE the Lease
	    //#ifdef LEASES
            CarbonReleaseAllLease();
	    //#endif
            data->type2_failures++;
            //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
         }
         
      }
      else{
         //printf("Thread(%d): Lost l1(%d) \n", data->tid, l1);
         // RELEASE the Lease
        //#ifdef LEASES 
	CarbonReleaseAllLease();
	//#endif
         data->failures++;
         //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
      }
      //end_time = CarbonGetTime();

      //data->total_latency += (end_time - start_time);
      //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   }

   // For Last iteration
   //data->total_latency += (end_time - start_time);
   //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
       
   pthread_barrier_wait (&barrier); 
   
   return 0;
}

unsigned thread_main_test_counter ( ThreadData* data )
{
   int write_val = 100;
   int read_val = 9;

   pthread_barrier_wait (&barrier); 

   x = time(0);

   int *values = data->snap;
   int *indices = data->indices;
   bool success = false;
   
   *lock_var_array[ 0 ] = 0; 
   printf("Thread %d:: snapsize %d:: lock[0]=%x: *lock[0]=%d \n", data->tid, snap_size, lock_var_array[ 0 ], *lock_var_array[ 0 ]);    
   //load the array to snapshot
   int j = 0;
   while (j < 1)
   {
      data->tries++;

      for( int i = 0; i < snap_size; i++ )
      {        
         while(true)
         {
            CarbonAddToWatchset((void*) lock_var_array[i], sizeof(int));

            unsigned int long res = CarbonValidateAndReadTemp((void*)lock_var_array[ i ], sizeof(int));
//            printf("tid=%d, res=%lu\n", data->tid, res);
            
            if (res == 0xffffffff)
            {
               //read failed due to watchset validation fail.
               CarbonRemoveFromWatchset((void*) lock_var_array[ (i) % locks_count ], sizeof(int));
               data->failures++;
               continue;
            }

            res +=1; //increment the counter val;

//            printf("tid=%d, write res=%lu\n", data->tid, res);
            bool write_status = CarbonValidateAndWrite((void*)lock_var_array[ (i) % locks_count ], (unsigned long int)res, sizeof(int));
            if (!write_status)
            {
               CarbonRemoveFromWatchset((void*) lock_var_array[ (i) % locks_count ], sizeof(int));
//               data->failures++;
               data->type2_failures;
               continue;
            }

            CarbonRemoveFromWatchset((void*) lock_var_array[ (i) % locks_count ], sizeof(int));
//            CarbonRemoveAllFromWatchset();
            data->lcount++;         
            break; //succeeded the op
         }
      }//for( int i = 0; i < snap_size; i++ )
      j++;
   }//   while (j < 100)

   pthread_barrier_wait (&barrier); 

   return 0;
}

unsigned thread_main_testdummy ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 

#if MSI_UNDERSTAND   
   
   head = (struct node*)malloc(sizeof(struct node));
   tail = (struct node*)malloc(sizeof(struct node));
   struct node* n = (struct node*)malloc(sizeof(struct node));
   
   head->next = tail;
   printf("head(%p), tail(%p) head->next(%p) n(%p)\n", head, tail, head->next, n);
   printf("head(%p), tail(%p) head->next(%p) &head->next(%p)\n", head, tail, head->next, &head->next);


   CarbonAddToWatchset((void*) head, sizeof(int));
   CarbonAddToWatchset((void*) head->next, sizeof(int));

   bool write_status = CarbonValidateAndWrite((void*)&head->next, (unsigned int long)n, sizeof(int));
   printf("wrote tid=%d, write res=%lu, head->next=%p\n", data->tid, write_status, head->next); 
   if(!write_status)
   {
      printf("write fail: tid=%d, continuing...\n", data->tid);
//      CarbonRemoveFromWatchset((void*) lock_var_array[ (data->tid + i) % locks_count ], sizeof(int));
//      data->failures++;
//      goto retry;
   }

//   CarbonRemoveFromWatchset((void*) head, sizeof(int));
//   CarbonRemoveFromWatchset((void*) head->next, sizeof(int));

#endif

   
   
//   CarbonAddToWatchset((void*) lock_var_array[0], sizeof(int));
//   CarbonAddToWatchset((void*) lock_var_array[1], sizeof(int));
   bool write_status = CarbonValidateWatchset();

   *lock_var_array[0] = 1;
   printf("tid=%d *lock_var_array[0]=%d\n", data->tid,  *lock_var_array[0]);
   
   write_status = CarbonValidateWatchset();

   //   unsigned int long read_counter = 1;
////   bool write_status = CarbonValidateAndWrite((void*)lock_var_array[0], (unsigned int long)1, sizeof(unsigned int));
//   bool write_status = CarbonValidateAndLock((void*)lock_var_array[0], (unsigned int long)1, sizeof(unsigned int));
//   printf("* lock_var_array[0]=%d\n", *lock_var_array[0]);
//   
//   CarbonRemoveFromWatchset((void*) lock_var_array[0], sizeof(int));
//   CarbonRemoveFromWatchset((void*) lock_var_array[1], sizeof(int));
   
   
//   CarbonAddToWatchset((void*) lock_var_array[0], sizeof(int));
//   
//   
//   bool write_status = CarbonValidateAndLock((void*)lock_var_array[0], 1, sizeof(int));
//   
//   CarbonRemoveFromWatchset((void*) lock_var_array[0], sizeof(int));

#if 0
   int j = 0;
   int k=0; //num succ ops
   while (CarbonGetTime() < max_duration)
   {
      
     for( int i = 0; i < snap_size; i++ ){ 
        retry: 
         ++j;
//        if (j > 2)
//           break;
         data->tries++;
         
         CarbonAddToWatchset((void*) lock_var_array[(data->tid + i) % locks_count], sizeof(int));

         unsigned int long read_counter = CarbonValidateAndReadTemp((void*)lock_var_array[ (data->tid + i) % locks_count ], sizeof(int));
//         printf("read tid=%d, step=%d, read_counter=(%lu), lock=(%p)\n", data->tid, j, read_counter, lock_var_array[ (data->tid + i) % locks_count ]); 
         
//         printf("read tid=%d read_counter=(%lu), lock=(%p, %d)\n", data->tid, read_counter, lock_var_array[ (data->tid + i) % locks_count ]
//                 , *lock_var_array[ (data->tid + i) % locks_count ]); 

         if (read_counter == 0xffffffff)
         {
            //read failed due to watchset validation fail.
            CarbonRemoveFromWatchset((void*) lock_var_array[ (data->tid + i) % locks_count ], sizeof(int));
            data->type2_failures++;
//            printf("read fail: tid=%d, step=%d, read_counter=%lu, lock=%p\n", data->tid, j, read_counter, lock_var_array[ (data->tid + i) % locks_count ]); 
            goto retry;
         }

         read_counter++;      
         bool write_status = CarbonValidateAndWrite((void*)lock_var_array[ (data->tid + i) % locks_count ], read_counter, sizeof(int));
//         printf("wrote tid=%d, step=%d, write res=%lu, lock=%p\n", data->tid, j, write_status, lock_var_array[ (data->tid + i) % locks_count ]); 

         if(!write_status)
         {
//            printf("write fail: tid=%d, continuing...\n", data->tid);
            CarbonRemoveFromWatchset((void*) lock_var_array[ (data->tid + i) % locks_count ], sizeof(int));
            data->failures++;
            goto retry;
         }
         else
         {
            data->lcount++;
//            printf("tid=%d, step=%d, write res=%lu, wrote=%d, lock=%p lock=%d\n", data->tid, j, write_status, read_counter,
//                    lock_var_array[ (data->tid + i) % locks_count ], *lock_var_array[ (data->tid + i) % locks_count ]); 
         }


         CarbonRemoveFromWatchset((void*) lock_var_array[(data->tid + i) % locks_count], sizeof(int));
     }
      ++k;
   }
   
#endif

    pthread_barrier_wait (&barrier); 

   return 0;
}


unsigned thread_main_snapshot_set_based ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   
   uint64_t n1, n2, l1, l2, dir;
   x = time(0);
   
   unsigned reader = 1;
   unsigned opcount = 0;
 
   int *values = data->snap;
   int *indices = data->indices;
   

   //int *values = (int *) malloc( data->snap_size * sizeof(int) );
   
   while ( CarbonGetTime() < max_duration )
   {		
      opcount++;
      if (opcount % 4 != 0) 
      {
         //I'm a READER
         data->tries++;

         //printf("Thread %d: ", data->tid);
         //load the array to snapshot
         for( int i = 0; i < snap_size; i++ )
         {
            CarbonAddToWatchset((void*) lock_var_array[ (data->tid + i) % locks_count ], sizeof(int));
         }
         //printf("\n");

         bool success = false;
         success = CarbonValidateWatchset();

         if( success == false )
         {
            //failed the snapshot
            data->failures++;
            printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
         }
      }
      else
      {
              //I'm a writer:

              for( int i = 0; i < 1; i++ ) 
              {
                      int index = (data->failures * 7 + data->tid * 17 + i * 13) % locks_count;
                      __sync_fetch_and_add( lock_var_array[index], 1 );
              }
              //membarstoreload();
      }
         
      //end_time = CarbonGetTime();

      //data->total_latency += (end_time - start_time);
      //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   }

   // For Last iteration
   //data->total_latency += (end_time - start_time);
   //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   printf ("LOCK ADDR: %p, %p", lock_var_array[ (data->tid + 0)], lock_var_array[ (data->tid + 1)]);
   pthread_barrier_wait (&barrier); 
   
   return 0;
}

unsigned thread_main_snapshot_value_based ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   
   uint64_t n1, n2, l1, l2, dir;
   x = time(0);

	unsigned opcount = 0;

	int *values = data->snap;
	int *indices = data->indices;

   printf("snap_size= %d, locks_count= %d \n", snap_size, locks_count );
   while ( CarbonGetTime() < max_duration )
   {
   
   	opcount++;
//                printf("tid-%d, opcount %d \n", data->tid, opcount );
		if ( opcount % 4 != 0 ) 
		{
			//I'm a READER
		   data->tries++;
			
			for( int i = 0; i < snap_size; i++ )
			{
				values[i] = *(lock_var_array[ ( data->tid + i) % locks_count ] );
//				printf("(values[%d]=%d) ", i, values[i] );
			}
//			printf("\n");
			
			bool success = true;
			
			//check the two versions
//			printf("Thread %d: ", data->tid);
			for( int i = 0; i < snap_size; i++ )
			{
//				printf("(%d) ",  *(lock_var_array[( (i) % locks_count )] ) );
				if( values[i] != *(lock_var_array[ ( data->tid + i ) % locks_count ] ) )
				{
					success = false;
                                        break;
				}
			}
			
//			printf ("\n");
		
			
			
			//bool success = ( (*(lock_var_array[l1]) == val1) && (*(lock_var_array[l2]) == val2) ) ;
		
		   //printf("Thread(%d) Tries = %d \n", data->tid, data->tries);
		   
		  
		  if (success == false)
		  {
		  		//failed the snapshot
				data->failures++;
                                printf("Thread(%d), value(%d, %d)  LOCK FAILURE ********* \n", data->tid, *(lock_var_array[( data->tid+0)] ), *(lock_var_array[( data->tid+1)]));
		  }
//                  else
//                  {
//                      printf("Thread(%d), Count(%d)  LOCK SUCCESS ********* \n", data->tid, data->lcount);
//                  }
		}
		else
		{
			//I'm a writer:
			
			for( int i = 0; i < 1; i++ ) {
				int index = (data->failures * 7 + data->tid * 17 + i * 13) % locks_count;
			
				__sync_fetch_and_add( lock_var_array[index], 1 );
				
			}
			
			
		}
         
      //end_time = CarbonGetTime();

      //data->total_latency += (end_time - start_time);
      //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   }

   // For Last iteration
   //data->total_latency += (end_time - start_time);
   //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
       
   //free( values );
       
   pthread_barrier_wait (&barrier); 
   
   return 0;
}



unsigned thread_main_multi_cas_locks_simple_leases ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   
   //printf("Thread(%d) passed barrier \n", data->tid);
   uint64_t n1, n2, l1, l2;
   x = time(0);

   n1 = xorshift64(  );
   l1 = n1 % locks_count;
   n2 = xorshift64( );
   l2 = n2 % locks_count;
   if(l1 == l2)
      l2 = (l1 + 1) % locks_count;
   
   if(lock_var_array[l1] > lock_var_array[l2]){
      int temp = l1;
      l1 = l2;
      l2 = temp;
   }
   assert(lock_var_array[l1] < lock_var_array[l2]);

   unsigned long start_time, end_time;

   //start_time = CarbonGetTime();
   
   while ( CarbonGetTime() < max_duration )
   {
      data->tries++;
      
      // Calling LEASE on the First Lock
      CarbonRequestLease((void*) lock_var_array[l1], sizeof(*lock_var_array[l1]), lease_time+100);
      //#ifdef LEASES
      //CarbonRequestGroupLease(lease_count, lease_time, lock_var_array[l1], lock_var_array[l2]);
      //#endif

      bool success = try_lock(lock_var_array[l1]);
      

      //printf("Thread(%d) Tries = %d \n", data->tid, data->tries);
      
      //start_time = CarbonGetTime();
      // Try to acquire the LOCK
      if( success == true ){

         // Calling LEASE on the First Lock
         CarbonRequestLease((void*) lock_var_array[l2], sizeof(*lock_var_array[l2]), lease_time );

         if( try_lock(lock_var_array[l2]) == true ){
         
            //printf("Thread(%d): Got l2(%d) \n", data->tid, l2);

            //printf("%lx %lx   ", lock_var_array[l1], lock_var_array[l2]);
            //printf("%i %i   ", l1, l2);
            
            // Increment Counters
            *( counter_array[data->tid] ) ++;
            //*( counter_array[data->tid + 1] ) ++;
            
            // Release both the Locks
            unlock(lock_var_array[l1]);
            unlock(lock_var_array[l2]);

            
            // RELEASE the Lease
	    //#ifdef LEASES
            CarbonReleaseAllLease();
            //#endif
            
            data->lcount++;
            /*
            if(data->lcount % 10000 == 0){
               printf("Thread(%d) Count = %d \n", data->tid, data->lcount);
               fflush(stdout);
            }
            */

            // Next locks
            n1 = xorshift64( );
            l1 = n1 % locks_count;
            n2 = xorshift64( );
            l2 = n2 % locks_count;
            if(l2 == l2)
               l2 = (l1 + 1) % locks_count;
            
            if(lock_var_array[l1] > lock_var_array[l2]){
               int temp = l1;
               l1 = l2;
               l2 = temp;
            }
            //assert(lock_var_array[l1] < lock_var_array[l2]);
         
         }
         else
         {
            // Release External Lock
            unlock(lock_var_array[l1]);

            // RELEASE the Lease
	    //#ifdef LEASES
            CarbonReleaseAllLease();
	    //#endif
            data->type2_failures++;
            //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
         }
         
      }
      else{
         //printf("Thread(%d): Lost l1(%d) \n", data->tid, l1);
         // RELEASE the Lease
        //#ifdef LEASES 
	CarbonReleaseAllLease();
	//#endif
         data->failures++;
         //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
      }
      //end_time = CarbonGetTime();

      //data->total_latency += (end_time - start_time);
      //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
   }

   // For Last iteration
   //data->total_latency += (end_time - start_time);
   //data->worst_latency = (data->worst_latency < (end_time - start_time))? (end_time - start_time) : data->worst_latency ;
       
   pthread_barrier_wait (&barrier); 
   
   return 0;
}



unsigned thread_main_cas_lock_var_CS ( ThreadData* data )
{
   pthread_barrier_wait (&barrier); 
   
   //printf("Thread(%d) passed barrier \n", data->tid);
   uint64_t n1, n2, l1, l2;
   x = time(0);

   n1 = xorshift64(  );
   l1 = n1 % locks_count;
   
   unsigned long start_time, end_time;
   unsigned long CS_time = 5000;

   
   while ( CarbonGetTime() < max_duration )
   {
      data->tries++;
      
      // Calling LEASE on the First Lock
      CarbonRequestLease((void*) lock_var_array[l1], sizeof(*lock_var_array[l1]), lease_time);

      // Try to acquire the LOCK
      if( try_lock(lock_var_array[l1]) == true ){
         
         start_time = CarbonGetTime();
         while(CarbonGetTime() - start_time < CS_time );
            
         // Release the Locks
         unlock(lock_var_array[l1]);
            
         // RELEASE the Lease
         CarbonReleaseAllLease();
            
         data->lcount++;
            
         // Next locks
         n1 = xorshift64( );
         l1 = n1 % locks_count;
         
      }
      else
      {
         //printf("Thread(%d): Lost l1(%d) \n", data->tid, l1);
         // RELEASE the Lease
         CarbonReleaseAllLease();
         data->failures++;
         //printf("Thread(%d), Count(%d)  LOCK FAILURE ********* \n", data->tid, data->lcount);
      }

   }

   pthread_barrier_wait (&barrier); 
   
   return 0;
}






