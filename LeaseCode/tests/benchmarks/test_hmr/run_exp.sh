#!/bin/bash

#run this to get table data on terminal and create step files in exp_output.
# example usage: ./run_exp.sh
echo "started: " `date`
iter=1
out_dir="exp_output"
rm -fr ${out_dir}/*.*
echo cleaned ${out_dir}/

cols="\n%20s %10s %10s %10s %10s %10s %10s\n"
printf "$cols" alg threads tput global_counter tot_ops tot_failed walltime

div_valid(){ case $1 in 0) false;;esac; }

thread_arr=(1 6 12 18 24 30 36 42 48 54 60)
algo_arr=(asmvinc globalvinc) 
#(globalread globalwrite globalvread globalvwrite globalcrbread globalcrbwrite) #(localread localwrite localinc localincfaa globalinc globalincfaa localvread localvwrite localvinc globalvinc)

for n in "${thread_arr[@]}"
do
    for alg in "${algo_arr[@]}"
    do
        for ((i=1; i<=$iter; i++))
        do
            tput=0
            fname=$out_dir/${alg}_n${n}_step${i}.txt;
            `/usr/bin/time -f "time_cmd_walltime=%e\nmax_res_mem=%M" -o $out_dir/temp_time make THREADS=$n ALG=$alg &> $fname`
            cat $out_dir/temp_time >> $fname
            tput=`cat $fname | grep Experiment_Throughput| cut -d"=" -f2| tr -d " "`
            i_g_counter=`cat $fname | grep final_global_counter| cut -d"=" -f2| tr -d " "`
            i_tot_ops=`cat $fname | grep "Total Exp Ops"| cut -d"=" -f2| tr -d " "`
            i_tot_failops=`cat $fname | grep "Total failed Ops"| cut -d"=" -f2| tr -d " "`
            i_walltime=`cat $fname | grep time_cmd_walltime| cut -d"=" -f2| tr -d " "`
            i_walltime=${i_walltime%.*}

            printf "%20s %10d %10d %10d %10d %10d %10d\n" "$alg" "$n" "$tput" "$i_g_counter" "$i_tot_ops" "$i_tot_failops" "$i_walltime"
            
            # copy sim output from results dir to local exp_output folder.
            cp ../../../results/latest/sim.out $out_dir/
            mv $out_dir/sim.out $out_dir/sim-${alg}-${n}-step${i}.out
        done
    done
    echo " --------------- "
done

echo "finished: " `date`
