#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <sys/time.h>

#include <assert.h>
#include "carbon_user.h"
#include "util.h"

#include<unistd.h>

volatile int* global_counter ;

extern "C" void foo(int x, int y)
{

}


//
//void foo2(int x)
//{
//}
//repnz prefix isRePne (xacquire with some instr cmov)
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP  (xrealease with some instr like cmov cmpxchg)
#define ASF_F3\
   ".byte 0xF3\n\t"

//banch not taken prefix
#define ASF_2E\
   ".byte 0x2E\n\t"

//banch taken prefix
#define ASF_3E\
   ".byte 0x3E\n\t"

//operand size override prefix
#define ASF_66\
   ".byte 0x66\n\t"

//address size override prefix
#define ASF_67\
   ".byte 0x67\n\t"

#define cmpxchg( ptr, _old, _new, fail_label ) { \
  volatile uint32_t *__ptr = (volatile uint32_t *)(ptr);   \
  asm goto(ASF_F2 "lock; cmpxchg %1,%0 \t\n"           \
    "jnz %l[" #fail_label "] \t\n"               \
    : /* empty */                                \
    : "m" (*__ptr), "r" (_new), "a" (_old)       \
    : "memory", "cc"                             \
    : fail_label );                              \
}

struct node_asmhmr{
    unsigned int key;
    bool mark;
    volatile unsigned int lock;
    struct node_asmhmr * next;
};//__attribute__((aligned(PADDING_BYTES)));
struct node_asmhmr * head;
struct node_asmhmr * tail;

void initNode(struct node_asmhmr * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    n->lock = 0; 
}

int main()
{
    *global_counter = 9;
   printf("global_counter=%p %u\n", global_counter, global_counter); 
   printf("&global_counter=%x\n", &global_counter); 
   printf("*global_counter=%lu\n", *global_counter); 
    
    if(posix_memalign( (void**)&head, 64, sizeof(struct node_asmhmr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, 0);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_asmhmr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }

    initNode(tail, 1000);
    head->next = tail;
    printf ("head=%p \n", head);    
    printf ("tail=%p \n", tail);    
    printf("node size=%lu \n", sizeof(node_asmhmr));
    struct node_asmhmr* dummypred;
    
    struct node_asmhmr* pred = head; 
    struct node_asmhmr* curr;
    printf ("pred=%lu \n", pred); 
    printf ("&pred->next=%lu \n", &pred->next);    
    printf ("pred->next=%lu \n", pred->next);    
    fflush(stdout);
    
    CarbonEnableModels();
//    printf("addtag"); fflush(stdout);
//    priprintf("addtag"); fflush(stdout);ntf("addtag"); fflush(stdout);
//    CarbonRead((void*)&pred->next, sizeof(struct node_asmhmr*));
//    CarbonWrite((void*)global_counter, 109, sizeof(unsigned int));
//    CarbonAddToWatchset((void*) pred, sizeof(struct node_asmhmr));
//    
//    CarbonRemoveFromWatchsetOpt((void*) pred);        

// remove from watchset enables with USEASMADDTAG in graphite    
//    __asm__ __volatile__ (ASF_66 ASF_F2 "mov %1, %0"
//        :"=r" (dummypred)
//        : "m" (*pred)
//        ); 

//curr = pred->next;

    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr)
            : "m" (pred->next)
            ); 
    
//    CarbonValidateAndReadTemp((void*)&pred->next, sizeof(struct node_asmhmr*));
//    __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//            :"=r" (curr)
//            : "m" (pred->next)
//            ); 
//        printf("sizeof(node_asmhmr *)=%lu sizeof(unsigned int)=%lu", sizeof(node_asmhmr *), sizeof(unsigned int));
    
//    CarbonRemoveAllFromWatchset();

    CarbonDisableModels();
    printf("*global_counter=%lu\n", *global_counter); 
   return 0;
}

#if 0
int main(int argc, char *argv[])
{
   int ret_val = posix_memalign( (void**) &global_counter, 64, sizeof(int));
   if(ret_val != 0){ 
      printf("Error: Could not allocate Counter!!! \n");
   }
////   printf ("\n\n");
   *global_counter = 9;
   printf("global_counter=%p %u\n", global_counter, global_counter); 
   printf("&global_counter=%x\n", &global_counter); 
   printf("*global_counter=%lu\n", *global_counter); 
//
   int i = 0;

   CarbonEnableModels();

//   CarbonRead((void*)&global_counter, sizeof(unsigned int));

//   while (++i < 2)
//   {
//      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));
   
//   __sync_bool_compare_and_swap(global_counter, 9, 1);
//   __asm__ __volatile__ (ASF_F2 "movl %1, %%eax; movl %%eax, %0;"
//           :"=r" (b)
//           :"r" (a)
//           :"%eax"
//            );

//   *global_counter = i;
//   gcntr = i;
//   __asm__ __volatile__ (ASF_F2 "movl %1, %0"
//           :"=r" (i)
//           : "r" (global_counter)
//            );
//   CarbonAddToWatchset((void*)global_counter, sizeof(int));
   
////DUmmy read for addtag set and removefrom Tag
//   __asm__ __volatile__ (ASF_F3 "mov %1, %0"
//           :"=r" (i)
//           : "m" (*global_counter)
//            );       


//DUmmy write for remove from tagset
//   __asm__ __volatile__ (ASF_F3 "mov %1, %0"
//           :"=m" (*global_counter)
//           : "r" (i)
//            );
  
//read
//   __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//           :"=r" (i)
//           : "m" (*global_counter)
//            );   

//    printf("gonna read sizeof(int)=%d...\n\n", i);
//
//   CarbonRemoveAllFromWatchset();
    
//write
//   __asm__ __volatile__ (ASF_F2 "mov %1, %0"
//           :"=m" (*global_counter)
//           : "r" (i)
//            );

   {   
//   int vnew = 1;
//   int vold = 9;
//   printf("before cmpxchg op: global_counter=%p *global_counter=%lu, old(%d) new(%d) \n", global_counter, *global_counter, vold, vnew); 
//   cmpxchg(global_counter, vold, vnew, retry);
//
//retry:
//   printf("after cmpxchg *global_counter=%lu, old(%d) new(%d) \n", *global_counter, vold, vnew); 
      }
  
//asm goto(ASF_F3 "lock; cmpxchg %1, %0 \t\n" \
//        "jz %l[fair_label] \t\n" \
//        :                              \
//        : "m" (*global_counter), "r" (_new), "a" (_old) \
//        : "memory", "cc"                                 \
//        : fair_label
//        )   ;
   
  
//        CarbonRemoveAllFromWatchset();
   
//    asm(
//        "movq %[arg1], %%rdi\n\t"
//        "movq %[arg2], %%rsi\n\t"
//        "call *%[callee]"
//        :
//        :[arg1]"r"((u_int64_t)(dest)),
//         [arg2]"r"((u_int64_t)(a)),
//         [callee]"r"(callee_test3)
//        :"cc"
//    );

//    CarbonWrite((void*)global_counter, 109, sizeof(unsigned int));
   
//   CarbonRead((void*)&global_counter, sizeof(unsigned int));
//   __sync_fetch_and_add(global_counter, 1);
//   CarbonRead1((void*)&global_counter, sizeof(unsigned int));
//   }
//   printf ("\n");

   CarbonDisableModels();
   
//   printf("gunna sleep in main end !!!!!!\n");
//   printf ("i=%d, gc=%d\n", i, gcntr);
//
//   printf("global_counter=%p %u\n", global_counter, global_counter); 
//   printf("&global_counter=%x\n", &global_counter); 
   printf("*global_counter=%lu\n", *global_counter); 

   return 0;
}
#endif

//void callee_test3(char* dest, int y)
//{
//    printf("%s %d\n", dest, y);
//};
//
//    char* dest = "test";
//    int y = 4;
//
//
//int main()
//{
//
//   int ret_val = posix_memalign( (void**) &global_counter, 64, sizeof(int));
//   if(ret_val != 0){ 
//      printf("Error: Could not allocate Counter!!! \n");
//   }
//
//   CarbonEnableModels();
//
////    __asm__(
////        ASF_F2 "mov %[arg1], %%rdi\n\t"
////        ASF_F2 "mov %[arg2], %%rsi\n\t"
////        "call *%[callee]"
////        :
////        :[arg1]"r"((u_int64_t)(&global_counter)),
////         [arg2]"r"((u_int64_t)(sizeof(unsigned int))),
////         [callee]"r"(CarbonRead)
////        :"cc"
////    );
//   
//   CarbonRead((void*)&global_counter, sizeof(unsigned int));
//   CarbonDisableModels();
//
//    return 0;
//}
