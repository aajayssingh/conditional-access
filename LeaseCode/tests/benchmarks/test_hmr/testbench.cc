#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <sys/time.h>

#include <assert.h>
#include "carbon_user.h"
#include "util.h"

#define DEBUG_PRINT if (0)

//#define LAZYLIST_MIMICK
//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define MAX_OPS                     10000

/****************************************
 * Globals: NOTE::using a struct for globals to pad causes reads to be weirdly slow.
 ****************************************/
// barrier synchronization object
//PAD;
pthread_barrier_t   barrier; 
//PAD;
volatile unsigned int* global_counter;
PAD;
std::atomic<unsigned int> gatomiccntr;
//PAD;
int thread_count;
//PAD;
char* algo_type;
//PAD;
volatile uint64_t op_based_exp_begtime = 0;
//PAD;
volatile uint64_t op_based_exp_endtime = 0;
//PAD;

class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_)
      : tid(tid_), num_threads(num_threads_)
   {
      if( posix_memalign( (void**) &local_counter, 64, sizeof(unsigned int) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }
      *local_counter = 0;
      num_writefails = 0;
      num_readfails = 0;
//      printf("*local_counter=%u\n", *local_counter); 
//      printf("&local_counter=%x\n", &local_counter); 
//      printf("local_counter=%x\n\n", local_counter); 
   }

   ~ThreadData()
   {}

//   PAD;
   int tid;
   int num_threads;
//   PAD; //padded each element while allocating
   unsigned long ops;
   PAD;
   unsigned long num_readfails;
   unsigned long num_writefails;
//   PAD;
   volatile unsigned int* local_counter;
//   PAD;
}__attribute__((aligned(PADDING_BYTES)));

// return time diff  in msecs
long int diff_time(struct timeval start, struct timeval end)
{
   return ((end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0) + 0.5; //milli sec
}

/****************************************
 * Function prototypes
 ****************************************/
void* threadMain(void* arg);



/****************************************
 * Function definitions
 ****************************************/

void* localreadworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 

      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

void* localwriteworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 

      *(d->local_counter) = MAX_OPS;
//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));

      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

void* localincworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
      //      printf("*local_counter=%u\n", *(d->local_counter)); 
      //      printf("&local_counter=%x\n", &(d->local_counter)); 
      //      printf("local_counter=%x\n\n", (d->local_counter)); 
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *d->local_counter;
      }
#endif

      *d->local_counter = *d->local_counter + 1;
//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *d->local_counter);

      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* localincfaaworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
      //      printf("*local_counter=%u\n", *(d->local_counter)); 
      //      printf("&local_counter=%x\n", &(d->local_counter)); 
      //      printf("local_counter=%x\n\n", (d->local_counter)); 
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *d->local_counter;
      }
#endif
      //      *d->local_counter = *d->local_counter + 1;
      __sync_fetch_and_add( d->local_counter, 1 );

//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *d->local_counter);

      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* globalincworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *global_counter;
      }      
#endif
      
      *global_counter = *global_counter + 1;
//      unsigned int loc_val = *(global_counter);
//      printf("tid=%d loc_val=%u, *(global_counter)=%u\n", loc_val, *global_counter);
      
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* globalincfaaworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *global_counter;
      }  
#endif
      
      __sync_fetch_and_add( global_counter, 1 );
//      unsigned int loc_val = *(global_counter);
//      printf("tid=%d loc_val=%u, *(global_counter)=%u\n", loc_val, *global_counter);
      
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* globalincatomicfaaworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *global_counter;
      }  
#endif
//      unsigned int ret_val = gatomiccntr.load(std::memory_order_acquire);
      gatomiccntr.fetch_add(1, std::memory_order_acq_rel);
//      unsigned int loc_val = *(global_counter);
//      printf("tid=%d loc_val=%u, *(global_counter)=%u\n", loc_val, *global_counter);
      
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* globalinccasworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 25){
         unsigned int vv = *global_counter;
      }  
#endif
      unsigned int read_val = *global_counter;
      bool status = __sync_bool_compare_and_swap(global_counter, read_val, read_val+1);
      if (!status)
      {
         d->num_readfails++;
         continue;
      }
     
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}


void* localvreadworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
      if (ret_val == 0xffffffff)
      { 
          CarbonRemoveAllFromWatchset();
          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_readfails++;
          continue;
//          goto retry; 
      }

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* localvreadnoretryworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* localvwriteworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

//      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
//      if (ret_val == 0xffffffff)
      bool write_status = CarbonValidateAndWrite((void*)d->local_counter, 109, sizeof(unsigned int));
      if (!write_status)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_writefails++;
          continue;
//          goto retry; 
      }

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* localvwritenoretryworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

//      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
//      if (ret_val == 0xffffffff)
      bool write_status = CarbonValidateAndWrite((void*)d->local_counter, 109, sizeof(unsigned int));

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* localcrbreadworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

      unsigned int long ret_val = CarbonRead((void*)d->local_counter, sizeof(unsigned int));

//      unsigned int loc_val = *(d->local_counter);
      CarbonRemoveAllFromWatchset();

      //      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

void* localcrbwriteworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

      bool write_status = CarbonWrite((void*)d->local_counter, 109, sizeof(unsigned int));
      
//      *(d->local_counter) = MAX_OPS;
      CarbonRemoveAllFromWatchset();
      
//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));

      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}


void* localvincworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 20){
         CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));

         unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
         if (ret_val == 0xffffffff)
         { 
             CarbonRemoveAllFromWatchset();
   //          assert (0 && "local read failed");
   //          printf("read failed\n");
             d->num_readfails++;
             continue;
   //          goto retry; 
         }
         CarbonRemoveAllFromWatchset();
      } 
#endif      
      
      //      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)d->local_counter, sizeof(unsigned int));
      
      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
      if (ret_val == 0xffffffff)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_readfails++;
          continue;
//          goto retry; 
      }
      ret_val += 1;
      bool write_status = CarbonValidateAndWrite((void*)d->local_counter, ret_val, sizeof(unsigned int));
      if (!write_status)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_writefails++;
          continue;
//          goto retry; 
      }

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;      
}

void* globalvincworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
#ifdef LAZYLIST_MIMICK
      int x = 0;
      while(++x < 20){
         CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));

         unsigned int long ret_val = CarbonValidateAndReadTemp((void*)global_counter, sizeof(unsigned int));
         if (ret_val == 0xffffffff)
         { 
             CarbonRemoveAllFromWatchset();
   //          assert (0 && "local read failed");
   //          printf("read failed\n");
             d->num_readfails++;
             continue;
   //          goto retry; 
         }
         CarbonRemoveAllFromWatchset();
      }  
#endif
      
      //      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));
      
      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)global_counter, sizeof(unsigned int));
      if (ret_val == 0xffffffff)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_readfails++;
          continue;
//          goto retry; 
      }
      ret_val += 1;
      bool write_status = CarbonValidateAndWrite((void*)global_counter, ret_val, sizeof(unsigned int));
      if (!write_status)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_writefails++;
          continue;
//          goto retry; 
      }

//      if(d->ops%500 == 0) printf("tid=%d d->ops=%u\n", tid, d->ops);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;      
}

void* globalvreadworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));

      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)global_counter, sizeof(unsigned int));
      if (ret_val == 0xffffffff)
      { 
          CarbonRemoveAllFromWatchset();
          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_readfails++;
          continue;
//          goto retry; 
      }

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* globalvwriteworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      unsigned int loc_val = *(d->local_counter);
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));

//      unsigned int long ret_val = CarbonValidateAndReadTemp((void*)d->local_counter, sizeof(unsigned int));
//      if (ret_val == 0xffffffff)
      bool write_status = CarbonValidateAndWrite((void*)global_counter, 109, sizeof(unsigned int));
      if (!write_status)
      { 
          CarbonRemoveAllFromWatchset();
//          assert (0 && "local read failed");
//          printf("read failed\n");
          d->num_writefails++;
          continue;
//          goto retry; 
      }

//      printf("tid=%d *(d->local_counter)=%lu loc_val=%u\n", ret_val, loc_val);
      CarbonRemoveAllFromWatchset();
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;   
}

void* globalreadworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 

      unsigned int loc_val = *(global_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;

}

void* globalwriteworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 

      *(global_counter) = MAX_OPS;
//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));

      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}


void* globalcrbreadworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 

//      unsigned int loc_val = *(global_counter);
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));

      unsigned int long ret_val = CarbonRead((void*)global_counter, sizeof(unsigned int));
      
      CarbonRemoveAllFromWatchset();

      //      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;

}

void* globalcrbwriteworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
//      printf("*local_counter=%u\n", *(d->local_counter)); 
//      printf("&local_counter=%x\n", &(d->local_counter)); 
//      printf("local_counter=%x\n\n", (d->local_counter)); 
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));

      bool write_status = CarbonWrite((void*)global_counter, 109, sizeof(unsigned int));

      CarbonRemoveAllFromWatchset();

//      *(global_counter) = MAX_OPS;
//      unsigned int loc_val = *(d->local_counter);
//      printf("tid=%d loc_val=%u, *(d->local_counter)=%u\n", loc_val, *(d->local_counter));

      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"
//issue1: Not sure whether MOV with repz or repnz is well defined.
//         what if the mov instruction doesn't execute incase of reads if ZF is already in adverse state
//         Also for writes if mov instr repeats its execution then it could cause double updates?
void* asmvincworker(void* data) 
{
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;
   unsigned int step = 1;
   unsigned int i=0;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
      CarbonAddToWatchset((void*)global_counter, sizeof(unsigned int));
      
      //read i = (*global_counter)
      __asm__ __volatile__ (ASF_F2 "mov %1, %0"
              :"=r" (i)
              : "m" (*global_counter)
               );   

      if (i == 0xffffffff)
      {
         d->num_readfails++;
//         printf("tid=%lu vread fail\n", tid);
         CarbonRemoveAllFromWatchset();
         continue;
      }
      
      i++;
      
      //write (*global_counter) = i lesson from atomic lib of harris CAS impl
      __asm__ __volatile__ (ASF_F2 "mov %1, %0"
              :"=m" (*global_counter)
              : "r" (i)
               );      
      int res = CarbonGetVOPStatus();
      if (res == 0)
      {
         d->num_writefails++;
//         printf("tid=%lu vwrite fail\n", tid);
         CarbonRemoveAllFromWatchset();
         continue;
      }
      
      CarbonRemoveAllFromWatchset();

      d->ops++;
      step++;
   }
   
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

int main(int argc, char *argv[])
{
    thread_count      = DEFAULT_THREAD_COUNT;
    
    // ---------------------------------------------------------
    // Parameters extraction
    //----------------------------------------------------------
    if(argc > 2)
    {
       thread_count   = atoi(argv[1]);
       algo_type      = argv[2];

       printf("\nMINIBENCH:\n");
       printf(" Threads=%i\n", thread_count); 
       printf(" algo_type=%s\n", algo_type); 
    }
    else 
    {
       printf("\nMINIBENCH: wrong arguments.\n");
       exit(EXIT_FAILURE);
    }
   
   
    // Initialize Barrier
    pthread_barrier_init (&barrier, NULL, thread_count);
    printf("Initialized Barrier\n");

   if( posix_memalign( (void**) &global_counter, 64, sizeof(unsigned int) ) != 0){
      printf("Error: posix_memalign could not allocate memory!!! \n");
      exit(EXIT_FAILURE);
   }
    
   // Allocate Thread data array
   ThreadData* thread_args[thread_count*PADDING_BYTES];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i*PADDING_BYTES] = new ThreadData(i, thread_count);
   }
   pthread_t thread_handles[thread_count];
   
   *global_counter = 0;
   printf("*global_counter=%u\n", *global_counter); 
   printf("&global_counter=%x\n", &global_counter); 
   printf("global_counter=%x\n\n", global_counter); 
   

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   CarbonEnableModels();
  
   // Create Threads
   if (strcmp(algo_type, "localread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }
   }
   else if(strcmp(algo_type, "localwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }
   }
   else if (strcmp(algo_type, "localinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "localincfaa") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localincfaaworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "globalinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "globalincfaa") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalincfaaworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }

   else if (strcmp(algo_type, "globalincatomicfaa") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalincatomicfaaworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   
   else if (strcmp(algo_type, "globalinccas") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalinccasworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "localvread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localvreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "localvwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localvwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }

   else if (strcmp(algo_type, "localvreadnoretry") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localvreadnoretryworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "localvwritenoretry") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localvwritenoretryworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   
   else if (strcmp(algo_type, "localvinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localvincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "globalvinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalvincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
//   else if (strcmp(algo_type, "globalcasinc") == 0)
//   {
//      for (int i = 1; i < thread_count; i++)
//      {
//         int ret = pthread_create(&thread_handles[i], NULL, globalcasincworker, (void*) thread_args[i*PADDING_BYTES]);
//         //printf("Created Thread %d\n", i);
//         if (ret != 0)
//         {
//            printf("ERROR spawning thread %i\n", i);
//            exit(EXIT_FAILURE);
//         }
//      }   
//   }   

   else if (strcmp(algo_type, "globalvread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalvreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "globalvwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalvwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "globalread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "globalwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }      
   else if (strcmp(algo_type, "globalcrbread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalcrbreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "globalcrbwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalcrbwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }      
   else if (strcmp(algo_type, "localcrbread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localcrbreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }   
   else if (strcmp(algo_type, "localcrbwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localcrbwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }      
   else if (strcmp(algo_type, "asmvinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
//         printf("Goin to create Thread %d\n\n", i);
         int ret = pthread_create(&thread_handles[i], NULL, asmvincworker, (void*) thread_args[i*PADDING_BYTES]);
//         printf("Created Thread %d\n\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }      
   

   else
   {
       printf("\nMINIBENCH: wrong algo type!\n");
       exit(EXIT_FAILURE);
   }


   printf("Threads Started ...\n");


   if (strcmp(algo_type, "localread") == 0)
   {
      localreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if(strcmp(algo_type, "localwrite") == 0)
   {
      localwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localinc") == 0)
   {
      localincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localincfaa") == 0)
   {
      localincfaaworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "globalinc") == 0)
   {
      globalincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   
   else if (strcmp(algo_type, "globalincatomicfaa") == 0)
   {
      globalincatomicfaaworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "globalincfaa") == 0)
   {
      globalincfaaworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "globalinccas") == 0)
   {
      globalinccasworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }

   else if (strcmp(algo_type, "localvread") == 0)   
   {
      localvreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localvwrite") == 0)
   {
      localvwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localvreadnoretry") == 0)   
   {
      localvreadnoretryworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localvwritenoretry") == 0)
   {
      localvwritenoretryworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localvinc") == 0)
   {
      localvincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "globalvinc") == 0)
   {
      globalvincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
//   else if (strcmp(algo_type, "globalcasinc") == 0)
//   {
//      globalcasincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
//   }   

   else if (strcmp(algo_type, "globalvread") == 0)
   {
      globalvreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }   
   else if (strcmp(algo_type, "globalvwrite") == 0)
   {
      globalvwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }   
   else if (strcmp(algo_type, "globalread") == 0)
   {
      globalreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }   
   else if (strcmp(algo_type, "globalwrite") == 0)
   {
      globalwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   } 
   else if (strcmp(algo_type, "globalcrbread") == 0)
   {
      globalcrbreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }   
   else if (strcmp(algo_type, "globalcrbwrite") == 0)
   {
      globalcrbwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localcrbread") == 0)
   {
      localcrbreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }   
   else if (strcmp(algo_type, "localcrbwrite") == 0)
   {
      localcrbwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "asmvinc") == 0)
   {
      asmvincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else
   {
       printf("\nMINIBENCH: wrong algo type!\n");
       exit(EXIT_FAILURE);
   }

   printf("Threads Finished ...\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   printf("Threads Joined\n");

   CarbonDisableModels(); 
   
   unsigned long total_tries = 0;
   unsigned long total_fails = 0;
   unsigned long total_readfails = 0;
   unsigned long total_writefails = 0;
   for (int i = 0; i < thread_count; i++)
   {
      ThreadData* data = thread_args[i*PADDING_BYTES];
      total_tries += data->ops;
      total_readfails += data->num_readfails ;
      total_writefails += data->num_writefails;
      total_fails += data->num_readfails + data->num_writefails;
      unsigned int v = *(data->local_counter);
      printf("tid=%d *local_counter=%u v=%u \n", i, *(data->local_counter), v);
   }
   printf("Total Exp Ops=%lu \n", total_tries);
   printf("Total read failed Ops=%lu \n", total_readfails);
   printf("Total write failed Ops=%lu \n", total_writefails);
   printf("Total failed Ops=%lu \n", total_fails);
   printf("final_global_counter=%u \n",( (strcmp(algo_type, "globalincatomicfaa") == 0) ? gatomiccntr.load() : *global_counter) );
   printf("Experiment_Throughput (Ops/s)=%llu \n", (total_tries)*1000000000/(op_based_exp_endtime - op_based_exp_begtime) );
   
   for (int i = 0; i < thread_count; i++)
   {
      delete thread_args[i*PADDING_BYTES];
   }
   return 0;
}
