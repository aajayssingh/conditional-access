#!/bin/bash
####
# the script is to create tabular data from the sim output to analyse the simlator stats
# usage ./simout_to_csv.sh > myfile.csv  after running run_exp.sh that creates the needed input files. Make sure to delete old files in exp_output folder.
####
out_dir="./exp_output"


count=0
for stepfile in `ls $out_dir/sim*`
do
    #get algo name from file name
    algo_name=`echo $stepfile | cut -d"/" -f3 | cut -d"-" -f2`
    tot_walltime=`cat $stepfile | grep "Shutdown Time" | tr -s ' '| cut -d" " -f5`

    head_row=("algo,") #header for csv
    stat_row=("$algo_name,") #number content of the row which has per thread stat


##    Get all | separated lines
    output=`cat $stepfile  | grep -a "Core Summary" -A200`
    while IFS= read -r line
    do
        #get comma separated line
        csv_line=`echo "${line}" | sed 's/,/ /g' | sed 's/|/,/g'`
#        echo csvline::"${csv_line}"
        echo "${csv_line}" >> ${out_dir}/raw_${algo_name}.csv
        title=`echo "${csv_line}" | cut -d"," -f1`
        stats=`echo "${csv_line}" | cut -d"," -f2-`
        editstat=$(echo $stats  | awk 'gsub(/,$/,x)') #remove last ,
#        echo $title stats::$stats
#        echo $title editstat::$editstat
        avg_stat=$(echo $editstat | awk -F',' '{s=0; for (i=1;i<=NF;i++)s+=$i; print s/NF;}')
#        echo title: "${title}"
#        echo AVG:: "${avg_stat}"
        head_row+=("${title},")
        stat_row+=("$avg_stat,")
    done <<< "${output}"

#    echo ${head_row[*]}
#    echo ${stat_row[*]}


if ((count==0))
then
    echo ${head_row[*]}
fi
    echo ${stat_row[*]}
count=$count+1
done