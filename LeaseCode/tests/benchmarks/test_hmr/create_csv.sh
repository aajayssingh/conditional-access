#!/bin/bash
#run this to output of run_exp.sh into csv format.
# example usage: ./create_csv.sh >> myfile.csv


out_dir="./exp_output"
echo "algo,threads,throughput,global_counter,tot_ops,walltime,max_res_mem, read_fail, write_fail, total_fail"
for stepfile in `ls $out_dir/*.txt`
do
alg=`cat $stepfile | grep algo_type | cut -d"=" -f2| tr -d " "`
threads=`cat $stepfile | grep Threads | head -1 |cut -d"=" -f2| tr -d " "`
throughput=`cat $stepfile | grep Experiment_Throughput | cut -d"=" -f2| tr -d " "`
g_counter=`cat $stepfile | grep final_global_counter | cut -d"=" -f2| tr -d " "`
tot_ops=`cat $stepfile | grep "Total Exp Ops" | cut -d"=" -f2| tr -d " "`
walltime=`cat $stepfile | grep time_cmd_walltime | cut -d"=" -f2| tr -d " "`
walltime=${walltime%.*}
max_res_mem=`cat $stepfile | grep max_res_mem | cut -d"=" -f2| tr -d " "`
read_fail=`cat $stepfile | grep "read failed" | cut -d"=" -f2| tr -d " "`
write_fail=`cat $stepfile | grep "write failed" | cut -d"=" -f2| tr -d " "`
total_fail=`cat $stepfile | grep "Total failed" | cut -d"=" -f2| tr -d " "`
#finished=`cat $stepfile | grep "key_checksum validation:PASS"| head -1`
##echo $finished
#if [[ "$finished" == "key_checksum validation:PASS" ]]; then
#    succ=true
#else
#    succ=false
#fi

echo $alg,$((threads)),$((throughput)),$((g_counter)),$((tot_ops)),$((walltime)),$((max_res_mem)),$((read_fail)),$((write_fail)),$((total_fail))
done
