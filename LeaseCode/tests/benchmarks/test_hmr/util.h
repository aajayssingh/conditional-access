#ifndef UTIL_H
#define UTIL_H
/*
 * Helper code shamelessly copied from Trevor's setbench.
 */
#pragma once

#include <iostream>
#include <atomic>
using namespace std;

#define ASM __asm__ __volatile__
#define membarstoreload() { ASM ("mfence;") ; }
#define CPU_RELAX asm volatile("pause\n": : :"memory");

#define PADDING_BYTES 64
#define MAX_THREADS 64

#define CAT2(x,y) x##y
#define CAT(x,y) CAT2(x,y)
#define PAD volatile char CAT(___padding, __COUNTER__)[PADDING_BYTES]

//for IBR based begin
#define EMPTY_FREQ 10  // every 10 retire
#define EPOCH_FREQ 15 // every 50 alloc
//for IBR based end

//for HP begin
typedef bool CallbackReturn;
typedef void* CallbackArg;
typedef CallbackReturn (*CallbackType)(CallbackArg);
//for HP end

class PaddedRandom{
private:
    volatile char padding[PADDING_BYTES-sizeof(unsigned int)];
    unsigned int seed;
public:
    PaddedRandom()
    {
        this->seed = 0;
    }
    
    PaddedRandom(int seed)
    {
        this->seed = seed;
    }
    
    void setSeed(int seed)
    {
        this->seed = seed;
    }
    
    unsigned int nextNatural(int n){
        seed ^= seed << 6;
        seed ^= seed >> 21;
        seed ^= seed << 7;
        int retVal = (int)(seed % n);
        
        return (retVal < 0 ? -retVal: retVal);
    }
    
}__attribute__((aligned(PADDING_BYTES)));

class DebugCounter{
private:
    struct paddedVLL{
        volatile long long v;
        volatile char padding[PADDING_BYTES-sizeof(long long)];
    };
    paddedVLL data[MAX_THREADS+1];
public:
    void add (const unsigned long int tid, const long long val){
        data[tid].v += val;
    }
    
    void inc (const unsigned long int tid)
    {
        add(tid, 1);
    }
    
    long long get (const unsigned long int tid)
    {
        return data[tid].v;
    }
    
    long long getTotal ()
    {
        long sum = 0;
        for (unsigned long int tid=0; tid < MAX_THREADS; ++tid ){
            sum += get(tid);
        }
        
        return sum;
    }
    
    void clear()
    {
        for (unsigned long int tid=0; tid < MAX_THREADS; ++tid ){
            data[tid].v = 0;
        }
    }
    
    DebugCounter()
    {
        clear();
    }
}__attribute__((aligned(PADDING_BYTES)));

template <typename T>
class ArrayList{
private:
    PAD;
    int __size;
    T **data;
public:
    const int capacity;
    PAD;
    ArrayList(const int _capacity):capacity(_capacity)
    {
        __size = 0;
        data = new T*[capacity]; //TODO not padded.
    }
    
    ~ArrayList()
    {
        delete[] data;
    }
    
    inline T* get(const int ix)
    {
        return data[ix];
    }
    
    inline int size()
    {
        return __size;
    }
    
    inline void add(T* const obj)
    {
        assert (__size < capacity);
        data[__size++] = obj;
    }

    inline void erase(const int ix)
    {
        assert (ix >= 0 && ix < __size);
        data[ix] = data[--__size];
    }
    
    inline void erase(T * const obj)
    {
        int ix = getIndex(obj);
        if (ix != -1) erase(ix);
    }
    
    inline int getIndex(T * const obj) {
        for (int i=0;i<__size;++i) {
            if (data[i] == obj) return i;
        }
        return -1;
    }
    
    inline bool contains(T * const obj) {
        return (getIndex(obj) != -1);
    }
    
    inline void clear() {
        __size = 0;
    }
    
    inline bool isFull() {
        return __size == capacity;
    }
    
    inline bool isEmpty() {
        return __size == 0;
    }
};


template<typename T>
class AtomicArrayList{
private:
    PAD;
    std::atomic_int __size;
    std::atomic_uintptr_t *data;
public:
    const int capacity;
    AtomicArrayList(const int _capacity) : capacity(_capacity){
        __size.store(0, memory_order_relaxed);
        data = new atomic_uintptr_t[capacity];        
    }
    
    ~AtomicArrayList(){
        delete[] data;
    }
    
    inline T* get(const int ix){
        return (T*) data[ix].load(memory_order_relaxed);
    }
    
    inline int size(){
        return __size.load(memory_order_relaxed);
    }
    
    inline void add(T* const obj){
        int sz = __size.load(memory_order_relaxed);
//        printf("AtomicArrayList:add:: size=%d, capacity=%d\n", sz, capacity);
        assert (sz < capacity);
        membarstoreload();
        data[sz].store((uintptr_t)obj, memory_order_relaxed);
        membarstoreload();
        __size.store(sz+1, memory_order_relaxed);
    }
    
    inline void erase(const int ix){
        int sz = __size.load(memory_order_relaxed);
        assert(ix >= 0 && ix < sz);
        if (ix != sz-1) // to handle the decreasing size in loop in hp retire? 
        {
//            printf("ix(%d) != sz-1(%d)\n", ix, sz-1);
            data[ix].store(data[sz-1].load(memory_order_relaxed), memory_order_relaxed);
        }
        __size.store(sz-1, memory_order_relaxed);
    }
    
    inline void erase(T * const obj) {
        int ix = getIndex(obj);
//        printf("ix(%d)\n", ix);

        if (ix != -1) erase(ix);
    }
    
    inline int getIndex(T * const obj) {
        int sz = __size.load(memory_order_relaxed);
        for (int i=0;i<sz;++i) {
            if (data[i].load(memory_order_relaxed) == (uintptr_t) obj) return i;
        }
        return -1;
    }
    
    inline bool contains(T * const obj) {
        return (getIndex(obj) != -1);
    }
    
    inline void clear() {
        membarstoreload();
        __size.store(0, memory_order_relaxed);
        membarstoreload();
    }
    
    inline bool isFull() {
        return __size.load(memory_order_relaxed) == capacity;
    }
    
    inline bool isEmpty() {
        return __size.load(memory_order_relaxed) == 0;
    }
    
    inline void printDebug() {
        int sz = __size.load(memory_order_relaxed);
        for (int i=0;i<sz;++i) {
            printf("data[%d]=%p ", i, data[i].load(memory_order_relaxed)) ;
        }
        
        printf("\n");
    }    
};


//debug counters
DebugCounter total_freed;
DebugCounter num_allocs;
DebugCounter num_retired;
        
#endif //UTIL_H