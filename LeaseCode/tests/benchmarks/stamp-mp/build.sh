#!/bin/bash

PROGS="intruder"
TARGETS="seq tl2"

for t in ${TARGETS}
do
    for p in ${PROGS}
    do
    	make -C ${p} -f  Makefile TARGET=${t} $*
	if [ $? -ne 0 ]; then
	    exit -1
	fi
    done     	
done
