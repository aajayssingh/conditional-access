#ifndef _SIMPLEPQ_
#define _SIMPLEPQ_
#include <stdint.h>
#include <queue>
#include <malloc.h>
#include "random.h"
#include <limits.h>
#include <time.h>
#include <assert.h>

#ifndef MY_QUEUE
#define MY_QUEUE
#endif

#define LEASE_TIME 100000

#ifdef MY_QUEUE
#include "boost/heap/d_ary_heap.hpp"
#include "boost/heap/priority_queue.hpp"
#include "boost/pool/pool_alloc.hpp"
#endif
#include "sfmt.h"

#include "carbon_user.h"

using namespace std;

uint64_t RDTSCP(){ 	
	uint64_t result = 0;
#ifdef _MSC_VER
#if _MSC_VER>=1600
	unsigned int Aux;
	result = __rdtscp(&Aux);
#endif
#else
	uint32_t high=0, low=0;
	asm volatile (
			"rdtscp\n\t"
			"mov %%edx,%0\n\t"
			"mov %%eax,%1\n\t":
			"=r"(high),"=r"(low)::"%rax","%rcx","%rdx");
	result = low+(uint64_t(high)<<32ULL);
#endif
	return result;
}
class MultiPQ
{
	private:
		struct node{
			int key;
			int value;
			node& operator=(const node &n)
			{
				key = n.key;
				value = n.value;
				return *this;
			}
		}__attribute__((aligned(64)));
#ifdef MY_QUEUE
		typedef boost::fast_pool_allocator<node> BoostIntAllocator;
		typedef boost::singleton_pool<boost::fast_pool_allocator_tag, 1024*1024> BoostIntAllocatorPool;
#endif
		struct compare
		{
			bool operator()(const node &n1, const node &n2) const
			{
				return n1.key > n2.key;
			}
		};
#ifdef MY_QUEUE
		typedef boost::heap::d_ary_heap<
			node,
			boost::heap::arity<8>, boost::heap::compare<compare>, BoostIntAllocatorPool > p_queue;
#else
		typedef std::priority_queue<node,std::vector<node>,compare> p_queue;
#endif

		struct queues {
			p_queue *queue;
			unsigned int long bucket_top;
			char x[64-sizeof(p_queue*)-sizeof(unsigned int long)];
		}__attribute__((aligned(64)));
		queues *simpleQueue ;
		int nb_threads;
//		unsigned int long *bucket_top;
		int m;
		struct padLock{
			volatile bool flag;
			char x[64-sizeof(bool)];
		}__attribute__((aligned(64)));
		volatile padLock *lock;

	public:
		MultiPQ(int n=1, int c=1);
		~MultiPQ();
		
		int insert(int key, int val, CRandomSFMT *rng, int d=1);
		int insert_v2(int key, int val, CRandomSFMT *rng, int d=1);
		int deleteMin( CRandomSFMT *rng, int *bucket=NULL);
		int deleteMin_v2( CRandomSFMT *rng, int *bucket=NULL);
		int getNbOfBuckets(){return m;}
		void displayInfo();

};
MultiPQ::MultiPQ(int n, int c): nb_threads(n) {
	//m = c*nb_threads;
	m = c;
	simpleQueue = (queues*)malloc((m+1)*sizeof(queues));
	lock = (padLock*)malloc((m+1)*sizeof(padLock));
	for(int i=0;i<m+1;i++){
		simpleQueue[i].queue = new p_queue();
		node NIL;
		NIL.key = INT_MAX;
		simpleQueue[i].queue->push(NIL);
		simpleQueue[i].bucket_top = INT_MAX;
		lock[i].flag=false;
      printf("LOCK[%d].flag(%lx) ", i, &(lock[i]) );
	}
   printf("\n");
};
MultiPQ::~MultiPQ(){
	free((void*)lock);
	for(int i=0;i<m+1;i++)
		delete simpleQueue[i].queue;
	free(simpleQueue);

#ifdef MY_QUEUE
	BoostIntAllocatorPool::purge_memory();
#endif
};
int MultiPQ::insert(int key,int val, CRandomSFMT *rng, int d){

	int i = rng->IRandomX(0,m-1);
	
   	CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
	while(!__sync_bool_compare_and_swap(&(lock[i].flag),false,true)){
      		CarbonReleaseAllLease();
		i = rng->IRandomX(0,m-1);
		
   		CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
	}
   //assert(lock[i].flag);

	node newNode;
	newNode.key = key;
	newNode.value = val;
	simpleQueue[i].queue->emplace(newNode); 
	simpleQueue[i].bucket_top = simpleQueue[i].queue->top().key;

	lock[i].flag=false;
   	CarbonReleaseAllLease();
	return i;
};
int MultiPQ::insert_v2(int key,int val, CRandomSFMT *rng, int d){

	/*
	int i = rng->IRandomX(0,m-1);
		
   	CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
	while(!__sync_bool_compare_and_swap(&(lock[i].flag),false,true)){
      		CarbonReleaseAllLease();
		i = rng->IRandomX(0,m-1);
		
   		CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
	}
   //assert(lock[i].flag);
	*/
//simpleQueue[i].queue->emplace(newNode); 
	//simpleQueue[i].bucket_top = simpleQueue[i].queue->top().key;

	//lock[i].flag=false;
   	//CarbonReleaseAllLease();
	//return i;
	struct compare comp;
	int i, k;
   	int temp;
	node newNode;
	newNode.key = key;
	newNode.value = val;
		

	while(1){
		i = rng->IRandomX(0,m-1);
		k = rng->IRandomX(0,m-1);
      		if(i==k)
         		k = (i+1) % m;

      //printf("Lock Addresses: %lx %lx \n", &(lock[i].flag), &(lock[k].flag) );

      		CarbonRequestGroupLease(2, LEASE_TIME, &(lock[i].flag), &(lock[k].flag));
      //assert( (lock[i].flag == true) || (lock[i].flag == false) );
      		int dummy = lock[i].flag;

		if( __sync_bool_compare_and_swap(&(lock[i].flag),false,true) ) {
         		assert(lock[i].flag);
         
         		if( __sync_bool_compare_and_swap(&(lock[k].flag),false,true) ){
            			assert(lock[k].flag);

            //uint32_t start_time = CarbonGetTime();

            			/* Choose the queue with the less elements */
				
            			if(simpleQueue[k].queue->size() < simpleQueue[i].queue->size())
				{
               				temp = i;
               				i = k;
               				k=temp;
            			} 
				
				lock[k].flag = false;
				//CarbonReleaseLease((void*) &(lock[k].flag), sizeof(lock[k].flag));
				CarbonReleaseAllLease();

				//put new element into the smaller queue
				simpleQueue[i].queue->emplace(newNode); 
				simpleQueue[i].bucket_top = simpleQueue[i].queue->top().key;
				
				//unlock
				lock[i].flag = false;
							
				//CarbonReleaseAllLease();
				

			        return i;
         		}
         		else {   // Did not acquire SECOND lock
            		// Release the first lock
            			lock[i].flag=false;
            			CarbonReleaseAllLease();
				//printf("Type 2 failure\n");
         		}
      		}
      		else {   // Did not acquire FIRST lock
         		CarbonReleaseAllLease();
			//printf("Type 1 failure\n");
      		}
	}

};

int MultiPQ::deleteMin(CRandomSFMT *rng, int *bucket){
	struct compare comp;
	int i, k;
	while(1){
		i = rng->IRandomX(0,m-1);
		k = rng->IRandomX(0,m-1);
		int temp;
		/* Choose the one with the smaller min */
		if(simpleQueue[i].bucket_top>simpleQueue[k].bucket_top){
			temp = i;
			i = k;
			k=temp;
		}

      		CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
		while((!__sync_bool_compare_and_swap(&(lock[i].flag),false,true))){
         		CarbonReleaseAllLease();
			i = rng->IRandomX(0,m-1);
			k = rng->IRandomX(0,m-1);
			if(simpleQueue[i].bucket_top>simpleQueue[k].bucket_top){
				temp = i;
				i = k;
				k=temp;
			}
      			CarbonRequestLease((void*) &(lock[i].flag), sizeof(lock[i].flag), LEASE_TIME);
		}
		assert(lock[i].flag);
		
		node dNode = simpleQueue[i].queue->top();
		if(dNode.key == INT_MAX){ // if queue is empty we unlock it and retry.
			lock[i].flag=false;
         CarbonReleaseAllLease();
			continue;
		}
		else 
			simpleQueue[i].queue->pop(); 
		simpleQueue[i].bucket_top = simpleQueue[i].queue->top().key;
		//unlock
		lock[i].flag=false;
      CarbonReleaseAllLease();
		if(bucket) // for debug
			*bucket = i;
		return dNode.key;
	}
	return -1;
}

/*
deleteMin version with both randomly chosen queues locked 
before comparing their mins: Better quality but less throughput 
*/
int MultiPQ::deleteMin_v2(CRandomSFMT *rng, int *bucket){
	struct compare comp;
	int i, k;
   int temp;

	while(1){
		i = rng->IRandomX(0,m-1);
		k = rng->IRandomX(0,m-1);
      		if(i==k)
         		k = (i+1) % m;

      //printf("Lock Addresses: %lx %lx \n", &(lock[i].flag), &(lock[k].flag) );

      CarbonRequestGroupLease(2, LEASE_TIME, &(lock[i].flag), &(lock[k].flag));
      //assert( (lock[i].flag == true) || (lock[i].flag == false) );
      int dummy = lock[i].flag;

		if( __sync_bool_compare_and_swap(&(lock[i].flag),false,true) ) {
         		assert(lock[i].flag);
         
         		if( __sync_bool_compare_and_swap(&(lock[k].flag),false,true) ){
            			assert(lock[k].flag);

            //uint32_t start_time = CarbonGetTime();

            /* Choose the one with the smaller min */
            			if(comp(simpleQueue[i].queue->top(),simpleQueue[k].queue->top()))
				{
               				temp = i;
               				i = k;
               				k=temp;
            			}
				lock[k].flag = false;
				//CarbonReleaseLease((void*) &(lock[k].flag), sizeof(lock[k].flag));

				CarbonReleaseAllLease();

            			/* Unlock the one with the greater min */
            			node dNode = simpleQueue[i].queue->top();
            			if(dNode.key == INT_MAX)
	    			{ 
				// if queue is empty we unlock it and retry. */
               				lock[i].flag=false;
					
            				//lock[k].flag=false;
            				//CarbonReleaseAllLease();           
               				//CarbonReleaseLease((void*) &(lock[i].flag), sizeof(lock[i].flag));
               //CarbonReleaseAllLease();
               				continue;
           			}
            			else 
               				simpleQueue[i].queue->pop(); 
            
            //unlock
            				lock[i].flag=false;
					
            				//lock[k].flag=false;
            			//	CarbonReleaseAllLease();
            
            //CarbonReleaseAllLease();
            				//irbonReleaseLease((void*) &(lock[i].flag), sizeof(lock[i].flag));
            
            				if(bucket) // for debug
               					*bucket = i;
            
            //printf("%u \t", CarbonGetTime() - start_time);
            				return dNode.key;
					
         		}
         		else {   // Did not acquire SECOND lock
            		// Release the first lock
            			lock[i].flag=false;
            			CarbonReleaseAllLease();
				//printf("Type 2 failure\n");
         		}
      		}
      		else {   // Did not acquire FIRST lock
         		CarbonReleaseAllLease();
			//printf("Type 1 failure\n");
      		}
	}

	return -1;
}


void MultiPQ::displayInfo(){
	unsigned long int total =0;
	for(int i=0;i<m;i++){
		cout << "#["<<i<<"] SIZE "<< simpleQueue[i].queue->size()-m;
		if(!simpleQueue[i].queue->empty())
			cout <<" TOP "<< simpleQueue[i].queue->top().key << endl;
		total +=simpleQueue[i].queue->size();
	}
	cout << "#TOTAL SIZE " << total << endl;

}

#endif
