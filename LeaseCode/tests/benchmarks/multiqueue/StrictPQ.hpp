#ifndef _STRICTPQ_
#define _STRICTPQ_
#include <stdint.h>
#include <queue>
#include <set>
#include <malloc.h>

class strictPQ
{
	private:
		struct node{
			int key;
			int value;
			int id;
			node& operator=(const node &n)
			{
				key = n.key;
				value = n.value;
				return *this;
			}
		}__attribute__((aligned(64)));
		struct compare
		{
			bool operator()(const node &n1, const node &n2) const
			{
				return n1.key < n2.key; 
			}
		};
		typedef std::multiset<node,compare> p_queue;
		p_queue* queue;

	public:
		strictPQ();
		~strictPQ();
		int insert(int key, int val, int id);
		int remove(int key);
		size_t size();
		void displayInfo();

};
strictPQ::strictPQ() {
	queue = new p_queue();
};
strictPQ::~strictPQ(){
	queue->clear();
	delete queue;
};
int strictPQ::insert(int key, int val, int id){

	node newNode;
	newNode.key = key;
	newNode.value = val;
	newNode.id = id;
	queue->insert(newNode); 
	return 1;
};
int strictPQ::remove(int key){
	unsigned int rank = -1;
	for(p_queue::iterator it = queue->begin();
			it != queue->end(); it++){
		node myKey = ((node)(*it));
		rank++;
		if(myKey.key == key){
			cout << myKey.id << ",";
			queue->erase(it);
			break;
		}
	}
	return rank;
};

size_t strictPQ::size(){
	return queue->size();
}

void strictPQ::displayInfo(){

}

#endif

