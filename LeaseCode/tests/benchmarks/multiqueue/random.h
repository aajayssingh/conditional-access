#ifndef _RANDOM_
#define _RANDOM_
#include <stdint.h>


	static inline uint64_t
getticks(void)
{
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}
	static inline unsigned long
xorshf96(unsigned long* x, unsigned long* y, unsigned long* z)  //period 2^96-1
{         
	unsigned long t;
	(*x) ^= (*x) << 16;
	(*x) ^= (*x) >> 5;
	(*x) ^= (*x) << 1;

	t = *x; 
	(*x) = *y;
	(*y) = *z;
	(*z) = t ^ (*x) ^ (*y);

	return *z;  
} 

	void
seed_rand(unsigned long seeds[3])  
{   
	// unsigned long seeds[3];
	seeds[0] = getticks() % 123456789;
	seeds[1] = getticks() % 362436069;
	seeds[2] = getticks() % 521288629;
	// return seeds;
}
#endif // _RANDOM_
