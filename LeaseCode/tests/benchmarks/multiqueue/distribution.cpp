#include <string.h>
#include <iostream>
#include <cstdlib>
#include <fstream>

//time mesure
#include <sys/time.h> 
#include <stdio.h> 
#include <unistd.h>
#include <sys/resource.h>

#include "MultiPQ.hpp"
#include "StrictPQ.hpp"
#include "random.h"


#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <pthread.h>
#include <list>
#include <string>
#include "sfmt.h"



using namespace std;


static int alarm_time=0;

static void alarm_handler( int arg)
{
	alarm_time = 1;
}

long int diff_time(struct timeval start, struct timeval end){
	return ((end.tv_sec  - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec)/1000.0) + 0.5; 
}
typedef int keyType;
MultiPQ *my_queue;
strictPQ *strict_queue;
int debug = false;
int stack = false;
int nb_threads=1;
int nb_queues=2;
bool print=false;
bool monotone=false;
unsigned int iter=1000;
unsigned long duration=10;
unsigned long int sampleSize = 1000000;
static struct timeval start_time, end_time;
pthread_barrier_t barrier;


struct padVar{
	CRandomSFMT *rng;   
	char x[64-sizeof(CRandomSFMT*)];
} __attribute__ ((aligned(64))); 
struct padLongVar{
	unsigned long int val;   
	char x[64-sizeof(long int)];
} __attribute__ ((aligned(64))); 

padLongVar ops[200]; // max number of threads 
padVar seeds[200]; // max number of threads 
padLongVar keySeeds[200]; // max number of threads 


void setAffinity(int id){
	int affi = id;
	cpu_set_t aff;
	CPU_ZERO(&aff);
	CPU_SET(affi, &aff);
	pthread_setaffinity_np(pthread_self(),sizeof(aff),&aff);
}  
/* Print stack info */
void print_threadInfo(int id){
	pthread_attr_t attr;
	pthread_getattr_np(pthread_self(), &attr);
	size_t   *stackaddr, stacksize;
	pthread_attr_getstack(&attr,(void**) &stackaddr,&stacksize);
	cout << " THREAD  "  <<  id << std::dec << " stack address " << std::hex << stackaddr  <<  "  stack size "  << stacksize << std::dec << endl;
}


struct log{
	int id;
	unsigned long int timestamp;
	int op;
	int key;
	int bucket;
	log& operator=(const log &n)
	{
		timestamp = n.timestamp;
		op = n.op;
		key = n.key;
		id = n.id;
		return *this;
	}
	string print(){
		return std::to_string(id)+","+std::to_string(timestamp)+","+std::to_string(key)+","+std::to_string(bucket)+","+std::to_string(op);
	}
};
struct compareLogs
{
	bool operator()(const struct log &n1, const struct log &n2) const
	{
		return n1.timestamp < n2.timestamp;
	}
};

std::list<struct log> local_log[200];



static inline long rand_range_re(long r, int id)
{      
	long v = xorshf96(&(keySeeds[id].val), &(keySeeds[id].val) + 1, &(keySeeds[id].val) + 2) % r;
	v++; 
	return v;
};

void* run_thread(void * arg){
	if(stack)
		print_threadInfo(*((int*)(arg)));
	int id =*((int*)(arg));
	setAffinity(id);


	keyType key;
	keyType prev_key = 0;
	long unsigned int last = 0;
	bool results = false;
	ops[id].val=0;

	//barrier.wait(nb_threads, 0);
	pthread_barrier_wait(&barrier);
	if ( id == 0 )
	{
		(void)signal(SIGALRM, &alarm_handler);
		(void)alarm(duration);
		gettimeofday(&start_time, NULL);
		cout << "# init done " << endl;
		if(debug)
			asm(
					"int3"
			   );
	} 
	//barrier.wait(nb_threads, 1);
	pthread_barrier_wait(&barrier);
	for(int j=0;j<iter;j++){
		struct log thread_log;
		int bucket;
		int rank;



		if(!monotone){
			key = rand_range_re(100000000, id);
		}else
			key = prev_key + rand_range_re(100, id);

		bucket = my_queue->insert((int)key,(int)key,(seeds[id].rng));
		strict_queue->insert((int)key,(int)key, id);
		/*
		thread_log.timestamp=RDTSCP();
		thread_log.op=1;
		thread_log.key=key;
		thread_log.id=id;
		thread_log.bucket=bucket;
		local_log[id].push_back(thread_log);
		ops[id].val+=1;
		*/
//		usleep(10000);

		prev_key = my_queue->deleteMin((seeds[id].rng),&bucket);
		rank = strict_queue->remove(prev_key);
		cout << bucket << "," << key <<","<< rank << endl ;
		/*
		thread_log.timestamp=RDTSCP();
		thread_log.op=0;
		thread_log.key=key;
		thread_log.id=id;
		thread_log.bucket=bucket;
		local_log[id].push_back(thread_log);
		ops[id].val+=1;
		*/
//		usleep(10000);
	}


	/* BARRIER FOR ALL THREADS */
	//     barrier.wait(nb_threads, 3);
	pthread_barrier_wait(&barrier);

	if ( id == nb_threads - 1 )
	{
		gettimeofday(&end_time, NULL);
	} 
	//     barrier.wait(nb_threads, 4);
	pthread_barrier_wait(&barrier);

	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	int i, c;
	keyType  key=0;
	long int ops_count = 0;

	struct option long_options[] = {
		{"debug",                  no_argument, NULL, 'd'},
		{"stack",                  no_argument, NULL, 's'},
		{"threads",                      no_argument,       NULL, 'n'},
		{"size",                      no_argument,       NULL, 'i'},
		{"iterations",                      no_argument,       NULL, 'k'},
		{"queues",                      no_argument,       NULL, 'c'},
		{"print",                      no_argument,       NULL, 'p'},
		{"help",                      no_argument,       NULL, 'h'},
		{NULL, 0, NULL, 0}
	};
	while(1) {

		i = 0;

		c = getopt_long(argc, argv, "n:u:i:r:w:k:c:h:p", long_options, &i);

		if(c == -1 ) break;

		if(c == 0 && long_options[i].flag == 0)
			c = long_options[i].val;

		switch(c){
			case 'n': 
				nb_threads = atoi(optarg);
				cout << "#Number of threads " << nb_threads << endl;
				break;
			case 'i' :
				sampleSize = atoi(optarg);
				cout << "# Initial size " << sampleSize << endl;
				break;
			case 'd':
				debug = true;
				cout << "#DEBUG MODE" << endl;
				break;
			case 's':
				stack = true;
				cout << "#WITH STACK INFO" << endl;
				break;
			case 'k' :
				iter = atoi(optarg);
				cout << "#Iterations " << iter << endl;
				break;
			case 'c' :
				nb_queues = atoi(optarg);
				cout << "#C  " << nb_queues << endl;
				break;
			case 'p' :
				print = true;
				break;
			case 'h' :
				cout << "Use:" << endl;
				cout << "./test_multiPQ [options]" << endl;
				cout << "Options:" << endl;
				cout << "\t-n, --threads" << endl;
				cout << "\t\tNumber of threads, default 1" << endl;
				cout << "\t-k, --iterations " << endl;
				cout << "\t\tNumber of operations by each thread" << endl;
				cout << "\t-i, --size" << endl;
				cout << "\t\tInitial size of the priority queue, default 1 000 000" << endl;
				cout << "\t-c, --queues " << endl;
				cout << "\t\tnb_queues = C*nb_threads, default C=1" << endl;
				cout << "\t-p, --print" << endl;
				cout << "\t\tPrint the queues sizes and top elements" << endl;
				cout << "\t--debug " << endl;
				cout << "\t\tDebug mode for SDE, activate an int3 instruction just after initialization phase" << endl;
				cout << "\t--stack" << endl;
				cout << "\t\tPrint thread stack info" << endl;
				cout << "Example:" << endl;
				cout << "\t./distribution -n 8 -c 2 -k 10000" << endl;
				return 0 ;
			default :
				break;
		}
	}
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_barrier_init(&barrier,NULL,nb_threads);


	srand((int)time(0));
	for(int i=0;i<nb_threads;i++){
		seeds[i].rng = new CRandomSFMT((int)(time(0)));//rand();
	}
	srand((int)time(0));
	for(int i=0;i<nb_threads;i++)
		keySeeds[i].val = rand();

	my_queue = new MultiPQ(nb_threads, nb_queues);
	strict_queue = new strictPQ();

	unsigned long int levelSeed[3];
	seed_rand(levelSeed);

	/* Init the data structure with random keys */
	unsigned long int last = 1;
	for(unsigned long int k=0;k<sampleSize;k++) {
		key = rand_range_re(100000000, 0);
		my_queue->insert((int)key,(int)key,(seeds[0].rng),0);
		strict_queue->insert((int)key,(int)key, -1);
		last = key;
	}; 
	if(print)
		my_queue->displayInfo();
	

	pthread_t thrs[nb_threads];
	int * ids[nb_threads];
	for(int k=0;k<nb_threads;k++){
		ids[k] = new int(k);
	}

	for(int k=0;k<nb_threads;k++){
		pthread_create (&(thrs[k]), &attr, run_thread, (void*)(ids[k]));
	}

	for (int k = 0; k < nb_threads; k ++)
	{
		(void)pthread_join (thrs[k],NULL);
		ops_count += ops[k].val;
	}

	/* Merge all logs */
	for(int i=1;i<nb_threads;i++)
		local_log[0].merge(local_log[i],compareLogs() );
	local_log[0].sort(compareLogs() );
	cout << "#SIZE OF LOG " << local_log[0].size() << endl;

	/* Replay the logs on the strict PQ */
	cout << "timestamp,ithread,bucket,key,index" << endl;
	/*
	for(std::list<struct log>::iterator it = local_log[0].begin();it!=local_log[0].end();it++){
		int rank, i=0;
		struct log log_entry = ((struct log)(*it));
		if(!(i%100))
			switch(((struct log)(*it)).op){
				case 1:
					strict_queue->insert(log_entry.key,log_entry.key, log_entry.id);
					break;
				case 0:
					cout << log_entry.timestamp << ",";
					rank = strict_queue->remove(log_entry.key);
					cout << log_entry.bucket << "," << log_entry.key <<","<< rank << endl ;
					break;
				default:
					assert(0); // Bad log entry
					break;
			}
		i++;
	}
	*/
	cout << "#SIZE " << strict_queue->size() << endl;



	gettimeofday(&end_time, NULL);
	pthread_barrier_destroy(&barrier);

	long int diff = diff_time(start_time,end_time);

	ofstream myfile;
	myfile.open ("oplog.csv");
	myfile << "#BEGIN" << endl;
	myfile << "thread,timestamp,key,bucket,op" << endl;
	for(std::list<struct log>::iterator it = local_log[0].begin();it!=local_log[0].end();it++)
		myfile << ((struct log)(*it)).print() << endl;
	myfile << "#END" << endl;
	myfile.close();

	if(print)
		my_queue->displayInfo();
	for(int k=0;(k<nb_threads);k++)
		delete ids[k];
	delete my_queue;
	delete strict_queue;
};

