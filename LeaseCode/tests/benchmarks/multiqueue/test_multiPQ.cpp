
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <fstream>

//time mesure
#include <sys/time.h> 
#include <stdio.h> 
#include <unistd.h>
#include <sys/resource.h>

#include "MultiPQ.hpp"
#include "sfmt.h"


#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <pthread.h>

#include "carbon_user.h"

using namespace std;


static int alarm_time=0;

static void alarm_handler( int arg)
{
	alarm_time = 1;
}

long int diff_time(struct timeval start, struct timeval end){
	return ((end.tv_sec  - start.tv_sec) * 1000 + (end.tv_usec - start.tv_usec)/1000.0) + 0.5; 
}
typedef int keyType;
MultiPQ *my_queue;
int debug = false;
int stack = false;
int nb_threads=2;
int nb_queues=4;
bool print=false;
bool monotone=false;
bool hyper=false;
unsigned long duration=10;
unsigned long int sampleSize = 1000;
static struct timeval start_time, end_time;
pthread_barrier_t barrier;


struct padVar{
	CRandomSFMT *rng;   
	char x[64-sizeof(CRandomSFMT*)];
} __attribute__ ((aligned(64))); 
struct padLongVar{
	unsigned long int val;   
	char x[64-sizeof(long int)];
} __attribute__ ((aligned(64))); 

padLongVar ops[200];// max number of threads TODO dynamic allocation
padVar seeds[200];// max number of threads TODO dynamic allocation
padLongVar keySeeds[200];// max number of threads TODO dynamic allocation


static inline long rand_range_re(long r, int id)
{      
	long v = xorshf96(&(keySeeds[id].val), &(keySeeds[id].val) + 1, &(keySeeds[id].val) + 2) % r;
	v++; 
	return v;
};
void setAffinity(int id){
	/*
   int affi = id;
	cpu_set_t aff;
	CPU_ZERO(&aff);
	CPU_SET(affi, &aff);
	pthread_setaffinity_np(pthread_self(),sizeof(aff),&aff);
   */
}; 
/* Print stack info */
void print_threadInfo(int id){
	pthread_attr_t attr;
	pthread_getattr_np(pthread_self(), &attr);
	size_t   *stackaddr, stacksize;
	pthread_attr_getstack(&attr,(void**) &stackaddr,&stacksize);
	cout << " THREAD  "  <<  id << std::dec << " stack address " << std::hex << stackaddr  <<  "  stack size "  << stacksize << std::dec << endl;
};

void* run_thread(void * arg){
	/*
   if(stack)
		print_threadInfo(*((int*)(arg)));
   */
	int id = *((int*)(arg));
	/*
   if(hyper) // Set for the 2-sockets Intel Haswell EP
		if((id<=13)||(id>41))
			setAffinity(id);
		else {
			if (id <= 27)
				setAffinity(id+14);
			else{
				setAffinity(id-14);
			}
		}
	else
	setAffinity(id);
   */

	keyType prev_key = 0;
	keyType key;
	long unsigned int max = 0;
	long unsigned int stat = 0;
	bool results = false;
	ops[id].val=0;

	/*
	//barrier.wait(nb_threads, 0);
	pthread_barrier_wait(&barrier);
   if ( id == 0 )
	{
		(void)signal(SIGALRM, &alarm_handler);
		(void)alarm(duration);
		gettimeofday(&start_time, NULL);
		cout << "# init done " << endl;
		if(debug)
			asm(
					"int3"
			   );
	} 
   */
   
	//barrier.wait(nb_threads, 1);
	pthread_barrier_wait(&barrier);
	
   //while(!alarm_time){
	while( CarbonGetTime() < duration*1000000 ){

		if(!monotone){
			key = rand_range_re(100000000, id);
		}else
			key = prev_key + rand_range_re(100, id);

		my_queue->insert_v2((int)key,(int)key,(seeds[id].rng));
		ops[id].val+=1;
		
      		if((prev_key=my_queue->deleteMin_v2(seeds[id].rng))!=-1)
			ops[id].val+=1; //deleteMin succeeded

	}

	/* BARRIER FOR ALL THREADS */
	//     barrier.wait(nb_threads, 3);
	pthread_barrier_wait(&barrier);

	if ( id == nb_threads - 1 )
	{
		gettimeofday(&end_time, NULL);
	} 
	//     barrier.wait(nb_threads, 4);
	pthread_barrier_wait(&barrier);

	//pthread_exit(NULL);
}

int main(int argc, char **argv) {
	int i, c;
	keyType  key=0;
	long int ops_count = 0;

	struct option long_options[] = {
		{"debug",                  no_argument, NULL, 'd'},
		{"stack",                  no_argument, NULL, 's'},
		{"threads",                      no_argument,       NULL, 'n'},
		{"duration",                      no_argument,       NULL, 'u'},
		{"size",                      no_argument,       NULL, 'i'},
		{"queues",                      no_argument,       NULL, 'c'},
		{"help",                      no_argument,       NULL, 'h'},
		{"print",                      no_argument,       NULL, 'p'},
		{"mono",                      no_argument,       NULL, 'm'},
		{"ht",                      no_argument,       NULL, 't'},
		{NULL, 0, NULL, 0}
	};
	while(1) {

		i = 0;

		c = getopt_long(argc, argv, "n:u:i:c:h:", long_options, &i);

		if(c == -1 ) break;

		if(c == 0 && long_options[i].flag == 0)
			c = long_options[i].val;

		switch(c){
			case 'n': 
				nb_threads = atoi(optarg);
				cout << "#Number of threads " << nb_threads << endl;
				break;
			case 'u': 
				duration = atoi(optarg);
				cout << "#Duration " << duration << endl;
				break;
			case 'i' :
				sampleSize = atoi(optarg);
				cout << "# Initial size " << sampleSize << endl;
				break;
			case 'd':
				debug = true;
				cout << "#DEBUG MODE" << endl;
				break;
			case 's':
				stack = true;
				cout << "#WITH STACK INFO" << endl;
				break;
			case 'c' :
				nb_queues = atoi(optarg);
				cout << "#C  " << nb_queues << endl;
				break;
			case 'p' :
				print = true;
				break;
			case 'm' :
				monotone = true;
				break;
			case 't' :
				hyper = true;
				break;
			case 'h' :
				cout << "Use:" << endl;
				cout << "./test_MultiPQ [options]" << endl;
				cout << "Options:" << endl;
				cout << "\t-n, --threads" << endl;
				cout << "\t\tNumber of threads, default 1" << endl;
				cout << "\t-i, --size" << endl;
				cout << "\t\tInitial size of the priority queue, default 1 000 000" << endl;
				cout << "\t-m, --mono" << endl;
				cout << "\t\tGenerate monotonic keys" << endl;
				cout << "\t-c, --queues " << endl;
				cout << "\t\tnb_queues = C*nb_threads, default C=1" << endl;
				cout << "\t-u, --duration" << endl;
				cout << "\t\tDuration of the test. default 10s" << endl;
				cout << "\t-p, --print" << endl;
				cout << "\t\tPrint the queues sizes and top elements" << endl;
				cout << "\t--debug " << endl;
				cout << "\t\tDebug mode for SDE, activate an int3 instruction just after initialization phase" << endl;
				cout << "\t--stack" << endl;
				cout << "\t\tPrint thread stack info" << endl;
				cout << "Example:" << endl;
				cout << "\t./test_MultiPQ -n 8 -c 2" << endl;
				return 0 ;

			default :
				break;
		}
	}
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	pthread_barrier_init(&barrier,NULL,nb_threads);


	srand((int)time(0));
	for(int i=0;i<nb_threads;i++){
		seeds[i].rng = new CRandomSFMT((int)(time(0)));//rand();
	}
	for(int i=0;i<nb_threads;i++)
		keySeeds[i].val = rand();

	my_queue = new MultiPQ(nb_threads, nb_queues);


	unsigned long int levelSeed[3];
	seed_rand(levelSeed);

	/* Init the data structure with random keys */
	for(unsigned long int k=0;k<sampleSize;k++) {
		key = rand_range_re(100000000, 0);
		my_queue->insert((int)key,(int)key,(seeds[0].rng));
	}; 

	pthread_t thrs[nb_threads];
	int * ids[nb_threads];
	for(int k=0;k<nb_threads;k++){
		ids[k] = new int(k);
	}

   /* ================================================================================ */
   /*    PARALLEL SECTION                                                              */
   /* ================================================================================ */
   CarbonEnableModels();
	
   for(int k=1; k<nb_threads; k++){
		//pthread_create (&(thrs[k]), &attr, run_thread, (void*)(ids[k]));
		pthread_create (&(thrs[k]), NULL, run_thread, (void*)(ids[k]));
	}

   // Call thread function 
   run_thread( ids[0] );

	for (int k = 1; k < nb_threads; k ++)
	{
		(void)pthread_join (thrs[k], NULL);
		ops_count += ops[k].val;
	}
   
   CarbonDisableModels();

   /* ================================================================================ */
	
   gettimeofday(&end_time, NULL);
	pthread_barrier_destroy(&barrier);

	long int diff = diff_time(start_time,end_time);
	printf("init_size, nb_threads, time(ms), op_count \n");
	//printf("%ld, %d, %ld, %ld \n", sampleSize, nb_threads, diff, ops_count);
	printf("%ld, %d, %ld, %ld \n", sampleSize, nb_threads, duration, ops_count);

   printf("Successful Ops = %lu \n", ops_count );
   printf("Throughput (Ops/s) = %f \n", (float)(ops_count)*1000/duration );
   printf("\n\n");

	if(print)
		my_queue->displayInfo();
	for(int k=0;(k<nb_threads);k++)
		delete ids[k];
	delete my_queue;
};
