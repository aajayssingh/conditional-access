#include <cstdio>
#include <cstdlib>
#include <pthread.h>
#include <vector>
#include <string.h>
#include <sys/time.h>

#include <assert.h>
#include "carbon_user.h"
#include "util.h"

#define DEBUG_PRINT if (0)

//#define DEBUG 1
#define DEFAULT_THREAD_COUNT        1
#define MAX_OPS                     10

/****************************************
 * Globals
 ****************************************/
// barrier synchronization object
PAD;
pthread_barrier_t   barrier; 
PAD;
volatile unsigned int* global_counter;
PAD;
int thread_count;
PAD;
char* algo_type;
PAD;
volatile uint64_t op_based_exp_begtime = 0;
PAD;
volatile uint64_t op_based_exp_endtime = 0;
PAD;

class ThreadData
{
public:
   ThreadData(int tid_, int num_threads_)
      : tid(tid_), num_threads(num_threads_)
   {
      if( posix_memalign( (void**) &local_counter, 64, sizeof(unsigned int*) ) != 0){
         printf("Error: posix_memalign could not allocate memory!!! \n");
         exit(EXIT_FAILURE);
      }
      printf("*local_counter=%u\n", *local_counter); 
      printf("&local_counter=%x\n", &local_counter); 
      printf("local_counter=%x\n", local_counter); 
   }

   ~ThreadData()
   {}

   int tid;
   int num_threads;
   PAD; //padded each element while allocating
   unsigned long ops;
   PAD;
   volatile unsigned int* local_counter;
   PAD;
}__attribute__((aligned(PADDING_BYTES)));

// return time diff  in msecs
long int diff_time(struct timeval start, struct timeval end)
{
   return ((end.tv_sec - start.tv_sec)*1000 + (end.tv_usec - start.tv_usec)/1000.0) + 0.5; //milli sec
}

/****************************************
 * Function prototypes
 ****************************************/
void* threadMain(void* arg);



/****************************************
 * Function definitions
 ****************************************/

void* localreadworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
      printf("*local_counter=%u\n", *(d->local_counter)); 
      printf("&local_counter=%x\n", &(d->local_counter)); 
      printf("local_counter=%x\n", (d->local_counter)); 

      volatile unsigned int val = *(d->local_counter);
      printf("tid=%d val=%u\n", val);
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

void* localwriteworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)
   {
     *(d->local_counter) = 1;
      d->ops++;
   }

   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   
   return NULL;
}

void* localincworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
     *(d->local_counter)++;
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}

void* globalincworker(void* data) {
   ThreadData* d = (ThreadData*)data;
   unsigned long int tid = d->tid;

   pthread_barrier_wait (&barrier); 
   
   if (!op_based_exp_begtime)
   {
      if (__sync_bool_compare_and_swap(&op_based_exp_begtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation beg_time (%lld)ns\n", op_based_exp_begtime);
      }
   }

   while (d->ops < MAX_OPS)//(CarbonGetTime() <= max_duration)//(++opcount<=2)//
   {
      (*global_counter)++;
      d->ops++;
   }
   pthread_barrier_wait (&barrier);
   if (!op_based_exp_endtime){
      if (__sync_bool_compare_and_swap(&op_based_exp_endtime, 0, CarbonGetTime()) )
      {
         printf ("opsBasedWorker:: Experiment simulation end_time (%lld)ns\n", (op_based_exp_endtime));
         printf ("opsBasedWorker:: Experiment simulation end_time duration(%lld)ns\n", (op_based_exp_endtime - op_based_exp_begtime));
      }
   }
   return NULL;
}


int main(int argc, char *argv[])
{
    thread_count      = DEFAULT_THREAD_COUNT;
    
    // ---------------------------------------------------------
    // Parameters extraction
    //----------------------------------------------------------
    if(argc > 2)
    {
       thread_count   = atoi(argv[1]);
       algo_type      = argv[2];

       printf("\nMINIBENCH:\n");
       printf(" Threads=%i\n", thread_count); 
       printf(" algo_type=%s\n", algo_type); 
    }
    else 
    {
       printf("\nMINIBENCH: wrong arguments.\n");
       exit(EXIT_FAILURE);
    }
   
   
    // Initialize Barrier
    pthread_barrier_init (&barrier, NULL, thread_count);
    printf("Initialized Barrier\n");

   if( posix_memalign( (void**) &global_counter, 64, sizeof(unsigned int*) ) != 0){
      printf("Error: posix_memalign could not allocate memory!!! \n");
      exit(EXIT_FAILURE);
   }
    
   // Allocate Thread data array
   ThreadData* thread_args[thread_count*PADDING_BYTES];
   for (int i = 0; i < thread_count; i++)
   {
      thread_args[i*PADDING_BYTES] = new ThreadData(i, thread_count);
   }
   pthread_t thread_handles[thread_count];
   
   printf("*global_counter=%u\n", *global_counter); 
   printf("&global_counter=%x\n", &global_counter); 
   printf("global_counter=%x\n", global_counter); 
   

/* ======================================================================================== */
/* PARALLEL SECTION */
/* ======================================================================================== */
   CarbonEnableModels();
  
   // Create Threads
   if (strcmp(algo_type, "localread") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localreadworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }
   }
   else if(strcmp(algo_type, "localwrite") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localwriteworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }
   }
   else if (strcmp(algo_type, "localinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, localincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else if (strcmp(algo_type, "globalinc") == 0)
   {
      for (int i = 1; i < thread_count; i++)
      {
         int ret = pthread_create(&thread_handles[i], NULL, globalincworker, (void*) thread_args[i*PADDING_BYTES]);
         //printf("Created Thread %d\n", i);
         if (ret != 0)
         {
            printf("ERROR spawning thread %i\n", i);
            exit(EXIT_FAILURE);
         }
      }   
   }
   else
   {
       printf("\nMINIBENCH: wrong algo type!\n");
       exit(EXIT_FAILURE);
   }

   if (strcmp(algo_type, "localread") == 0)
   {
      localreadworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if(strcmp(algo_type, "localwrite") == 0)
   {
      localwriteworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "localinc") == 0)
   {
      localincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else if (strcmp(algo_type, "globalinc") == 0)
   {
      globalincworker((ThreadData*) thread_args[0*PADDING_BYTES]);
   }
   else
   {
       printf("\nMINIBENCH: wrong algo type!\n");
       exit(EXIT_FAILURE);
   }

   printf("Threads Finished\n");
   for (int i = 1; i < thread_count; i++)
   {
      pthread_join(thread_handles[i], NULL);
   }
   printf("Threads Joined\n");

   CarbonDisableModels(); 
   
   unsigned long total_tries = 0;
   for (int i = 0; i < thread_count; i++)
   {
      ThreadData* data = thread_args[i*PADDING_BYTES];
      total_tries += data->ops;
   }
   printf("Total Exp Ops=%u \n", total_tries);
   printf("Experiment_Throughput (Ops/s)=%u \n", 
   (total_tries)*1000000000/(op_based_exp_endtime - op_based_exp_begtime) );

   
   
   for (int i = 0; i < thread_count; i++)
   {
      delete thread_args[i*PADDING_BYTES];
   }
   return 0;
}
