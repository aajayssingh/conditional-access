/**
 * The code has shamelessly been copied from https://github.com/urcs-sync/Interval-Based-Reclamation to make Interval Based memory reclamation work with Setbench benchmark to 
 * compare Interval based reclamation algorithms and their accompanying memory reclamtion algorithms with NBR and DEBRA.
 * Please refer to original Interval Based Memory Reclamation paper, PPOPP 2018.
 * Ajay Singh (@J)
 * Multicore lab uwaterloo
 */

#ifndef CONCURRENT_PRIMITIVES_H
#define CONCURRENT_PRIMITIVES_H

#include"util.h"

#define CACHE_LINE_SIZE PADDING_BYTES //BYTES_IN_CACHE_LINE

template<typename T>
class padded{
public:
    T ui;
private:
    uint8_t pad [ 0 != sizeof(T)%CACHE_LINE_SIZE 
            ? CACHE_LINE_SIZE - (sizeof(T)%CACHE_LINE_SIZE)
            : CACHE_LINE_SIZE ];
public:
    padded<T>(): ui(){};
    padded<T>(const T& val): ui(val){};
    padded<T>& operator= (const T& val){
        ui = val;
        return *this;
    };
    operator T(){ return T(ui); }    
};

template<typename T>
class paddedAtomic{
public:
    std::atomic<T> ui;
private:
    uint8_t pad [ 0 != sizeof(T)%CACHE_LINE_SIZE 
            ? CACHE_LINE_SIZE - (sizeof(T)%CACHE_LINE_SIZE)
            : CACHE_LINE_SIZE ];
public:
    paddedAtomic<T>(): ui(){};
    paddedAtomic<T>(const T& val): ui(val){};
    paddedAtomic<T>& operator= (const T& val){
        ui.store(val);
        return *this;
    };
    operator T(){ return T(ui.load()); }    
};

#endif //CONCURRENT_PRIMITIVES_H