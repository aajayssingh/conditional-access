/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <cstdio>
//#include <cstdlib>
//#include <pthread.h>
//#include <vector>
//#include <string.h>
//#include <sys/time.h>

#include "util.h"

int main()
{
   PaddedU64Random* rng;
   srand(time(NULL));
//   rng[0].setSeed(1);
   rng =  new PaddedU64Random(); 
   const int maxops = 10;
   const int maxrange = 100;

   for (int i = 0; i < maxops; i++){
      unsigned long int key = rng->nextNatural(maxrange) + 1;
      printf("%lu ", key);
   }

   printf("\n");

   return 0;
}

// MAJOR RNG BUG: using reference for rng matter & while assigning to my_rng
//int main()
//{
//   PaddedU64Random rng[2];
//   srand(time(NULL));
//   rng[0].setSeed(1);
//   rng[1].setSeed(1);
//   const int maxops = 100;
//   const int maxrange = 10000;
//   {   
//         PaddedU64Random my_rng = rng[0];
//
//         printf("rng[0]=%p &rng[0]=%p my_rng=%p &my_rng=%p\n", rng[0], &rng[0], my_rng, &my_rng);
//
//         for (int i = 0; i < maxops; i++){
//            unsigned long int key = my_rng.nextNatural(maxrange) + 1;
//            printf("%lu ", key);
//         }
//         printf("\n");
//         
//                  for (int i = 0; i < maxops; i++){
//            unsigned long int key = my_rng.nextNatural(maxrange) + 1;
//            printf("%lu ", key);
//         }
//         printf("\n");
//      }
//   
//   {
//      
//      rng[0].setSeed(1);
//      PaddedU64Random my_rng = rng[0];
//
//      printf("rng[0]=%p &rng[0]=%p my_rng=%p &my_rng=%p\n", rng[0], &rng[0], my_rng, &my_rng);
//
//      for (int i = 0; i < maxops; i++){
//         unsigned long int key = my_rng.nextNatural(maxrange) + 1;
//         printf("%lu ", key);
//      }
//      printf("\n");
//   }
//      
//      
//      
//   {
//         int a = 10;
//         printf("a=%d &a=%p\n", a ,&a);
//         
//         int &b = a;
//         printf("b=%d &b=%p\n", b, &b);
//         
//         
//   }
//   return 0;
//}