#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

extern "C" {
  typedef int (*mallctl_t)(const char *name, void *oldp, size_t *oldlenp, void *newp, size_t newlen);
}

bool check_jemalloc() {
  mallctl_t mallctl = (mallctl_t) ::dlsym(RTLD_DEFAULT, "mallctl");
  return (mallctl != NULL);
}


int main()
{
	printf("enter\n");
	void* p = malloc(100000);
//	sleep (100);
	free(p);
	printf("return %d\n ", check_jemalloc());
	return 0;
}
