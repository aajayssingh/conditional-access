# USAGE
# To run on modern machine: change use_simulator=0 and pass HMR_DS=ds
# eg make HMR_DS=extbst use_simulator=0
#

CC := /usr/bin/gcc-4.8
CXX := /usr/bin/g++-4.8
APP_CXX := /usr/bin/g++-4.8
APP_SPECIFIC_CXX_FLAGS = -std=c++11 -w

use_simulator ?= 1
ifeq ($(use_simulator), 0)
    APP_SPECIFIC_CXX_FLAGS += -DDISABLECARBON #-DDISABLE_PREFILL # -g -fsanitize=address 
endif

#default minibench build
TARGET = minibench.out
SOURCES = minibench.cc

#use NDEBUG to disable asserts and #ifndef NDEBUG to disable assert related code
# meant to execute when asserts are enabled.
#APP_SPECIFIC_CXX_FLAGS += -DADDDUMMYINST

# this USEASMADDTAG implementation is only for lazylist as size of node is 24 hardocoded in redirect_memory.cc.
# NOTE: Also enable USEASMADDTAG macro in Makefile.common if enabled this here.
#APP_SPECIFIC_CXX_FLAGS += -DUSEASMADDTAG 

APP_SPECIFIC_CXX_FLAGS += -DASMVLOCK #for hmr algos
APP_SPECIFIC_CXX_FLAGS += -DSETBENCHLOCK #for non hmr algos
APP_SPECIFIC_CXX_FLAGS += -DNDEBUG #-DDISABLE_FREECALLS
APP_SPECIFIC_CXX_FLAGS += -DU64RNG  # to enable xorshift rng#-DDISABLE_PREFILL # -g -fsanitize=address 
#APP_SPECIFIC_CXX_FLAGS += -DDEBUGRNG # to enable printing num ins deletes
APP_SPECIFIC_CXX_FLAGS += -I./ds/lazylist/ -I./ds/stack/ -I./ds/extbst/ -I./misc/ -I./reclaimer/

# supply this var while running make file
HMR_DS ?= lazylist

ifeq ($(HMR_DS),lazylist)
    $(info $(HMR_DS))
#    TARGET = minibench_lazylist.out
#    SOURCES = minibench_lazylist.cc
    THREADS	    ?= 1 	# Application threads
    MAX_DURATION    ?= 3 	# Duration (in Milliseconds) to run the test
    KEYRANGE	    ?= 10
    INS		    ?= 50
    DEL		    ?= 50
    ALG		    ?= lazylist_none
    TRIAL	    ?= 1	#input the trial number from cmdline to seed the random num generator
    MAXOPS	    ?= 1000

APP_SPECIFIC_CXX_FLAGS += -DLAZYLIST
endif

ifeq ($(HMR_DS),stack)
    $(info $(HMR_DS))
#    TARGET = minibench_stack.out
#    SOURCES = minibench_stack.cc
    THREADS	    ?= 1 	# Application threads
    MAX_DURATION    ?= 3 	# Duration (in Milliseconds) to run the test
    KEYRANGE	    ?= 100
    INS		    ?= 50
    DEL		    ?= 50
    ALG		    ?= stack_asm_hmr #stack_none
    TRIAL	    ?= 1	#input the trial number from cmdline to seed the random num generator
    MAXOPS	    ?= 1000

APP_SPECIFIC_CXX_FLAGS += -DSTACK
endif

ifeq ($(HMR_DS),extbst)
    $(info $(HMR_DS))
#    TARGET = minibench_extbst.out
#    SOURCES = minibench_extbst.cc
    THREADS	    ?= 1 	# Application threads
    MAX_DURATION    ?= 3 	# Duration (in Milliseconds) to run the test
    KEYRANGE	    ?= 10
    INS		    ?= 50
    DEL		    ?= 50
    ALG		    ?= extbst_none
    TRIAL	    ?= 1	#input the trial number from cmdline to seed the random num generator
    MAXOPS	    ?= 1000

APP_SPECIFIC_CXX_FLAGS += -DEXTBST
endif

APP_FLAGS ?= $(THREADS) $(MAX_DURATION) $(KEYRANGE) $(INS) $(DEL) $(ALG) $(TRIAL) $(MAXOPS)
CORES = $(THREADS)

APP_SPECIFIC_LD_LIBS := -latomic

ifeq ($(isoldgcc), 0)
APP_CXX := g++
endif

ifeq ($(use_simulator), 0)

LDFLAGS := -lpthread -latomic
all: nosim_minibench

nosim_minibench: minibench.cc
	$(APP_CXX) $(APP_SPECIFIC_CXX_FLAGS) -fpermissive minibench.cc -o minibench -O3 $(LDFLAGS)
clean:
	rm *.out *.o *.d minibench
else
    include ../../Makefile.tests
endif
