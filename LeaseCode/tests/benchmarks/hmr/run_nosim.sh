#!/bin/bash

#run this to get table data on terminal and create step files in exp_output.
# internal script to be only called from configandrun_exp.sh
# example usage: ./run_exp.sh
range=$1
maxops=$2
num_ins=$3
num_del=$4
iter=${iterations}

#echo "in run_exp.sh"
#echo "dssize="${range} "maxops="${maxops}
#echo ins=${num_ins} del=${num_del}
#echo iterations=${iter}
#echo algos="${alg_arr[@]}" 
#echo threads="${threads_arr[@]}"
#echo DS=$ds
echo "started: " `date`
timestamp=`date +"%d%h%H%M%S"`

out_dir="exp_result/exp_out"

if [[ -d "${out_dir}" ]]
then
    echo "${out_dir} exists."
#    rm -fr ${out_dir}/*.*
    mv ${out_dir} ${out_dir}_${timestamp}
    echo backedup ${out_dir}/ in ${out_dir}_${timestamp}
fi

mkdir ${out_dir}
echo "created ${out_dir}"

cols="\n%20s %10s %10s %10s %10s %15s %15s %15s %10s %15s %10s %5s %5s %10s %10s %10s %10s %10s %10s\n"
printf "$cols" alg threads tput dssize epoch tot_retired tot_allocated tot_freed walltime max_res_memKB tot_ops ins del succins succdel attins attdel free_retire_ratio retries

div_valid(){ case $1 in 0) false;;esac; }

#thread_arr=(1 2 4) # 42 48 54 60)
#algo_arr=(lazylist_none lazylist_asm_hmr lazylist_hmr lazylist_hp lazylist_ibr_hp lazylist_ibr lazylist_ibr_he lazylist_ibr_rcu lazylist_ibr_qsbr) 
#num_ins=10
#num_del=10
#range=100
dur=5 #not used since using op based benchmark
#maxops=1000

ValidNum () {
    re='^[0-9]+$'
    if ! [[ $1 =~ $re ]] ; then
       echo "error: Not a number" >&2; exit 1
    fi
}

for n in "${threads_arr[@]}"
do
    for alg in "${alg_arr[@]}"
    do
#        tput_sum=0; ds_size_sum=0; epoch_sum=0; tot_retired_sum=0; tot_allocated_sum=0; tot_freed_sum=0; walltime_sum=0; max_resmem_sum=0 
        for ((i=1; i<=$iter; i++))
        do
#            tput=0
            singlealg=`echo ${alg}| tr _ -` #replace _ with -. To be used for plotting script sto find alg name easily.
            fname=$out_dir/app_${singlealg}_${n}_step${i}.txt;
            
            `/usr/bin/time -f "time_cmd_walltime=%e\nmax_res_mem=%M" -o $out_dir/temp_time.txt taskset -c 0-6 ./minibench ${n} ${dur} ${range} ${num_ins} ${num_del} ${alg} ${i} ${maxops} &> $fname`

#            `/usr/bin/time -f "time_cmd_walltime=%e\nmax_res_mem=%M" -o $out_dir/temp_time.txt make INS=${num_ins} DEL=${num_del} MAX_DURATION=${dur} KEYRANGE=${range} THREADS=${n} ALG=${alg} MAXOPS=${maxops} {TRIAL}=$i HMR_DS=${ds} &> $fname`
            
            if [[ $? -ne 0 ]]
            then
                echo "${singlealg}:${n}:step${i} crashed! saving in file crashed*.txt"
                i=$((i-1))
                mv $fname $out_dir/crashed_app_${singlealg}_${n}_step${i}.txt
                continue
            fi
            cat $out_dir/temp_time.txt >> $fname
            tput=`cat $fname | grep Experiment_Throughput| cut -d"=" -f2| tr -d " "`
            i_ds_size=`cat $fname | grep experiment_ds_size| cut -d"=" -f2| tr -d " "`
            i_epoch=`cat $fname | grep Epoch| cut -d"=" -f2| tr -d " "`
            i_retired=`cat $fname | grep total_retired| cut -d"=" -f2| tr -d " "`
            i_allocated=`cat $fname | grep total_allocated| cut -d"=" -f2| tr -d " "`
            i_freed=`cat $fname | grep total_freed| cut -d"=" -f2| tr -d " "`
            i_walltime=`cat $fname | grep time_cmd_walltime| cut -d"=" -f2| tr -d " "`
            i_walltime=${i_walltime%.*}
            i_max_resmem=`cat $fname | grep max_res_mem| cut -d"=" -f2| tr -d " "`
            i_tot_ops=`cat $fname | grep "Total Exp Ops"| cut -d"=" -f2| tr -d " "`
            i_ins=`cat $fname | grep -w "ins"| cut -d"=" -f2| tr -d " "`
            i_del=`cat $fname | grep -w "del"| cut -d"=" -f2| head -1 | tr -d " "`

            i_succins=`cat $fname | grep -w "Total SUCC_INS"| cut -d"=" -f2| tail -1 | tr -d " "`
            i_succdel=`cat $fname | grep -w "Total SUCC_DEL"| cut -d"=" -f2| tail -1 | tr -d " "`
            i_attins=`cat $fname | grep -w "Total ATT_INS"| cut -d"=" -f2| tail -1 | tr -d " "`
            i_attdel=`cat $fname | grep -w "Total ATT_DEL"| cut -d"=" -f2| tail -1 | tr -d " "`

            i_retries=`cat $fname | grep total_retries| cut -d"=" -f2| head -1 | tr -d " "`

#            ValidNum $i_retries

            ret_temp=${i_retired}

            div_valid "${ret_temp}" || ret_temp=1
            i_ret_free_ratio=$(echo "scale=2; ${i_freed}/${ret_temp}"|bc)

            printf "%20s %10d %10d %10d %10d %15d %15d %15d %10d %15d %10d %5d %5d %10d %10d %10d %10d %10f %10d\n" "$singlealg" "$n" "$tput" "$i_ds_size" "$i_epoch" "$i_retired" "$i_allocated" "$i_freed" "$i_walltime" "$i_max_resmem" "$i_tot_ops" "${i_ins}" "${i_del}" "${i_succins}" "${i_succdel}" "${i_attins}" "${i_attdel}" \
             "${i_ret_free_ratio}" "${i_retries}"
            
            # copy sim output from results dir to local exp_output folder.
#            cp ../../../results/latest/sim.out $out_dir/
#            mv $out_dir/sim.out $out_dir/sim_${singlealg}_${n}_step${i}.out
        done
    done
    echo " --------------- "
done

echo "finished: " `date`

`mkdir ${out_dir}/trial_data; cd ${out_dir}; mv *.* trial_data/`
echo "moved all app and sim output files to trial data"

##create csv for app stats
./create_appstat_csv.sh ${out_dir}/trial_data > ${out_dir}/appstat_${num_ins}${num_del}_${dur}_${range}_${maxops}.csv
echo "created app stats file: appstat_${num_ins}${num_del}_${dur}_${range}_${maxops}.csv"

#create csv for simstats for temp analysis quicky at same machine. Otherwise python script can be used at machine 
#where analysis will happen.
#./create_simstat_csv.sh ${out_dir}/trial_data > ${out_dir}/simstat_${num_ins}${num_del}_${dur}_${range}_${maxops}_avgdatalegacy.csv
#echo "created sim stats file: simstat_${num_ins}${num_del}_${dur}_${range}_${maxops}_avgdatalegacy.csv"

`mkdir ${out_dir}/scripts; cp run_exp.sh ${out_dir}; cp configandrun_exp.sh ${out_dir}; cp onetouch_appnsim_plotter.sh ${out_dir}; cp scripts/*.* ${out_dir}/scripts`
echo "created scripts directory and copied current run_exp.sh."

if [[ -d "${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops}" ]]
then
    echo "${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops} exists."
#    rm -fr ${out_dir}/*.*
    mv ${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops} ${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops}_${timestamp}
    echo backedup ${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops}/ in ${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops}_${timestamp}
fi

mv ${out_dir} ${out_dir}_${num_ins}${num_del}_${dur}_${range}_${maxops}
echo "moved" ${out_dir} "--->" ${out_dir}_${num_ins}${num_del}_${dur}_sz${range}_ops${maxops}




#            `/usr/bin/time -f "time_cmd_walltime=%e\nmax_res_mem=%M" -o $out_dir/temp_time.txt taskset -c 0-6 ./minibench ${n} ${dur} ${range} ${num_ins} ${num_del} ${alg} ${i} ${maxops} &> $fname`
