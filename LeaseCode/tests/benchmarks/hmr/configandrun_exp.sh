#!/bin/bash

################################################################################
# set the configuration parameters for experiments:
#alg_arr=(lazylist_none lazylist_ibr_rcu lazylist_ibr_qsbr lazylist_asm_hmr lazylist_hmr lazylist_ibr lazylist_ibr_he lazylist_hp lazylist_ibr_hp)
alg_arr=(lazylist_asmhmr_trv)
#alg_arr=(lazylist_asm_hmr lazylist_asmhmr_trv lazylist_none lazylist_none_carbon lazylist_hmr) # lazylist_asm_hmr lazylist_hp)
#alg_arr=(stack_none stack_hmr stack_asm_hmr)
#alg_arr=(extbst_none extbst_ibr extbst_asm_hmr extbst_hmr extbst_ibr_he extbst_ibr_hp extbst_ibr_rcu)
#alg_arr=(extbst_asm_hmr extbst_hmr)
threads_arr=(1 2 16)
#threads_arr=(1)

iterations=2
ins_arr=(0) #keep ins and del array same size
del_arr=(0) #keep ins and del array same size
maxops_arr=(2000)
dssize_arr=(1000) #next 100 and 2000
#ds="stack"
ds="lazylist" #"lazylist"
#ds="extbst"

################################################################################
echo "make clean and running once before starting experiment"
`make clean > delme.txt 2>&1` 
`HMR_DS=$ds make THREADS=1 ALG=${ds}_none INS=50 DEL=50 KEYRANGE=100 MAXOPS=100 > delme2.txt 2>&1`
echo "make clean and running done check delme.txt delme2.txt"
################################################################################
for sz in "${dssize_arr[@]}"
do
    for mxops in "${maxops_arr[@]}"
    do
        for((idx=0; idx<${#ins_arr[@]}; ++idx))
        do
            echo "EXPERIMENT CONFIGURATION: "
            echo "dssize="${sz} "maxops="${mxops}
            echo ins=${ins_arr[idx]} del=${del_arr[idx]}
            echo iterations=${iterations}
            echo algos="${alg_arr[@]}" 
            echo threads="${threads_arr[@]}"
            echo ""
            source run_exp.sh ${sz} ${mxops} ${ins_arr[idx]} ${del_arr[idx]} #${iterations} #"${alg_arr[@]}" "${threads_arr[@]}"
            echo "#########"
            echo ""
        done #ins

    done #mxops

done #sz


#password="ajay"
#username="ajay"
#Ip="10.42.0.1"
#srcpath="./exp_result/"
#destpath="~/shdir/Day105/"
##scp -r ./exp_result/ ajay@10.42.0.1:~/shdir/Day105/
#scp -r ${srcpath} $username@$Ip:${destpath}


#sshpass -p "$password" scp /<PATH>/final.txt $username@$Ip:/root/<PATH>