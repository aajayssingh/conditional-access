#!/usr/bin/env python

'''
script to plot throughput from appstat csv. 
'''

import matplotlib.pyplot as plt
import pandas as pd
import os
import glob

#for file in glob.glob(os.path.join("../", 'appstat_*.csv')):
#    df = pd.read_csv(file)
#    sub_df = df.iloc[:, [0,1,2]]
#    stat_name = sub_df.columns[2]
#    sub_df = pd.pivot_table(sub_df, values=sub_df.columns[2], columns='algo', index=sub_df.columns[1], aggfunc='mean')
#    list_cols = list(sub_df.columns)
#    sub_df.plot(y=list_cols, kind="line", xticks=list(sub_df.index), marker='D', title=stat_name+" "+file.split("/")[1])
#    sub_df.plot(y=list_cols, kind="bar", title=stat_name+" "+file.split("/")[1])


import itertools
marker = itertools.cycle(('o', 'v', '^', 's', 'p', 'P', '*', 'D', 'x', 'd' )) 

for file in glob.glob(os.path.join("../", 'appstat_*.csv')):
    df = pd.read_csv(file)
    sub_df = df.iloc[:, [0,1,2]]
    stat_name = sub_df.columns[2]
    sub_df = pd.pivot_table(sub_df, values=sub_df.columns[2], columns='algo', index=sub_df.columns[1], aggfunc='mean')

    ax = sub_df.plot(kind='line', title=stat_name+" "+file.split("/")[1], xticks=list(sub_df.index)) #, figsize=(10, 5)
    for i, line in enumerate(ax.get_lines()):
        line.set_marker(next(marker))
        line.set_linewidth(2)
        line.set_markersize(6)
     
    # adding legend
    ax.legend(ax.get_lines(), sub_df.columns, loc='best')
    plt.grid()
#    plt.show()
    plt.savefig("../"+stat_name.strip()+file.split("/")[1].split(".")[0]+'.png')
#    plt.savefig("../"+stat_name.strip()+file.split("/")[1].split(".")[0]+'.png', dpi=300)
    plt.close('all')
