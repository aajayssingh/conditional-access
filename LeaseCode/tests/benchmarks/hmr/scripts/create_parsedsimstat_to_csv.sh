
data_dir="../trial_data"


count=0
for stepfile in `ls $data_dir/parsedsim*.txt`
do
    #get algo name from file name
    file_name=`echo $stepfile | cut -d"/" -f3 | cut -d"." -f1`
    algo_name=`echo $file_name | cut -d"_" -f2`
    threads=`echo $file_name | cut -d"_" -f3`


    head_row=("algo, threads,") #header for csv
    stat_row=("$algo_name, $threads,") #number content of the row which has per thread stat

##    Get all | separated lines
    output=`cat $stepfile`
    while IFS= read -r line
    do
        #get comma separated line
    #    echo line::"${line}"
        title=`echo "${line}" | cut -d"=" -f1 | tr -d " "`
        stats=`echo "${line}" | cut -d"=" -f2- | tr -d " "`
    #    echo $title stats::$stats
        head_row+=("${title},")
        stat_row+=("$stats,")
    done <<< "${output}"

#    echo ${head_row[*]}
#    echo ${stat_row[*]}


if ((count==0))
then
    echo ${head_row[*]}
fi
    echo ${stat_row[*]}
count=$count+1
done
