#!/usr/bin/env python

'''
script to plot each stat item in a separate graph from generated sim output csv 
'''

import matplotlib.pyplot as plt
import pandas as pd

#plt.rcParams['axes.prop_cycle'] = plt.cycler(color=["r", "#e94cdc", "0.7"]) 
#plt.rcParams.update(plt.rcParamsDefault)
df = pd.read_csv('../simstat_po.csv', sep=',',header=None, index_col=None)

#delete last column of df
df = df.iloc[:, :-1]

#make first row of csv the header of df
new_header = df.iloc[0]
df = df[1:]
df.columns = new_header


#for selective algos
#lines = []
#with open('simplot_input.txt') as f:
#    lines = f.readlines()
#f.close()
#
#count = 0
#algos = []
#for line in lines:
#    count += 1
#    algos.append(line.strip(' \n\t'))
#
#df = df.loc[df['algo'].isin(algos)]


#convert all column data to numeric
col_list = list(df.columns)
col_list.pop(0)
df[col_list] = df[col_list].apply(pd.to_numeric)

#calculate num of columns or algos to be plotted
algo_list = list(set(df.algo))


df_dict={}
for col in range (2, len(df.columns)):
    sub_df = df.iloc[:, [0,1,col]]
    stat_name = sub_df.columns[2]
    sub_df = pd.pivot_table(sub_df, values=sub_df.columns[2], columns='algo', index=sub_df.columns[1])
    
    df_dict[stat_name] = sub_df
    
#    #sort legend order to get same color for each run
#    handles, labels = ax.get_legend_handles_labels()
#    # sort both labels and handles by labels
#    labels, handles = zip(*sorted(zip(labels, handles), key=lambda t:t[0]))
#    ax.legend(handles, labels)
    
#    leg = ax.get_legend()
#    leg.legendHandles[0].set_color('red')
#    leg.legendHandles[1].set_color('yellow')
#    leg.legendHandles[1].set_color('brown')


    #bar plot
    ax1 = sub_df.plot(y=algo_list, kind="bar", title=stat_name)
    plt.savefig("../simplots/"+stat_name.strip()+'_bar.png')
    
    #line plot
    ax2 = sub_df.plot(y=algo_list, kind="line", title=stat_name, xticks=list(sub_df.index))
    plt.savefig("../simplots/"+stat_name.strip()+'_line.png')
#    plt.show()
    
    plt.close('all')
#    plt.close()
    
#for subdf in df_list:
   