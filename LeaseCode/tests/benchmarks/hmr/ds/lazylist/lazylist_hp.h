#ifndef LAZYLIST_HP_H
#define LAZYLIST_HP_H

#include <limits.h>
#include "lock.h"
#include "hzrdptrs.h"
#include <atomic>

class list_info{
public:
    void * obj;
    atomic_uintptr_t * ptrToObj;
    atomic_bool * nodeContainingPtrToObjIsMarked;
    
    list_info ( void * _obj, atomic_uintptr_t * _ptrToObj,
    atomic_bool * _nodeContainingPtrToObjIsMarked) : obj(_obj), ptrToObj(_ptrToObj), 
    nodeContainingPtrToObjIsMarked(_nodeContainingPtrToObjIsMarked)
    {}
    list_info(){}
    ~list_info(){}
};

inline CallbackReturn callbackCheckNotRetired(CallbackArg arg){
    list_info *info = (list_info*) arg;
    if((void*)info->ptrToObj->load(memory_order_relaxed) == info->obj){
        membarstoreload();
        if (!info->nodeContainingPtrToObjIsMarked->load(memory_order_relaxed)){
            return true;
        }
    }
}

#define if_fail_to_protect_node(info, tid, _obj, arg2, arg3) \
    info.obj = _obj; \
    info.ptrToObj = (atomic_uintptr_t*) arg2; \
    info.nodeContainingPtrToObjIsMarked = (atomic_bool*)arg3; \
    if (!hp->protect(tid, _obj, callbackCheckNotRetired, (void*) &info))

class lazylist_hp{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    //pad??
    struct node{
        unsigned int key;
        bool mark;
#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif
        struct node * next;

        node(unsigned int key): key(key)
        {
            mark = false;
            next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        }
    };
    
    hzrdptrs<struct node> *hp;

    struct node * head;
    struct node * tail;    
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_hp();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_hp::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
}

lazylist_hp::lazylist_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_hp\n");
//    head = new node(min_key);
//    tail = new node(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    initNode(tail, max_key);

    head->next = tail;
    
    //hp
    hp = new hzrdptrs<struct node>(num_threads);
}

lazylist_hp::~lazylist_hp()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_hp\n");
    delete hp;
}

bool lazylist_hp::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_hp::contains(const unsigned int tid, const unsigned int key)
{
    list_info info;
    retry:
    struct node * pred = head;
    if_fail_to_protect_node(info, tid, pred, &head, &head->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    
    
    struct node * curr = pred->next;
    if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    
    while(curr->key < key)
    {
        hp->unprotect(tid, pred);
        pred = curr;
        curr = curr->next;
        if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
            hp->unprotectAll(tid);
            goto retry;
        }    
    }
    
    bool res = ((!curr->mark) && (curr->key == key)); 
    hp->unprotectAll(tid);
    return res;
}

bool lazylist_hp::insert(const unsigned int tid, const unsigned int key)
{
    list_info info;
    retry:
    
    struct node * pred = head;
    if_fail_to_protect_node(info, tid, pred, &head, &head->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    
    struct node * curr = pred->next;
    if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    

    while(curr->key < key)
    {
        hp->unprotect(tid, pred);

        pred = curr;
        curr = curr->next;
        if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
            hp->unprotectAll(tid);
            goto retry;
        }    
    }
    
    if (curr->key == key)
    {
        hp->unprotectAll(tid);
        return false;
    }   

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        hp->unprotectAll(tid);

        goto retry;        
    }
    
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);

    new_node->next = curr;
    pred->next = new_node;
    hp->incAllocCounter(tid);

    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    hp->unprotectAll(tid);

    return true;
}

bool lazylist_hp::erase(const unsigned int tid, const unsigned int key)
{
    list_info info;
    retry:
    struct node * pred = head;
    if_fail_to_protect_node(info, tid, pred, &head, &head->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    
    
    struct node * curr = pred->next;
    if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
        hp->unprotectAll(tid);
        goto retry;
    }    
    while(curr->key < key)
    {
        hp->unprotect(tid, pred);
        pred = curr;
        curr = curr->next;
        if_fail_to_protect_node(info, tid, curr, &pred->next, &pred->mark){
            hp->unprotectAll(tid);
            goto retry;
        }    
    }

    
    if (curr->key != key)
    {
        hp->unprotectAll(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        hp->unprotectAll(tid);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;
//    free(curr);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    hp->retire(tid, curr);

    hp->unprotectAll(tid);
    return true;
}

long long lazylist_hp::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_hp::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_hp::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

#endif // LAZYLIST_HP_H
