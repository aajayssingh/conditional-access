/* 
 * File:   lazylist_ibr.h
 * Author: mc
 *
 * Created on May 14, 2021, 5:31 PM
 */

#ifndef LAZYLIST_IBR_H
#define LAZYLIST_IBR_H

#include "lock.h"
#include "ibr.h"

struct node_ibr; //fwd declaration to satiate complaining compiler.
ibr<struct node_ibr> *rec_ibr = NULL;

struct node_ibr{
    unsigned int key;
    bool mark;
#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif
//    struct node_ibr * next;
    std::atomic<struct node_ibr*> next;

    //rec based members
    uint64_t birth_epoch;
    uint64_t retire_epoch;

    node_ibr(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        assert(rec_ibr !=  NULL);
        
        birth_epoch = rec_ibr->getEpoch();
        retire_epoch = UINT64_MAX;
    }
};

class lazylist_ibr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    //pad??

    struct node_ibr * head;
    struct node_ibr * tail;    
    bool validate(struct node_ibr * pred, struct node_ibr * curr);
    void initNode(struct node_ibr * n, unsigned int key);
    void printNode(struct node_ibr * n);

public:
    lazylist_ibr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr::initNode(struct node_ibr * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(rec_ibr !=  NULL);

    n->birth_epoch = rec_ibr->getEpoch();
    n->retire_epoch = UINT64_MAX;
}

lazylist_ibr::lazylist_ibr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_ibr\n");
    rec_ibr = new ibr<struct node_ibr>(num_threads); //WARNING: NEEDS TO BE THE FIRST THING

//    head = new node_ibr(min_key);
//    tail = new node_ibr(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node_ibr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_ibr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);    
    
    head->next = tail;
}

lazylist_ibr::~lazylist_ibr()
{
    struct node_ibr * curr = head;
    struct node_ibr * temp_node_ibr = NULL;
    
    while(curr != NULL)
    {
        temp_node_ibr = curr;
        curr = curr->next;
        
        free(temp_node_ibr);
    }
    printf("dtor::lazylist_ibr\n");
    delete rec_ibr;
}

bool lazylist_ibr::validate(struct node_ibr * pred, struct node_ibr * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_ibr::contains(const unsigned int tid, const unsigned int key)
{
    rec_ibr->startOp(tid);
    
    struct node_ibr * pred = head;
    struct node_ibr * curr = rec_ibr->read(tid, pred->next);
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_ibr->read(tid, curr->next);
    }
    
    bool res = ((!curr->mark) && (curr->key == key)); 
    rec_ibr->endOp(tid);

    return res;
}

bool lazylist_ibr::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    rec_ibr->startOp(tid);

    struct node_ibr * pred = head;
    struct node_ibr * curr = rec_ibr->read(tid, pred->next);

    while(curr->key < key)
    {
        pred = curr;
        curr = rec_ibr->read(tid, curr->next);
    }
    
    if (curr->key == key)
    {
        rec_ibr->endOp(tid);
        return false;
    }   
    

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        rec_ibr->endOp(tid);
        goto retry;        
    }
    
//    struct node_ibr * new_node_ibr = new node_ibr(key);
    struct node_ibr * new_node_ibr;
    if(posix_memalign( (void**)&new_node_ibr, 64, sizeof(struct node_ibr)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node_ibr, key);
   
    
    new_node_ibr->next = curr;
    pred->next = new_node_ibr;
    rec_ibr->incAllocCounter(tid);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    rec_ibr->endOp(tid);

    return true;
}

bool lazylist_ibr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    rec_ibr->startOp(tid);
    
    struct node_ibr * pred = head;
    struct node_ibr * curr = rec_ibr->read(tid, pred->next);

    while(curr->key < key)
    {
        pred = curr;
        curr = rec_ibr->read(tid, curr->next);
    }
    
    if (curr->key != key)
    {
        rec_ibr->endOp(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        rec_ibr->endOp(tid);
        goto retry;        
    }
    
    curr->mark = true;
//    pred->next = curr->next;
    pred->next.store(curr->next, std::memory_order_release);// = curr->next;

    // Since simulator load/store are non faulty. a thread can access a null node_ibr and
// if members of a null node_ibr without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid node_ibrs.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    rec_ibr->retire(tid, curr);    
    
    rec_ibr->endOp(tid);
    return true;
}

long long lazylist_ibr::getSumOfKeys()
{
    long long sum = 0;
    struct node_ibr * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr::getDSSize()
{
    long long count = 0;
    struct node_ibr * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr::printDebuggingDetails()
{
    //print list    
    printf("LIST: \n");
    struct node_ibr * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u (be=%lu, re=%lu)", curr->key, curr->birth_epoch, curr->retire_epoch);
        curr = curr->next;
    }
    printf("\n");
}


#endif /* LAZYLIST_IBR_H */

