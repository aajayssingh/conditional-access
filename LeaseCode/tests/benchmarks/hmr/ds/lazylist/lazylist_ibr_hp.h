/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lazylist_ibr_hp.h
 * Author: mc
 *
 * Created on Sept 8, 2021, 22:20
 */

#ifndef LAZYLIST_IBR_HP_H
#define LAZYLIST_IBR_HP_H

#include "lock.h"
#include "ibr_hp.h"

#define MAX_HP 2
struct node_hp; //fwd declaration to satiate complaining compiler.
ibr_hp<struct node_hp> *rec_hp = NULL;

struct node_hp{
    unsigned int key;
    bool mark;
#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif
//    struct node_hp * next;
    std::atomic<struct node_hp*> next;

    node_hp(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        assert(rec_hp !=  NULL);
    }
};

class lazylist_ibr_hp{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    //pad??

    struct node_hp * head;
    struct node_hp * tail;    
    bool validate(struct node_hp * pred, struct node_hp * curr);
    void initNode(struct node_hp * n, unsigned int key);
    void printNode(struct node_hp * n);

public:
    lazylist_ibr_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr_hp();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr_hp::initNode(struct node_hp * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(rec_hp !=  NULL);
}

lazylist_ibr_hp::lazylist_ibr_hp(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_ibr_hp\n");
    rec_hp = new ibr_hp<struct node_hp>(num_threads, MAX_HP); //WARNING: NEEDS TO BE THE FIRST THING

//    head = new node_hp(min_key);
//    tail = new node_hp(max_key);
    if(posix_memalign( (void**)&head, 64, sizeof(struct node_hp)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node_hp)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);    

    head->next = tail;
}

lazylist_ibr_hp::~lazylist_ibr_hp()
{
    struct node_hp * curr = head;
    struct node_hp * temp_node_ibr = NULL;
    
    while(curr != NULL)
    {
        temp_node_ibr = curr;
        curr = curr->next;
        
        free(temp_node_ibr);
    }
    printf("dtor::lazylist_ibr_hp\n");
    delete rec_hp;
}

bool lazylist_ibr_hp::validate(struct node_hp * pred, struct node_hp * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_ibr_hp::contains(const unsigned int tid, const unsigned int key)
{
    unsigned int count_idx = 0;
    struct node_hp * pred = head;    
    struct node_hp * curr = rec_hp->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_hp->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_hp->getReservation(tid, 0) == rec_hp->getReservation(tid, 1));
//    printf("cont:: (%lld, %lld)", rec_hp->getReservation(tid, 0), rec_hp->getReservation(tid, 1));
    bool res = ((!curr->mark) && (curr->key == key)); 
    
    rec_hp->clear(tid);
    return res;
}

bool lazylist_ibr_hp::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int count_idx = 0;
    struct node_hp * pred = head;    
    struct node_hp * curr = rec_hp->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_hp->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_hp->getReservation(tid, 0) == rec_hp->getReservation(tid, 1));
//    printf("insert:: (%lld, %lld)", rec_hp->getReservation(tid, 0), rec_hp->getReservation(tid, 1));
    
    if (curr->key == key)
    {
        rec_hp->clear(tid);
        return false;
    }   
    

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        rec_hp->clear(tid);
        goto retry;        
    }
    
//    struct node_hp * new_node_ibr = new node_hp(key);
    struct node_hp * new_node_ibr;
    if(posix_memalign( (void**)&new_node_ibr, 64, sizeof(struct node_hp)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node_ibr, key);

    new_node_ibr->next = curr;
    pred->next = new_node_ibr;
    rec_hp->incAllocCounter(tid);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    rec_hp->clear(tid);
    return true;
}

bool lazylist_ibr_hp::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    
    unsigned int count_idx = 0;
    struct node_hp * pred = head;    
    struct node_hp * curr = rec_hp->read(tid, count_idx%2, pred->next);//pred->next;
    count_idx++;
    while(curr->key < key)
    {
        pred = curr;
        curr = rec_hp->read(tid, count_idx%2, curr->next);//curr->next;
        count_idx++;
    }
    
//    assert (rec_hp->getReservation(tid, 0) == rec_hp->getReservation(tid, 1));
//    printf("erase:: (%lld, %lld)", rec_hp->getReservation(tid, 0), rec_hp->getReservation(tid, 1));
    
    if (curr->key != key)
    {
        rec_hp->clear(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        rec_hp->clear(tid);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next.store(curr->next, std::memory_order_seq_cst);// = curr->next;

    // Since simulator load/store are non faulty. a thread can access a null node_hp and
// if members of a null node_hp without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid node_ibrs.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
    rec_hp->retire(tid, curr);    
    
    rec_hp->clear(tid);
    return true;
}

long long lazylist_ibr_hp::getSumOfKeys()
{
    long long sum = 0;
    struct node_hp * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr_hp::getDSSize()
{
    long long count = 0;
    struct node_hp * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr_hp::printDebuggingDetails()
{
    //print list    
    printf("LIST: \n");
    struct node_hp * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u ", curr->key);
        curr = curr->next;
    }
    printf("\n");
}


#endif /* LAZYLIST_IBR_HP_H */

