#ifndef LAZYLIST_IBR_QSBR_H
#define LAZYLIST_IBR_QSBR_H

#include "lock.h"
#include "ibr_qsbr.h"

class lazylist_ibr_qsbr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    //pad??
    struct node{
        unsigned int key;
        bool mark;
#ifdef SETBENCHLOCK
        volatile unsigned int lock;
#else
        pthread_mutex_t pt_lock;
#endif
        struct node * next;

        node(unsigned int key): key(key)
        {
            mark = false;
            next = NULL;
#ifdef SETBENCHLOCK            
            lock = 0;
#else            
            if (pthread_mutex_init(&pt_lock, NULL) != 0){
                printf("Mutex Initialization failed!");
                exit(EXIT_FAILURE);
            }
#endif
        }
    };

    ibr_qsbr<struct node> *qsbr;
    
    struct node * head;
    struct node * tail;    
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_ibr_qsbr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_ibr_qsbr();
    
    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_ibr_qsbr::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
#ifdef SETBENCHLOCK    
    n->lock = 0;
#else
    if (pthread_mutex_init(&n->pt_lock, NULL) != 0){
        printf("Mutex Initialization failed!");
        exit(EXIT_FAILURE);
    }
#endif
    assert(qsbr !=  NULL);
}

lazylist_ibr_qsbr::lazylist_ibr_qsbr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_ibr_qsbr\n");
    head = new node(min_key);
    tail = new node(max_key);
    head->next = tail;
    
    qsbr = new ibr_qsbr<struct node>(num_threads);
}

lazylist_ibr_qsbr::~lazylist_ibr_qsbr()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_ibr_qsbr\n");
    delete qsbr;
}

bool lazylist_ibr_qsbr::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

bool lazylist_ibr_qsbr::contains(const unsigned int tid, const unsigned int key)
{
    qsbr->startOp(tid);
    
    struct node * pred = head;
    struct node * curr = pred->next;
    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    bool res = ((!curr->mark) && (curr->key == key)); 
    qsbr->endOp(tid);

    return res;
}

bool lazylist_ibr_qsbr::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    qsbr->startOp(tid);

    struct node * pred = head;
    struct node * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key == key)
    {
        qsbr->endOp(tid);
        return false;
    }   
    

#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif
    
    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        qsbr->endOp(tid);
        goto retry;        
    }
    
//    struct node * new_node = new node(key);
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    new_node->next = curr;
    pred->next = new_node;
    
    qsbr->incAllocCounter(tid);
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    qsbr->endOp(tid);

    return true;
}

bool lazylist_ibr_qsbr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    qsbr->startOp(tid);
    
    struct node * pred = head;
    struct node * curr = pred->next;

    while(curr->key < key)
    {
        pred = curr;
        curr = curr->next;
    }
    
    if (curr->key != key)
    {
        qsbr->endOp(tid);
        return false;
    }   
    
#ifdef SETBENCHLOCK   
    acquireLock(&pred->lock);
    acquireLock(&curr->lock);
#else
    pthread_mutex_lock(&pred->pt_lock);
    pthread_mutex_lock(&curr->pt_lock);
#endif

    if (!validate(pred, curr))
    {
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif
        qsbr->endOp(tid);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;

    // Since simulator load/store are non faulty. a thread can access a null node and
// if members of a null node without seg faults. This would only cause inf loop or 
//    threads appearing to be freezed etc.. Watch out for such errors in future
// If such errors occur I might very well be accessing invalid nodes.
//    printf("freed\n");
//    free(curr);
//    curr = NULL;
    
#ifdef SETBENCHLOCK 
        releaseLock(&pred->lock, tid);
        releaseLock(&curr->lock, tid);
#else        
        pthread_mutex_unlock(&pred->pt_lock);
        pthread_mutex_unlock(&curr->pt_lock);
#endif

    qsbr->retire(tid, curr);    
    qsbr->endOp(tid);
    return true;
}

long long lazylist_ibr_qsbr::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_ibr_qsbr::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_ibr_qsbr::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
}

#endif // LAZYLIST_IBR_QSBR_H
