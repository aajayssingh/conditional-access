#ifndef LAZYLIST_H
#define LAZYLIST_H

#include <limits.h>
#include "lock.h"
#include "util.h"
//#define OPT
#define DEBUG if(0)
//use this to enablke default pthread mutex lock.
//#define LOCK_DEBUG

//not part of class as node type is needed to write MACROS or cleanUP function.
// could make inline functions part of class as well.
struct node{
    unsigned int key;
    bool mark;
    volatile unsigned int lock;
    struct node * next;

    node(unsigned int key): key(key)
    {
        mark = false;
        next = NULL;
        lock = 0; 
    }
};//__attribute__((aligned(PADDING_BYTES)));

/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void cleanUp(const unsigned int tid, struct node * pred = NULL, struct node * curr = NULL) __attribute__((always_inline));


/*
 * release locks if any and remove all watched addresses, before retrying op or returning from the op.
 */
template<bool usepred=false, bool usecurr=false>
void cleanUp(const unsigned int tid, struct node * pred = NULL, struct node * curr = NULL)
{
//    if (usepred)
//    {
//        assert (pred && "usepred is true but pred == NULL" );
//    }
//    if (usecurr)
//    {
//        assert (curr && "usecurr is true but curr == NULL" );
//    }
    if (usepred && pred) releaseLock(&pred->lock, tid);
    if (usecurr && curr) releaseLock(&curr->lock, tid);
    CarbonRemoveAllFromWatchset();    
}

class lazylist_hmr{
private:
    const unsigned int num_threads;
    const unsigned int min_key;
    const unsigned int max_key;
    
    PAD;
    struct node * head;
    PAD;
    struct node * tail;
    PAD;
    
    bool validate(struct node * pred, struct node * curr);
    void initNode(struct node * n, unsigned int key);
    void printNode(struct node * n);

public:
    lazylist_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key);
    ~lazylist_hmr();
    
    std::pair<struct node *, struct node *> locate(const unsigned int tid, const unsigned int key);

    bool contains(const unsigned int tid, const unsigned int key);
    bool insert(const unsigned int tid, const unsigned int key);
    bool erase(const unsigned int tid, const unsigned int key);
    
    long long getSumOfKeys();
    long long getDSSize();
    void printDebuggingDetails();
};

void lazylist_hmr::initNode(struct node * n, const unsigned int key)
{
    n->key  = key;
    n->mark = false;
    n->next = NULL;
    n->lock = 0; 
}

void lazylist_hmr::printNode(struct node * n)
{
    printf("n(%p)::  ", n);
    printf("n->key(%u), ", n->key);
    printf("n->mark(%u), ", n->mark);
    printf("n->next(%p), ", n->next);
    printf("n->lock(%u)\n", n->lock);
}

lazylist_hmr::lazylist_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads), min_key(min_key),
        max_key(max_key)
{
    printf("ctor::lazylist_hmr\n");

    if(posix_memalign( (void**)&head, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(head, min_key);

    if(posix_memalign( (void**)&tail, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    
    assert(max_key != 0xffffffff && "dont use 0xffffffff in app as its special return status of vread");
    
    initNode(tail, max_key);
    head->next = tail;
}

lazylist_hmr::~lazylist_hmr()
{
    struct node * curr = head;
    struct node * temp_node = NULL;
    
    while(curr != NULL)
    {
        temp_node = curr;
        curr = curr->next;
        
        free(temp_node);
    }
    printf("dtor::lazylist_hmr\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());   
    printf("total_retries=%lld \n", num_retries.getTotal());        

//    printf("~lazylist_hmr: sizeof(struct node)=%d, sizeof(struct node*)=%d\n", sizeof(struct node), sizeof(struct node*));
}

bool lazylist_hmr::validate(struct node * pred, struct node * curr)
{
    return ((!pred->mark) && (!curr->mark) && (pred->next == curr));
}

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
    if (ret_val == 0xffffffff) \
    { \
        CarbonRemoveAllFromWatchset(); \ 
        goto retry; \
    }

//#define try_vread(field_addr, ret_val, field_size) \
//    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size); \
//    DEBUG printf("vread field (%p)\n",(void*)ret_val);
//    if (ret_val == 0xffffffff) \
//    { \
//        CarbonRemoveAllFromWatchset(); \    
//        DEBUG printf("retry (%p)\n", (void*)ret_val); \
//        goto retry; \
//    }

std::pair<struct node *, struct node *> lazylist_hmr::locate(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value
    
    struct node * pred = head;
    CarbonAddToWatchset((void*) pred, sizeof(struct node));

    //tryvread and type cast ret_val and addto watchset if its node address that is read.
    try_vread(&pred->next, ret_val, sizeof(struct node*))    
    struct node * curr = (struct node *)ret_val; //will this cause read of the address in read_val?
    CarbonAddToWatchset((void*) curr, sizeof(struct node));    

    unsigned int curr_key = 0xffffffff;
    while((curr_key = (unsigned int)CarbonValidateAndReadTemp((void*)&curr->key, sizeof(unsigned int))) && curr_key < key)
    {
        CarbonRemoveFromWatchset((void*) pred, sizeof(struct node));        
        pred = curr;
       
        try_vread(&curr->next, ret_val, sizeof(struct node*))    
        curr = (struct node *)ret_val; //will this cause read of the address in read_val?
        CarbonAddToWatchset((void*) curr, sizeof(struct node));    
    }
    
    //check if loop was exited due to failed validation for curr->key read
    if (curr_key == 0xffffffff)
    { 
//        CarbonRemoveAllFromWatchset();    
        cleanUp<>(tid);
        num_retries.add(tid, 1);
        goto retry;
    }
    return std::make_pair (pred, curr);;
}       

bool lazylist_hmr::contains(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node *, struct node *> dest_nodes = locate(tid, key);
    struct node *pred = dest_nodes.first;
    struct node *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting

    try_vread(&curr->mark, ret_val, sizeof(bool))    
    bool curr_mark = (bool)ret_val; //UINFO: downcasting
    bool res = ((!curr_mark) && (curr_key == key));
//    CarbonRemoveAllFromWatchset();    
    cleanUp<>(tid);
    
    return res;
}

//UINFO: remove watched address before returning from all return points of the functions.
bool lazylist_hmr::insert(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node *, struct node *> dest_nodes = locate(tid, key);
    struct node *pred = dest_nodes.first;
    struct node *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting
    if (curr_key == key)
    {
        cleanUp<>(tid);
        return false;
    }   
    
    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0 && "not cachelinesize aligned");

    if (!tryAcquireLock(&pred->lock, tid))
    {
        cleanUp<>(tid);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        cleanUp<true, false>(tid, pred);
        goto retry;        
    }

    if (!validate(pred, curr))
    {
        cleanUp<true, true>(tid, pred, curr);
        goto retry;        
    }
    
    struct node * new_node;
    if(posix_memalign( (void**)&new_node, 64, sizeof(struct node)) != 0)
    {
        printf("Error: posix_memalign could not allocate memory!!! \n");
        exit(EXIT_FAILURE);
    }
    initNode(new_node, key);
    
    num_allocs.add(tid, 1);
    
    new_node->next = curr;
    pred->next = new_node;

    cleanUp<true, true>(tid, pred, curr);
    return true;
}

bool lazylist_hmr::erase(const unsigned int tid, const unsigned int key)
{
    retry:
    unsigned int long ret_val = 0; //UINFO: need to be after retry to avoid gettinga stale value

    std::pair<struct node *, struct node *> dest_nodes = locate(tid, key);
    struct node *pred = dest_nodes.first;
    struct node *curr = dest_nodes.second;
    
    try_vread(&curr->key, ret_val, sizeof(unsigned int))    
    unsigned int curr_key = (unsigned int)ret_val; //UINFO: downcasting
    if (curr_key != key)
    {        
      cleanUp<>(tid);
      return false;
    }
    
    assert(((unsigned long int)pred)%64 == 0 and ((unsigned long int)curr)%64 == 0);

    if (!tryAcquireLock(&pred->lock, tid))
    {
        cleanUp<>(tid);
        goto retry;        
    }
    
    if (!tryAcquireLock(&curr->lock, tid))
    {
        cleanUp<true, false>(tid, pred);
        goto retry;        
    }

    if (!validate(pred, curr))
    {
        cleanUp<true, true>(tid, pred, curr);
        goto retry;        
    }
    
    curr->mark = true;
    pred->next = curr->next;    
    
    cleanUp<true, true>(tid, pred, curr);
    free (curr);
    num_retired.add(tid, 1);
    total_freed.add(tid, 1);

    return true;
}

long long lazylist_hmr::getSumOfKeys()
{
    long long sum = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        sum += curr->key;
        curr = curr->next;
    }
    
    return sum;
}

long long lazylist_hmr::getDSSize()
{
    long long count = 0;
    struct node * curr = head->next;
    while(curr != tail)
    {
        count += 1;
        curr = curr->next;
    }
    
    return count;
}

void lazylist_hmr::printDebuggingDetails()
{
    //print list
    printf("LIST: \n");
    struct node * curr = head;
    while(curr !=  NULL)
    {
//        printf("--> %u(%p)", curr->key, curr);
        printf("--> %u", curr->key);
        curr = curr->next;
    }
    printf("\n");
    printf("total_retired=%lld \n", num_retired.getTotal());
    printf("total_allocated=%lld \n", num_allocs.getTotal());
    printf("total_freed=%lld \n", total_freed.getTotal());
    printf("total_retries=%lld \n", num_retries.getTotal());
}

#endif // LAZYLIST_H
