/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   extbst_asm_hmr.h
 * Author: mc
 *
 * Created on September 27, 2021, 6:50 PM
 */
#ifndef EXTBST_ASM_HMR_H
#define EXTBST_ASM_HMR_H

#include "lock.h"
#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

#define VERROR 0xffffffff

struct asm_node{
    unsigned int key;
    volatile unsigned int lock;
    bool mark;

    struct asm_node* left;
    struct asm_node* right;

    bool isLeaf()
    {
        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;
    }

    bool isParentOf(struct asm_node * other)
    {
        return ((left == other) || (right == other));
    }    
};

class extbst_asm_hmr
{
private:

    struct searchRecord{
        asm_node* volatile	gp; //grand parent
        asm_node* volatile	p; //parent
        asm_node* volatile	n; //leaf asm_node
        unsigned int leaf_key;
        
        searchRecord(asm_node* _gp, asm_node* _p, asm_node* _n, unsigned int _leaf_key): gp(_gp), p(_p), n(_n), leaf_key(_leaf_key){}
    };    
    
    asm_node* volatile root;
    const unsigned int num_threads;
    const unsigned int min_key, max_key;
    
    asm_node* allocateNode(const unsigned int tid)
    {
//        struct asm_node* nd =  new asm_node();
        struct asm_node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(asm_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            fflush(stdout);
            assert(0 && "posix_memalign could not allocate memory");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff && "allocated address cannot be this reserved value");
        
        num_allocs.add(tid, 1);

        return nd;
    }
    
    asm_node* createLeafNode(const unsigned int tid, const unsigned int key)
    {
        return createInternalNode(tid, key, NULL, NULL);
    }
    
    asm_node* createInternalNode(const unsigned int tid, const unsigned int key, asm_node * left, asm_node * right)
    {
        asm_node* new_node = allocateNode(tid);
        new_node->key = key;
        new_node->lock = 0;
        new_node->mark = 0;
        new_node->left = left;
        new_node->right = right;
        return new_node;     
    }
    
public:
    extbst_asm_hmr(const unsigned int _num_threads, const unsigned int _min_key, const unsigned int _max_key): num_threads(_num_threads), min_key(_min_key), max_key(_max_key)
    {
        printf("node size=%lu \n", sizeof(asm_node));
        fflush(stdout);
        asm_node* leftRoot = createLeafNode(0, _min_key);
        asm_node* rightRoot = createLeafNode(0, _max_key);
        root = createInternalNode(0, _min_key, leftRoot, rightRoot);
    }
    
    ~extbst_asm_hmr()
    {
        //free all nodes
        freeTree(root);
        printf("dtor::extbst_asm_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   
        printf("total_retries=%lld \n", num_retries.getTotal());       
    }
    
    void freeTree (struct asm_node* nd)
    {
        if(nd==NULL)
            return;

        freeTree(nd->right);
        freeTree(nd->left);

        free(nd);
    }

    
    unsigned int long isLeaf(struct asm_node* nd, const unsigned int tid)
    {
        asm_node * left = NULL;
        asm_node * right = NULL;

//        printf("tid=%d add to ws %p\n", tid, nd);
        CarbonAddToWatchset((void*) nd, sizeof(struct asm_node));

        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (left)
                : "m" (nd->left)
                ); 
        if ((unsigned int long)left == VERROR)
        { 
            CarbonRemoveAllFromWatchset();
            return VERROR; 
        }
        
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
                :"=r" (right)
                : "m" (nd->right)
                ); 
        if ((unsigned int long)right == VERROR)
        { 
            CarbonRemoveAllFromWatchset();
            return VERROR; 
        }

        bool res = (left == NULL);
        assert (!res || (right == NULL));            
        return res;        
    }
    
    //USAGENOTE: Ensure that you invoke CarbonRemoveAllFromWatchset() at the function 
    // that calls search(). Search return gp, p and n added in watchset.   
    struct searchRecord search(const unsigned int tid, const unsigned int key)
    {
        retry:

        asm_node * gp = NULL;
        asm_node * p = NULL;
        asm_node * n = root;
        
        do
        {
            unsigned int long is_leaf = isLeaf(n, tid);
            if (is_leaf == VERROR)
            {
                //num_retries.add(tid, 1);
//                CarbonRemoveAllFromWatchset(); //already removed by the function returning VERROR
                goto retry;
            }
            else if (1 == is_leaf)
            {
                break; //successfully reached the location
            }
            
            //not a leaf and all vreads succeeded so far continue tree traversal
            // n is in watchset get next node
            
            //get current node key
            unsigned int curr_key;
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr_key)
            : "m" (n->key)
            ); 
            if ((unsigned int long)curr_key == VERROR)
            { 
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry; 
            }
            
            //read left node
            asm_node * curr_left = NULL;            
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr_left)
            : "m" (n->left)
            ); 
            if ((unsigned int long)curr_left == VERROR)
            { 
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry; 
            }

            //read right node
            asm_node * curr_right = NULL;            
            __asm__ __volatile__ (ASF_F2 "mov %1, %0"
            :"=r" (curr_right)
            : "m" (n->right)
            ); 
            if ((unsigned int long)curr_right == VERROR)
            { 
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry; 
            }
            
            if (gp)
            {
                CarbonRemoveFromWatchset((void*) gp, sizeof(struct asm_node));
            }

            gp = p;
            p = n;
            n = (key <= curr_key) ? curr_left : curr_right;            
        }while(true);
        
        //get current node key
        unsigned int curr_key;
        __asm__ __volatile__ (ASF_F2 "mov %1, %0"
        :"=r" (curr_key)
        : "m" (n->key)
        ); 
        if ((unsigned int long)curr_key == VERROR)
        { 
//            printf("curr_right == VERROR retrying OL\n");

            CarbonRemoveAllFromWatchset();
            //num_retries.add(tid, 1);
            goto retry; 
        }
        
        // INV: at this point This thread have gp, p and n in watchset
        // I mean there was a time when gp, p and n were in watchset.
        return searchRecord(gp, p, n, curr_key);
    }
    
    // NOTE for HMR needn't check for mark and next pointers Validation is not needed as VLOCK has implicit validation.
    bool validateIns(asm_node* p, asm_node* n)
    {
        assert(!p->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!n->mark && "Since the node is VLOCKED it cannot be marked.");

        return ( (!(n->mark)) &&(!(p->mark)) && (p->isParentOf(n)) );
    }
    
    // NOTE for HMR needn't check for mark and next pointers Validation is not needed as VLOCK has implicit validation.
    bool validateErase(asm_node* gp, asm_node* p, asm_node* n)
    {
        bool res =  ( (!gp->mark) && (gp->isParentOf(p)) && (!p->mark) && (p->isParentOf(n)) && (!n->mark));
//        printf("%d %d %d %d %d\n", (!gp->mark), (gp->isParentOf(p)), (!p->mark), (p->isParentOf(n)), (!n->mark));
        assert(!gp->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!p->mark && "Since the node is VLOCKED it cannot be marked.");
        assert(!n->mark && "Since the node is VLOCKED it cannot be marked.");
        return res;
    }
    
    bool insert(const unsigned int tid, const unsigned int key){
        retry :
        {
            struct searchRecord ret = search(tid, key);
            if (ret.leaf_key == key)
            {
                CarbonRemoveAllFromWatchset();
                return false; //key already present
            }            

            if (!tryAcquireLock(&ret.p->lock, tid))
            {
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }

            if (!tryAcquireLock(&ret.n->lock, tid))
            {
                releaseLock(&ret.p->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }            

            //decide childs for new newnode
            asm_node* newLeaf = createLeafNode(tid, key);
            bool leftdir = (key < ret.leaf_key);
            asm_node * leftChild = (leftdir) ? newLeaf : ret.n;
            asm_node * rightChild = (leftdir) ? ret.n: newLeaf;
//            asm_node* newInternalNode = createInternalNode(tid, key, leftChild, rightChild);
            assert(ret.leaf_key == ret.n->key);
            asm_node* newInternalNode = (leftdir)? createInternalNode(tid, key, leftChild, rightChild) : createInternalNode(tid, ret.leaf_key, leftChild, rightChild);

            {
                // FIXME: Validation not needed as retry vlocks have implicit validation
                if ( validateIns(ret.p, ret.n) )
                {
                    if(ret.n == ret.p->left)
                    {
                        ret.p->left = newInternalNode;
                    }
                    else{
                        ret.p->right = newInternalNode;
                    }
                    assert(!(ret.p->mark));
                    assert(!(ret.n->mark));
                    
                    releaseLock(&ret.p->lock, tid);
                    releaseLock(&ret.n->lock, tid);
                    CarbonRemoveAllFromWatchset();
                    return true;
                }
                //validation failed
//                printf("tid=%u validation failed key=%u\n", tid, key);
                free(newLeaf);
                free(newInternalNode);
                
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);
                CarbonRemoveAllFromWatchset();
                goto retry;
            }
        } //retry
    }
    
    bool erase(const unsigned int tid, const unsigned int key){

        retry:
        {
            //INV: when search returns gp. p and n are in watchset.
            struct searchRecord ret = search(tid, key);
            
            if (ret.leaf_key != key)
            {
                CarbonRemoveAllFromWatchset();
                return false; //key already present
            }            

            // at this point if other thread has marked any of gp, p & n or 
            // in any way modified gp, p,n then this thread would fail its watchset 
            // validation. Hence safe.
            if (!tryAcquireLock(&ret.gp->lock, tid))
            {
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }


            if (!tryAcquireLock(&ret.p->lock, tid))
            {
                releaseLock(&ret.gp->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }
           
            if (!tryAcquireLock(&ret.n->lock, tid))
            {
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);
                CarbonRemoveAllFromWatchset();
                //num_retries.add(tid, 1);
                goto retry;        
            }            
           // after all gp, p and n have been locked then no other thread could modify
           // these nodes by virtue of mutual exclusion provided by VLOCKs
           // That means I can assert that gp p and n are never seen as marked.
            {
                // FIXME: Validation not needed as retry vlocks have implicit validation
                if ( validateErase(ret.gp, ret.p, ret.n) )
                {
                    //mark n and p any thread doing concurrent vread/vwrite will fail
                    // as marking will cause watchset to be downgraded.
                    assert(!(ret.gp->mark));
                    assert(!(ret.p->mark));
                    assert(!(ret.n->mark));
                    
                    ret.p->mark =  true;
                    ret.n->mark =  true;

                    asm_node * sibling = (ret.p->left == ret.n) ? ret.p->right : ret.p->left;
                    if (ret.gp->left == ret.p)
                    {
                        ret.gp->left = sibling;
                    }
                    else
                    {
                        ret.gp->right = sibling;
                    }

                    releaseLock(&ret.gp->lock, tid);
                    releaseLock(&ret.p->lock, tid);       
                    releaseLock(&ret.n->lock, tid);
                    CarbonRemoveAllFromWatchset();

                    free (ret.p);
                    free (ret.n);
                
                    num_retired.add(tid, 1);
                    total_freed.add(tid, 1);

                    return true;

                }
                releaseLock(&ret.gp->lock, tid);
                releaseLock(&ret.p->lock, tid);
                releaseLock(&ret.n->lock, tid);  
                CarbonRemoveAllFromWatchset();
            }
        }//while
        
        return false;
    }
    
    bool contains(const unsigned int tid, const unsigned int key){
        struct searchRecord ret = search(tid, key);
        // It safe and correct to return leaf_key as there was a time
        // when this thread vread leaf node and read the leaf_key during search operation.
        assert ((ret.leaf_key <= max_key) && (ret.leaf_key > min_key));
        CarbonRemoveAllFromWatchset();
        return (key == ret.leaf_key);
    }
    
    long long sumLeafKeys(asm_node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key) )
            return nd->key;
        else
            return sumLeafKeys(nd->left) + sumLeafKeys(nd->right);                       
    }    
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      sum = sumLeafKeys(root);
      return sum;
    }
    
    long long countLeafs(asm_node *nd)
    {
        if (nd == NULL)
            return 0;
        if (nd->isLeaf() && ( nd->key > min_key) && (nd->key < max_key)  )
            return 1;
        else
            return countLeafs(nd->left) + countLeafs(nd->right);                       
    }
  
    
   long long getDSSize()
   {
      long long size = 0;
      size = countLeafs(root);
      return size;
   }      
    
    void printTree(asm_node * nd)
    {
        if (nd == NULL)
            return;
        printTree(nd->left);
        assert(!nd->mark);
//        printf("%u(%p) ", nd->key, nd);
        std::cout << nd->key<<"-"<<nd<<" ";
        assert(!nd->mark);

        printTree(nd->right);               
    }

    void printLeafs(asm_node *nd)
    {
        if (nd == NULL)
            return;
        if (nd->isLeaf())
        {
            printf("%u ", nd->key);
        }
        printLeafs(nd->left);
        printLeafs(nd->right);                       
    }

    
    void printDebuggingDetails()
    {
//        std::cout<<"Printing tree:\n";
//        printTree(root);
//        std::cout<<std::endl;

        std::cout<<"Printing leafs:\n";
        printLeafs(root);
        std::cout<<std::endl;        
        
    }       
};

#endif /* EXTBST_ASM_HMR_H */

