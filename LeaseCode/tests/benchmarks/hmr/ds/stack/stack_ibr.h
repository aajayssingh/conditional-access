/* 
 * File:   stack_ibr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_IBR_H
#define STACK_IBR_H

#include "ibr.h"

struct node_ibr; //fwd declaration to satiate complaining compiler.
ibr<struct node_ibr> *rec_ibr = NULL;

struct node_ibr{
    unsigned int value;
    std::atomic<struct node_ibr*> next;

    //rec based members
    uint64_t birth_epoch;
    uint64_t retire_epoch;

    node_ibr(unsigned int key): value(key)
    {
        next = NULL;
        assert(rec_ibr !=  NULL);
        
        birth_epoch = rec_ibr->getEpoch();
        retire_epoch = UINT64_MAX;
    }
};


class stack_ibr
{
private:
    const unsigned int num_threads;
    PAD;
    std::atomic<struct node_ibr*> _top;
    PAD;
    node_ibr* sentinal_node; // at the start _top points to this dummy node_ibr with value 0
//    PAD;
//    rec_ibr<struct node_ibr> *rec_ibr;

public:
   stack_ibr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        rec_ibr = new ibr<struct node_ibr>(num_threads);
 
        sentinal_node = new node_ibr(0);
        _top.store(sentinal_node, std::memory_order_relaxed);
   }
   ~stack_ibr()
   {
        node_ibr* next_node = _top.load(std::memory_order_relaxed);
        node_ibr* temp_node;
        while( next_node != NULL) // include sentinel node_ibr as well
        {
            temp_node = next_node;
            next_node = next_node->next.load(std::memory_order_relaxed);
            free(temp_node);
        }
       
        printf("dtor::stack_ibr\n");
//        printf("total_retired=%lld \n", num_retired.getTotal());
//        printf("total_allocated=%lld \n", num_allocs.getTotal());
//        printf("total_freed=%lld \n", total_freed.getTotal());       
        printf("total_retries=%lld \n", num_retries.getTotal());   
        delete rec_ibr;
   }
   
   unsigned int peek(const unsigned int tid) 
   {
        rec_ibr->startOp(tid);    
        
        node_ibr* curr_top = rec_ibr->read(tid, _top);
        unsigned int top_val = curr_top->value;
        rec_ibr->endOp(tid);
        return top_val;
    }
   
   bool push(const unsigned int tid, const unsigned int key) 
   {
        rec_ibr->startOp(tid);    
        bool success = false;
        node_ibr* nd = new node_ibr(key);
//        num_allocs.add(tid, 1);
        rec_ibr->incAllocCounter(tid);
        do
        {
            node_ibr* curr_top = rec_ibr->read(tid, _top);
            nd->next = curr_top;

            // Perform CAS Operation
//            success =  __sync_bool_compare_and_swap(&_top, curr_top, nd);
            success = _top.compare_exchange_strong(curr_top, nd);
            if(success)
            {
                rec_ibr->endOp(tid);
                return true;
            }
//            num_retries.add(tid, 1);
        } while(true);
    }

    unsigned int pop(const unsigned int tid)
    {
        rec_ibr->startOp(tid);    

        bool success = false;
        do {
                node_ibr* curr_top = rec_ibr->read(tid, _top);
                if(curr_top == sentinal_node)
                {
                    rec_ibr->endOp(tid);
                    return false;   //empty stack
                }
                // Perform CAS Operation
                success =  _top.compare_exchange_strong(curr_top, curr_top->next);

                if( success ) 
                {
                    unsigned int top_val = curr_top->value;
                    rec_ibr->retire(tid, curr_top);
                    rec_ibr->endOp(tid);
                    return top_val;
                }
//                num_retries.add(tid, 1);
        } while(true);
    }
    
    long long getSumOfKeys()
    {
      long long sum = 0;
      node_ibr* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         sum = sum + next_node->value;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return sum;
    }

   long long getDSSize()
   {
      long long size = 0;
      node_ibr* next_node = _top.load(std::memory_order_relaxed);

      while( next_node != sentinal_node){
         size ++;
         next_node = next_node->next.load(std::memory_order_relaxed);
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        node_ibr* next_node = _top.load(std::memory_order_relaxed);

        while( next_node != NULL)
        {
            printf("-->%u", next_node->value);
            next_node = next_node->next.load(std::memory_order_relaxed);
        }
        printf("\n");
    }   

};

#endif /* STACK_IBR_H */

