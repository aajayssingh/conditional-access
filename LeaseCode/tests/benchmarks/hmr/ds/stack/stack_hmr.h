/* 
 * File:   stack_hmr.h
 * Author: mc
 *
 * Created on August 16, 2021, 9:18 PM
 */

#ifndef STACK_HMR_H
#define STACK_HMR_H

#include "lock.h"
#include "util.h"

//repnz prefix isRePne
#define ASF_F2\
   ".byte 0xF2\n\t"

//repz prefix isREP
#define ASF_F3\
   ".byte 0xF3\n\t"

#define try_vread(field_addr, ret_val, field_size) \
    ret_val = CarbonValidateAndReadTemp((void*)field_addr, field_size);\
    if (ret_val == 0xffffffff)\
    {\
        CarbonRemoveAllFromWatchset();\
        continue;\
    }


struct hmr_node 
{
    unsigned int value;
    bool mark;
    struct hmr_node* next;
}; //__attribute__((aligned(PADDING_BYTES)));

        
class stack_hmr
{
private:
    const unsigned int num_threads;
    PAD;
    struct hmr_node* _top_sentinel;
    struct hmr_node* _bottom_sentinel;
    PAD;

    hmr_node* allocateAndInit(const unsigned int val)
    {
        struct hmr_node* nd;
        if(posix_memalign( (void**)&nd, 64, sizeof(hmr_node)) != 0)
        {
            printf("Error: posix_memalign could not allocate memory!!! \n");
            exit(EXIT_FAILURE);
        }
        
        assert (((unsigned int long)nd) != 0xffffffff);
        //init posix mem allocated node
        nd->value = val;
        nd->mark = 0;
        //        printf("adding tid(%u) key=%u val=%u \n", tid, key, nd->value);
        nd->next = NULL;
        
        return nd;
    }
    
public:
   stack_hmr(const unsigned int num_threads, const unsigned int min_key,
        const unsigned int max_key): num_threads(num_threads)
   {
        _bottom_sentinel = allocateAndInit(min_key);
        _top_sentinel = allocateAndInit(max_key);

        _top_sentinel->next = _bottom_sentinel;
        
        printDebuggingDetails();
    }
   ~stack_hmr()
   {
        struct  hmr_node* next_hmr_node = _top_sentinel;
        struct  hmr_node* temp_asm_node;
        while( next_hmr_node != NULL)
        {
            temp_asm_node = next_hmr_node;
            next_hmr_node = next_hmr_node->next;
            free(temp_asm_node);
        }
       
        printf("dtor::stack_hmr\n");
        printf("total_retired=%lld \n", num_retired.getTotal());
        printf("total_allocated=%lld \n", num_allocs.getTotal());
        printf("total_freed=%lld \n", total_freed.getTotal());   
        printf("total_retries=%lld \n", num_retries.getTotal());        
   }
   
    unsigned int peek(const unsigned int tid) 
    {
        unsigned int long ret_val = 0;
        do
        {
            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct hmr_node));

            hmr_node* firsthmr_node;
            try_vread(&_top_sentinel->next, ret_val, sizeof(struct hmr_node*))    
            firsthmr_node = (struct hmr_node *)ret_val;

            if(firsthmr_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // stack is empty
            }

            CarbonAddToWatchset((void*) firsthmr_node, sizeof(struct hmr_node));

            unsigned int firsthmr_node_val;
            try_vread(&firsthmr_node->value, ret_val, sizeof(unsigned int))    
            firsthmr_node_val = (unsigned int)ret_val;
            
            bool firsthmr_node_mark;
            try_vread(&firsthmr_node->mark, ret_val, sizeof(bool))    
            firsthmr_node_mark = (bool)ret_val;

            assert(firsthmr_node_mark != 1);
            
            CarbonRemoveAllFromWatchset();    
            return firsthmr_node_val;
        }while(true);
    }

   //NOTE: The way VREAD works is you add a hmr_node to ws and then you vread its field.
   // vreading the hmr_node itself fails validation. 
   // Later may check whether that is correct behaviour of API spec or not.
   // Therefore I cannt validate top and have to use top->next as real pointer to top.
    bool push(const unsigned int tid, const unsigned int key) 
    {
        unsigned int long ret_val = 0;
        struct hmr_node* nd = allocateAndInit(key);
        num_allocs.add(tid, 1);

        do
        {
            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct hmr_node));
            try_vread(&_top_sentinel->next, ret_val, sizeof(struct hmr_node*))    
            nd->next = (struct hmr_node *)ret_val;

            bool write_status = CarbonValidateAndWrite((void*)&_top_sentinel->next, (unsigned int long)nd, sizeof(hmr_node*));
            
            if (write_status)
            {
               CarbonRemoveAllFromWatchset();
               return true;
            }

            CarbonRemoveAllFromWatchset();
//            num_retries.add(tid, 1);
        }while(true);
     }
    
    //returs value in hmr_node. 0 < value < max_key
    unsigned int pop(const unsigned int tid)
    {
        unsigned int long ret_val = 0;
        do
        {
            CarbonAddToWatchset((void*) _top_sentinel, sizeof(struct hmr_node));

            hmr_node* firsthmr_node;
            try_vread(&_top_sentinel->next, ret_val, sizeof(struct hmr_node*))    
            firsthmr_node = (struct hmr_node *)ret_val;
            
            if(firsthmr_node == _bottom_sentinel)
            {
                CarbonRemoveAllFromWatchset();    
                return false;   // TODO: This should be a NULL value
            }
            
            bool write_status = CarbonValidateAndWrite((void*)&_top_sentinel->next, (unsigned int long)firsthmr_node->next, sizeof(hmr_node*));
            
            if (write_status)
            {
//                firsthmr_node->mark = true; MARKING not needed if empty based validation used
                unsigned int deleted_val = firsthmr_node->value;
                free(firsthmr_node);
                num_retired.add(tid, 1);
                total_freed.add(tid, 1);
    
                CarbonRemoveAllFromWatchset();
                return deleted_val; //firsthmr_node->value;
            }

            CarbonRemoveAllFromWatchset();    
//            num_retries.add(tid, 1);
        }while(true);
    }
    
    long long getSumOfKeys()
    {
      int sum = 0;
      hmr_node* next_hmr_node = _top_sentinel->next;

      while( next_hmr_node != _bottom_sentinel){
         sum = sum + next_hmr_node->value;
         next_hmr_node = next_hmr_node->next;
      }

      return sum;
    }

   long long getDSSize()
   {
      int size = 0;
      hmr_node* next_hmr_node = _top_sentinel->next;

      while( next_hmr_node != _bottom_sentinel){
         size ++;
         next_hmr_node = next_hmr_node->next;
      }

      return size;
   }  

    void printDebuggingDetails()
    {
        hmr_node* next_hmr_node = _top_sentinel;

        while( next_hmr_node != NULL)
        {
//            printf("-->%u(%p)(%d) ", next_hmr_node->value, next_hmr_node, next_hmr_node->mark);
//            printf("-->%u", next_hmr_node->value);
            assert(next_hmr_node->mark == 0 && "mark bit should be false");
            
            next_hmr_node = next_hmr_node->next;
        }
        printf("\n");
    }   

};

#endif /* STACK_HMR_H */

