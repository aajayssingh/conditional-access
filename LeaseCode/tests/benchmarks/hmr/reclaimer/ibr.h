/* 
 * File:   ibr.h
 * Author: mc
 *
 * Created on May 14, 2021, 5:31 PM
 * Ibr has per read overhead. Needs instrumentation of ds nodes for birth and retire epochs.
 * Robust???
 */

#ifndef IBR_H
#define IBR_H

#include <list>
#include "concurrent_primitives.h"
#include "util.h"

template <typename T>
class ibr{
private:
    const unsigned int numProcesses;
    unsigned int empty_freq;
    unsigned int epoch_freq;
    
    paddedAtomic<uint64_t> *upper_reservs; //per thread
    paddedAtomic<uint64_t> *lower_reservs; //per thread

    //per thread static allocation to fix why ibr is faster than none. 
    // conclusion: Didnt make any diff
//    paddedAtomic<uint64_t> upper_reservs[64]; 
//    paddedAtomic<uint64_t> lower_reservs[64]; //per thread
    
    padded<uint64_t> *retire_counters; //per thread
    padded<uint64_t> *alloc_counters; //per thread
    
    padded<std::list<T*>> *retired;
    
    PAD;
    std::atomic<uint64_t> epoch;//FIXME: may be use padded atomic as two PADS may screw up perf due to prefetch
    PAD;
            
public:

    inline uint64_t getEpoch()
    {
        return epoch.load(std::memory_order_acquire);
    }

    inline bool startOp(const unsigned int tid, const bool read_only = false)
    {
        bool result = true;
        
        uint64_t e = getEpoch();//epoch.load(std::memory_order_acquire);
//        printf ("startOp:: epoch=%lld\n", e);
        lower_reservs[tid].ui.store(e, std::memory_order_seq_cst);
        upper_reservs[tid].ui.store(e, std::memory_order_seq_cst);

        return result;
    }
    
    inline void endOp(const unsigned int tid)
    {
        upper_reservs[tid].ui.store(UINT64_MAX, std::memory_order_release);
        lower_reservs[tid].ui.store(UINT64_MAX, std::memory_order_release);
    }
    
//    void read(const unsigned int tid, T *obj)
    T* read(const unsigned int tid, std::atomic<T*>& obj)
    {
        uint64_t prev_epoch = upper_reservs[tid].ui.load(std::memory_order_acquire);
        while (true)
        {
            T* ptr = obj.load(std::memory_order_acquire);
            uint64_t curr_epoch = getEpoch();
            if (curr_epoch == prev_epoch)
            {
                return ptr; //fast path to avoid a store if epoch hasnt changed
            }
            else
            {
                // upper_reservs[tid].ui.store(curr_epoch, std::memory_order_release);
                upper_reservs[tid].ui.store(curr_epoch, std::memory_order_seq_cst);
                prev_epoch = curr_epoch;
            }
        }
    }
  
    // to be called when a node is succesfully allocated.
    inline void incAllocCounter(const unsigned int tid)
    {
        alloc_counters[tid] = alloc_counters[tid] + 1;     
        //simulating when to inc epoch, ibr has epoc freq as function of num allocs.
        //I shall have numops count instead of alloc count.
        if (alloc_counters[tid] % (epoch_freq) == 0)
        {
            epoch.fetch_add(1, std::memory_order_acq_rel);
        }
    }
    
    inline void retire(const unsigned int tid, T *obj)
    {
        if (obj == NULL)
        {
            return;
        }
        
        std::list<T*> *myTrash = &(retired[tid].ui);
        // for(auto it = myTrash->begin(); it!=myTrash->end(); it++){
        // 	assert(it->obj!=obj && "double retire error");
        // }

        uint64_t retire_epoch = getEpoch();//epoch.load(std::memory_order_acquire);
        assert (obj->retire_epoch == UINT64_MAX 
                && "retire epoch shall be set here the first time!");
        obj->retire_epoch = retire_epoch; //set retire epoch
        
        myTrash->push_back(obj); // add obj in my limbo bag
        retire_counters[tid] = retire_counters[tid] + 1;
        if (retire_counters[tid] % empty_freq == 0)
        {
            empty(tid);
        }
        
    }

    bool conflict(uint64_t *lower_epochs, uint64_t *upper_epochs, uint64_t birth_epoch, uint64_t retire_epoch)
    {
        for (int i = 0; i < numProcesses; i++)
        {
            if (upper_epochs[i] >= birth_epoch && lower_epochs[i] <= retire_epoch)
            {
                return true;
            }
        }
        return false;
    }    
    
    void empty(const unsigned int tid)
    {
        //read all epochs
        uint64_t upper_epochs_arr[numProcesses]; //per thread upper_reservs
        uint64_t lower_epochs_arr[numProcesses]; //per thread lower_reservs
        for (int i = 0; i < numProcesses; i++)
        {
            //sequence matters.
            lower_epochs_arr[i] = lower_reservs[i].ui.load(std::memory_order_acquire);
            upper_epochs_arr[i] = upper_reservs[i].ui.load(std::memory_order_acquire);
        }

        // erase safe objects
        std::list<T*> *myTrash = &(retired[tid].ui);

//        int b4_empty_sz = myTrash->size();

        for (auto iterator = myTrash->begin(), end = myTrash->end(); iterator != end;)
        {
            T* ptr = *iterator;
            if (!conflict(lower_epochs_arr, upper_epochs_arr, ptr->birth_epoch, ptr->retire_epoch))
            {
//                printf("tid=%d when retiring object has be=%lld re=%lld\n"
//                        , tid, ptr->birth_epoch, ptr->retire_epoch);
                free(ptr);
                
                total_freed.add(tid, 1);
                
                iterator = myTrash->erase(iterator); //return iterator corresponding to next of last erased item
            }
            else
            {
                ++iterator;
            }
        }
//        printf("ibr::tid=%d emptying: before sz=%d after sz=%d\n", tid, b4_empty_sz, myTrash->size());
    }
    
    ibr(const unsigned int _numProcesses) : numProcesses(_numProcesses)
    {
        empty_freq = EMPTY_FREQ; //30; //100
        epoch_freq = EPOCH_FREQ; //150; //50 //150;
        retired = new padded<std::list<T*>>[numProcesses];
        upper_reservs = new paddedAtomic<uint64_t>[numProcesses];        
        lower_reservs = new paddedAtomic<uint64_t>[numProcesses];        
        retire_counters = new padded<uint64_t>[numProcesses];
        alloc_counters = new padded<uint64_t>[numProcesses];
        
        for (int i = 0; i < numProcesses; i++)
        {
            upper_reservs[i].ui.store(UINT64_MAX, std::memory_order_release);
            lower_reservs[i].ui.store(UINT64_MAX, std::memory_order_release);
            retired[i].ui.clear();
            retire_counters[i].ui = 0;
            alloc_counters[i].ui = 0;            
        }
        epoch.store(0, std::memory_order_release);
    }
    
    ~ibr()
    {
        unsigned int total_retired = 0;
        unsigned int total_allocated = 0;
//        unsigned int total_reclaimed = 0;
        
        printf("\n pt_retired=");
        for (int i = 0; i < numProcesses; i++)
        {
            printf("%lu ", retire_counters[i].ui);        
        }
        printf("\n pt_allocated=");
        for (int i = 0; i < numProcesses; i++)
        {
            total_retired += retire_counters[i].ui;
            total_allocated += alloc_counters[i].ui;
            printf("%lu ", alloc_counters[i].ui);        
        }
        printf("\n");

        printf("Epoch=%lu \n", getEpoch());
        printf("total_retired=%u \n", total_retired);
        printf("total_allocated=%u \n", total_allocated);
        printf("total_freed=%lld \n", total_freed.getTotal());

        delete [] upper_reservs;
        delete [] lower_reservs;
        delete [] retired;
        delete [] retire_counters;
        delete [] alloc_counters;
    }
};

#endif /* IBR_H */

