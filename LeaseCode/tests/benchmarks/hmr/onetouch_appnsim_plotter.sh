#!/bin/bash
# README
# Directory structure at modern machine exp_result/exp_out_5050*/.....
# execute simstat_runme.sh from exp_result/ --> this will create simplots/ in each experiment folder[exp_out_*/]
# 
# Also will create throughput plots 
#
# How to run: copy this file one level up at modern machine and run.

for dir in `ls -d -- */`
do
cddir=$dir"scripts/"
echo "inside $cddir"

echo "plotting simstats"
`cd $cddir; echo pwd ;./parse_and_format_simoutput.sh`
echo "created simplots/ in $dir"

echo "plotting appstats"
`cd ${cddir}; python3 appstatplotter.py`
echo "created throughputappstat*.png in $dir"
echo ""
done
