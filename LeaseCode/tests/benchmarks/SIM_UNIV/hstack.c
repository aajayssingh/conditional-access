#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <omp.h>
#include <string.h>
#include <stdint.h>

#include "config.h"
#include "primitives.h"
#include "rand.h"
#include "thread.h"

#include "hstack.h"

#include "carbon_user.h"

StackHSynchStruct object_struct CACHE_ALIGN;
int64_t d1 CACHE_ALIGN, d2;

void SHARED_OBJECT_INIT(void) {
    stackHSynchInit(&object_struct);   
}

// Global Variables
pthread_barrier_t barr;

inline static void Execute(void* Arg) {
    HStackThreadState lobject_struct;
    long i, rnum;
    volatile int j;
    long id = (long) Arg;

    setThreadId(id);
    //_thread_pin(id);
    simSRandom(id + 1L);
    stackThreadStateInit(&object_struct, &lobject_struct, (int)id);
    if (id == N_THREADS - 1)
        d1 = getTimeMillis();
    // Synchronization point
    int rc = pthread_barrier_wait(&barr);
    if (rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD) {
        printf("Could not wait on barrier\n");
        exit(-1);
    }
    start_cpu_counters(id);
    for (i = 0; i < RUNS; i++) {
        // perform a push operation
        applyPushPop(&object_struct, &lobject_struct, POP_OP, id);
#ifdef DEBUG
        __successful_ops[id].v++;
#endif
        rnum = simRandomRange(1, MAX_WORK);
        for (j = 0; j < rnum; j++)
            ; 
        // perform a pop operation
        applyPushPop(&object_struct, &lobject_struct, id, id);
#ifdef DEBUG
        __successful_ops[id].v++;
#endif
        rnum = simRandomRange(1, MAX_WORK);
        for (j = 0; j < rnum; j++)
            ; 
    }
    stop_cpu_counters(id);
}

inline static void* EntryPoint(void* Arg) {
    Execute(Arg);
    return NULL;
}

inline static pthread_t StartThread(int arg) {
    long id = (long) arg;
    void *Arg = (void*) id;
    pthread_t thread_p;
    int thread_id;

    /*
    pthread_attr_t my_attr;
    pthread_attr_init(&my_attr);
    thread_id = pthread_create(&thread_p, &my_attr, EntryPoint, Arg);
    */

    thread_id = pthread_create(&thread_p, NULL, EntryPoint, Arg);
    return thread_p;
}

int main(void) {
    pthread_t threads[N_THREADS];
    int i;
    // Barrier initialization
    if (pthread_barrier_init(&barr, NULL, N_THREADS)) {
        printf("Could not create the barrier\n");
        return -1;
    }

    SHARED_OBJECT_INIT();

    // ========================================================================== //
    // Enable Carbon Perf Models
    // =========================================================================== //
    CarbonEnableModels();
    
    for (i = 1; i < N_THREADS; i++)
        threads[i] = StartThread(i);
   
    // Thread 0 called by Main
    long id = 0;
    void *Arg = (void*) id;
    EntryPoint(Arg);

    // Joining
    for (i = 1; i < N_THREADS; i++)
        pthread_join(threads[i], NULL);
    
    CarbonDisableModels();
    // ========================================================================== //
   
    /*
    for (i = 0; i < N_THREADS; i++)
        threads[i] = StartThread(i);

    for (i = 0; i < N_THREADS; i++)
        pthread_join(threads[i], NULL);
   */

    d2 = getTimeMillis();

    unsigned long _total_successful_ops = 0;
    for (i = 0; i < N_THREADS; i++)
       _total_successful_ops += __successful_ops[i].v;
    
    printf("time: %d\t", (int) (d2 - d1));
    printStats();

    printf("\n\n");
#ifdef DEBUG
    printf("Successful Ops = %lu \n", _total_successful_ops );
    printf("Throughput (Ops/s) = %f \n", (float)(_total_successful_ops)*1000/DURATION );
    printf("\n\n");
    
    fprintf(stderr, "object state:    counter: %d rounds: %d\n", object_struct.object_struct.counter, object_struct.object_struct.rounds);
#endif

    if (pthread_barrier_destroy(&barr)) {
        printf("Could not destroy the barrier\n");
        return -1;
    }
    return 0;
}
