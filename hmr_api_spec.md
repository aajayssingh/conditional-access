# HMR Specification

HMR is an ISA extension that allows threads to safely access shared data without
epochs, hazard pointers, or other software-based memory protection.  The core
idea is to add a way to *tag* memory locations.  If a tagged location is evicted
from a thread's cache, then subsequent validating reads (`vread`) and validating
writes (`vwrite`) will fail without faulting.

It appears that this design subsumes the functionality of
Load-Linked/Store-Conditional, and also enables a sort of nonfaulting K-LL (load
linked of location X only if up to K preceding load linked instructions are
still in-cache), and a K-LL-1-SC (store conditional to location X only if up to
K preceding load linked instructions are still in-cache).  Note that in all
cases, the instructions would likely only be obstruction free.

## New Architectural State

For the time being, this discussion is focused on an x86_64 extension.  The
decision to focus on x86_64 is partly due to convenience (we are familiar with
it), and partly because our simulation of HMR uses Graphite, and hence is for
the x86_64 ISA.

### `TagSet`

We introduce a new structure, `TagSet`.  `TagSet` is a low-latency,
per-hardware-thread unordered map.  It maps physical addresses of cache blocks
(46-bit values) to small unsigned integers (8 bit values for now).

There are at least three implementations of TagSet:

* As a separate physical structure from the cache.
* As part of the cache
* As part of the cache, but with only one bit per block (with a special victim
  counter)

### Additions to `EFLAGS`

We will require a low-latency mechanism for knowing when a `vread` or `vwrite`
fails.  The most natural approach is to extend the `EFLAGS` register with a new
bit: `EFLAGS.V`.  The bit defaults to unset.  When a `vread` or `vwrite` fails,
the bit is set.  The bit gets cleared when it is read, or as part of the
`cleartags` instruction.

## New Instructions

Below we describe the instructions that are added to the x86_64 ISA.  Note that
the simulator does not allow us to add new instruction opcodes.  A later section
will discuss how these new instructions are realized within Graphite.

### `tag` -- Fetch Data and Add Cache Line to `TagSet`

The `tag` instruction fetches a cache line.  If the cache line address is not
present in the `TagSet`, then it is added to the `TagSet` with a count of 1.  If
the cache line address is present in the `TagSet`, and the count is nonzero,
then the count is incremented.  If the cache line address is present in the
`TagSet`, but the count is zero, then the count is not changed.

* Instruction Format: `tag (%0)`
* Argument: `%0`: A register holding a virtual address or an immediate address.
* Addressing modes: Any/all can be supported.
* Effects: Updates the `TagSet` as follows:
  * Let `P` be the 46-bit physical block address corresponding to the
    translation of `%0`
  * If `TagSet[P]` is not defined, set `TagSet[P]` to `1`
  * If `TagSet[P] > 0`, update `TagSet[P]` to the value `1 + TagSet[P]`
  * If `TagSet[P] == 0`, do not update `TagSet`
* Fault: If the CPU is unable to translate `%0` to `P`, a segmentation fault will
  be raised.

**errors** Do we need to distinguish between tagging for `vread` and tagging for
`vwrite`?  If a thread calls `tag` on an address that is in `SHARED` state in
the cache, is it actually possible for a subsequent `vwrite` to be implemented
correctly?

### `untag` -- Remove Cache Line from `TagSet`

The `untag` instruction allows a programmer to indicate that a tagged address is
no longer interesting, and should no longer be tracked.

* Instruction Format: `untag (%0)`
* Argument: `%0`: A register holding a virtual address, or an immediate address.
* Addressing modes: Any/all can be supported.
* Effects: Updates the `TagSet` as follows:
  * Let `P` be the 46-bit physical block address corresponding to the
    translation of `%0`
  * If `TagSet[P]` is not defined, do nothing
  * If `TagSet[P] > 1`, update `TagSet[P]` to the value `TagSet[P] - 1`
  * If `TagSet[P] == 1`, update `TagSet` by removing the mapping for `P`.
* Fault: None

### `vread` -- Read if `TagSet` Valid

The `vread` instruction reads data from the cache.  In most regards, it is like
the existing x86_64 `mov` instruction.  The key differences are that (1) the
physical address of the access must be in the `TagSet` with a nonzero count, and
(2) if the data is not in the cache, the `vread` fails without faulting.

* Instruction Format: `vreadSS (%0), %1`
* Argument `%0`: A register holding a virtual address that will be read, or an
  immediate address.
* Argument `%1`: A register that will hold the value that was read %0.
* Argument `SS`: A size specifier.  This will be encoded in the opcode, and also
  can be inferred by the size of `%1`.  For example, `vread (%rax), %bl` has
  an `SS` of one byte.
* Addressing modes: Any/all can be supported.
* Alignment: The SS-length sequence of bytes, starting at `%0`, must all be on
  the same cache line.
* Effects: Update `%1` as follows:
  * Let `P` be the 46-bit physical block address corresponding to the
    translation of %0.
  * If `TagSet[P]` is nonzero, then update `%1` as if this were a `mov`
    instruction.
  * If `TagSet[P]` is zero, then set `EFLAGS.V`.
* Fault: If `TagSet[P]` is not defined, a segmentation fault will be raised.

### `vwrite` -- Write if `TagSet` Valid

The `vwrite` instruction writes data to memory.  In most regards, it is like the
existing x86_64 `mov` instruction.  the key differences are that (1) the
physical address of the access must be in the `TagSet` with a nonzero count, and
(2) if the line is not in the cache, the `vwrite` fails without faulting.

* Instruction Format: `vwriteSS %0, (%1)`
* Argument `%0`: A register holding a value to write, or an immediate value
* Argument `%1`: A register holding a virtual address that will be updated, or
  an immediate address.
* Argument `SS`: A size specifier.  This will be encoded in the opcode, and also
  can be inferred by the size of `%0`.  For example, `vwrite %eax, (%rcx)` has
  an `SS` of four bytes.
* Addressing modes: Any/all can be supported.
* Alignment: The SS-length sequence of bytes, starting at `%1`, must all be on
  the same cache line.
* Effects: Update memory as follows:
  * Let `P` be the 46-bit physical block address corresponding to the
    translation of %0.
  * If `TagSet[P]` is nonzero, then update `P` as if this were a `mov`
    instruction.
  * If `TagSet[P]` is zero, then set `EFLAGS.V`.
* Fault: If `TagSet[P]` is not defined, a segmentation fault will be raised.

### `cleartags` -- Clear the `TagSet`

The `cleartags` instruction clears the `TagSet` and also clears `EFLAGS.V`.

* Instruction Format: `cleartags`
* Effects: Removes all mappings from the `TagSet`
* Fault: None

### `jv` -- Jump if `EFLAGS.V`

The `jv` instruction is a conditional jump, only if `v` is set.

## Extensions to Existing Events

### Cache Eviction

When the cache line with 46-bit physical block address `P` is evicted, if
`TagSet[P]` is defined, then set `TagSet[P]` to zero.  An eviction may be due to
a coherence event, capacity, or associativity conflict.

**errors** For the time being, we are not thinking about M to S and other cache
state downgrades.  We are only thinking about present/not present.

### Context Switches

On a context switch, clear `TagSet`.  The operating system is responsible for
logging the entire `EFLAGS`, including `EFLAGS.v`, and restoring it when the
program swaps back in.

## Simulator API

We cannot implement the proposed instruction set in Graphite, because PIN is not
open-source, and thus we cannot change the instruction decoder to accept new
opcodes.  This is problematic, because the default way to hook into Graphite is
by issuing a function call, and making a function call when a single asm
instruction would suffice causes a substantial slowdown due to code generation
(primarily register allocation issues).  Additionally, we do not have a
mechanism for adding a bit to `EFLAGS`.  To compensate for these deficiencies,
we leverage the following simulator API:

### `set_vflag(volatile bool*)` -- Provide address for `EFLAGS.V` emulation

This Graphite function should be called once per thread.  It notifies Graphite
that the provided virtual address should be used to simulate `EFLAGS.V`.
Whenever Graphite would otherwise set `EFLAGS.V`, it will instead set this
program data to 1.

### `if (my_eflag)` -- Simulate the behavior of `jv`

Since we cannot simulate `jv` effectively, we use this C++ statement to get a
two-instruction sequence.  The `my_eflag` argument should be a thread-local
`volatile bool`, which was previously passed to `set_vflag` by the calling
thread.  The assembly sequence will consist of a `test` that sets `ZF`, and then
a `jnz`.

### `lock adc %0, %0` -- `tag (%0)`

When Graphite encounters the `lock adc` instruction, it will check if the two
operands are the same register, in r64/r64 mode.  If so, then instead of
performing a `lock adc`, it will execute its instruction sequence for updating
the thread's `TagSet`.

### `lock sbb %0, %0` -- `untag (%0)`

When Graphite encounters the `lock sbb` instruction, it will check if the two
operands are the same register, in r64/r64 mode.  If so, then instead of
performing a `lock sbb`, it will execute its instruction sequence for updating
the thread's `TagSet`.

### `lock mov (%0), %1` -- `vread (%0), %1`

When graphite encounters a `lock mov` instruction, with a memory operand as the
first operand, it will perform a `vread`.  This can result in the thread's
`my_eflag` getting set.

### `lock mov %0, (%1)` -- `vwrite %0, (%1)`

When graphite encounters a `lock mov` instruction, with a memory operand as the
second operand, it will perform a `vwrite`.  This can result in the thread's
`my_eflag` getting set.

### `cleartags()` -- `cleartags`

Since `cleartags` is not on the critical path, we do not overload an existing
opcode.  Instead, we use a Graphite function.

## Implementation Details

### Dedicated Structure

This uses a special structure for the `TagSet`.  This can induce high latency
for remote writes, since they have to invalidate the cache *and* update the tag
set.

### 1-Bit Cache Implementation

This approach stores a bit per cache line.  Unfortunately, `untag` doesn't work
so well, because an eviction loses all tracking of that address, and thus there
is no chance to `untag` and then resume/recover.  This means that the programmer
must track everything that is tagged, to avoid double-tagging or
double-untagging.

This implementation may have high latency for `vread` and `vwrite`, because
there need to be some kind of synchrony between the pipeline and the cache, so
that a `vread` can stall until the line's bit is checked.  Note that `vwrite`
can probably check during the time when the memory update sits in the reorder
buffer.

### Cache Implementation with Counters

This approach stores a counter per cache line, and one more counter per hardware
thread.  The per-line counters allow double-tagging and double-untagging.

The purpose of the extra counter is for recovery.  If a line is evicted, and its
tag count was nonzero, then its count gets added to the extra counter.  Any
`untag` that misses in the cache decrements this counter.  So long as the
counter is zero, `vread` and `vrite` should succeed.

The compiler can probably check if you untag something you've never tagged.

## Progress

What aspects are best effort / spurious failure?  We could define it so that
everything is a no-op / fail.

## Variations

Let's not forget about the idea of having a "hold all vwrites until the end"

## Comparison with similar looking concepts/ literatrayte

[@J] How shall we differentiate HMR from LL-SC proposed by Jensen, Hagensen, and Broughton in 1987. Which is kind of a hardware proposal for LL-SC(?)
      sync_clear sounds like untagAll
      sync_load sounds like tag()
      sync_store counds like vwrite().
      region of code bwteen tag() and untag() sounds like the paper's "presumed exclusive access region".
[@j] One obvious difference is that HMR is proposed as a Safe Memory Reclamation mechanism, unlike ll-sc which is an optimistic synchronisation mechanism. 
