This repository contains artifact related to the research paper titled "Efficient Hardware Primitives for Immediate Memory Reclamation in Optimistic Data Structures" by Ajay Singh, Trevor Brown (University Of Waterloo), and Michael Spear(Lehigh University), which appears in the proceedings of IPDPS 2023. The paper introduces a memory reclamation technique named Conditional Access, which provides immediate and fast memory reclamation for concurrent data structures. The technique includes a set of conditional primitives proposed in hardware. To prototype Conditional Access, a multicore simulator called Graphite was used. You can find the Graphite simulator's code at: https://github.com/mit-carbon/Graphite".



The artifact was last run and tested on Ubuntu 12.04 with GCC 4.6.3 (VM as well as on desktop) which is the platform we built the Graphite simulator successfully on. Dependencies and environment needed to build Graphite are available on Wiki. The intel pin tool version we use is pin-2.13-61206 which is provided in this repo.

After all dependencies and platform is ready.

# To build: 
goto LeaseCode/ and do make clean or make to make whole simulator.

The benchmark Conditional Access uses resides at: LeaseCode/tests/benchmarks/carbonised_hmr

# To execute the benchmark with individual data structures:
from within carbonised_hmr/ run:
time HMR_DS=hashtable make THREADS=2 ALG=hashtable_none INS=100 DEL=0 KEYRANGE=1000 MAXOPS=100

# To run from script:
./configandrun_exp.sh 

# To run the benchmark outside simulator:
./configandrun_nosim_exp.sh 

# contact
To get more help or support contact ajay.singh1@uwaterloo.ca